#!/bin/bash
set -xeu

# This script is intended to be run in a new virtual machine, it has been
# tested with a fresh minimal installation of Debian 12.2 running on a
# VirtualBox with 2048 MB of RAM and 15 GB disk (more than the default), and
# took about 35 minutes.

# The virtual machine was set-up as follows:
#
#   Install Debian 12.2 with only "standard system utilities"
#   Set the root and user password to "user"
#   Reboot
#   > apt install lightdm firefox-esr openbox sudo gvfs
#   Select directly "ok" then answers no to the question
#   > localepurge
#   > apt install --no-install-recommends lxqt-core
#   > adduser user sudo
#   > systemctl enable lightdm
#   Enable autologin by editing /etc/lightdm/lightdm.conf
#   Reboot
#
#   Then this script

echo "*** Installing dependencies, please type the sudo password ***"
sudo apt -y install make gcc git emacs curl libgmp-dev rsync patch bubblewrap unzip
echo "Dependencies ok"

if which opam
then
    echo "Opam available"
else
    echo "*** Installing opam ***"
    sh <(wget -O- https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
    opam init --bare
fi

eval `opam config env`

echo "*** Downloading and unzipping dislog.tar.gz ***"
wget "https://gitlab.inria.fr/amoine/dislog/-/archive/main/dislog-main.tar.gz" -O dislog.tar.gz
tar zxvf dislog.tar.gz --one-top-level=dislog --strip-components 1
cd dislog/

echo "Downloading dependencies"
cd deps && ./dldeps.sh && cd ..

./setup.sh

echo "*** Success ***"

opam install coqide

echo "\n(require 'package)\n(add-to-list 'package-archives '(\"melpa\" . \"https://melpa.org/packages/\") t)\n(package-initialize)" >> ~/.emacs

# Then, to save space
#   > apt install localepurge zerofree
#   > apt autoremove && apt clean
#   > rm -rf ~/.cache/mozilla
#   ...
#   then run zerofree
