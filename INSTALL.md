# Setup & Build Instructions

This project needs [opam](https://opam.ocaml.org/doc/Install.html) to
install dependencies.
This project uses the [dune](https://dune.build/) build system.

You can run `./setup.sh` to create a local opam switch with correct
dependencies, and build the project.
Please allow at least 30 minutes for the installation and build to
run.

To manually build the project, run `make` or `dune build`.

## Dependencies

This project was tested with the following versions/commit SHA.

| Software     | URL                                      | Version                                  |
|--------------|------------------------------------------|------------------------------------------|
| OCaml        | https://ocaml.org/                       | 4.14.1                                   |
| Coq          | https://coq.inria.fr/                    | 8.17.1                                   |
| coq-stdpp    | https://gitlab.mpi-sws.org/iris/stdpp    | d12547596422edff325bddbee51560ae0a2bfd3f |
| coq-iris     | https://gitlab.mpi-sws.org/iris/iris     | f0e415b62be689c9055cefde985a49c0c11b52c7 |
| coq-diaframe | https://gitlab.mpi-sws.org/iris/diaframe | 1c3b5549a3829805617f4122b34d3c2cb85ca282 |

## In case of a dependency hell:

We made a script downloading iris-related dependencies.

* Remove any local switch `opam switch remove .`
* Go in the `deps/` directory.
* Run the `dldeps.sh` script.
* Return in this directory.
* Run `setup.sh`. The script will install the downloaded libraries.
