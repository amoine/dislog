# Artifact documentation

This is the artifact documentation for the article:
*DisLog: A Separation Logic for Disentanglement*.

The virtual machine file `dislog-vm.ova` as well as an archive
`dislog.tar.gz` should be provided with this documentation.

The files we refer to in this documentation appear in the virtual
machine as well as in the archive.
Their last version can also be downloaded or browsed online at:

  https://gitlab.inria.fr/amoine/dislog

## Generalities

Top-level functions must contain no locations. The syntax does not actually
forbid it, so a side-condition `locs(e) = ∅` appears from time to
time.

## List of definitions & claims

All the Coq files are located in the `src/` directory.

* Figure 1a: the translation of the example is in
  `examples/scratchpad.v`.
* Figure 1b: the translation of the lock is in `examples/lock.v`.
* Figure 3: in `lang/syntax.v`.

| Construction | Definition |
|--------------|------------|
| Values       | `val`      |
| Block        | `storable` |
| Primitive    | `prim`     |
| Expression   | `expr`     |
| Contexts     | `ctx`      |

* Figures 4 & 5, in the `lang/` directory.

| Reduction | Definition       | File               |
|-----------|------------------|--------------------|
| Head      | `head_step`      | `head_semantics.v` |
| Pure      | `eval_call_prim` | `head_semantics.v` |
| Scheduler | `sched_step`     | `semantics.v`      |
| Main      | `step`           | `semantics.v`      |

For the head reduction:
  + the two reductions HeadCASSucc and HeadCASFail of the paper are
summarized into one reduction `HeadCAS`.
  + the two reductions HeadIfTrue and HeadIfFalse of the paper are
summarized into one reduction `HeadIf`.
  + the reduction HeadCall of the paper is split into two reductions
`HeadCall` and `HeadCallClo`.

| Construction      | Definition  | File               |
|-------------------|-------------|--------------------|
| Store             | `store`     | `head_semantics.v` |
| Allocation map    | `amap`      | `head_semantics.v` |
| Computation Graph | `graph`     | `head_semantics.v` |
| Task Tree         | `task_tree` | `semantics.v`      |

The "program state" tuple (store, allocation map, computation
graph) is always inlined.

* Figure 6, in the `logic/base` directory.

| Rule        | Lemma        | File       |
|-------------|--------------|------------|
| ClockMono   | `clock_mon`  | `interp.v` |
| PrecRefl    | `prec_refl`  | `interp.v` |
| PrecTrans   | `prec_trans` | `interp.v` |
| Other rules | `wp_*`       | `wp.v`     |

The `Atomic` predicate is defined in `lang/atomic.v`

* Figure 7: in `logic/base/wp.v`.

The names of rules are prefixed by `wp_`.
IfTrue and IfFalse are summarized into one rule `wp_if`.
CasSucc and CasFail are also summarized into one rule `wp_cas`.
The `wp_alloc` rule also generates a "meta-token", as usual.

The ParWeak rule also appears in `wp.v`.

* Figure 8: in `lang/reducible.v`. The predicate is named `reducible`.

* Figure 9: in the `logic/base` directory.

| Modality    | File    |
|-------------|---------|
| `wp`        | `wp.v`  |
| `wpg`       | `wpg.v` |

For `wpg` we use a presentation a little bit different than in the paper,
but we prove it equivalent with `paper_wpg` at the end of the `wpg.v`
file.

* Soundness Theorem of Dislog: in `logic/base/wp_adequacy.v`, theorem
  `wp_paper_adequacy`.
This theorem is a corollary of the soundness of `wpg.v`, proved in
`logic/base/wpg_adequacy.v`.

* Figure 10: in the `logic/base` directory.
The pure invariant is defined in `pureinv.v`, other definitions are in
`interp.v`.

* Figure 11: we develop a generic interface, successor to `monPred`
  in `logic/high_level/monpred2.v`.

* Figures 11 & 12: we instantiate the previous interface and prove the
  rules of DisLog+ in `logic/high_level/wpm.v`.

The names of rules are prefixed by `wpm_`.

* Soundness Theorem of Dislog+: in `logic/high_level/wpm_adequacy.v`,
  theorem `wpm_paper_adequacy`.

The Conversion lemma is in `logic/high_level/conversion.v`.

* Figure 13: in `logic/high_level/wonly.v`.

The file first constructs a general interface, and then specializes it
to points-to assertions with the `mapsto_wo` predicate.

* Figure 14: in `logic/high_level/objectivity.v`,
except for the rule MementoPre+, called `wpm_mementopre` in
`logic/high_level/wpm.v`.

* Figure 15: in `examples/lock.v`.

* Figure 16: in the `examples/` directory.

| Case study | File            |
|------------|-----------------|
| Parfor     | `parfor.v`      |
| Loookup    | `lookup_lazy.v` |

* Figure 17: in the `examples/deduplication` directory.

| Case study | File        |
|------------|-------------|
| Hash Set   | `hashset.v` |
| Dedup      | `dedup.v`   |

The functions take the capacity C and the hash function h as
arguments, rather than using global parameters.
The specifications require that the capacity is non-zero and that the
hash function behaves correctly.
The hset predicate is named hashset.

* Figure 18: in the `examples/deduplication_lazy` directory.

| Case study | File           |
|------------|----------------|
| Hash Set   | `lhashset.v`   |
| Dedup Lazy | `dedup_lazy.v` |

The lhset predicate is named lhashset.

## Download, installation, sanity-testing

### Option 1: virtual machine

The file `dislog-vm.ova` provided with this documentation can be
imported into VirtualBox 7.0 through File > Import Appliance.
The VM's login and password are both "user", although no login should be required.

The virtual machine rests on Debian 12.2 with LXQt and OpenBox.
This combination allows for a very small image.

Inside the VM, one can open a file e.g. `wp_adequacy.v` by clicking
into the two desktop shortcuts for either CoqIDE or Emacs.
Otherwise, one can open a terminal and:
* Go into the directory `/home/user/Desktop/dislog`.
* Type `coqide src/logic/base/wp_adequacy.v`.
Replace `coqide` with `emacs` to run Proof General instead.

### Option 2: installation for OPAM users

Users familiar with Coq and OPAM may find it simpler to:
* Clone the git repository: `git@gitlab.inria.fr:amoine/dislog.git`.
* Follow the instructions in the file `INSTALL.md`.

### Option 3: from-scratch installation

We built a script to set up all software dependencies in a newly-created VM,
and reviewers experiencing difficulties following `INSTALL.md` and wishing to
avoid downloading the provided VM might try follow the script we used to build
the virtual machine, as it is intended to install everything from
scratch.

One should read the `build-artifact.sh` file, and when required run:

```
wget -O- https://gitlab.inria.fr/amoine/dislog/-/raw/main/build-artifact.sh | bash
```

This script is intended to be run in a new (Debian) virtual machine,
it has been tested with a fresh minimal installation of Debian 12.2
with LxQt running on VirtualBox with 2048 MB of RAM and 25 GB disk,
and took about 35 minutes.

## Evaluation instructions

Users can check that all files compile and that no `Admitted` or `Axiom`
remains. It suffices to open the file `src/noaxioms.v` and play with it
interactively.
If the Coq command `Print Assumptions xxx` prints "Closed under the
global context" indicates, then `xxx` has no dependencies ([reference](https://coq.inria.fr/refman/proof-engine/vernacular-commands.html#coq:cmd.Print-Assumptions)).

Users can also open some selected `.v` files inside CoqIDE or Proof
General and evaluate the whole file to check that no errors occur and
to verify that the objects and statements mentioned in the claims are
what they are supposed to be.

The `metrics.sh` scripts calls `coqwc` on sub-directories and prints
the result.

## Additional artifact description

We believe the repository is self-contained: the structure of the project, as
well as a link to installation instructions, appear in the `README.md` file of
the repository: `https://gitlab.inria.fr/amoine/dislog`
