#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Specific Commit id
STDPP="d12547596422edff325bddbee51560ae0a2bfd3f"
IRIS="f0e415b62be689c9055cefde985a49c0c11b52c7"
DIAFRAME="1c3b5549a3829805617f4122b34d3c2cb85ca282"

# Otherwise, cleanup and re-download
rm -rf stdpp iris diaframe

curl -o stdpp.zip "https://gitlab.mpi-sws.org/iris/stdpp/-/archive/$STDPP/stdpp-$STDPP.zip"
unzip stdpp.zip
mv "stdpp-$STDPP" stdpp
rm stdpp.zip

curl -o iris.zip "https://gitlab.mpi-sws.org/iris/iris/-/archive/$IRIS/iris-$IRIS.zip"
unzip iris.zip
mv "iris-$IRIS" iris
rm iris.zip

curl -o diaframe.zip "https://gitlab.mpi-sws.org/iris/diaframe/-/archive/$DIAFRAME/diaframe-$DIAFRAME.zip"
unzip diaframe.zip
mv "diaframe-$DIAFRAME" diaframe
rm diaframe.zip
