From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates gen_heap.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp.

Section wpg_par.
Context `{!interpGS Σ}.

(* ------------------------------------------------------------------------ *)

Lemma reducible_join σ α G T1 T2 (v1 v2:val):
  pureinv G α σ (Node T1 T2) (Par v1 v2) ->
  reducible σ α G (Node T1 T2) (Par v1 v2).
Proof.
   intros [Hdom Hcomp].
   apply rootsde_join in Hcomp.
   destruct Hcomp as (?,(?,(?&?))); subst.
   solve_reducible. apply SchedJoin.
   1,3:apply is_fresh.
   2:{ rewrite Hdom. apply is_fresh. }
   all:reflexivity.
Qed.

(* A definitely strange lemma, allowing to give yourself a Plain assertion without consuming
   resources. I was not able to do without it. Maybe there is a simpler path. *)
Lemma give_plain E1 E2 Φ P (Ψ:iProp Σ) :
  Plain P ->
  (Φ ={E1,E2}=∗ P) -∗
   Φ ∗ (Φ ={E1,E2}=∗ P -∗ Ψ)
  ={E1,E2}=∗ Ψ.
Proof.
  iIntros (?) "E (H1&H2)".

  rewrite /fupd /bi_fupd_fupd /uPred_fupd. simpl.
  rewrite /uPred_fupd seal_eq /fancy_updates.uPred_fupd_def. simpl.
  iIntros "X".

  iAssert (◇ P)%I as "#P".
  { iMod ("E" with "H1 X") as "R". iMod "R" as "(?&?&?)". by iFrame. }
  iMod ("H2" with "[$][$]") as ">(?&?&Z)". iModIntro. iMod "P". iModIntro.
  iFrame. by iApply "Z".
Qed.

Local Lemma is_val_false1 e :
  ¬ is_val e ->
  to_val e = None.
Proof. intros. by apply is_val_false. Qed.

Lemma wpg_par_ctx E T1 T2 (e1 e2:expr) Q1 Q2:
  wpg E T1 e1 Q1 -∗
  wpg E T2 e2 Q2 -∗
  wpg E (Node T1 T2) (Par e1 e2)
  (fun t' v => ∃ (l:loc) (t1 t2:timestamp) (v1 v2:val),
       ⌜v=VLoc l⌝ ∗ l ↦ [v1;v2] ∗ meta_token l (⊤ ∖ ↑dislog_nm) ∗
       t1 ≼ t' ∗ t2 ≼ t' ∗
       Q1 t1 v1 ∗ Q2 t2 v2).
Proof.
  iIntros "H1 H2".
  iLöb as "IH" forall (T1 T2 e1 e2). iApply wpg_unfold.
  wpg_intros. simpl.

  (* Are we going to join? *)
  destruct_decide (decide (is_val e1 /\ is_val e2)) as Hdec.
  {  intros_mod. iClear "IH".
    rewrite !is_val_true in Hdec.
    destruct Hdec as ((v1,->)&(v2,->)).
    iDestruct "Hi" as "(%Hcomp&?)".
    iSplitR.
    { eauto using reducible_join. }

    intros_post. apply invert_step_join in Hstep.
    destruct Hstep as (t1,(t2,(w,(l,(?&?&?&?&?&?&?&?&?))))); subst.

    apply pureinv_par_inv in Hcomp. destruct Hcomp.
    rewrite !wpg_unfold. do 2 iModIntro.
    iMod "Hclose" as "_".
    iMod ("H1" with "[-H2]") as ">[% (%E1&(%&?)&H1)]".
    { by iFrame. }
    iMod ("H2" with "[-H1]") as ">[% (%E2&(%&Hi)&?)]".
    { by iFrame. }
    inversion E1; subst. inversion E2; subst.
    rewrite /wpg_pre. simpl.
    iMod (interp_alloc _ _ _ l (SBlock [v1;v2]) w with "[$]") as "(_&?&?)".
    1,2:eauto using pdom.
    iMod (interp_join with "[$]") as "(?&?&?)". iFrame.
    iSplitR. { iModIntro. destruct H3. eauto using pureinv_join. }
    iModIntro. iIntros. intros_mod. iMod "Hclose". iModIntro.
    iExists w. iSplitR; first eauto. iFrame.
    iExists l,t,_,v1,v2. iFrame. eauto. }

  iDestruct "Hi" as "(%Hcomp&Hi)".
  destruct (pureinv_par_inv _ _ _ _ _ _ _ Hcomp) as (?&?).

  (* We use [give_plain] to give "reducible" in the post, without consuming [interp]. *)
  iApply (give_plain _ _ (wpg E T1 e1 Q1 ∗ interp σ α G)%I (⌜¬ is_val e1 -> reducible σ α G T1 e1⌝%I )).
  { iIntros "(H1&Hi)". rewrite !wpg_unfold.
    destruct_decide (decide (is_val e1)).
    { intros_mod. iPureIntro. naive_solver. }
    { iMod ("H1" with "[Hi]") as "X". by iFrame. rewrite is_val_false1; last by naive_solver.
      iDestruct "X" as "(%&_)". iModIntro. iPureIntro. eauto. } }
  iFrame. iIntros "(H1&?)".

  iApply (give_plain _ _ (wpg E T2 e2 Q2 ∗ interp σ α G)%I (⌜¬ is_val e2 -> reducible σ α G T2 e2⌝%I )).
  { iIntros "(H1&Hi)". rewrite !wpg_unfold.
    destruct_decide (decide (is_val e2)).
    { intros_mod. iPureIntro. naive_solver. }
    { iMod ("H1" with "[Hi]") as "X". by iFrame. rewrite is_val_false1; last by naive_solver.
      iDestruct "X" as "(%&_)". iModIntro. iPureIntro. eauto. } }
  iFrame. iIntros "(H2&Hi)".

  intros_mod. iIntros (H1 H2). iSplitR.
  { iPureIntro. eapply RedPar; eauto. destruct (is_val e1), (is_val 2); naive_solver. }

  intros_post.
  apply invert_step_par in Hstep; last done.

  (* The step tells us where to reduce. *)
  destruct Hstep as [ (?&?&?&?&?) | (?&?&?&?&?) ]; subst.
  { rewrite {1}wpg_unfold /wpg_pre.
    iSpecialize  ("H1" with "[Hi]"). by iFrame.
    rewrite is_val_false1; last by eapply step_no_val.
    iMod "Hclose". iMod "H1" as "(_&H1)".
    iMod ("H1" with "[%//]") as "H1". do 2 iModIntro.
    iMod "H1" as "((%&Hi)&?)". iModIntro.
    iFrame. iSplit.
    { eauto using pureinv_par_r,pureinv_par_l. }
    { iApply ("IH" with "[$][$]"). } }
  { rewrite (wpg_unfold E T2) /wpg_pre.
    iSpecialize ("H2" with "[Hi]"). by iFrame.
    rewrite is_val_false1; last by eapply step_no_val.
    iMod "Hclose". iMod "H2" as "(_&H2)". iMod ("H2" with "[%//]") as "H2". do 2 iModIntro.
    iMod "H2" as "((%&?)&?)". iModIntro.
    iFrame. iSplit.
    { eauto using pureinv_par_r,pureinv_par_l. }
    { iApply ("IH" with "[$][$]"). } }
Qed.

Lemma wpg_fork E t e1 e2 Q :
  ▷ (∀ t1 t2, t ≼ t1 ∗ t ≼ t2 -∗ wpg E (Node (Leaf t1) (Leaf t2)) (Par e1 e2) Q) -∗
  wpg E (Leaf t) (Par e1 e2) Q.
Proof.
  iIntros "Hwp". iApply wpg_unfold.
  wpg_intros. intros_mod.

  iSplitR.
  { eauto using reducible_fork. }

  intros_post.
  apply invert_step_fork in Hstep. destruct Hstep as (?&?&?&(v,(w,(?&?&?&?)))).
  subst.
  iDestruct "Hi" as "(%&?)".

  iMod (interp_fork _ _ _ t v w with "[$]") as "(?&?&?)".
  do 2 iModIntro.
  iMod "Hclose" as "_". iFrame.
  iSplitR.
  { eauto using pureinv_fork. }
  { iApply ("Hwp" $! v w with "[$]"). }
Qed.

(* This rule combines [wpg_fork] and [wpg_par_ctx], plus a monotonicity
   lemma. This strange wording is needed to allow the use of [t1] and
   [t2] as soon as possible in the proof. *)
Lemma wpg_par E t e1 e2 Q:
  ▷ (∀ t1 t2, t ≼ t1 ∗ t ≼ t2 ={E}=∗
    (∃ Q1 Q2, wpg E (Leaf t1) e1 Q1 ∗ wpg E (Leaf t2) e2 Q2 ∗
     (∀ t' v, (∃ (l:loc) (t1 t2:timestamp) (v1 v2:val),
       ⌜v=VLoc l⌝ ∗ l ↦ [v1;v2] ∗ meta_token l (⊤ ∖ ↑dislog_nm) ∗
       t1 ≼ t' ∗ t2 ≼ t' ∗
       Q1 t1 v1 ∗ Q2 t2 v2) -∗ Q t' v )))  -∗
  wpg E (Leaf t) (Par e1 e2) Q.
Proof.
  iIntros "Hwp".
  iApply wpg_fork. iModIntro. iIntros (t1 t2) "Hu".
  iApply fupd_wpg.
  iMod ("Hwp" $! t1 t2 with "Hu") as "[%Q1 [%Q2 (H1&H2&Hpost)]]".
  iModIntro. iApply (wpg_mono with "[-Hpost]").
  iApply (wpg_par_ctx with "H1 H2"). iIntros.
  iApply "Hpost". iFrame.
Qed.

(* Only for showoff *)
Lemma wpg_par_weak E t e1 e2 Q1 Q2 :
  (∀ t1 t2,
      (t ≼ t1 -∗ wpg E (Leaf t1) e1 Q1) ∗
      (t ≼ t2 -∗ wpg E (Leaf t2) e2 Q2)) -∗
  wpg E (Leaf t) (Par e1 e2)
  (fun t' v => ∃ (l:loc) (t1 t2:timestamp) (v1 v2:val),
       ⌜v=VLoc l⌝ ∗ l ↦ [v1;v2] ∗ meta_token l (⊤ ∖ ↑dislog_nm) ∗
       t1 ≼ t' ∗ t2 ≼ t' ∗
       Q1 t1 v1 ∗ Q2 t2 v2
  ).
Proof.
  iIntros "H12".
  iApply wpg_par. iModIntro. iIntros (t1 t2) "(Ht1 & Ht2)".
  iDestruct ("H12" $! t1 t2) as "[H1 H2]".
  iSpecialize ("H1" with "[$]").
  iSpecialize ("H2" with "[$]").
  iModIntro. iExists Q1,Q2. iFrame. iIntros. iFrame.
Qed.

End wpg_par.
