From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates gen_heap.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp wpg_more.

Section wpg_closure.
Context `{!interpGS Σ}.

Lemma pureinv_closure G α σ t (l:loc) clo :
  l ∉ dom σ ->
  dom σ = dom α ->
  all_abef G α t (locs clo) ->
  pureinv G (<[l:=t]> α) (<[l:=SClo clo]> σ) (Leaf t) l.
Proof.
  intros.
  constructor; eauto using rootsde_alloc.
  rewrite !dom_insert_L. set_solver.
Qed.

Lemma wpg_closure E t clo:
  ⊢ wpg E (Leaf t) (Clo clo)
    (fun _ v => ∃ l, ⌜v=VLoc l⌝ ∗ Func l clo ∗ meta_token l (⊤ ∖ ↑dislog_nm)).
Proof.
  iIntros.

  (* We give ourselves the clocks of the the body. *)
  iApply mementopre_iterated. iIntros.

  rewrite wpg_unfold. wpg_intros. intros_mod.
  iDestruct "Hi" as "(%Hcomp&?)".
  iSplitR. { eauto using reducible_closure, pdom. }
  intros_post.
  apply invert_step_closure in Hstep.
  destruct Hstep as (l,(?&?&?&?&?&?)); subst.
  inversion Hcomp as [? Hrootsde].
  iMod (interp_alloc _ _ _ l (SClo clo) t with "[$]") as "(?&?&?&?)".
  1,2:eauto.
  iMod (gen_heap.mapsto_persist with "[$]").
  do 2 iModIntro. iMod "Hclose".
  iFrame. iSplitR.
  { iPureIntro. apply pureinv_closure; eauto.
    inversion Hrootsde; subst.
    2:{ exfalso. destruct K; naive_solver. }
    eapply all_abef_mon_set; last eauto.
    set_solver. }
  iApply wpg_val. iModIntro.
  iExists l. iSplitR; first done.
  replace (locs (Clo clo)) with (locs clo) by set_solver.
  iFrame. iExists _. iFrame "∗#".
Qed.

End wpg_closure.
