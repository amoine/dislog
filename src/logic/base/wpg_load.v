From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp.

Section wpg_load.
Context `{!interpGS Σ}.

Lemma wpg_load E t bs q (i:Z) (v:val) (l:loc) :
  (0 <= i < Z.of_nat (length bs))%Z ->
  bs !! (Z.to_nat i) = Some v ->
  l ↦{q} bs ∗ v ◷? t -∗
  wpg E (Leaf t) (Load l i) (fun _ w => ⌜w=v⌝ ∗ l ↦{q} bs).
Proof.
  iIntros (? Hi) "(?&?)".
  rewrite wpg_unfold /wpg_pre. wpg_intros. intros_mod.
  iDestruct "Hi" as "(%Hcomp&?)".
  iDestruct (interp_exploit_mapsto with "[$]") as "%Hl".
  iDestruct (interp_exploit_vclock with "[$]") as "%".
  { eauto using pdom. }
  iSplitR. { eauto using reducible_load. }
  intros_post.
  eapply invert_step_load in Hstep.
  destruct Hstep as (?,(?,(Hl'&Hi'&?&?&?&?&?))); subst.
  rewrite Hl' in Hl. injection Hl. intros ->.
  rewrite Hi' in Hi. injection Hi. intros ->.
  iFrame. do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
  iSplitR.
  { inversion Hcomp. eauto using pureinv_leaf_val. }
  iApply wpg_val. eauto.
Qed.

End wpg_load.
