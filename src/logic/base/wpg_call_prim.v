From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp.

Section wpg_call_prim.
Context `{!interpGS Σ}.

Lemma eval_call_prim_is_no_loc p v1 v2 v :
  eval_call_prim p v1 v2 = Some v -> ¬ is_loc v.
Proof.
Proof.
  intros E. destruct p,v1,v2; simpl in E; naive_solver.
Qed.

Lemma eval_call_prim_inj p v1 v2 x1 x2 :
  eval_call_prim p v1 v2 = Some x1 ->
  eval_call_prim p v1 v2 = Some x2 ->
  x1 = x2.
Proof. intros. destruct p,v1,v2; naive_solver. Qed.

Lemma wpg_call_prim E t (p:prim) v1 v2 v :
  eval_call_prim p v1 v2 = Some v ->
  ⊢ wpg E (Leaf t) (CallPrim p v1 v2) (fun _ v' => ⌜v'=v⌝)%I.
Proof.
  iIntros (?).
  iApply wpg_unfold.
  wpg_intros. intros_mod.
  iDestruct "Hi" as "(%Hcomp&?)".
  iSplitR.
  { eauto using reducible_call_prim. }
  intros_post. do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
  eapply invert_step_call_prim in Hstep; eauto.
  destruct Hstep as (?&?&?&?&?&?&?). subst.
  replace x with v by eauto using eval_call_prim_inj.
  iFrame. iSplitR.
  { inversion Hcomp.
    eauto using pureinv_leaf_val,vabef_no_loc,eval_call_prim_is_no_loc. }
  iApply wpg_val. eauto.
Qed.

End wpg_call_prim.
