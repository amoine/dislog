From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg wpg_store interp.

Section wpg_cas.
Context `{interpGS Σ}.

Local Lemma pureinv_cas_fail σ α G t e :
  locs e = ∅ ->
  dom σ = dom α ->
  pureinv G α σ (Leaf t) e.
Proof.
  intros Ht. intros.
  constructor; eauto.
  { apply RDeLeaf. rewrite Ht. apply set_Forall_empty. }
Qed.

Lemma wpg_cas E t (l:loc) (i:Z) (v1 v1' v2:val) bs q :
  (v1 = v1' -> q=DfracOwn 1%Qp) ->
  (0 ≤ i < length bs)%Z ->
  bs !! Z.to_nat i = Some v1' ->
  l ↦{q} bs ∗ v1' ◷? t -∗
  wpg E (Leaf t) (CAS l i v1 v2)
    (fun _ v => ⌜v=VBool (bool_decide (v1=v1'))⌝ ∗
       l ↦{q} (if decide (v1=v1') then <[Z.to_nat i:=v2]> bs else bs)).
Proof.
  iIntros (Hq ??) "(?&?)".
  iApply wpg_unfold. wpg_intros. intros_mod. iDestruct "Hi" as "(%Hcomp&?)".
  iDestruct (interp_exploit_mapsto with "[$]") as "%".
  iDestruct (interp_exploit_vclock with "[$]") as "%".
  { eauto using pdom. }
  iSplitR. { eauto using reducible_cas. }
  intros_post. apply invert_step_cas in Hstep.
  destruct Hstep as (?&?&?&(bs',(v1'',(?&?&?&?&?&?)))). subst.
  assert (bs' = bs) by naive_solver. subst bs'.
  assert (v1'' = v1') by naive_solver. subst v1''.
  destruct_decide (decide (v1=v1')) as eqv.
  { rewrite Hq // eqv. simpl. rewrite bool_decide_eq_true_2//.
    iMod (interp_store _ _ _ _ _ i v2 with "[$]") as "(?&?)".
    do 2 iModIntro. iFrame. iMod "Hclose" as "_".
    iSplitR.
    { iPureIntro. inversion Hcomp. apply pureinv_store; eauto.
      apply elem_of_dom. eauto. }
    iApply wpg_val. iFrame. eauto. }
  { do 2 iModIntro. iFrame. iMod "Hclose" as "_".
    rewrite bool_decide_eq_false_2 //. iFrame.
    iSplitR. { iPureIntro. inversion Hcomp. eauto using pureinv_cas_fail. }
    iApply wpg_val. iFrame. eauto. }
Qed.

End wpg_cas.
