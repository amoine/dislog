From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.

From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp.

Section wpg_length.
Context `{!interpGS Σ}.

Lemma wpg_length E t bs (l:loc) q :
  l ↦{q} bs -∗
  wpg E (Leaf t) (Length l) (fun _ w => ⌜w=length bs⌝ ∗ l ↦{q} bs).
Proof.
  iIntros.
  iApply wpg_unfold. wpg_intros. intros_mod. iDestruct "Hi" as "(%Hc&?)".
  iDestruct (interp_exploit_mapsto with "[$]") as "%Hl".
  iSplitR. { eauto using reducible_length. }
  intros_post.
  eapply invert_step_length in Hstep.
  destruct Hstep as (?,(Hl'&?&?&?&?&?)); subst.
  rewrite Hl' in Hl. injection Hl. intros ->. iFrame.
  do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
  iSplitR.
  { iPureIntro. inversion Hc. apply pureinv_leaf_val; eauto.
    apply not_is_loc_vabef. easy. }
  iApply wpg_val. eauto.
Qed.

End wpg_length.
