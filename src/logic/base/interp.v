From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Import fancy_updates gen_heap.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap agree frac dfrac.

From dislog.utils Require Import graph more_iris.
From dislog.lang Require Import syntax semantics.
From dislog.logic Require Import wpg.

(******************************************************************************)
(* This file defines [interp], and related [clock] and  [prec]. *)

Local Canonical Structure timestampO := leibnizO timestamp.

Class interpGS (Σ : gFunctors) :=
  InterpG {
    iinvgs : invGS_gen HasNoLc Σ; (* invariants *)
    istore : gen_heapGS loc storable Σ; (* the store *)
    igraph : inG Σ (authUR (gsetUR (timestamp * timestamp))); (* the graph *)
    γgraph : gname; (* A name for the ghost cell of the graph *)
    }.

#[export] Existing Instance istore.
#[export] Existing Instance igraph.

Ltac destruct_interp Hi :=
  iDestruct Hi as "(Hσ & #Hα & HG)".

Section Interp.

Context `{interpGS Σ}.

(******************************************************************************)
(* [interp] *)

Definition interp_graph G : iProp Σ := own γgraph (● G).

Definition dislog_nm : namespace := nroot.@"dislog".
Definition allocated_at (l:loc) (t:timestamp) : iProp Σ := meta l dislog_nm t.
Definition interp_amap α : iProp Σ := [∗ map] l ↦ t ∈ α, allocated_at l t.

Definition interp (σ:store) (α:amap) (G:graph) : iProp Σ :=
  gen_heap_interp σ ∗
  interp_amap α ∗
  interp_graph G.

(******************************************************************************)
(* [prec1] & [prec] *)

(* [prec1] would suffice by itself, but it is not reflexive nor transitive
   without access to the [interp] predicate.
   We close over using the [prec] predicate. *)

Definition prec1 (t1 t2:timestamp) : iProp Σ :=
  own γgraph (◯ {[(t1,t2)]}).

Lemma prec1_exploit G t1 t2 :
  interp_graph G -∗ prec1 t1 t2 -∗ ⌜reachable G t1 t2⌝.
Proof.
  iIntros "Ha Hf".
  iDestruct (auth_gset_elem_of with "Ha Hf") as "%".
  iPureIntro. by apply rtc_once.
Qed.

Fixpoint prec_aux (t1:timestamp) xs (t2:timestamp) : iProp Σ :=
  match xs with
  | nil => ⌜t1=t2⌝
  | x::xs => prec1 t1 x ∗ prec_aux x xs t2 end.

Definition prec (t1 t2:timestamp) : iProp Σ :=
  ∃ (xs:list timestamp), prec_aux t1 xs t2.

(* We reuse the notation ≼ that is already present for cmras,
   but only in bi_scope. *)
Local Notation "t1 ≼ t2" := (prec t1 t2)
  (at level 70, format "t1  ≼  t2") : bi_scope.

Global Instance prec_aux_timeless xs t1 t2 : Timeless (prec_aux t1 xs t2).
Proof. revert t1. induction xs; apply _. Qed.
Global Instance prec_aux_persistent xs t1 t2 : Persistent (prec_aux t1 xs t2).
Proof. revert t1. induction xs; apply _. Qed.
Global Instance prec_timeless t1 t2 : Timeless (t1 ≼ t2).
Proof. apply _. Qed.
Global Instance prec_persistent t1 t2 : Persistent (t1 ≼ t2).
Proof. apply _. Qed.

Lemma prec_refl t : ⊢ t ≼ t.
Proof. iExists nil. easy. Qed.

Lemma prec_once t1 t2 : prec1 t1 t2 -∗ t1 ≼ t2.
Proof. iIntros. iExists [t2]. simpl. eauto. Qed.

Lemma prec_trans t1 t2 t3 : t1 ≼ t2 -∗ t2 ≼ t3 -∗ t1 ≼ t3.
Proof.
  iIntros "[%xs Hxs] [%ys Hys]".
  iExists (xs++ys).
  iInduction xs as [|] "IH" forall (t1 ys).
  { iDestruct "Hxs" as "%". subst. iFrame. }
  { iDestruct "Hxs" as "(?&?)".
    iFrame. iApply ("IH" with "[$][$]"). }
Qed.

Lemma prec_exploit G t1 t2 :
  interp_graph G -∗ t1 ≼ t2 -∗ ⌜reachable G t1 t2⌝.
Proof.
  iIntros "? [%xs Hxs]".
  iInduction xs as [|] "IH" forall (t1).
  { iDestruct "Hxs" as "%". subst. easy. }
  { iDestruct "Hxs" as "(?&?)".
    iDestruct ("IH" with "[$][$]") as "%".
    iDestruct (prec1_exploit with "[$][$]") as "%".
    iPureIntro. etrans; eauto. }
Qed.

Lemma interp_prec_exploit G t1 t2  σ α :
  interp σ α G -∗ t1 ≼ t2 -∗ ⌜reachable G t1 t2⌝.
Proof.
  iIntros "(?&?&?) ?". iApply (prec_exploit with "[$][$]").
Qed.

(******************************************************************************)
(* ◷ *)


Lemma meta_in_dom `{Countable A} σ (l : loc) (N : namespace) (x : A) :
  meta l N x -∗ gen_heap_interp  σ -∗ ⌜l ∈ dom σ⌝.
Proof.
  rewrite gen_heap.meta_unseal. iIntros "[% (?&?)]".
  iIntros "[% (%&?&?)]".
  iDestruct (ghost_map.ghost_map_lookup with "[$][$]") as "%X".
  iPureIntro. apply elem_of_dom_2 in X. set_solver.
Qed.

Definition clock (l:loc) (t:timestamp) : iProp Σ :=
  ∃ t0, allocated_at l t0 ∗ t0 ≼ t.

Local Notation "l ◷ t" := (clock l t)
  (at level 20, format "l  ◷  t") : bi_scope.

Global Instance clock_timeless l t : Timeless (l ◷ t).
Proof. apply _. Qed.
Global Instance clock_persistent l t : Persistent (l ◷ t).
Proof. apply _. Qed.

Lemma clock_mon l t t' :
  l ◷ t -∗ t ≼ t' -∗ l ◷ t'.
Proof.
  iIntros "[%u0 (?&H1)] H2". iExists u0. iFrame.
  iApply (prec_trans with "H1 H2").
Qed.

Lemma interp_exploit_clock σ α G l t :
  dom σ = dom α ->
  interp σ α G ∗ l ◷ t -∗ ⌜abef G α t l⌝.
Proof.
  iIntros (Hdom) "(Hi&[% (?&?)])". destruct_interp "Hi".
  iDestruct (meta_in_dom with "[$][$]") as "%Hl".
  rewrite Hdom in Hl. apply elem_of_dom in Hl. destruct Hl as (u0,Hl).
  iDestruct (big_sepM_lookup with "Hα") as "Hx". eauto.
  iDestruct (meta_agree with "[$][$]") as "->".
  iDestruct (prec_exploit with "[$][$]") as  "%".
  iPureIntro. rewrite /abef Hl //.
Qed.

Definition vclock (v:val) (t:timestamp) : iProp Σ :=
  match v with
  | VLoc l => l ◷ t
  | _ => True end.

Local Notation "v ◷? t" := (vclock v t)
  (at level 20, format "v  ◷?  t") : bi_scope.

Lemma vclock_mon v t t' :
  v ◷? t -∗ t ≼ t' -∗ v ◷? t'.
Proof. destruct v; eauto using clock_mon. Qed.

Global Instance vclock_timeless v t : Timeless (v ◷? t).
Proof. destruct v; apply _. Qed.
Global Instance vclock_persistent v t : Persistent (v ◷? t).
Proof. destruct v; apply _. Qed.

Lemma not_is_loc_vabef G α t v :
  ¬ is_loc v -> vabef G α t v.
Proof. destruct v; naive_solver. Qed.

Lemma interp_exploit_vclock σ G α t v :
  dom σ = dom α ->
  interp σ α G ∗ v ◷? t -∗ ⌜vabef G α t v⌝.
Proof.
  iIntros (?) "(Hi&?)".
  destruct_decide (decide (is_loc v)) as Hv.
  { apply is_loc_inv in Hv. destruct Hv as (l,->).
    iApply interp_exploit_clock; eauto. }
  { eauto using not_is_loc_vabef. }
Qed.

Lemma interp_exploit_mapsto σ α G l q bs :
  interp σ α G ∗ mapsto l q bs -∗ ⌜σ !! l = Some bs⌝.
Proof.
  iIntros "(Hi&?)". destruct_interp "Hi".
  iApply (gen_heap.gen_heap_valid with "[$][$]").
Qed.

Definition Func v clo : iProp Σ :=
  match v with
  | VCode c => ⌜c=clo /\ locs clo = ∅⌝
  | VLoc l => mapsto l DfracDiscarded (SClo clo) ∗
                  ∃ t, allocated_at l t ∗ ([∗ set] l' ∈ locs clo, l' ◷ t)
  | _ => False end.
Lemma Func_eq v clo :
  Func v clo = match v with
  | VCode c => ⌜c=clo /\ locs clo = ∅⌝
  | VLoc l => mapsto l DfracDiscarded (SClo clo) ∗
                  ∃ t, allocated_at l t ∗ ([∗ set] l' ∈ locs clo, l' ◷ t)
  | _ => False end%I.
Proof. done. Qed.
Global Instance timeless_Func v clo : Timeless (Func v clo).
Proof. destruct v; apply _. Qed.
Global Instance persistent_Func v clo : Persistent (Func v clo).
Proof. destruct v; apply _. Qed.

(******************************************************************************)
(* Various update of interp *)

Lemma interp_alloc σ α G l b t :
  l ∉ dom σ ->
  dom σ = dom α ->
  interp σ α G ==∗ allocated_at l t ∗ interp (<[l:=b]> σ) (<[l:=t]> α) G ∗ mapsto l (DfracOwn 1) b ∗ meta_token l (⊤∖↑dislog_nm).
Proof.
  iIntros (? Hdom) "Hi".
  destruct_interp "Hi".
  iMod (gen_heap_alloc _ l b with "[$]") as "(?&?&Ht)".
  { apply not_elem_of_dom. eauto. }
  iDestruct (meta_token_difference l with "Ht") as "(?&?)"; last iFrame.
  { set_solver. }
  iMod (meta_set with "[$]") as "#?"; last iFrame "#".
  { set_solver. }
  iApply big_sepM_insert.
  { apply not_elem_of_dom; set_solver. }
  by iFrame "#".
Qed.

Local Notation "l ↦{ dq } v" := (mapsto l dq (SBlock v))
  (at level 20, format "l  ↦{ dq }  v") : bi_scope.
Local Notation "l ↦{# dq } v" := (mapsto l (DfracOwn dq) (SBlock v))
  (at level 20, format "l  ↦{# dq }  v") : bi_scope.
Local Notation "l ↦ v" := (mapsto l (DfracOwn 1) (SBlock v))
  (at level 20, format "l  ↦  v") : bi_scope.

Lemma interp_store σ α G bs (l:loc) (i:Z) (v:val) :
  interp σ α G ∗ l ↦ bs ==∗
  interp (<[l:=SBlock (<[Z.to_nat i:=v]> bs)]> σ) α G ∗ l ↦ (<[Z.to_nat i:=v]> bs).
Proof.
  iIntros "(Hi&?)".
  destruct_interp "Hi". iFrame "#∗".
  iApply (gen_heap.gen_heap_update with "[$][$]").
Qed.

Lemma interp_graph_join G t1 t2 t' :
  interp_graph G ==∗
  interp_graph (graph_join G t1 t2 t') ∗ t1 ≼ t' ∗ t2 ≼ t'.
Proof.
  iIntros "Ha".
  iMod (auth_gset_insert (t1,t') with "[$]") as "(?&?)".
  iMod (auth_gset_insert (t2,t') with "[$]") as "(?&?)".
  iDestruct (prec_once with "[$]") as "?".
  iDestruct (prec_once with "[$]") as "?".
  replace ({[(t2, t')]} ∪ ({[(t1, t')]} ∪ G)) with (graph_join G t1 t2 t') by set_solver.
  by iFrame.
Qed.

Lemma interp_join σ α G t1 t2 t' :
  interp σ α G ==∗
  interp σ α (graph_join G t1 t2 t') ∗ t1 ≼ t' ∗ t2 ≼ t'.
Proof.
  iIntros "Hi". destruct_interp "Hi". iFrame "%#∗".
  iApply interp_graph_join. iFrame.
Qed.

Lemma interp_graph_fork G t v w :
  interp_graph G ==∗
  interp_graph (graph_fork G t v w) ∗ t ≼ v ∗ t ≼ w.
Proof.
  iIntros.
  iMod (auth_gset_insert (t,v) with "[$]") as "(?&?)".
  iMod (auth_gset_insert (t,w) with "[$]") as "(?&?)".
  iDestruct (prec_once with "[$]") as "?".
  iDestruct (prec_once with "[$]") as "?".
  replace ({[(t, w)]} ∪ ({[(t, v)]} ∪ G)) with (graph_fork G t v w) by set_solver.
  by iFrame.
Qed.

Lemma interp_fork σ α G t v w :
  interp σ α G ==∗ interp σ α (graph_fork G t v w) ∗ t ≼ v ∗ t ≼ w.
Proof.
  iIntros "Hi". destruct_interp "Hi". iFrame "%#∗".
  iApply interp_graph_fork. iFrame.
Qed.

Lemma alloc_prec' t t' G:
  reachable G t t' ->
  interp_graph G ==∗ interp_graph G ∗ t ≼ t'.
Proof.
  iIntros (Hr) "Ha".
  iInduction Hr as [|] "IH".
  { iFrame. by iApply prec_refl. }
  iMod (auth_gset_insert (x,y) with "[$]") as "(?&?)".
  replace (({[(x, y)]} ∪ G)) with G by set_solver.
  iMod ("IH" with "[$]") as "(?&?)". iFrame.
  iDestruct (prec_once with "[$]") as "?".
  by iApply (prec_trans x y z with "[$][$]").
Qed.

Lemma interp_insert_abef σ α G l t :
  abef G α t l ->
  interp σ α G ==∗ interp σ α G ∗ l ◷ t.
Proof.
  iIntros (Habef) "Hi". destruct_interp "Hi".
  rewrite /abef in Habef. destruct (α !! l) eqn:Hl; last easy.
  iDestruct (big_sepM_lookup with "[$]") as "#?"; first eauto.
  iMod (alloc_prec' with "[$]") as "(?&#?)". eauto.
  iFrame "#∗". iModIntro. iExists _. iFrame "#".
Qed.

Lemma alloc_prec t t' σ α G  :
  reachable G t t' ->
  interp σ α G ==∗ interp σ α G ∗ t ≼ t'.
Proof.
  iIntros (?) "Hi". destruct_interp "Hi".
  iMod (alloc_prec' with "[$]") as "(?&?)". eauto. by iFrame.
Qed.

End Interp.

Global Notation "l ↦{ dq } v" := (mapsto l dq (SBlock v))
  (at level 20, format "l  ↦{ dq }  v") : bi_scope.
Global Notation "l ↦{# dq } v" := (mapsto l (DfracOwn dq) (SBlock v))
  (at level 20, format "l  ↦{# dq }  v") : bi_scope.
Global Notation "l ↦ v" := (mapsto l (DfracOwn 1) (SBlock v))
  (at level 20, format "l  ↦  v") : bi_scope.

Global Notation "t1 ≼ t2" := (prec t1 t2)
  (at level 70, format "t1  ≼  t2") : bi_scope.

Global Notation "l ◷ t" := (clock l t)
  (at level 20, format "l  ◷  t") : bi_scope.
Global Notation "v ◷? t" := (vclock v t)
  (at level 20, format "v  ◷?  t") : bi_scope.

Global Instance interpGS_DisLogGS `{interpGS Σ} : dislogGS Σ.
Proof.
  exact (DislogGS iinvgs interp).
Defined.

Module Initialization.

  Definition interpΣ : gFunctors :=
    #[ invΣ;
       gen_heapΣ loc storable;
       GFunctor (authUR (gsetUR (timestamp * timestamp)))].

  (* The difference between the *PreG and *G is the presence of the names
     of ghost cells. (ie. gname) *)
  Class interpPreG (Σ : gFunctors) :=
  { piinvgs : invGpreS Σ;
    pistore : gen_heapGpreS loc storable Σ;
    pigraph : inG Σ (authUR (gsetUR (timestamp * timestamp)));
  }.

  #[global] Existing Instance piinvgs.
  #[global] Existing Instance pistore.
  #[global] Existing Instance pigraph.

  Global Instance subG_interpPreG Σ :
    subG interpΣ Σ → interpPreG Σ.
  Proof. solve_inG. Qed.

  Global Instance interpPreG_interpΣ : interpPreG interpΣ.
  Proof. eauto with typeclass_instances. Qed.

  Lemma interp_init `{!interpPreG Σ, hinv:!invGS_gen HasNoLc Σ} σ α G :
    dom σ = dom α ->
    ⊢ |==> ∃ hi : interpGS Σ, ⌜@iinvgs Σ hi = hinv⌝ ∗
    interp σ α G ∗ ([∗ map] l ↦ v ∈ σ, mapsto l (DfracOwn 1) v).
  Proof.
    iIntros (Hdom). rewrite /interp.
    iMod (gen_heap_init σ) as "[% (?&?&Hn)]".
    iMod (own_alloc (● G)) as "[%γgraph ?]".
    { by apply auth_auth_valid. }
    iExists (@InterpG Σ hinv _ _ γgraph).
    iSplitR; try easy. iFrame.

    clear G.
    iInduction σ as [|] "IH" using map_ind forall (α Hdom).
    { rewrite dom_empty_L in Hdom. symmetry in Hdom. apply dom_empty_inv_L in Hdom. subst.
      by iApply big_sepM_empty. }
    { rewrite dom_insert_L in Hdom. assert (i ∈ dom α) as Hi by set_solver.
      rewrite big_sepM_insert_delete. iDestruct "Hn" as "(?&?)".
      apply elem_of_dom in Hi. destruct Hi as (y,?).
      rewrite -(insert_delete α i y) //. iApply big_sepM_insert.
      { rewrite lookup_delete //. }
      iMod (meta_set with "[$]"); last iFrame. set_solver.
      rewrite delete_notin //. apply not_elem_of_dom in H0.
      iApply ("IH" with "[%][$]"). rewrite dom_delete_L. set_solver. }
  Qed.

End Initialization.

Module FullInitialization.
  Export Initialization.
  (* This cannot be global as we want to keep Σ as a parameter:
     we thus do _not_ want coq to use interpΣ *)

  #[export] Instance interpGS_interpΣ : interpGS interpΣ.
  Proof.
    constructor; eauto with typeclass_instances.
    3:exact xH.
    { constructor.
      { constructor; eauto with typeclass_instances. all:exact xH. }
      { (* Cannot be solved by solve_inG. *)
        constructor. rewrite /interpΣ /invΣ /lcΣ.
        { now apply InG with (inG_id:=3%fin). }
        all:exact xH. } }
    repeat (constructor; eauto with typeclass_instances).
  Qed.
End FullInitialization.
