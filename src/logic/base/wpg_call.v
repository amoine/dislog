From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp wpg_more.

Section wpg_call.
Context `{!interpGS Σ}.

Lemma roots_call (l:loc) self args body G α t vs :
  length args = length vs ->
  all_abef G α t (locs body) ->
  rootsde G α (Leaf t) (Call l (Val <$> vs)) ->
  rootsde G α (Leaf t) (substs' (zip (self :: args) (VLoc l :: vs)) body).
Proof.
  intros ? Hm Hr.
  apply rootsde_leaf_inv in Hr.
  replace (locs (Call l (Val <$> vs))) with ({[l]} ∪ locs vs) in Hr.
  2:{ rewrite /locs /locs_expr. simpl. f_equal. rewrite location_list_val //. }
  apply RDeLeaf.
  eapply all_abef_mon_set. apply locs_substs'.
  rewrite snd_zip.
  2:{ simpl. lia. }
  replace (locs (VLoc l::vs)) with ({[l]} ∪ locs vs) by set_solver.
  apply set_Forall_union; eauto.
Qed.

Lemma pureinv_call_clo (l:loc) self args body G α σ t vs :
  length args = length vs ->
  all_abef G α t (locs body) ->
  pureinv G α σ (Leaf t) (Call l (Val <$> vs)) ->
  pureinv G α σ (Leaf t) (substs' (zip (self :: args) (VLoc l :: vs)) body).
Proof.
  intros ? ? [].
  constructor; eauto using roots_call.
Qed.

Lemma rootde_call self args body G α t vs :
  length args = length vs ->
  locs body = ∅ ->
  rootsde G α (Leaf t) (Call (VCode (Lam self args body)) (Val <$> vs)) ->
  rootsde G α (Leaf t) (substs' (zip (self :: args) ((VCode (Lam self args body)) :: vs)) body).
Proof.
  intros ? Hlb Hr.
  apply rootsde_leaf_inv in Hr.
  replace (locs (Call (VCode (Lam self args body)) (Val <$> vs))) with (locs vs) in Hr.
  2:{ rewrite /locs /locs_expr. simpl. f_equal. rewrite location_list_val. set_solver. }
  apply RDeLeaf.
  eapply all_abef_mon_set. apply locs_substs'.
  rewrite snd_zip.
  2:{ simpl. lia. }
  rewrite Hlb right_id_L.
  apply set_Forall_union; eauto.
  rewrite /locs /location_val. simpl. easy.
Qed.

Lemma pureinv_call self args body G α σ t vs :
  locs body = ∅ ->
  length args = length vs ->
  pureinv G α σ (Leaf t) (Call (VCode (Lam self args body)) (Val <$> vs)) ->
  pureinv G α σ (Leaf t) (substs' (zip (self :: args) (VCode (Lam self args body) :: vs)) body).
Proof.
  intros ? ? [].
  constructor; eauto using roots_call, rootde_call.
Qed.

Lemma wpg_call E t self args body ts vs Q v :
  length args = length vs →
  ts = Val <$> vs ->
  Func v (Lam self args body) -∗
  ▷ (wpg E (Leaf t) (substs' (zip (self::args) (v::vs)) body) Q) -∗
  wpg E (Leaf t) (Call v ts) Q.
Proof.
  iIntros (? ?) "Hl Hwp".
  iApply (vmementopre v). set_solver. iIntros "#Hv".
  iApply wpg_unfold.
  wpg_intros. intros_mod. iDestruct "Hi" as "(%Hcomp&?)". simpl.
  destruct v; eauto.
  { iDestruct "Hl" as "(?&[% (#Ht0&HX)])".
    iAssert (t0 ≼ t)%I as "#Hprec".
    { iDestruct "Hv" as "[% (Ht&?)]".
      iDestruct (gen_heap.meta_agree with "Ht0 Ht") as "->". done. }
    iAssert ([∗ set] l' ∈ locs (Lam self args body), l' ◷ t)%I with "[HX]" as "?".
    { iApply (big_sepS_impl with "[$]"). iModIntro. iIntros. iApply (clock_mon with "[$][$]"). }
    iDestruct (exploit_all_abef_set with "[$]") as "%Habef". eauto using pdom.
    iDestruct (interp_exploit_mapsto with "[$]") as "%Hl".

    iSplitR.
    assert (forall l, l ∈ locs body -> abef G α t l).
    { intros. apply Habef. set_solver. }
    { iPureIntro. eapply reducible_call_clo; eauto. subst. rewrite fmap_length //. }
    intros_post. do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
    eapply invert_step_call_clo in Hstep; eauto.
    destruct Hstep as (?&?&?&?&?&?). subst.
    iFrame. iPureIntro. apply pureinv_call_clo; eauto. }
  { iSplitR.
    {  eauto using reducible_call. }
    intros_post. do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
    iDestruct "Hl" as "(%&%)". subst.
    eapply invert_step_call in Hstep; eauto.
    destruct Hstep as (?&?&?&?&?). subst.
    iFrame. iPureIntro. apply pureinv_call; eauto. }
Qed.

End wpg_call.
