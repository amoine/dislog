From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp.

Section wpg_store.
Context `{!interpGS Σ}.

Lemma pureinv_store σ α G l t bs e :
  locs e = ∅ ->
  l ∈ dom σ ->
  dom σ = dom α ->
  pureinv G α (<[l:=SBlock bs]> σ) (Leaf t) e.
Proof.
  intros Ht ? ?.
  constructor.
  { rewrite dom_insert_L. set_solver. }
  { apply RDeLeaf. rewrite Ht. apply set_Forall_empty. }
Qed.

Lemma wpg_store E t (bs:list val) (l:loc) (i:Z) (v:val) :
  (0 <= i < length bs)%Z ->
  l ↦ bs -∗
  wpg E (Leaf t) (Store l i v) (fun _ w => ⌜w=VUnit⌝ ∗ l ↦ (<[Z.to_nat i:=v]> bs)).
Proof.
  iIntros.
  iApply wpg_unfold. wpg_intros. iDestruct "Hi" as "(%Hc&Hi)".
  iDestruct (interp_exploit_mapsto with "[$]") as "%Hl".
  intros_mod.
  iSplitR. { eauto using reducible_store. }
  intros_post.
  eapply invert_step_store in Hstep.
  destruct Hstep as (?,(Hl'&?&?&?&?&?)); subst.
  rewrite Hl' in Hl. injection Hl. intros ->.
  iMod (interp_store with "[$]") as "(?&?)". iFrame.
  do 2 iModIntro. iMod "Hclose" as "_". iModIntro.
  iSplitR.
  { inversion Hc. iPureIntro. apply pureinv_store; eauto.
    now apply elem_of_dom. }
  iApply wpg_val. eauto.
Qed.

End wpg_store.
