From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates gen_heap.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics reducible invert_step.
From dislog.logic Require Import wpg interp all_abef.

Section wpg_alloc.
Context `{!interpGS Σ}.

Lemma pureinv_alloc G (l:loc) t α σ bs :
  l ∉ dom σ ->
  dom σ = dom α ->
  pureinv G (<[l:=t]> α) (<[l:=SBlock bs]> σ) (Leaf t) l.
Proof.
  intros.
  constructor; eauto using rootsde_alloc.
  rewrite !dom_insert_L. set_solver.
Qed.

Lemma wpg_alloc E t (i:Z) (v:val) :
  (0 <= i)%Z ->
  ⊢ wpg E (Leaf t) (Alloc i v)
      (fun _ w => ∃ l, ⌜w=VLoc l⌝ ∗ l ↦ (replicate (Z.to_nat i) v) ∗ meta_token l (⊤∖↑dislog_nm)).
Proof.
  iIntros.
  rewrite wpg_unfold. wpg_intros. intros_mod.
  iDestruct "Hi" as "(%Hc&?)". inversion Hc.

  iSplitR. { eauto using reducible_alloc. }
  intros_post.
  apply invert_step_alloc in Hstep.
  destruct Hstep as (l,(?&?&?&?&?&?)); subst.

  iMod (interp_alloc _ _ _ l (init_block i v) t with "[$]") as "(?&?&?)".
  1,2:eauto. do 2 iModIntro.
  iMod "Hclose" as "_". iModIntro. iFrame.
  iSplitR. { eauto using pureinv_alloc. }
  iApply wpg_val. iExists _. iFrame. eauto.
Qed.

End wpg_alloc.
