From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics.

(* ------------------------------------------------------------------------ *)
(* [all_abef] the iteration of [abef] *)

Definition all_abef G α (t:timestamp) (L:gset loc) :=
  set_Forall (abef G α t) L.

Lemma all_abef_union G α t L1 L2 :
  all_abef G α t (L1 ∪ L2) <-> all_abef G α t L1 /\ all_abef G α t L2.
Proof.
  split.
  { intros. split.
    { eapply set_Forall_union_inv_1. eauto. }
    { eapply set_Forall_union_inv_2. eauto. } }
  { intros (?&?). apply set_Forall_union; eauto. }
Qed.

Lemma all_abef_mon_set G α t L L' :
  L ⊆ L' ->
  all_abef G α t L' ->
  all_abef G α t L.
Proof.
  rewrite /all_abef /set_Forall.
  set_solver.
Qed.

Lemma all_abef_mon_graph G G' α t L :
  G ⊆ G' ->
  all_abef G α t L ->
  all_abef G' α t L.
Proof.
  intros ??. eapply set_Forall_impl; eauto using abef_mon_graph.
Qed.

Lemma all_abef_mon_amap G α α' t L :
  α ⊆ α' ->
  all_abef G α t L ->
  all_abef G α' t L.
Proof.
  intros ??. eapply set_Forall_impl; eauto using abef_mon_amap.
Qed.

Lemma all_abef_pre_reachable G α t t' L :
  reachable G t t' ->
  all_abef G α t L ->
  all_abef G α t' L.
Proof.
  intros ??. eapply set_Forall_impl; eauto using abef_pre_reachable.
Qed.

Lemma all_abef_elem l G α t L :
  l ∈ L ->
  all_abef G α t L ->
  abef G α t l.
Proof.
  intros.
  rewrite -(@set_Forall_singleton _ (gset loc)).
  eapply all_abef_mon_set. 2:eauto. set_solver.
Qed.

(* ------------------------------------------------------------------------ *)
(* [rootsde] the compatibility of the task tree with a term. *)

Inductive rootsde : graph -> amap -> task_tree -> expr -> Prop :=
| RDeLeaf : forall G α (t:timestamp) (e:expr),
    all_abef G α t (locs e) ->
    rootsde G α (Leaf t) e
| RDeCtx : forall G α (T:task_tree) (K:ctx) (e:expr),
    rootsde G α T e ->
    (set_Forall (fun t => all_abef G α t (locs K)) (leaves T)) ->
    rootsde G α T (fill_item K e)
| RDePar : forall G α (T1 T2:task_tree) (e1 e2:expr),
    rootsde G α T1 e1 ->
    rootsde G α T2 e2 ->
    rootsde G α (Node T1 T2) (Par e1 e2).

Lemma rootsde_mon_graph G G' α T e :
  G ⊆ G' ->
  rootsde G α T e ->
  rootsde G' α T e.
Proof.
  induction 2;
    eauto using RDeLeaf, RDeCtx, RDePar, all_abef_mon_graph.
  eapply RDeCtx; eauto.
  eapply set_Forall_impl; eauto. intros. eauto using all_abef_mon_graph.
Qed.

Lemma rootsde_mon_amap G α α' T e :
  α ⊆ α' ->
  rootsde G α T e ->
  rootsde G α' T e.
Proof.
  induction 2;
    eauto using RDeLeaf, RDeCtx, RDePar, all_abef_mon_amap.
  eapply RDeCtx; eauto.
  eapply set_Forall_impl; eauto. intros. eauto using all_abef_mon_amap.
Qed.

Lemma rootsde_join G α T1 T2 (v1 v2:val) :
  rootsde G α (Node T1 T2) (Par v1 v2) ->
  exists n1 n2, T1=Leaf n1 /\ T2=Leaf n2.
Proof.
  intros E. inversion E; subst; elim_ctx.
  inversion H4; subst.
  2:{ inversion H; elim_ctx. }
  inversion H6; subst.
  2:{ inversion H0; elim_ctx. }
  naive_solver.
Qed.

Lemma rootsde_par G α T1 T2 e1 e2 :
  rootsde G α (Node T1 T2) (Par e1 e2) ->
  rootsde G α T1 e1 /\ rootsde G α T2 e2.
Proof. inversion 1; elim_ctx. naive_solver. Qed.

Lemma rootsde_node_no_val G α T1 T2 e :
  rootsde G α (Node T1 T2) e ->
  ¬ is_val e.
Proof. inversion 1; intros ?; elim_ctx. eauto. Qed.

Lemma rootsde_leaf_inv G α t e:
  rootsde G α (Leaf t) e ->
  all_abef G α t (locs e).
Proof.
  remember (Leaf t) as T.
  induction 1.
  { naive_solver. }
  { subst. rewrite locs_fill_item.
    simpl in *. rewrite set_Forall_singleton in H0.
    apply all_abef_union; eauto. }
  { inversion HeqT. }
Qed.

Lemma rootsde_inv_ctx G α T K e :
  ¬ is_val e ->
  rootsde G α T (fill_item K e) ->
  (set_Forall (fun t => all_abef G α t (locs K)) (leaves T)) /\ rootsde G α T e.
Proof.
  intros Ht Hcomp.
  inversion Hcomp; subst.
  { rewrite locs_fill_item all_abef_union in H. rewrite set_Forall_singleton.
    split; first naive_solver. apply RDeLeaf. naive_solver. }
  { destruct T.
    { apply rootsde_leaf_inv in Hcomp.
      rewrite locs_fill_item all_abef_union in Hcomp. simpl in *.
      rewrite set_Forall_singleton. destruct Hcomp.
      split; first done. apply RDeLeaf. naive_solver. }
    { apply fill_item_inj in H. destruct H as (?,?); subst.
      all:eauto using rootsde_node_no_val. } }
  { inversion H; elim_ctx. }
Qed.

Lemma rootsde_alloc G α t (l:loc) :
  rootsde G (<[l:=t]> α) (Leaf t) l.
Proof.
  apply RDeLeaf.
  replace (locs (Val l)) with ({[l]}:gset loc) by set_solver.
  apply set_Forall_singleton.
  apply abef_insert.
Qed.

(* ------------------------------------------------------------------------ *)
(* [pureinv] *)

Record pureinv (G:graph) (α:amap) (σ:store) (T:task_tree) (e:expr) :=
  { pdom : dom σ = dom α;
    pcmp : rootsde G α T e;
  }.

Lemma pureinv_mon_graph G G' α σ T e :
  G ⊆ G' ->
  pureinv G  α σ T e ->
  pureinv G' α σ T e.
Proof.
  intros ? [].
  constructor; eauto using rootsde_mon_graph.
Qed.

Lemma pureinv_ctx G α σ T K e :
  (set_Forall (λ t, all_abef G α t (locs K)) (leaves T)) ->
  pureinv G α σ T e ->
  pureinv G α σ T (fill_item K e).
Proof.
  intros ? [].
  constructor; eauto using RDeCtx.
Qed.

Lemma pureinv_if G α σ t (b:bool) e1 e2:
  pureinv G α σ (Leaf t) (If b e1 e2) ->
  pureinv G α σ (Leaf t) (if b then e1 else e2).
Proof.
  intros [? Hcomp].
  constructor; eauto.
  apply rootsde_leaf_inv in Hcomp.
  apply RDeLeaf.
  eapply all_abef_mon_set. 2:eauto.
  destruct b; set_solver.
Qed.

Lemma pureinv_let_val G α σ t x (v:val) (e:expr):
  pureinv G α σ (Leaf t) (Let x v e) ->
  pureinv G α σ (Leaf t) (subst' x v e).
Proof.
  intros [? Hcomp].
  constructor; eauto.
  apply rootsde_leaf_inv in Hcomp.
  apply RDeLeaf.
  eapply all_abef_mon_set; eauto using locs_subst'.
Qed.

Lemma pureinv_fork G α σ t e1 e2 v w :
  pureinv G α σ (Leaf t) (Par e1 e2) ->
  pureinv (graph_fork G t v w) α σ (Node (Leaf v) (Leaf w)) (Par e1 e2).
Proof.
  intros [? Hcomp].
  apply rootsde_leaf_inv in Hcomp.
  rewrite locs_par all_abef_union in Hcomp. destruct Hcomp as (?&?).
  constructor; eauto.
  { apply RDePar; apply RDeLeaf.
    { apply all_abef_pre_reachable with t.
      { apply edge_reachable. apply edge_graph_upd. set_solver. }
      { apply all_abef_mon_graph with G; eauto using graph_fork_incl. } }
    { apply all_abef_pre_reachable with t.
      { apply edge_reachable. apply edge_graph_upd. set_solver. }
      { apply all_abef_mon_graph with G; eauto using graph_fork_incl. } } }
Qed.

Lemma pureinv_par_inv G α σ T1 T2 e1 e2 :
  pureinv G α σ (Node T1 T2) (Par e1 e2) -> pureinv G α σ T1 e1 /\ pureinv G α σ T2 e2.
Proof.
  intros [? Hcomp].
  destruct (rootsde_par _ _ _ _ _ _ Hcomp).
  split; constructor; eauto.
Qed.

Lemma pureinv_par_l G α T1 T2 e1 e2 σ σ' α' G' T1' e1':
  step σ α G T1 e1 σ' α' G' T1' e1' ->
  pureinv G α σ (Node T1 T2) (Par e1 e2) ->
  pureinv G' α' σ' T1' e1' ->
  pureinv G' α' σ' (Node T1' T2) (Par e1' e2).
Proof.
  intros Hstep [? Hcomp] [] .
  constructor; eauto.
  { apply rootsde_par in Hcomp. destruct Hcomp.
    apply RDePar; eauto.
    apply rootsde_mon_graph with G; eauto using step_inv_graph.
    apply rootsde_mon_amap with α; eauto using step_inv_amap. }
Qed.

Lemma pureinv_par_r G α T1 T2 e1 e2 σ σ' α' G' T2' e2':
  step σ α G T2 e2 σ' α' G' T2' e2' ->
  pureinv G α σ (Node T1 T2) (Par e1 e2) ->
  pureinv G' α' σ' T2' e2' ->
  pureinv G' α' σ' (Node T1 T2') (Par e1 e2').
Proof.
  intros Hstep [? Hcomp] [].
  constructor; eauto.
  { apply rootsde_par in Hcomp. destruct Hcomp.
    apply RDePar; eauto.
    apply rootsde_mon_graph with G; eauto using step_inv_graph.
    apply rootsde_mon_amap with α; eauto using step_inv_amap. }
Qed.

Lemma pureinv_join G t1 t2 w (l:loc) bs α σ:
  l ∉ dom σ ->
  dom σ = dom α ->
  pureinv (graph_join G t1 t2 w) (<[l:=w]> α) (<[l:=SBlock bs]> σ) (Leaf w) l.
Proof.
  intros.
  constructor.
  { rewrite !dom_insert_L. set_solver. }
  { apply RDeLeaf. rewrite /locs /location_expr. simpl.
    apply set_Forall_singleton. apply abef_insert. }
Qed.

Local Lemma locs_no_loc (v:val) :
  ¬ is_loc v ->
  locs (Val v) = ∅.
Proof. destruct v; naive_solver. Qed.

Lemma pureinv_leaf_val G α σ t v :
  dom σ = dom α ->
  vabef G α t v ->
  pureinv G α σ (Leaf t) v.
Proof.
  intros.
  constructor; eauto.
  { apply RDeLeaf.
    destruct_decide (decide (is_loc v)) as Hv.
    { apply is_loc_inv in Hv. destruct Hv as (?,->).
      rewrite /locs /location_expr. simpl.
      apply set_Forall_singleton. eauto. }
    { rewrite locs_no_loc //. } }
Qed.

Lemma pureinv_init G t e :
  locs e = ∅ ->
  pureinv G ∅ ∅ (Leaf t) e.
Proof.
  intros Ht.
  constructor.
  { rewrite !dom_empty_L //. }
  { apply RDeLeaf. rewrite Ht. apply set_Forall_empty. }
Qed.
