From iris.prelude Require Import prelude.
From iris.proofmode Require Import proofmode.

From dislog.lang Require Import semantics.
From dislog.logic Require Import wpg interp.

Section utils.
Context `{interpGS Σ}.

Definition all_abef (t:timestamp) (vs:list val) : iProp Σ :=
  [∗ list] v ∈ vs, v ◷? t.

Lemma all_abef_nil t :
  ⊢ all_abef t nil.
Proof. easy. Qed.

Lemma all_abef_extract t vs i v :
  vs !! i = Some v ->
  all_abef t vs -∗ v ◷? t.
Proof. iIntros. iApply (big_sepL_lookup with "[$]"). eauto. Qed.

Lemma all_abef_mon t t' vs :
  t ≼ t' -∗
  all_abef t vs -∗
  all_abef t' vs.
Proof.
  iIntros "#? #E".
  iApply (big_sepL_impl with "[$]").
  iModIntro. iIntros. iApply (vclock_mon with "[$][$]").
Qed.

Lemma all_abef_subseteq t xs ys :
  xs ⊆ ys ->
  all_abef t ys -∗ all_abef t xs.
Proof.
  iIntros (Hincl) "#Habef".
  iApply big_sepL_forall. iIntros (j x Hj).
  apply elem_of_list_lookup_2 in Hj.
  apply Hincl in Hj. apply elem_of_list_lookup_1 in Hj. destruct Hj as (?,?).
  iApply (all_abef_extract with "[$]"). eauto.
Qed.

Lemma all_abef_replicate n t v :
  v ◷? t -∗
  all_abef t (replicate n v).
Proof.
  iIntros. iInduction n as [|] "IH".
  { by iApply big_sepL_nil. }
  { iApply big_sepL_cons. iFrame "#". }
Qed.

Lemma all_abef_insert t i v vs :
  i < length vs ->
  all_abef t vs -∗
  v ◷? t -∗
  all_abef t (<[i:=v]> vs).
Proof.
  iIntros.
  destruct (vs !! i) eqn:E.
  2:{ exfalso. apply lookup_ge_None_1 in E. lia. }
  iDestruct (big_sepL_insert_acc with "[$]") as "(_&Hb)". eauto.
  by iApply "Hb".
Qed.

End utils.
