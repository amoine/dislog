From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.lang Require Import syntax semantics reducible.
From dislog Require Import wpg interp.

Section pre_adequacy.
Context `{iG:! interpGS Σ}.

(******************************************************************************)
(* Adequacy *)

Definition adequate_pre σ α G T e Q : iProp Σ :=
  match to_val e with
  | Some v => ∃ t, ⌜T=Leaf t⌝ ∗ Q t v
  | None => ⌜reducible σ α G T e⌝ end.

Lemma wpg_progress σ α G E T e Q :
  ⌜pureinv G α σ T e⌝ ∗ interp σ α G ∗ wpg E T e Q ={E, ∅}=∗
  adequate_pre σ α G T e Q.
Proof.
  iIntros "(Hc&Hi&Hwp)".
  rewrite wpg_unfold /wpg_pre /adequate_pre.

  destruct (to_val e) eqn:Ht.
  { destruct e; inversion Ht. subst v0.
    iMod ("Hwp" with "[-]") as ">[% (->&?&?)]". by iFrame.
    intros_mod. eauto. }
  iMod ("Hwp" with "[$]") as "[%Hred Hwp]". eauto.
Qed.

Definition step '(x1,x2,x3,x4,x5) '(y1,y2,y3,y4,y5) := step x1 x2 x3 x4 x5 y1 y2 y3 y4 y5.

Lemma wpg_preservation n E e σ e' σ' Q G G' α α' T T' :
  nsteps step n (σ,α,G,T,e) (σ',α',G',T',e') ->
  ⌜pureinv G α σ T e⌝ ∗ interp σ α G ∗ wpg E T e Q ={E,∅}=∗ |={∅}▷=>^n |={∅,E}=>
  ⌜pureinv G' α' σ' T' e'⌝ ∗ interp σ' α' G' ∗ wpg E T' e' Q.
Proof.
  revert σ α G T e Q.
  induction n; intros σ α G T e Q;
    inversion 1; subst;
    iIntros "(%Hcorr&Hi&Hwp)".
  { iApply fupd_mask_intro. set_solver. iIntros "Hclose". iFrame.
    iMod "Hclose". eauto. }
  { destruct y as ((((?,?),?),?),?).
    rewrite wpg_unfold /wpg_pre. simpl.
    assert (to_val e = None) as ->.
    { apply is_val_false. eapply step_no_val. eauto. }
    iMod ("Hwp" with "[Hi]") as "[_ Hwp]". by iFrame.
    iModIntro. iMod ("Hwp" with "[%//]") as "Hwp". do 2 iModIntro.
    iMod "Hwp" as "((?&?)&?)".
    iMod (IHn with "[$]") as "?".
    all:eauto. }
Qed.

Lemma wpg_adequacy_pre n E e σ e' σ' Q G G' α α' T T' :
  nsteps step n (σ,α,G,T,e) (σ',α',G',T',e') ->
  ⌜pureinv G α σ T e⌝ ∗ interp σ α G ∗ wpg E T e Q
  ={E,∅}=∗ |={∅}▷=>^n |={∅}=>
  adequate_pre σ' α' G' T' e' Q.
Proof.
  iIntros (Hsteps) "?".
  iMod (wpg_preservation with "[$]") as "?".
  { apply Hsteps. }
  iModIntro.
  iApply (step_fupdN_wand with "[$]").
  iIntros ">(%&?&?)". subst.
  iMod (wpg_progress with "[-]") as "?".
  { iFrame. iPureIntro. eauto. }
  by iFrame.
Qed.

End pre_adequacy.

Import Initialization.

(* ------------------------------------------------------------------------ *)
(* Final theorems *)

(* [adequate] is the conjunction of [strongly_reducible] and [post_valid]*)
Definition adequate σ α G T e Q :=
  match to_val e with
  | Some v => exists t, T = Leaf t /\ Q t v
  | None => reducible σ α G T e end.

Lemma wpg_adequacy_open `{!interpPreG Σ} σ α G T e σ' α' G' T' e' Q :
  dom σ = dom α ->
  pureinv G α σ T e ->
  rtc step (σ,α,G, T,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ([∗ map] l↦v ∈ σ, gen_heap.mapsto l (DfracOwn 1) v) -∗ wpg ⊤ T e (fun t' v => ⌜Q t' v⌝)) ->
  adequate σ' α' G' T' e' Q.
Proof.
  intros ? Hlt Hsteps Hwp.
  apply rtc_nsteps in Hsteps.
  destruct Hsteps as (n,Hsteps).

  eapply uPred.pure_soundness.
  apply (@step_fupdN_soundness_no_lc _ _ _ _ (S n) 0). iIntros.

  iMod (interp_init σ α G) as "[%HinterpGS [%He [Hi Hdia]]]".
  { eauto. }

  iDestruct (Hwp HinterpGS with "[$]") as "Hwp".

  iDestruct (@wpg_adequacy_pre _ _ n with "[Hi Hwp]") as "Hadequate".
  { eauto. }
  { iFrame. eauto. }

  rewrite -He.

  iApply (fupd_mono _ _ with "Hadequate").
  iIntros.

  rewrite step_fupdN_S_fupd. simpl. do 2 iModIntro.
  iModIntro. iStopProof. apply step_fupdN_mono. iIntros ">Hwp !>".
  rewrite /adequate_pre /adequate. destruct (to_val e'); eauto.
  iDestruct "Hwp" as "[% (%&%)]". iPureIntro. eauto.
Qed.

Lemma wpg_adequacy_gen σ α G T e σ' α' G' T' e' Q :
  dom σ = dom α ->
  pureinv G α σ T e ->
  rtc step (σ,α,G,T,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ([∗ map] l↦v ∈ σ, gen_heap.mapsto l (DfracOwn 1) v) -∗ wpg ⊤ T e (fun t v => ⌜Q t v⌝)) ->
  adequate σ' α' G' T' e' Q.
Proof.
  intros. eapply wpg_adequacy_open; eauto with typeclass_instances.
Qed.

(* The main theorem *)
Theorem wpg_adequacy t e σ' α' G' T' e' Q :
  locs e = ∅ ->
  rtc step (∅,∅,∅,Leaf t,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ⊢ wpg ⊤ (Leaf t) e (fun t v => ⌜Q t v⌝)) ->
  adequate σ' α' G' T' e' Q.
Proof.
  intros Ht Hrtc Hwp. eapply wpg_adequacy_gen; eauto.
  { rewrite !dom_empty_L //. }
  { eauto using pureinv_init. }
  { iIntros. iApply Hwp. }
Qed.
