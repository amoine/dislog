From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph more_iris.
From dislog.lang Require Import syntax reducible invert_step atomic.
From dislog.lang Require Export semantics.
From dislog.logic Require Export pureinv.

Set Implicit Arguments.

(* We define the WP w.r.t to an abstract [interp] predicate,
   quantified here. This predicate is realized in [interp.v] *)
Class dislogGS (Σ : gFunctors) :=
  DislogGS {
      (* No later credits, allowing a simple statement of [wpg_par.give_plain] *)
      iinvgs : invGS_gen HasNoLc Σ;
      interp : store -> amap -> graph -> iProp Σ;
    }.

#[global] Existing Instance iinvgs.

(* Some ltac *)
Ltac wpg_intros := iIntros (σ α G) "Hi".
Ltac intros_mod := iApply fupd_mask_intro; [ set_solver | iIntros "Hclose"].
Ltac intros_post := iIntros (????? Hstep).
Ltac mod_all H Z := iMod (H with "[%//]") as H; do 2 iModIntro; iMod H as Z.

Section wpg.
Context `{!dislogGS Σ}.

(* ------------------------------------------------------------------------ *)
(* The actual wpg *)

(* [winterp] is the conjunction of the pure correctness [pureinv] and
   the state interpretation [interp]. We separate the two for clarity. *)

Definition winterp G α σ T e : iProp Σ := ⌜pureinv G α σ T e⌝ ∗ interp σ α G.

(* As usual, the WP is defined as a guarded fixpoint. [wpg_pre] is
   the "factored-out" version. *)

Definition wpg_pre
  (wpg : coPset -d> task_tree -d> expr -d> (timestamp -d> val -d> iPropO Σ) -d> iPropO Σ) :
  coPset -d> task_tree -d> expr -d> (timestamp -d> val -d> iPropO Σ) -d> iPropO Σ := λ E T e Q,
  (∀ σ α G, winterp G α σ T e ={E,∅}=∗
  match to_val e with
  | Some v => |={∅,E}=> ∃t, ⌜T=Leaf t⌝ ∗ winterp G α σ T e ∗ Q t v
  | None => ⌜reducible σ α G T e⌝ ∗
       (∀ α' σ' G' T' e', ⌜step σ α G T e σ' α' G' T' e'⌝  ={∅}=∗ ▷ |={∅,E}=>
          winterp G' α' σ' T' e' ∗ wpg E T' e' Q)
  end)%I.

Local Instance wpg_pre_contractive : Contractive wpg_pre.
Proof.
  rewrite /wpg_pre /= => n wpg wp' Hwp ? ? e Q.
  repeat (f_contractive || f_equiv); apply Hwp.
Qed.

(* wpg as the fixpoint of wpg_pre *)
Definition wpg : coPset -> task_tree -> expr -> (timestamp -> val -> iProp Σ) -> iProp Σ :=
  fixpoint wpg_pre.

Lemma wpg_unfold E t e Q :
  wpg E t e Q ⊣⊢ wpg_pre wpg E t e Q.
Proof. apply (fixpoint_unfold wpg_pre). Qed.

(* ------------------------------------------------------------------------ *)
(* Structural rules *)

Lemma fupd_wpg E T e Q :
  (|={E}=> wpg E T e Q) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp".
  rewrite !wpg_unfold /wpg_pre. wpg_intros.
  destruct (to_val e).
  { iMod "Hwp" as "Hwp". by iApply "Hwp". }
  iIntros. iMod "Hwp".
  iApply ("Hwp" with "[$]").
Qed.

Lemma bupd_wpg E T e Q :
  (|==> wpg E T e Q) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp". iApply fupd_wpg. by iFrame.
Qed.

Lemma wpg_fupd E T e Q :
  wpg E T e (fun t v => |={E}=> Q t v) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp".
  iLöb as "IH" forall (T e).
  rewrite !wpg_unfold /wpg_pre. wpg_intros.
  destruct (to_val e).
  { iMod ("Hwp" with "[$]") as "Hwp".
    iMod "Hwp" as "[% (?&?&>?)]". intros_mod. iMod "Hclose".
    iExists _. by iFrame. }
  iIntros. iMod ("Hwp" with "[$]") as "(Hr&Hwp)". iModIntro.
  iFrame "Hr". intros_post.
  mod_all "Hwp" "(?&?)". iFrame.
  by iApply "IH".
Qed.

Lemma wpg_strong_mono E T e P Q :
  wpg E T e P -∗
  (∀ t v, P t v ={E}=∗ Q t v) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp HPQ".
  iLöb as "IH" forall (T e).
  rewrite !wpg_unfold /wpg_pre.
  wpg_intros.
  destruct (to_val e).
  { iMod ("Hwp" with "[$]") as ">[% (->&?&?)]".
    iMod ("HPQ" with "[$]"). intros_mod. iMod "Hclose". iModIntro. iFrame. eauto. }
  { iIntros. iMod ("Hwp" with "[$]") as "(Hr&Hwp)". iFrame "Hr".
    iModIntro. iIntros.
    mod_all "Hwp" "(?&?)". iFrame. iModIntro.
    iApply ("IH" with "[$][$]"). }
Qed.

Lemma wpg_mono E T e P Q :
  wpg E T e P -∗
  (∀ t v, P t v -∗ Q t v) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp HPQ".
  iApply (wpg_strong_mono with "Hwp").
  iIntros. iApply "HPQ". by iFrame.
Qed.

Lemma wpg_frame_step P E T e Q :
  ¬ is_val e ->
  ▷ P -∗
  wpg E T e Q -∗
  wpg E T e (fun t v => Q t v ∗ P).
Proof.
  iIntros (He) "HP Hwp".
  rewrite !wpg_unfold. wpg_intros.
  iSpecialize ("Hwp" with "[Hi]"); first by iFrame.
  replace (to_val e) with (@None val) by (symmetry; by apply is_val_false).
  iIntros. iMod "Hwp" as "(?&Hwp)". iModIntro. iFrame.
  iIntros. mod_all "Hwp" "(?&?)". iFrame.
  iModIntro. iApply (wpg_mono with "[$]"). iIntros. by iFrame.
Qed.

Lemma wpg_end E T e Q :
  wpg E T e (fun t v => wpg E (Leaf t) v Q) -∗
  wpg E T e Q.
Proof.
  iIntros "Hwp".
  iLöb as "IH" forall (T e).
  rewrite !wpg_unfold /wpg_pre.
  wpg_intros.
  destruct (to_val e) eqn:Eq.
  { iMod ("Hwp" with "[$]") as ">[% (->&?&Hwp)]".
    rewrite wpg_unfold. assert (e=Val v) by (destruct e; naive_solver). subst.
    iMod ("Hwp" with "[$]") as ">[% (%Eq'&?)]". injection Eq'. intros <-.
    iExists t. intros_mod. iMod "Hclose". by iFrame. }
  { iIntros. iMod ("Hwp" with "[$]") as "(Hr&Hwp)". iFrame "Hr".
    iModIntro. iIntros.
    mod_all "Hwp" "(?&?)". iFrame. iModIntro.
    iApply ("IH" with "[$]"). }
Qed.

(* ------------------------------------------------------------------------ *)
(* rules of the wpg *)

Lemma wpg_val E t v Q :
  Q t v -∗ wpg E (Leaf t) (Val v) Q.
Proof.
  iIntros.
  rewrite wpg_unfold.
  wpg_intros. intros_mod. iExists _.  iFrame. by iMod "Hclose".
Qed.

Lemma wpg_if E t (b:bool) e1 e2 Q :
  wpg E (Leaf t) (if b then e1 else e2) Q -∗
  wpg E (Leaf t) (If b e1 e2) Q.
Proof.
  iIntros "Hwp".
  iApply wpg_unfold. wpg_intros; intros_mod.
  iSplitR. { eauto using reducible_if. }
  intros_post. do 2 iModIntro. iMod "Hclose".
  apply invert_step_if in Hstep.
  destruct Hstep as (?&?&?&?&?); subst.
  iDestruct "Hi" as "(%&?)". iFrame.
  eauto using pureinv_if.
Qed.

Lemma wpg_let_val E t x (v:val) e Q :
  wpg E (Leaf t) (subst' x v e) Q -∗
  wpg E (Leaf t) (Let x v e) Q.
Proof.
  iIntros "Hwp".
  iApply wpg_unfold. wpg_intros. intros_mod.
  iSplitR. { eauto using reducible_let_val. }
  intros_post. do 2 iModIntro. iMod "Hclose". iModIntro.
  apply invert_step_let_val in Hstep.
  destruct Hstep as (?&?&?&Heq&?); subst.
  iDestruct "Hi" as "(%&?)". iFrame.
  eauto using pureinv_let_val.
Qed.

Lemma to_val_fill_item K e : to_val (fill_item K e) = None.
Proof. destruct K; naive_solver. Qed.

Lemma to_val_Some_inv e v : to_val e = Some v -> e = Val v.
Proof. destruct e; naive_solver. Qed.

Lemma pureinv_bind G α σ T K e :
  (is_val e -> is_leaf T) ->
  pureinv G α σ T (fill_item K e) ->
  (set_Forall (fun t => all_abef G α t (locs K)) (leaves T)) /\ pureinv G α σ T e.
Proof.
  intros C [? Hcomp].
  destruct_decide (decide (is_val e)).
  { destruct T; last naive_solver.
    apply rootsde_leaf_inv in Hcomp.
    rewrite locs_fill_item all_abef_union in Hcomp. simpl. rewrite set_Forall_singleton.
    split; first naive_solver.
    constructor; eauto.  apply RDeLeaf. naive_solver. }
  { apply rootsde_inv_ctx in Hcomp; eauto.
    split; first naive_solver.
    constructor; naive_solver.  }
Qed.

Lemma step_inv_reach t S σ α G T e σ' α' G' T' e1 :
  step σ α G T e σ' α' G' T' e1  ->
  set_Forall (λ t : timestamp, all_abef G α t S) (leaves T) ->
  t ∈ leaves T'  ->
  all_abef G' α t S.
Proof.
  induction 1; intros Hforall Hu; eauto.
  { inversion H; subst; eauto.
    { rewrite set_Forall_singleton in Hforall.
      eapply all_abef_pre_reachable with t0.
      { apply rtc_once. set_solver. }
      { eapply all_abef_mon_graph; eauto using graph_fork_incl. } }
    { simpl in *. rewrite elem_of_singleton in Hu. subst.
      eapply all_abef_pre_reachable with t1.
      { apply rtc_once. set_solver. }
      { eapply all_abef_mon_graph; eauto using graph_join_incl.
        apply Hforall. set_solver. } } }
  all: rewrite leaves_node in Hforall.
  all: rewrite leaves_node elem_of_union in Hu.
  { destruct Hu.
    { apply IHstep; eauto. eapply set_Forall_union_inv_1; eauto. }
    { apply all_abef_mon_graph with G; first (eapply step_inv_graph; eauto).
      apply Hforall. set_solver. } }
  { destruct Hu.
    { apply all_abef_mon_graph with G; first (eapply step_inv_graph; eauto).
      apply Hforall. set_solver. }
    { apply IHstep; eauto. eapply set_Forall_union_inv_2; eauto. } }
Qed.

Lemma val_inv_final_tree σ α G T e σ' α' G' T' e1 :
  step σ α G T e σ' α' G' T' e1 ->
  is_val e1 ->
  is_leaf T'.
Proof. induction 1; intros; try done; elim_ctx; inversion H; subst; done. Qed.

(* This is a somewhat odd precondition, but I need it for the induction to succeed.
   See [wpg_bind] for a simpler lemma. *)
Lemma wpg_bind_pre E T K e Q:
  (is_val e -> is_leaf T) ->
  wpg E T e (fun t v => wpg E (Leaf t) (fill_item K v) Q) -∗
  wpg E T (fill_item K e) Q.
Proof.
  iIntros (Ht) "Hwp".
  iLöb as "IH" forall (T e Ht).
  rewrite !wpg_unfold.
  wpg_intros.

  iDestruct "Hi" as "(%Hcomp&Hi)".

  assert ((set_Forall (fun t => all_abef G α t (locs K)) (leaves T)) /\ pureinv G α σ T e) as (?&?)
      by eauto using pureinv_bind.

  iMod ("Hwp" with "[Hi]") as "Hwp".
  { by iFrame. }

  destruct (to_val e) eqn:Hvt.
  { apply to_val_Some_inv in Hvt. rewrite Hvt.
    iMod "Hwp" as "[%u (%&(?&?)&Hwp)]".
    subst. rewrite wpg_unfold /wpg_pre.
    iApply ("Hwp" with "[-]"). by iFrame. }

  rewrite to_val_fill_item. iModIntro.
  apply is_val_false in Hvt.
  iDestruct "Hwp" as "(%Hred&Hwp)".
  iSplitR. { eauto using RedCtx. }
  intros_post.

  apply invert_step_fill_item in Hstep; last eauto.
  destruct Hstep as (e1,(?&?)). subst.
  mod_all "Hwp" "((%&?)&Hwp)". iFrame.
  iSplitR.
  { iPureIntro. apply pureinv_ctx; eauto.
    intros t HT'.
    destruct Hcomp.
    eapply all_abef_mon_amap; first (eapply step_inv_amap; eauto).
    eauto using step_inv_reach. }

  iModIntro. iApply "IH".
  { iPureIntro. eauto using val_inv_final_tree. }
  iFrame.
Qed.

Lemma wpg_bind E t e K Q :
  wpg E (Leaf t) e (fun t' v => wpg E (Leaf t') (fill_item K v) Q) -∗
  wpg E (Leaf t) (fill_item K e) Q.
Proof. apply wpg_bind_pre. eauto. Qed.

(* We can open invariants around atomic operations. *)
Lemma wpg_atomic E1 E2 t e Q :
  Atomic e ->
  (|={E1,E2}=> wpg E2 (Leaf t) e (fun t' v => |={E2,E1}=> Q t' v )) ⊢ wpg E1 (Leaf t) e Q.
Proof.
  iIntros (Hatomic) "Hwp". rewrite !wpg_unfold /wpg_pre.
  wpg_intros.
  rewrite Atomic_no_val //.
  iMod ("Hwp"  with "[$]") as ">(Hr&Hwp)". iModIntro.
  iFrame "Hr". intros_post.
  mod_all "Hwp" "(?&Hwp)".
  apply Atomic_correct in Hstep; eauto. destruct Hstep as (->&(?,->)).
  rewrite wpg_unfold /wpg_pre. simpl.
  iMod ("Hwp" with "[$]") as ">[% (%&?&>?)]".
  iFrame "%∗". subst. iModIntro.
  iApply wpg_val. inversion H. by iFrame.
Qed.
End wpg.

(* ------------------------------------------------------------------------ *)
(* We prove equivalence with the WP showed in the paper. *)

Section paper_wpg.
Context `{!dislogGS Σ}.

Definition to_val_force (e:expr) :=
  match e with
  | Val v => v
  | _ => inhabitant end.
Definition to_leaf_force (T:task_tree) :=
  match T with
  | Leaf t => t
  | _ => inhabitant end.

Definition paper_wpg_pre
  (paper_wpg : coPset -d> task_tree -d> expr -d> (timestamp -d> val -d> iPropO Σ) -d> iPropO Σ) :
  coPset -d> task_tree -d> expr -d> (timestamp -d> val -d> iPropO Σ) -d> iPropO Σ := λ E T e Q,
    ((⌜is_val e⌝ ∗ ∀ σ α G, winterp G α σ T e ={E}=∗ ⌜is_leaf T⌝ ∗ winterp G α σ T e ∗ Q (to_leaf_force T) (to_val_force e))
  ∨ (⌜¬ is_val e⌝ ∗ ∀ σ α G, winterp G α σ T e ={E,∅}=∗
        ⌜reducible σ α G T e⌝ ∗
       (∀ α' σ' G' T' e', ⌜step σ α G T e σ' α' G' T' e'⌝  ={∅}=∗ ▷ |={∅,E}=>
          winterp G' α' σ' T' e' ∗ paper_wpg E T' e' Q)))%I.

Local Instance paper_wpg_pre_contractive : Contractive paper_wpg_pre.
Proof.
  rewrite /paper_wpg_pre /= => n paper_wpg wp' Hwp ? ? e Q.
  repeat (f_contractive || f_equiv); apply Hwp.
Qed.

Definition paper_wpg : coPset -> task_tree -> expr -> (timestamp -> val -> iProp Σ) -> iProp Σ :=
  fixpoint paper_wpg_pre.

Lemma paper_wpg_unfold E t e Q :
  paper_wpg E t e Q ⊣⊢ paper_wpg_pre paper_wpg E t e Q.
Proof. apply (fixpoint_unfold paper_wpg_pre). Qed.

Lemma wpg_equiv E T e Q :
  paper_wpg E T e Q ⊣⊢ wpg E T e Q.
Proof.
  iLöb as "IH" forall (T e Q).
  rewrite wpg_unfold paper_wpg_unfold /wpg_pre /paper_wpg_pre.
  iSplit; iIntros "Hwp".
  { iIntros.
    iDestruct "Hwp" as "[(%He&Hwp)|(%He&Hwp)]".
    { apply is_val_true in He. destruct He as (?&->). simpl.
      iMod ("Hwp" with "[$]") as "(%Hl&?&?)".
      destruct T; last (exfalso; done). intros_mod. iMod "Hclose". iModIntro.
      iExists _. by iFrame. }
    { apply is_val_false in He. rewrite He.
      iMod ("Hwp" with "[$]") as "(?&Hwp)". iFrame. iModIntro.
      iIntros. iMod ("Hwp" with "[%//]") as "Hwp". do 2 iModIntro.
      iMod "Hwp" as "(?&?)". iModIntro. iFrame. by iApply "IH". } }
  { iIntros.
    destruct_decide (decide (is_val e)) as He.
    { iLeft. iFrame "%". iIntros. iMod ("Hwp" with "[$]") as "Hwp".
      apply is_val_true in He. destruct He as (?&->).
      iMod "Hwp" as "[% (->&?&?)]". by iFrame. }
    { iRight. iFrame "%". iIntros.
      iMod ("Hwp" with "[$]") as "Hwp".
      apply is_val_false in He. rewrite He. iDestruct "Hwp" as "(?&Hwp)".
      iModIntro. iFrame. iIntros. iMod ("Hwp" with "[%//]") as "Hwp".
      do 2 iModIntro. iMod ("Hwp") as "(?&?)". iModIntro. iFrame.
      by iApply "IH". } }
Qed.

End paper_wpg.
