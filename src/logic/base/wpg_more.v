From iris.proofmode Require Import base proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Import weakestpre.
From iris.algebra Require Import gset gmap frac.

From dislog.utils Require Import graph.
From dislog.lang Require Import syntax semantics invert_step atomic reducible.
From dislog.logic Require Import wpg interp.

(* ------------------------------------------------------------------------ *)
(* This file defines important, non syntax-directed reasoning rules for
   dislog *)

Section wpg_more.
Context `{!interpGS Σ}.

(* ------------------------------------------------------------------------ *)
(* Toward [tempus_fugit] *)

(* If all leaves are reachable before a step, then they are still reachable
   after the step. *)
Local Lemma step_inv_tree t0 σ α G T e σ' α' G' T' e' :
  set_Forall (reachable G t0) (leaves T) ->
  step σ α G T e σ' α' G' T' e' ->
  set_Forall (reachable G' t0) (leaves T').
Proof.
  intros Hforall.
  induction 1; eauto; simpl in *.
  { inversion H; subst; simpl in *.
    { set_solver. }
    { rewrite set_Forall_singleton in Hforall.
      intros x Hx. transitivity t1.
      { eauto using reachable_mon,graph_fork_incl. }
      { apply rtc_once; set_solver. } }
    { intros x Hx. rewrite elem_of_singleton in Hx. subst.
      transitivity t1.
      { eapply reachable_mon; eauto using graph_join_incl. apply Hforall. set_solver. }
      { apply rtc_once. set_solver. } } }
  { apply set_Forall_union.
    { apply IHstep. by eapply set_Forall_union_inv_1. }
    { intros x Hx. eapply reachable_mon. by eapply step_inv_graph.
      apply Hforall. set_solver. } }
  { apply set_Forall_union.
    { intros x Hx. eapply reachable_mon. by eapply step_inv_graph.
      apply Hforall. set_solver. }
    { apply IHstep. by eapply set_Forall_union_inv_2. } }
Qed.

Lemma deduce_set_Forall t0 T σ α G :
  ([∗ set] t ∈ T, t0 ≼ t) ∗ interp σ α G -∗
  ⌜set_Forall (reachable G t0) T⌝.
Proof.
  iIntros "(HT& Hi)".
  iInduction T as [|] "IH" using set_ind_L.
  { eauto. }
  { rewrite big_sepS_insert; last set_solver.
    iDestruct "HT" as "(?&?)".
    iDestruct (interp_prec_exploit with "[$][$]") as "%".
    iDestruct ("IH" with "[$][$]") as "%".
    iPureIntro. apply set_Forall_union; eauto.
    apply set_Forall_singleton. eauto. }
Qed.

Lemma alloc_set_Forall t0 T σ α G :
  set_Forall (reachable G t0) T ->
  interp σ α G ==∗
  interp σ α G ∗ ([∗ set] t ∈ T, t0 ≼ t).
Proof.
  iIntros (HT) "Hi".
  iInduction T as [|] "IH" using set_ind_L.
  { eauto. }
  { rewrite big_sepS_insert; last set_solver.
    iMod ("IH" with "[%][$]") as "(?&?)".
    { by eapply set_Forall_union_inv_2. }
    apply set_Forall_union_inv_1, set_Forall_singleton in HT.
    iMod (alloc_prec with "[$]") as "(?&?)". eauto.
    by iFrame. }
Qed.

(* A general form of [wpg_tempus_fugit] needed for the induction to succeed. *)
Local Lemma tempus_fugit_pre t0 E T e Q :
  ([∗ set] t ∈ leaves T, t0 ≼ t) ∗ wpg E T e (fun t' v => t0 ≼ t' -∗ Q t' v) -∗
  wpg E T e Q.
Proof.
  iIntros "(Ht0&Hwp)".
  iLöb as "IH" forall (T e).
  rewrite !wpg_unfold /wpg_pre. wpg_intros. iDestruct "Hi" as "(%&Hi)".
  iDestruct (deduce_set_Forall with "[$]") as "%HX".
  iMod ("Hwp" with "[Hi]") as "Hwp". by iFrame.
  destruct (to_val e).
  { iModIntro. iMod "Hwp" as "[% (->&?&HQ)]". iModIntro.
    iExists _. iFrame.
    iSplitR; first easy. iApply "HQ". simpl. rewrite big_sepS_singleton.
    by iFrame. }
  { iDestruct "Hwp" as "(?&Hwp)". iModIntro. iFrame.
    intros_post. mod_all "Hwp" "((?&?)&?)".
    eapply step_inv_tree in HX; eauto.
    iMod (alloc_set_Forall with "[$]") as "(?&?)". eauto.
    iFrame. iModIntro.
    iApply ("IH" with "[$]"). iApply (wpg_mono with "[$]").
    iIntros (? ?) "HQ #?". iApply ("HQ" with "[$]"). }
Qed.

(* Tempus fugit: the current timestamp of a task precedes its end timestamp. *)
Lemma tempus_fugit t E e Q :
  wpg E (Leaf t) e (fun t' v => t ≼ t' -∗ Q t' v) -∗
  wpg E (Leaf t) e Q.
Proof.
  iIntros. iApply (tempus_fugit_pre t).
  rewrite big_sepS_singleton.
  iSplitR; [ by iApply prec_refl | iFrame ].
Qed.

(* ------------------------------------------------------------------------ *)
(* [mementopre] *)

Lemma mementopre l E t e Q :
  l ∈ locs e ->
  (l ◷ t -∗ wpg E (Leaf t) e Q) -∗
  wpg E (Leaf t) e Q.
Proof.
  iIntros (?) "HP".
  rewrite !wpg_unfold /wpg_pre. wpg_intros. iDestruct "Hi" as "(%Hcomp&?)".
  inversion Hcomp.
  apply rootsde_leaf_inv in pcmp.
  assert (abef G α t l) by eauto.
  iMod (interp_insert_abef with "[$]") as "(Hi&?)". eauto.
  iApply ("HP" with "[$][Hi]"). by iFrame.
Qed.

Lemma vmementopre v E t e Q :
  locs v ⊆ locs e ->
  (v ◷? t -∗ wpg E (Leaf t) e Q) -∗
  wpg E (Leaf t) e Q.
Proof.
  iIntros (Hlv) "HP".
  destruct_decide (decide (is_loc v)) as Hv.
  { apply is_loc_inv in Hv. destruct Hv as (l,Hl). subst v. iApply (mementopre with "[$]").
    rewrite /locs /location_val /locs_val in Hlv. rewrite /locs. set_solver. }
  { iApply "HP". destruct v; naive_solver. }
Qed.

Definition all_abef_set t g : iProp Σ :=
  [∗ set] l ∈ g, l ◷ t.

Lemma mementopre_iterated E t e Q :
  (all_abef_set t (locs e) -∗ wpg E (Leaf t) e Q) -∗
  wpg E (Leaf t) e Q.
Proof.
  iIntros "E".
  remember (locs e) as g.
  assert (g ⊆ locs e) by set_solver. clear Heqg.
  iInduction g as [|l] "IH" using set_ind_L.
  { iApply "E". by iApply big_sepS_empty. }
  { iApply (mementopre l). set_solver. iIntros.
    iApply ("IH" with "[%]"). set_solver.
    iIntros. iApply "E". iApply big_sepS_union. set_solver.
    rewrite big_sepS_singleton. iFrame "#". }
Qed.

Lemma exploit_all_abef_set G α σ t X :
  dom σ = dom α ->
  all_abef_set t X ∗ interp σ α G -∗
  ⌜all_abef G α t X⌝.
Proof.
  iIntros (?) "(HS&?)". rewrite /all_abef_set.
  iApply big_sepS_pure_1. rewrite !big_sepS_forall. iIntros.
  iSpecialize ("HS" with "[%//]").
  iApply interp_exploit_clock; eauto.
Qed.

(* ------------------------------------------------------------------------ *)
(* [mementopost] *)

Lemma mementopost E T e Q :
  wpg E T e (fun t v => v ◷? t -∗ Q t v) -∗
  wpg E T e Q.
Proof.
  iIntros. iApply wpg_end. iApply (wpg_mono with "[$]").
  iIntros (??) "HP". iApply (vmementopre v). eauto.
  iIntros. iApply wpg_val. by iApply "HP".
Qed.

(* ------------------------------------------------------------------------ *)
(* [tempusatomic] *)

Lemma tempusatomic E t e Q :
  Atomic e ->
  wpg E (Leaf t) e (fun t' v => ⌜t'=t⌝ -∗ Q t v) -∗
  wpg E (Leaf t) e Q.
Proof.
  iIntros (?) "Hwp".
  rewrite !wpg_unfold /wpg_pre.
  wpg_intros. rewrite Atomic_no_val //.
  iMod ("Hwp" with "[$]") as "(?&Hwp)". iModIntro. iFrame.
  intros_post.
  mod_all "Hwp" "(?&Hwp)".
  apply Atomic_correct in Hstep; eauto. destruct Hstep as (->&(?,->)).
  rewrite wpg_unfold /wpg_pre. simpl.

  iMod ("Hwp" with "[$]") as ">[% (%&?&HP)]". iModIntro.
  iFrame "%∗". inversion H0. subst. iApply wpg_val. by iApply "HP".
Qed.

(* ------------------------------------------------------------------------ *)
(* [wpg_bind_inv] *)

Local Lemma reducible_bind_inv σ α G T K e :
  ¬ is_val e ->
  reducible σ α G T (fill_item K e) ->
  reducible σ α G T e.
Proof.
  inversion 2; subst; elim_ctx.
  { exfalso. by eapply sched_step_no_ctx. }
  { apply fill_item_inj in H1; eauto using reducible_no_val. naive_solver. }
Qed.

Local Lemma all_abef_invert T X σ α G :
  dom σ = dom α →
  ([∗ set] t ∈ T, [∗ set] l ∈ X,  l ◷ t) ∗ interp σ α G -∗
  ⌜set_Forall (λ t : timestamp, all_abef G α t X) T⌝.
Proof.
  iIntros (?) "(HS&?)".
  iApply big_sepS_pure_1. rewrite !big_sepS_forall. iIntros. iSpecialize ("HS" with "[%//]").
  iApply big_sepS_pure_1. rewrite !big_sepS_forall. iIntros. iSpecialize ("HS" with "[%//]").
  iApply interp_exploit_clock; eauto.
Qed.

Local Lemma all_abef_preserve1 t α G X σ:
  all_abef G α t X  ->
  interp σ α G ==∗ interp σ α G ∗ ([∗ set] l ∈ X,  l ◷ t).
Proof.
  iIntros (HT) "Hi".
  iInduction X as [|] "IH" using set_ind_L.
  { eauto. }
  { rewrite big_sepS_insert; last set_solver.
    iMod ("IH" with "[%][$]") as "(?&?)".
    { by eapply set_Forall_union_inv_2. }
    apply set_Forall_union_inv_1, set_Forall_singleton in HT.
    iMod (interp_insert_abef with "[$]") as "(?&?)". apply HT.
    by iFrame. }
Qed.

Local Lemma all_abef_preserve σ α G X L:
  set_Forall (λ t : timestamp, all_abef G α t X) L ->
  interp σ α G ==∗ interp σ α G ∗ ([∗ set] t ∈ L, [∗ set] l ∈ X, l ◷ t).
Proof.
  iIntros (HT) "Hi".
  iInduction L as [|] "IH" using set_ind_L.
  { eauto. }
  { rewrite big_sepS_insert; last set_solver.
    iMod ("IH" with "[%][$]") as "(?&?)".
    { by eapply set_Forall_union_inv_2. }
    apply set_Forall_union_inv_1, set_Forall_singleton in HT.
    iMod (all_abef_preserve1 with "[$]") as "(?&?)". apply HT.
    by iFrame. }
Qed.

Local Lemma wpg_bind_inv_pre E T e K Q :
  (is_val e -> is_leaf T) ->
  ([∗ set] t ∈ (leaves T), [∗ set] l ∈ locs K, l ◷ t) ∗ wpg E T (fill_item K e) Q -∗
  wpg E T e (fun t' v => wpg E (Leaf t') (fill_item K v) Q ).
Proof.
  iIntros (HT) "(Hlocs&Hwp)".
  iLöb as "IH" forall (T e HT).
  iApply wpg_unfold. wpg_intros.
  destruct (to_val e) eqn:He.
  { apply to_val_Some_inv in He. rewrite He. subst.
    destruct T; last (exfalso; naive_solver).
    intros_mod. iMod "Hclose".
    iModIntro. iExists _. by iFrame. }
  { rewrite wpg_unfold.
    iDestruct "Hi" as "(%Hc&Hi)".
    iDestruct (all_abef_invert with "[$]") as "%".
    { by inversion Hc. }
    iSpecialize ("Hwp" with "[Hi]").
    { iFrame. iPureIntro. eauto using pureinv_ctx. }
    rewrite to_val_fill_item.
    iMod "Hwp" as "(%&Hwp)".
    iModIntro. iSplitR.
    { iPureIntro. eapply reducible_bind_inv; eauto. destruct e; naive_solver. }
    intros_post. pose proof (StepBind _ _ _ _ _ _ _ _ _ _ K Hstep).
    mod_all "Hwp" "((%Hc'&Hi)&?)".

    assert (set_Forall (λ t : timestamp, all_abef G' α' t (locs K)) (leaves T')).
    { intros t' Ht'.
      eapply all_abef_mon_amap.
      { eapply step_inv_amap; eauto. by inversion Hc. }
      eauto using step_inv_reach. }

    iMod (all_abef_preserve with "[$]") as "(?&HL)". eauto.
    iDestruct ("IH" with "[%]HL[$]") as "?".
    { eauto using val_inv_final_tree. }

    iFrame. iPureIntro. apply pureinv_bind in Hc'; eauto using val_inv_final_tree.
    naive_solver. }
Qed.

(* [wpg_bind_inv] has an unusual precondition: the locations of the
   evaluation context should have been previously allocated. *)
Lemma wpg_bind_inv E t e K Q :
  ([∗ set] l ∈ locs K, l ◷ t) ∗ wpg E (Leaf t) (fill_item K e) Q -∗
  wpg E (Leaf t) e (fun t' v => wpg E (Leaf t') (fill_item K v) Q ).
Proof.
  iIntros "(?&?)". iApply wpg_bind_inv_pre. done.
  simpl. rewrite big_sepS_singleton. iFrame.
Qed.

(* ------------------------------------------------------------------------ *)
(* [wpg_binds] applies multiple times [wpg_bind].
   Relies on [memento_pre] and [wpg_bind_inv] *)

(* [fill_items] takes a list _in reverse order_ *)
Fixpoint fill_items (ks:list ctx) e :=
  match ks with
  | [] => e
  | K::ks => fill_items ks (fill_item K e) end.

Lemma fill_items_app ks1 ks2 e :
  fill_items (ks1 ++ ks2) e = fill_items ks2 (fill_items ks1 e).
Proof. revert ks2 e; induction ks1; naive_solver. Qed.

Lemma wpg_binds E t (e:expr) ks (Q:timestamp -> val -> iProp Σ) :
  wpg E (Leaf t) e (fun t' (v:val) => wpg E (Leaf t') (fill_items ks v) Q) -∗
  wpg E (Leaf t) (fill_items ks e) Q.
Proof.
  iIntros "Hwp".
  iInduction ks as [|] "IH" using List.rev_ind forall (e Q).
  { iApply wpg_end. iFrame. }
  { simpl. iApply mementopre_iterated.  iIntros "HX".
    rewrite fill_items_app.
    iApply wpg_bind.
    iApply "IH".  iApply tempus_fugit. iApply (wpg_mono with "[$]"). iIntros.
    iApply wpg_bind_inv. rewrite fill_items_app. simpl. iFrame.
    rewrite /all_abef_set. rewrite !big_sepS_forall. iIntros.
    iApply (clock_mon with "[-][$]"). iApply "HX". iPureIntro.
    rewrite locs_fill_item. set_solver. }
Qed.

End wpg_more.
