From iris.proofmode Require Import base proofmode.
From dislog.lang Require Import syntax semantics reducible.
From dislog.logic Require Import wpg interp wp wpg_adequacy.

Lemma wp_adequacy t e σ' α' G' T' e' Q :
  locs e = ∅ ->
  rtc step (∅,∅,∅,Leaf t,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ⊢ wp ⊤ t e (fun t v => ⌜Q t v⌝)) ->
  adequate σ' α' G' T' e' Q.
Proof.
  intros ? ? Hwp.
  eapply wpg_adequacy; eauto.
  intros ??.
  specialize (Hwp _ _).
  rewrite wp_eq in Hwp.
  iIntros. iApply wpg_mono. iApply Hwp.
  iIntros. rewrite post_val. by iFrame.
Qed.

Lemma wp_paper_adequacy t e σ' α' G' T' e' Q :
  locs e = ∅ ->
  rtc step (∅,∅,∅,Leaf t,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ⊢ wp ⊤ t e (fun t v => ⌜Q t v⌝)) ->
  (reducible σ' α' G' T' e' ∨ (is_val e' /\ is_leaf T')).
Proof.
  intros ? ? Hwp. eapply wp_adequacy in Hwp; eauto.
  rewrite /adequate in Hwp. destruct (to_val e') eqn:He'; eauto.
  right. apply to_val_Some_inv in He'. naive_solver.
Qed.
