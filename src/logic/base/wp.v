From iris.proofmode Require Import proofmode.

From dislog.logic Require Import wpg wpg_alloc wpg_closure wpg_call wpg_call_prim wpg_load wpg_store wpg_par wpg_cas wpg_length.

From dislog.lang Require Export semantics.
From dislog.lang Require Export notation atomic.
From dislog.logic Require Export interp enc wpg_more all_abef.

(* ------------------------------------------------------------------------ *)
(* This file constructs the [wp] predicate over the [wpg]
   predicate. [wp] features:
   + A single timestamp parameter, rather than a task-tree.
   + A _typed_ post-condition, simplifying the reasoning.
 *)

Global Notation Post A Σ := (timestamp -> A -> iProp Σ).

Section wp.
Context `{interpGS Σ}.

Definition wp `{Enc A} (E:coPset) (t:timestamp) (e:expr) (Q:Post A Σ) : iProp Σ :=
  wpg E (Leaf t) e (fun t v => post (Q t) v).

Lemma wp_eq `{Enc A} E t e (Q:Post A Σ) :
  wp E t e Q = wpg E (Leaf t) e (fun t v => post (Q t) v).
Proof. reflexivity. Qed.

(* ------------------------------------------------------------------------ *)
(* Structural rules *)

Lemma fupd_wp (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  (|={E}=> wp E t e Q) -∗
  wp E t e Q.
Proof. apply fupd_wpg. Qed.

Lemma bupd_wp (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  (|==> wp E t e Q) -∗
  wp E t e Q.
Proof. iIntros "Hwp". iApply fupd_wp. by iMod "Hwp". Qed.

Lemma wp_fupd (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  wp E t e (fun t v => |={E}=> Q t v) -∗
  wp E t e Q.
Proof.
  iIntros. iApply wpg_fupd; iApply (wpg_mono with "[$]").
  iIntros. by iApply post_fupd.
Qed.

Lemma wp_strong_mono (A:Type) (EA:Enc A) E t e (P Q:Post A Σ) :
  wp E t e P -∗
  (∀ t v, P t v ={E}=∗ Q t v) -∗
  wp E t e Q.
Proof.
  iIntros "Hwp HPQ". iApply (wpg_strong_mono with "[$] [-]").
  iIntros. iApply (post_strong_mono _ _ E with "[HPQ][$]"). set_solver.
  iIntros. iApply "HPQ". iFrame.
Qed.

Lemma wp_strong_mono_val (A:Type) (EA:Enc A) E t e (P:Post A Σ) (Q:timestamp -> val -> iProp Σ) :
  wp E t e P -∗
  (∀ t v, P t v ={E}=∗ Q t (enc v)) -∗
  wp E t e Q.
Proof.
  iIntros "Hwp HPQ". iApply (wpg_strong_mono with "[$] [-]").
  iIntros (??). rewrite !post_eq.
  iIntros "[% (->&?)]". iExists _. iSplitR; first easy.
  iApply ("HPQ" with "[$]").
Qed.

Lemma wp_mono (A:Type) (EA:Enc A) E t e (P Q:Post A Σ) :
  wp E t e P -∗
  (∀ t v, P t v -∗ Q t v) -∗
  wp E t e Q.
Proof.
  iIntros "H1 H2".
  iApply (wp_strong_mono with "H1").
  iIntros. by iApply "H2".
Qed.

Lemma wp_mono_val (A:Type) (EA:Enc A) E t e (P:Post A Σ) (Q:timestamp -> val -> iProp Σ)  :
  wp E t e P -∗
  (∀ t v, P t v -∗ Q t (enc v)) -∗
  wp E t e Q.
Proof.
  iIntros "H1 H2".
  iApply (wp_strong_mono_val with "H1").
  iIntros. by iApply "H2".
Qed.

Lemma wp_frame (A:Type) (EA:Enc A) E t e (P:iProp Σ) (Q:Post A Σ) :
  P ∗ wp E t e Q -∗
  wp E t e (fun t' v => P ∗ Q t' v).
Proof.
  iIntros "(?&?)".
  iApply (wp_mono with "[$]").
  iIntros. iFrame.
Qed.

Lemma wp_frame_step (A:Type) (EA:Enc A) P E t e (Q:Post A Σ) :
  ¬ is_val e ->
  ▷ P -∗
  wp E t e Q -∗
  wp E t e (fun t' v => Q t' v ∗ P).
Proof.
  iIntros. iApply (wpg_mono with "[-]").
  iApply (wpg_frame_step P with "[$]"); eauto.
  iIntros (??). rewrite !post_eq. iIntros "([% (?&?)]&?)".
  iExists _. iFrame.
Qed.

Lemma wp_end (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  wp E t e (fun t (v:A) => wp E t (enc v) Q) -∗
  wp E t e Q.
Proof.
  iIntros. iApply wpg_end.
  iApply (wpg_mono with "[$]"). iIntros (??). rewrite post_eq.
  iIntros "[% (->&?)]".
  iFrame.
Qed.

(* ------------------------------------------------------------------------ *)
(* Timestamp-related rules *)

Lemma wp_tempus_fugit (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  wp E t e (fun t' v => t ≼ t' -∗ Q t' v) -∗
  wp E t e Q.
Proof.
  iIntros.
  iApply tempus_fugit.
  iApply (wpg_mono with "[$]").
  iIntros. iApply post_mono. 2:iFrame. iIntros (?) "HQ".
  iApply "HQ".  iFrame "#".
Qed.

Lemma wp_tempus_atomic (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  Atomic e ->
  wp E t e (fun _ v => Q t v) -∗
  wp E t e Q.
Proof.
  iIntros.
  iApply tempusatomic. done.
  iApply (wpg_mono with "[$]").
  by iIntros.
Qed.

Lemma wp_mementopre l (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  l ∈ locs e ->
  (l ◷ t -∗ wp E t e Q) -∗
  wp E t e Q.
Proof. apply mementopre. Qed.

Lemma wp_vmementopre v (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  locs v ⊆ locs e ->
  (v ◷? t -∗ wp E t e Q) -∗
  wp E t e Q.
Proof. apply  vmementopre. Qed.

Lemma wp_mementopost (A:Type) (EA:Enc A) E t e (Q:Post A Σ) :
  wp E t e (fun t' v => (enc v) ◷? t' -∗ Q t' v) -∗
  wp E t e Q.
Proof.
  iIntros. iApply mementopost. iApply (wpg_mono with "[$]").
  iIntros (??). rewrite !post_eq. iIntros "[%(->&HP)] #?".
  iExists _. iSplitR; first easy. by iApply "HP".
Qed.

Lemma wp_val (A:Type) (EA:Enc A) E t v v' (Q:Post A Σ) :
  v = enc v' ->
  Q t v' -∗ wp E t (Val v) Q.
Proof.
  iIntros (->) "HQ". iApply wpg_val.
  iExists _. by iFrame.
Qed.

(* ------------------------------------------------------------------------ *)
(* Syntax-directed rules *)

Lemma wp_if (A:Type) (EA:Enc A) E t (b:bool) (e1 e2:expr) (Q:Post A Σ) :
  wp E t (if b then e1 else e2) Q -∗
  wp E t (If b e1 e2) Q.
Proof. iIntros. iApply  wpg_if. destruct b; iFrame. Qed.

Lemma wp_let_val (A:Type) (EA:Enc A) E t x (v:val) e (Q:Post A Σ) :
  wp E t (subst' x v e) Q -∗
  wp E t (Let x v e) Q.
Proof. apply wpg_let_val. Qed.

Lemma wp_bind (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E t (e:expr) K (Q:Post A Σ) :
  wp E t e (fun t' (v:B) => wp E t' (fill_item K (enc v)) Q) -∗
  wp E t (fill_item K e) Q.
Proof.
  iIntros "Hwp".
  iApply wpg_bind.
  iApply (wpg_mono with "Hwp"). iIntros (??). rewrite post_eq.
  iIntros "[% (->&?)]". iFrame.
Qed.

Lemma wp_binds (A:Type) (EA:Enc A) E t (e:expr) ks (Q:Post A Σ) :
  wp E t e (fun t' (v:val) => wp E t' (fill_items ks v) Q) -∗
  wp E t (fill_items ks e) Q.
Proof.
  iIntros. iApply wpg_binds. iApply (wpg_mono with "[$]").
  iIntros (??). rewrite post_eq. iIntros "[% (->&?)]".
  iApply (wpg_mono with "[$]"). iIntros. iFrame.
Qed.

Lemma wp_par (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E t e1 e2 Q:
  (∀ t1 t2, t ≼ t1 ∗ t ≼ t2 ={E}=∗ ∃ (Q1:Post A Σ) (Q2:Post B Σ),
     wp E t1 e1 Q1 ∗
     wp E t2 e2 Q2 ∗
     (∀ (t1 t2:timestamp) (v1:A) (v2:B) t' l,
         l ↦ [enc v1;enc v2] ∗ meta_token l (⊤ ∖ ↑dislog_nm) ∗
         t1 ≼ t' ∗ t2 ≼ t' ∗
         Q1 t1 v1 ∗ Q2 t2 v2 ={E}=∗ (Q t' l)
     )) -∗
     wp E t (Par e1 e2) Q.
Proof.
  iIntros "Hwp".
  iApply wpg_fork. iModIntro. iIntros.
  iApply fupd_wpg. iMod ("Hwp" with "[$]") as "[%Q1 [%Q2 (H1&H2&Hwp)]]".
  iModIntro. iApply wpg_fupd.
  iApply (wpg_mono with "[H1 H2]").
  { iApply (wpg_par_ctx with "H1 H2"). }
  iIntros (??) "[% [% [% [% [% (->&?&?&?&?&E1&E2)]]]]]".
  iApply post_loc.
  rewrite !post_eq. iDestruct "E1" as "[% (->&?)]". iDestruct "E2" as "[% (->&?)]".
  iApply ("Hwp" with "[$]").
Qed.

(* Only for showoff *)
Lemma wp_par_weak E t e1 e2 Q1 Q2 :
  (∀ t1 t2,
      (t ≼ t1 -∗ wpg E (Leaf t1) e1 Q1) ∗
      (t ≼ t2 -∗ wpg E (Leaf t2) e2 Q2)) -∗
  wp E t (Par e1 e2)
  (fun t' (l:loc) => ∃ (t1 t2:timestamp) (v1 v2:val),
       l ↦ [v1;v2] ∗ meta_token l (⊤ ∖ ↑dislog_nm) ∗
       t1 ≼ t' ∗ t2 ≼ t' ∗
       Q1 t1 v1 ∗ Q2 t2 v2).
Proof.
  iIntros "H12". iApply (wpg_mono with "[-]").
  { iApply (wpg_par_weak with "[$]"). }
  iIntros (??) "[% [% [% [% [% (->&?&?&?&?&?&?) ]]]]]".
  iApply post_loc. repeat iExists _. iFrame.
Qed.

Lemma wp_load (A:Type) (EA:Enc A) E t bs q (i:Z) (v:A) (l:loc) :
  (0 <= i < Z.of_nat (length bs))%Z ->
  bs !! (Z.to_nat i) = Some (enc v) ->
  l ↦{q} bs -∗ (enc v) ◷? t -∗
  wp E t (Load l i) (fun _ (w:A) => ⌜w=v⌝ ∗ l ↦{q} bs).
Proof.
  iIntros.
  iApply (wpg_mono with "[-]").
  { iApply (wpg_load with "[$]"); eauto. }
  iIntros (? ?) "(->&?)".
  iExists _. by iFrame.
Qed.

Lemma wp_call (A:Type) (EA:Enc A) E t self args body ts vs v (Q:Post A Σ) :
  length args = length vs →
  ts = Val <$> vs ->
  Func v (Lam self args body) -∗
  ▷ (wp E t (substs' (zip (self::args) (v::vs)) body) Q) -∗
  wp E t (Call v ts) Q.
Proof. apply wpg_call. Qed.

Lemma wp_call_prim (A:Type) (EA:Enc A) E t (p:prim) v1 v2 (v:A) :
  eval_call_prim p v1 v2 = Some (enc v) ->
  ⊢ wp E t (CallPrim p v1 v2) (fun _ (v':A) => ⌜v'=v⌝)%I.
Proof.
  iIntros. iApply wpg_mono. iApply wpg_call_prim; eauto.
  iIntros (??) "->". iExists _. eauto.
Qed.

Lemma wp_alloc E t (i:Z) (v:val) :
  (0 <= i)%Z ->
  ⊢ wp E t (Alloc i v)
      (fun _ (l:loc) => l ↦ (replicate (Z.to_nat i) v) ∗ meta_token l (⊤ ∖ ↑dislog_nm)).
Proof.
  iIntros.
  iApply wp_mono. iApply wpg_alloc. eauto.
  iIntros (??) "(?&?)"; subst. by iFrame.
Qed.

Lemma wp_closure E t clo :
  ⊢ wp E t (Clo clo)
      (fun _(l:loc) =>  Func l clo ∗ meta_token l (⊤ ∖ ↑dislog_nm)).
Proof.
  iIntros.
  iApply wpg_mono.
  iApply wpg_closure.
  iIntros (??) "[%l (%&?&?)]"; subst.
  iExists l. by iFrame.
Qed.

Lemma wp_store E t bs (l:loc) (i:Z) (v:val) :
  (0 <= i < (Z.of_nat (length bs)))%Z ->
  l ↦ bs -∗
  wp E t (Store l i v) (fun _ (_:unit) => l ↦ (<[Z.to_nat i:=v]> bs)).
Proof.
  iIntros. iApply (wpg_mono with "[-]"). iApply wpg_store; eauto.
  iIntros (??) "(%&?)". subst. iExists tt. by iFrame.
Qed.

Lemma wp_length E t bs (l:loc) p :
  l ↦{p} bs -∗
  wp E t (Length l) (fun _ (i:Z) => ⌜i = length bs⌝ ∗ l ↦{p} bs).
Proof.
  iIntros. iApply (wpg_mono with "[-]"). iApply wpg_length; eauto.
  iIntros (??) "(->&?)". iExists _. by iFrame.
Qed.

Lemma wp_cas E t (l:loc) (i:Z) (v1 v1' v2:val) bs q :
  (v1=v1' -> q=DfracOwn 1%Qp) ->
  (0 ≤ i < length bs)%Z ->
  bs !! Z.to_nat i = Some v1' ->
  l ↦{q} bs ∗ v1' ◷? t -∗
  wp E t (CAS l i v1 v2)
    (fun _ (b:bool) => ⌜b=bool_decide (v1=v1')⌝ ∗
       l ↦{q} (if decide (v1=v1') then <[Z.to_nat i:=v2]> bs else bs)).
Proof.
  iIntros.
  iApply (wpg_mono with "[-]").
  iApply wpg_cas; eauto.
  iIntros (? ?) "(->&?)". iApply post_bool. eauto.
Qed.

Lemma wp_atomic (A:Type) (EA:Enc A) E1 E2 t e (Q:Post A Σ) :
  Atomic e ->
  (|={E1,E2}=> wp E2 t e (fun t' v => |={E2,E1}=> Q t' v )) ⊢ wp E1 t e Q.
Proof.
  iIntros.
  iApply wpg_atomic; eauto.
  iApply (fupd_mono with "[$]").
  iIntros. iApply (wpg_mono with "[$]"). iIntros.
  by iApply post_fupd.
Qed.
End wp.

(* ------------------------------------------------------------------------ *)
(* Proofmode instances *)

Global Instance is_except_0_wp `{!interpGS Σ} A (EA:Enc A) E t e (Q:Post A Σ) :
  IsExcept0 (wp E t e Q).
Proof.
  rewrite /IsExcept0. iIntros. iApply fupd_wp.
  rewrite -except_0_fupd -fupd_intro //.
Qed.

Global Instance elim_modal_bupd_wp `{!interpGS Σ} (A:Type) (EA:Enc A) E t e p P (Q:Post A Σ) :
  ElimModal True p false (|==> P) P (wp E t e Q) (wp E t e Q).
Proof.
  rewrite /ElimModal bi.intuitionistically_if_elim
    (bupd_fupd E) fupd_frame_r bi.wand_elim_r.
  iIntros. by iApply fupd_wp.
Qed.

Global Instance elim_modal_fupd_wp `{!interpGS Σ} (A:Type) (EA:Enc A) E t e p P (Q:Post A Σ) :
  ElimModal True p false
    (|={E}=> P) P
    (wp E t e Q) (wp E t e Q)%I.
Proof.
  rewrite /ElimModal bi.intuitionistically_if_elim fupd_frame_r bi.wand_elim_r.
  iIntros. by iApply fupd_wp.
Qed.

Global Instance elim_modal_fupd_wp_atomic `{!interpGS Σ} (A:Type) (EA:Enc A) E1 E2 t e p P (Q:Post A Σ) :
  ElimModal (Atomic e) p false
    (|={E1,E2}=> P) P
    (wp E1 t e Q) (wp E2 t e (fun t' v => |={E2,E1}=> Q t' v))%I | 100.
Proof.
  intros ?.
  rewrite bi.intuitionistically_if_elim
    fupd_frame_r bi.wand_elim_r.
  iIntros. iApply (wp_atomic with "[$]"). eauto.
Qed.

Global Instance add_modal_fupd_wp `{!interpGS Σ} (A:Type) (EA:Enc A) E t e P (Q:Post A Σ) :
  AddModal (|={E}=> P) P (wp E t e Q).
Proof. rewrite /AddModal fupd_frame_r bi.wand_elim_r. iIntros. by iApply fupd_wp. Qed.

Global Instance elim_acc_wp_atomic `{!interpGS Σ} {X} (A:Type) (EA:Enc A) E1 E2 t α clock γ e (Q:Post A Σ) :
  ElimAcc (X:=X) (Atomic e)
    (fupd E1 E2) (fupd E2 E1)
    α clock γ (wp E1 t e Q)
    (λ x, wp E2 t e (fun t' v => |={E2}=> clock x ∗ (γ x -∗? Q t' v)))%I | 100.
Proof.
  iIntros (?) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
  iSpecialize ("Hinner" with "[$]").
  iApply (wp_mono with "[$]").
  iIntros (? v) ">[Hclock HΦ]". iApply "HΦ". by iApply "Hclose".
Qed.

Global Instance elim_acc_wp_nonatomic `{!interpGS Σ} {X} (A:Type) (EA:Enc A) E t α clock γ e (Q:Post A Σ) :
  ElimAcc (X:=X) True (fupd E E) (fupd E E)
    α clock γ (wp E t e Q)
    (λ x, wp E t e (fun t v => |={E}=> clock x ∗ (γ x -∗? Q t v)))%I .
Proof.
  iIntros (_) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
  iApply wp_fupd.
  iSpecialize ("Hinner" with "[$]").
  iApply (wp_mono with "[$]").
  iIntros (? v) ">[Hclock HΦ]". iApply "HΦ". by iApply "Hclose".
Qed.

Global Opaque Func.
Global Opaque wp.
