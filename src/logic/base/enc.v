From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Export fancy_updates.
From dislog.lang Require Import syntax.

(******************************************************************************)
(* [Enc A], a typeclass for types that can be encoded to values.
   Inspired by Charguéraud's CFML
   https://gitlab.inria.fr/charguer/cfml2/-/blob/master/lib/coq/SepLifted.v *)

Class Enc (A:Type) := enc : A -> val.
Global Instance Enc_unit : Enc unit := fun _ => VUnit.
Global Instance Enc_int : Enc Z := VInt.
Global Instance Enc_loc : Enc loc := VLoc.
Global Instance Enc_bool : Enc bool := VBool.
Global Instance Enc_val : Enc val := id.

Lemma enc_unit (x:unit) :
  enc x = VUnit.
Proof. easy. Qed.
Global Instance inj_enc_unit : @Inj unit val (=) (=) enc.
Proof. intros [] [] ?. easy. Qed.

Lemma enc_int (x:Z) :
  enc x = VInt x.
Proof. reflexivity. Qed.
Global Instance inj_enc_int : @Inj Z val (=) (=) enc.
Proof. intros ? ? E. injection E. easy. Qed.

Lemma enc_loc (x:loc) :
  enc x = VLoc x.
Proof. easy. Qed.
Global Instance inj_enc_loc : @Inj loc val (=) (=) enc.
Proof. intros ? ? E. injection E. easy. Qed.

Lemma enc_bool (x:bool) :
  enc x = VBool x.
Proof. easy. Qed.
Global Instance inj_enc_bool : @Inj bool val (=) (=) enc.
Proof. intros ? ? E. injection E. easy. Qed.

Lemma enc_val (x:val) :
  enc x = x.
Proof. easy. Qed.
Global Instance inj_enc_val : @Inj val val (=) (=) enc.
Proof. intros ? ? ?. easy. Qed.

Ltac rew_enc_step tt :=
  first [ rewrite enc_unit
        | rewrite enc_int
        | rewrite enc_bool
        | rewrite enc_loc
        | rewrite enc_val ].

Ltac rew_enc_core tt :=
  repeat (rew_enc_step tt).

Tactic Notation "rew_enc" :=
  rew_enc_core tt.

(******************************************************************************)
Section Post.
Context {PROP:bi} `{BiAffine PROP}.

Definition post `{Enc A} (Q:A -> PROP) : val -> PROP :=
  fun v => (∃ a, ⌜v = enc a⌝ ∗ Q a)%I.

Lemma post_eq `{Enc A} (Q:A -> PROP)  :
  post Q = fun v => (∃ a, ⌜v = enc a⌝ ∗ Q a)%I.
Proof. reflexivity. Qed.

Lemma post_inj `{Enc A} `{Inj A val (=) (=) enc} (Q:A -> PROP) :
  forall v, post Q (enc v) ≡ Q v.
Proof.
  intros v.
  iSplit.
  { iIntros "[% (%E&?)]". rewrite inj_iff in E. subst. easy. }
  { iIntros. iExists _.  eauto. }
Qed.

Lemma post_val (Q:val -> PROP) :
  forall v, post Q v ≡ Q v.
Proof. intros v. rewrite -(enc_val v) post_inj //. Qed.

Lemma post_unit (Q:unit -> PROP) :
  post Q VUnit ≡ Q tt.
Proof. rewrite -(enc_unit tt) post_inj //. Qed.

Lemma post_int (Q:Z -> PROP) :
  forall n, post Q (VInt n) ≡ Q n.
Proof. intros n. rewrite -(enc_int n) post_inj //. Qed.

Lemma post_bool (Q:bool -> PROP) :
  forall b, post Q (VBool b) ≡ Q b.
Proof. intros n. rewrite -(enc_bool _) post_inj //. Qed.

Lemma post_loc (Q:loc -> PROP) :
  forall l, post Q (VLoc l) ≡ Q l.
Proof. intros l. rewrite -(enc_loc l) post_inj //. Qed.

(******************************************************************************)

Lemma post_strong_mono `{BiFUpd PROP} (A:Type) (EA:Enc A) E1 E2 (Q Q':A -> PROP) (v:val) :
  E1 ⊆ E2 ->
  (∀ a, Q a ={E1}=∗ Q' a) -∗
  post Q v ={E2}=∗ post Q' v.
Proof.
  iIntros (?) "Hq [%a [-> Ha]]".
  iSpecialize ("Hq" with "Ha").
  iApply fupd_mask_mono. eauto. iApply (fupd_mono with "[$]").
  iIntros. iExists _. by iFrame.
Qed.

Lemma post_strong_mono' `{BiBUpd PROP} (A:Type) (EA:Enc A) (Q Q':A -> PROP) (v:val) :
  (∀ a, Q a ==∗ Q' a) -∗
  post Q v ==∗ post Q' v.
Proof.
  iIntros "Hq [%a [-> Ha]]".
  iMod ("Hq" with "[$]"). iExists _. by iFrame.
Qed.

Lemma post_mono (A:Type) (EA:Enc A) (Q Q':A -> PROP) (v:val) :
  (∀ a, Q a -∗ Q' a) -∗
  post Q v -∗ post Q' v.
Proof.
  iIntros "Hq [%a [%E ?]]".
  iSpecialize ("Hq" with "[$]"). iExists _. by iFrame.
Qed.

Lemma post_fupd (A:Type) (EA:Enc A) `{BiFUpd PROP} E1 E2 (Q:A -> PROP) (v:val) :
  post (fun v => |={E1,E2}=> Q v) v ={E1,E2}=∗ post Q v.
Proof. iIntros "[% (?&?)]". iExists _. by iFrame. Qed.

End Post.

Global Opaque post.
