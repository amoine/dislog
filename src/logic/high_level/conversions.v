From iris.proofmode Require Import proofmode.
From dislog.logic Require Import wpg wp wpm.

(******************************************************************************)
(* Conversion between [wp] and [wpm]. *)

Section Conversions.
Context `{interpGS Σ}.

Lemma conversion (A:Type) (EA:Enc A) E (e:expr) (P:vProp Σ) (Q:A -> vProp Σ) :
  (P ⊢ wpm E e Q) <-> (∀ t, P t ⊢ wp E t e (fun t' v => (Q v) t')).
Proof.
  split.
  { intros [Hwp]. iIntros. iDestruct (Hwp with "[$]") as "Hwp".
    rewrite wpm_eq. iApply "Hwp". by iApply prec_refl. }
  { intros Hwp. constructor. iIntros.
    rewrite wpm_eq. iIntros (?) "?".
    iDestruct (monPred_mono with "[$]") as "?".
    by iApply Hwp. }
Qed.

Lemma wp_wpm (A:Type) (EA:Enc A) E (e:expr) (Q:A -> vProp Σ) :
  ⎡∀ t, wp E t e (fun t v => Q v t)⎤ -∗ wpm E e Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "HP". iIntros (?). iIntros. rewrite monPred_at_embed.
  iApply "HP".
Qed.

Lemma wp_wpm' (A:Type) (EA:Enc A) E (e:expr) (P:vProp Σ) (Q:A -> vProp Σ) :
  P ∗ ⎡∀ t, P t -∗ wp E t e (fun t v => Q v t)⎤ -∗
  wpm E e Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?). rewrite monPred_at_sep. iIntros "(HP&Hwp)".
  rewrite monPred_at_embed. iIntros (?). iIntros.
  iAssert (P j) with "[HP]" as "HP".
  { iApply monPred_mono. by iFrame. }
  by iApply "Hwp".
Qed.

(* This variant is to be used in proofs. *)
Lemma wp_wpm_whole_world (A:Type) (EA:Enc A) E (e:expr) (P:vProp Σ) (Q:A -> vProp Σ) :
  (∀ t, P t -∗ wp E t e (fun t v => Q v t)) ->
  P -∗ wpm E e Q.
Proof.
  intros Hwp. iIntros. iApply (wp_wpm' _ _ _ _ P). iFrame.
  iStopProof. apply embed_emp_valid. iIntros. by iApply Hwp.
Qed.

Lemma wpm_wp (A:Type) (EA:Enc A) E t (e:expr) (Q:A -> vProp Σ) :
  wpm E e Q t ⊢ wp E t e (fun t' v => Q v t').
Proof. iIntros "Hwp". iApply "Hwp". by iApply prec_refl. Qed.

(* This variant is to be used in proofs. *)
Lemma wpm_wp_whole_world (A:Type) (EA:Enc A) E (e:expr) (P:vProp Σ) (Q:A -> vProp Σ) :
  (P -∗ wpm E e Q) ->
  (∀ t, P t -∗ wp E t e (fun t v => Q v t)).
Proof.
  intros [Hwp]. intros. apply bi.entails_wand.
  specialize (Hwp t). rewrite monPred_at_wand in Hwp.
  iIntros. iDestruct (Hwp with "[][][$]") as "?". rewrite monPred_at_emp //.
  by iApply prec_refl.
  by iApply wpm_wp.
Qed.

End Conversions.
