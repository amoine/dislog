From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import cancelable_invariants.
From iris.algebra Require Import gset gmultiset frac_auth numbers agree.

From dislog.utils Require Import more_iris bool_cmra.

(******************************************************************************)
(* [wonly]: an abstract predicate, allowing for multiple write on the same
   resource, leveraging Iris cancellable invariants. *)

Class wonlyG (V:Type) `{Countable V} Σ :=
  { wonly_cinvG : cinvG Σ;
    wonly_gsetG : inG Σ (frac_authUR (gset_disjUR V));
    wonly_numG : inG Σ (authUR bool_andUR);
    wonly_origG : inG Σ (agreeR (leibnizO V))
  }.

Local Existing Instance wonly_cinvG.
Local Existing Instance wonly_gsetG.
Local Existing Instance wonly_numG.
Local Existing Instance wonly_origG.

Definition wonlyΣ (V:Type) `{Countable V} : gFunctors := #[cinvΣ ; GFunctor (frac_authUR (gset_disjUR V)); GFunctor (authUR bool_andUR); GFunctor (agreeR (leibnizO V)) ].

Global Instance subG_wonlyΣ (V:Type) `{Countable V}  {Σ} : subG (wonlyΣ V) Σ → wonlyG V Σ.
Proof. solve_inG. Qed.

Record wonly_info := mkwo
  { a : gname; (* for the invariant *)
    b : gname; (* for the multiset *)
    c : gname; (* for the num of elems *)
    d : gname; (* for the original elem *)
  }.

Section Wonly.
Context `{Countable V}.
Context `{invGS_gen hlc Σ, !wonlyG V Σ}.
Context (N:namespace).
Context (Φ:V -> iProp Σ).

Definition inv_wonly_internal (γ1:gname) (γ2:gname) (γ3:gname) : iProp Σ :=
  ∃ (A:gset V), own γ1 (●F (GSet A)) ∗ own γ2 (● (bool_decide (A=∅))) ∗ ∃ v, Φ v ∗ if decide (A=∅) then @own _ _ wonly_origG γ3 (to_agree v) else ⌜v ∈ A⌝.

Local Instance inv_wonly_internal_timeless γ1 γ2 γ3  :
  (forall v, Timeless (Φ v)) ->
  Timeless (inv_wonly_internal γ1 γ2 γ3 ).
Proof. intros ?.  apply bi.exist_timeless. intros. case_decide; apply _. Qed.

Definition inv_wonly γ : iProp Σ :=
  cinv N γ.(a) (inv_wonly_internal γ.(b) γ.(c) γ.(d)).

Definition wonly_orig γ v0 := @own _ _ wonly_origG γ.(d) (to_agree v0).

Definition wonly γ q vs : iProp Σ :=
  inv_wonly γ  ∗ cinv_own γ.(a) q ∗ ∃ xs, ⌜xs ⊆ vs⌝ ∗ own γ.(c) (◯ (bool_decide (vs=∅))) ∗ own γ.(b) (◯F{q} (GSet xs)).

Lemma wonly_alloc E v :
  Φ v ={E}=∗ ∃ γ, wonly_orig γ v ∗ wonly γ 1 ∅.
Proof.
  iIntros "Hv".
  iMod (own_alloc (●F (GSet ∅) ⋅ ◯F (GSet ∅))) as "[%b (Hb&?)]".
  { now apply frac_auth_valid. }
  iMod (own_alloc (● true ⋅ ◯ true)) as "[%c (Hc&?)]".
  { now apply auth_both_valid. }
  iMod (@own_alloc _ _ wonly_origG (to_agree v)) as "[%d Hd]".
  { done. }
  rewrite (@core_id_dup (agreeR (leibnizO V)) (to_agree v)).
  iDestruct "Hd" as "(Hd&?)".

  iMod (cinv_alloc _ _ (inv_wonly_internal b c d) with "[Hv Hb Hc Hd]" )%I as "[%a (#?&?)]".
  { iModIntro. iExists _. iFrame. rewrite bool_decide_true; last set_solver.
    iFrame. iExists _. iFrame.  }
  iModIntro. iExists (mkwo a b c d). iFrame "#∗". iExists _. iFrame "# ∗".
  iPureIntro. set_solver.
Qed.

Lemma wonly_cancel_not_empty γ vs :
  vs ≠ ∅ ->
  wonly γ 1 vs ={↑N}=∗ ▷ ∃ v, ⌜v ∈ vs⌝ ∗ Φ v.
Proof.
  iIntros (?) "(HI&?&[% (%&?&?)])".
  iMod (cinv_cancel with "[$][$]") as "HP". set_solver.
  do 2 iModIntro.
  iDestruct "HP" as "[% (Ha&Ha1&[% (?&HX)])]".
  iExists v. iFrame. iDestruct (own_valid_2 with "Ha [$]") as "%Hv".
  apply frac_auth_agree_L in Hv. inversion Hv. subst. clear Hv.
  iDestruct (own_valid_2 with "Ha1 [$]") as "%Hv".
  apply auth_both_valid_discrete in Hv. destruct Hv as (Hv&_).
  case_decide; eauto. subst. rewrite bool_decide_false in Hv; try easy.
  case_bool_decide; eauto.
  { exfalso. subst. apply bool_and_included in Hv; try easy. }
  rewrite decide_False //. iDestruct "HX" as "%". iPureIntro. set_solver.
Qed.

Lemma wonly_cancel_empty γ v :
  wonly_orig γ v ∗ wonly γ 1 ∅ ={↑N}=∗ ▷ Φ v.
Proof.
  iIntros  "(Ho&(HI&?&[% (%&?&?)]))".
  iMod (cinv_cancel with "[$][$]") as "HP". set_solver.
  do 2 iModIntro.
  iDestruct "HP" as "[% (Ha&Ha1&[%(?&HX)])]".
  assert (xs = ∅) by set_solver. subst.
  iDestruct (own_valid_2 with "Ha [$]") as "%Hv".
  apply frac_auth_agree_L in Hv. inversion Hv. subst.
  case_decide; last congruence.
  iDestruct (own_valid_2 with "Ho [$]") as "%Ho".
  rewrite to_agree_op_valid_L in Ho. subst. iFrame.
Qed.

Lemma bool_decide_empty_op `{Countable K} (vs1 vs2:gset K) :
  bool_decide (vs1 ⋅ vs2 = ∅) = bool_decide (vs1 = ∅) && bool_decide (vs2 = ∅).
Proof.
  rewrite gset_op. repeat case_bool_decide; set_solver.
Qed.

Lemma wonly_join γ q1 q2 vs1 vs2 :
  wonly γ q1 vs1 -∗ wonly γ q2 vs2 -∗ wonly γ (q1 + q2) (vs1 ∪ vs2).
Proof.
  iIntros "(?&?&[%a1 (%&Hn1&Ha1)]) (?&?&[%a2 (%&Hn2&Ha2)])". iFrame.
  rewrite -gset_op fractional.fractional. iFrame.
  iDestruct (own_valid_2 with "Ha1 Ha2") as "%Hv".
  rewrite -frac_auth_frag_op frac_auth_frag_valid in Hv. destruct Hv as (?&Hv).
  apply gset_disj_valid_op in Hv.
  iExists (a1 ∪ a2).
  iSplitR; last first.
  iSplitR "Ha1 Ha2".
  { iCombine "Hn1 Hn2" as "?". rewrite bool_decide_empty_op //. }
  { rewrite -gset_disj_union // frac_auth_frag_op own_op. iFrame. }
  iPureIntro. set_solver.
Qed.

Lemma wonly_split γ q1 q2 vs1 vs2 :
  wonly γ (q1 + q2) (vs1 ∪ vs2) -∗ (wonly γ q1 vs1 ∗ wonly γ q2 vs2)%I.
Proof.
  rewrite /wonly -gset_op fractional.fractional.
  iIntros "(#?&(?&?)&[%xs (%Hxs&HE&Hxs)])". rewrite gset_op in Hxs.
  iFrame "#∗".
  rewrite bool_decide_empty_op. iDestruct "HE" as "(HE1&?)". iFrame.
  rewrite (union_difference_L (vs1 ∩ xs) xs); last set_solver.
  rewrite -gset_disj_union; last set_solver.
  rewrite frac_auth_frag_op own_op. iDestruct "Hxs" as "(H1&?)".
  iSplitL "H1"; iExists _; iFrame; iPureIntro; try set_solver.
Qed.

Local Lemma overwrite `{Countable K} v (A xs: gset K) :
  xs ⊆ A ->
  ∃ A' B', v ∈ A' /\ B' ⊆ {[v]} /\ (GSet A, GSet xs) ~l~> (GSet A', GSet B').
Proof.
  intros.
  destruct_decide (decide (v ∈ (A ∖ xs))).
  { exists (A ∖ xs), ∅. split_and !.
    1,2:set_solver. apply gset_disj_dealloc_local_update. }
  { exists ({[v]} ∪ (A∖ xs)), {[v]}. split_and !.
      1,2:set_solver.
      etrans. apply gset_disj_dealloc_local_update.
      apply gset_disj_alloc_empty_local_update. set_solver. }
Qed.

(* We can always "borrow" (ie. open an invariant), to get the current value,
   and overwrite the historic *)
Lemma wonly_borrow E γ q vs :
  ↑N ⊆ E →
  wonly γ q vs ={E,E∖↑N}=∗ (▷ ∃ v, Φ v) ∗ (∀ v, Φ v ={E∖↑N,E}=∗ wonly γ q {[v]}).
Proof.
  iIntros (?) "(#HI&?&[%xs (%&Hxs1&Hxs2)])".
  iMod (cinv_acc  with "[$][$]") as "([%(>Ha&>Hv1&Hv)]&?&Hback)". eauto.
  iModIntro. iFrame.
  iSplitL "Hv".
  { iModIntro.
    iDestruct "Hv" as "[% (?&?)]".
    iExists _. iFrame. }
  { iIntros (v) "Hv".
    iDestruct (own_valid_2 with "Ha [$]") as "%Hv".
    apply frac_auth_included_total in Hv. apply gset_disj_included in Hv.
    apply (overwrite v) in Hv. destruct Hv as (A'&B'&(?&?&Hl)).
    iMod (own_update_2 with "Ha [$]") as "(Ha&Hf)".
    { apply frac_auth_update. apply Hl. }
    iMod (own_update_2 with "Hv1 [$]") as "(?&Hf1)".
    { apply auth_update. apply bool_and_local_update. }

    iMod ("Hback" with "[-Hf Hf1]") as "_".
    { iModIntro. iExists _. iFrame. case_bool_decide; first set_solver.
      iFrame. iExists _.  rewrite decide_False //. iFrame. iPureIntro. set_solver. }
    iModIntro. iFrame "#".  iExists _. iFrame. eauto. }
Qed.

End Wonly.

From dislog.logic Require Import wpm conversions.

(******************************************************************************)
(* We instantiate [wonly] for mapsto in vProp.
   We focus on pointsto of size 1 (ie. plain references). *)

Section wonlyplus.
Context `{interpGS Σ, !wonlyG val Σ}.
Context (N:namespace).

Definition mapsto_wos (γ:wonly_info) (l:loc) (q:Qp) (X:gset val) : iProp Σ :=
  wonly N (fun (v:val) => gen_heap.mapsto l (DfracOwn 1) (SBlock [v]))%I γ q X.

Definition mapsto_wo (γ:wonly_info) (l:loc) (q:Qp) (X:gset val) : vProp Σ :=
  ⎡mapsto_wos γ l q X⎤ ∗ [∗ set] vs ∈ X, vclockm vs.

Local Notation "l ⟾{ γ , qp } X" := (mapsto_wo γ l qp X)
  (at level 20, format "l  ⟾{ γ , qp }  X") : bi_scope.
Local Notation "l ⟾{ γ } X" := (mapsto_wo γ l 1 X)
  (at level 20, format "l  ⟾{ γ }  X") : bi_scope.

Definition wo_orig γ v : vProp Σ :=
  embed (wonly_orig γ v) ∗ vclockm v.

Lemma mapsto_wo_frac γ l q1 q2 X1 X2 :
  l ⟾{γ,(q1+q2)} (X1 ∪ X2) ⊣⊢ l ⟾{γ,q1} X1 ∗ l ⟾{γ,q2} X2.
Proof.
  iSplit.
  { iIntros "(?&#?)".
    iDestruct (wonly_split with "[$]") as "(?&?)". iFrame.
    iSplit; iApply (big_sepS_subseteq with "[$]"); set_solver. }
  { iIntros "((Ha&?)&(?&?))".
    iDestruct (wonly_join with "Ha [$]") as "?". iFrame.
    iApply big_sepS_union_persistent. iFrame. }
Qed.

Global Instance fracsto_mapsto_wo_fractional γ l X :
  fractional.Fractional (λ q : Qp, l ⟾{γ,q} X)%I.
Proof. intros ??. rewrite -mapsto_wo_frac idemp_L //. Qed.

Lemma mapsto_wo_start E l v :
  l ↦ [v] ={E}=∗ ∃ γ, wo_orig γ v ∗ l ⟾{γ} ∅.
Proof.
  iIntros "(H1&H2)". rewrite big_sepL_singleton. iFrame.
  iMod (wonly_alloc with "[H1]") as "[% (?&?)]".
  2:{ iExists _. iFrame. by iApply big_sepS_empty.  }
  by iFrame.
Qed.

Lemma mapsto_wo_end E γ l X :
  ↑N ⊆ E ->
  X ≠ ∅ ->
  l ⟾{γ} X ={E}=∗ ∃v, ⌜v ∈ X⌝ ∗ l ↦ [v].
Proof.
  iIntros (??) "(?&#?)". iMod (fupd_mask_subseteq _) as "Hclose".
  2:iMod (wonly_cancel_not_empty with "[$]") as ">[%v' (%Hv'&?)]".
  1,2:eauto.
  iMod "Hclose". iModIntro. iExists _. iFrame "%∗". iApply big_sepL_singleton.
  iDestruct (big_sepS_elem_of_acc with "[$]") as "(?&_)". eauto. iFrame.
Qed.

Lemma mapsto_wo_cancel E γ l v :
  ↑N ⊆ E ->
  wo_orig γ v ∗ l ⟾{γ} ∅ ={E}=∗ l ↦ [v].
Proof.
  iIntros (?) "((?&#?)&?&_)".
  iMod (fupd_mask_subseteq _) as "Hclose".
  2:iMod (wonly_cancel_empty with "[$]") as ">?". eauto.
  iMod "Hclose". iModIntro.
  iFrame. by iApply big_sepL_singleton.
Qed.

Lemma wpm_store_wo E γ (l:loc) q (v:val) X:
  ↑N ⊆ E ->
  l ⟾{γ,q} X  -∗
  wpm E (Store l 0 v)
    (fun (_:unit) => l ⟾{γ,q} {[v]}).
Proof.
  intros. apply wp_wpm_whole_world. iIntros (?) "Hm".
  rewrite /mapsto_wo !monPred_at_sep monPred_at_embed monPred_at_big_sepS.
  iDestruct "Hm" as "(?&#HX)".
  iApply (wp.wp_vmementopre v). set_solver. iIntros.
  iMod (wonly_borrow with "[$]") as "(>[%v' Hl]&Hback)". eauto. constructor.
  iApply wp.wp_tempus_atomic. constructor.
  iApply (wp.wp_mono with "[Hl]").
  { iApply (wp.wp_store with "[$]"). simpl. lia. }
  iIntros (? _) "?".
  simpl.
  iMod ("Hback" with "[$]").
  iModIntro. rewrite /mapsto_wo !monPred_at_sep monPred_at_embed monPred_at_big_sepS.
  iFrame. iApply big_sepS_singleton. iFrame "#".
  destruct v; simpl; rewrite ?monPred_at_emp //.
Qed.

End wonlyplus.

Global Notation "l ⟾{ N , γ , qp } X" := (mapsto_wo N γ l qp X)
  (at level 20, format "l  ⟾{ N , γ , qp }  X") : bi_scope.
Global Notation "l ⟾{ N , γ } X" := (mapsto_wo N γ l 1 X)
  (at level 20, format "l  ⟾{ N , γ }  X") : bi_scope.

(* We export fancy notations for the top mask. *)
Global Notation "l ⟾{ γ , qp } X" := (mapsto_wo nroot γ l qp X)
  (at level 20, format "l  ⟾{ γ , qp }  X") : bi_scope.
Global Notation "l ⟾{ γ } X" := (mapsto_wo nroot γ l 1 X)
  (at level 20, format "l  ⟾{ γ }  X") : bi_scope.

Global Opaque wonly.
Global Opaque mapsto_wo.
