From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import gen_heap.
From dislog.logic Require Import wp.

From dislog.lang Require Export semantics.
From dislog.lang Require Export notation atomic.
From dislog.logic Require Export interp enc wpg_more monpred2 all_abef.

(******************************************************************************)
(* Instantiation of monpred2. *)

Ltac monPred_unseal := MonPred.unseal.

Global Instance prec_preorder `{interpGS Σ} : BiPreOrder prec.
Proof. constructor; eauto using prec_refl. iIntros (???) "(?&?)". iApply (prec_trans x y z with "[$][$]"). Qed.

Canonical Structure prec_bi_index Σ `{interpGS Σ} : biIndex (iProp Σ) :=
  @BiIndex (iProp Σ) timestamp _ prec _.

Local Notation R := (bi_index_rel _ _).
Definition mono_alt `{I:biIndex PROP} (f:I -> PROP) :=
  ∀ x y, □(R x y) ⊢ f x -∗ f y.

Program Definition MonPred' `{I:biIndex PROP} (P:I -> PROP) : mono_alt P → monPred _ _ :=
  fun _ => MonPred P _.
Next Obligation. intros P HP ? Hm. iIntros (??) "(?&?)". iApply (Hm with "[$][$]").
Defined.

Definition vProp Σ `{interpGS Σ} := monPred (iProp Σ) (prec_bi_index Σ).
Definition vPropO Σ `{interpGS Σ} := monPredO (iProp Σ) (prec_bi_index Σ).
Definition vPropI Σ `{interpGS Σ} := monPredI (prec_bi_index Σ).

Bind Scope bi_scope with vProp.

(******************************************************************************)
(* Lifted maspto & clocks. *)

Section wpm.
Context `{interpGS Σ}.

Program Definition clockm (l:loc) :=
  MonPred' (clock l) _.
Next Obligation. iIntros. iApply (clock_mon with "[$][$]"). Qed.

Local Notation "l ◷now" := (clockm l)
  (at level 20, format "l  ◷now") : bi_scope.

Global Instance clockm_timeless l : Timeless (l ◷now).
Proof. apply monpred_timeless. apply _. Qed.
Global Instance clockm_persistent l : Persistent (l ◷now).
Proof. apply monPred_persistent. apply _. Qed.

Definition vclockm (v:val) :=
  match v with
  | VLoc l => l ◷now
  | _ => emp end%I.

Local Notation "v ◷?now" := (vclockm v)
  (at level 20, format "v  ◷?now") : bi_scope.

Global Instance vclockm_timeless v : Timeless (v ◷?now).
Proof. destruct v; apply _. Qed.
Global Instance vclockm_persistent v : Persistent (v ◷?now).
Proof. destruct v; apply _. Qed.

(* [mapsto l s vs] asserts both ownership and clocks.  *)
Definition mapsto (l:loc) (q:dfrac) (vs:list val) : vProp Σ :=
  ⎡l ↦{q} vs⎤ ∗ [∗ list] v ∈ vs, v ◷?now.

Lemma mapsto_at l q vs i :
  (mapsto l q vs) i ≡(l ↦{q} vs ∗ all_abef i vs)%I.
Proof.
  rewrite /mapsto. rewrite monPred_at_sep monPred_at_embed monPred_at_big_sepL /vclockm.
  iSplit; iIntros "(?&?)"; iFrame; iApply (big_sepL_impl with "[$]"); iModIntro; iIntros.
  { destruct x; done. }
  { destruct x; try done; rewrite monPred_at_emp //. }
Qed.

Local Notation "l ↦{ dq } v" := (mapsto l dq v)
  (at level 20, format "l  ↦{ dq }  v") : bi_scope.
Local Notation "l ↦{# dq } v" := (mapsto l (DfracOwn dq) v)
  (at level 20, format "l  ↦{# dq }  v") : bi_scope.
Local Notation "l ↦ v" := (mapsto l (DfracOwn 1) v)
  (at level 20, format "l  ↦  v") : bi_scope.

Global Instance mapsto_timeless l q vs : Timeless (l ↦{q} vs).
Proof. apply monpred_timeless. intros. rewrite mapsto_at. apply _. Qed.

Definition Func (v:val) (func:function) : vProp Σ :=
  ⎡Func v func⎤.

Lemma Func_eq v clo :
  Func v clo = ⎡interp.Func v clo⎤%I.
Proof. reflexivity. Qed.

Global Instance func_persistent v clo : Persistent (Func v clo).
Proof. apply _. Qed.
Global Instance func_timeless v clo : Timeless (Func v clo).
Proof. apply monpred_timeless. intros. rewrite /Func monPred_at_embed. apply _. Qed.

Lemma post_at `{EA:Enc A} (Q:A -> vProp Σ) (v:val) i :
  post Q v i ⊣⊢ ∃ a, ⌜v = enc a⌝ ∗ Q a i.
Proof.
  rewrite post_eq. monPred_unseal. repeat f_equiv.
Qed.

(******************************************************************************)
(* [wpm] itself. *)

Definition wpm `{EA:Enc A} E e (Q:A -> vProp Σ) : vProp Σ :=
  monPred_upclosed (fun t => wp E t e (fun t' v => Q v t'))%I.

Lemma wpm_eq `{EA:Enc A} E e (Q:A -> vProp Σ) :
  wpm E e Q = monPred_upclosed (fun t => wp E t e (fun t' v => Q v t'))%I.
Proof. reflexivity. Qed.

Lemma fupd_wpm (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  (|={E}=> wpm E e Q) ⊢
  wpm E e Q.
Proof.
  constructor. iIntros (?) "Hwp". rewrite monPred_at_fupd.
  iIntros (?). iIntros. iApply fupd_wp. by iApply "Hwp".
Qed.

Lemma bupd_wpm (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  (|==> wpm E e Q) ⊢
  wpm E e Q.
Proof.
  constructor. iIntros (?) "Hwp". rewrite monPred_at_bupd.
  iIntros (?). iIntros. iApply bupd_wp. by iApply "Hwp".
Qed.

Lemma wpm_end (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  wpm E e (fun v => wpm E (enc v) Q) ⊢
  wpm E e Q.
Proof.
  constructor. iIntros (?) "Hwp".
  iIntros (?). iIntros. iApply wp_end.
  iSpecialize ("Hwp" with "[$]"). iApply (wp_mono with "[$]").
  iIntros (??) "Hwp". iSpecialize ("Hwp" with "[]"). by iApply prec_refl. iFrame.
Qed.

Lemma wpm_mono (A:Type) (EA:Enc A) E e (P Q:A -> vProp Σ) :
  wpm E e P -∗
  (∀ v, P v -∗ Q v) -∗
  wpm E e Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". monPred_unseal. iIntros (y) "#? Hm". simpl. iIntros.
  iApply wp_tempus_fugit.
  iApply (wp_mono with "[-Hm]").
  { iApply "Hwp". iModIntro. iApply (prec_trans _ y with "[$][$]"). }
  { iIntros (??). iIntros. iApply "Hm".
    { simpl. iModIntro. iApply (prec_trans _ j with "[$][$]").  }
    iFrame. }
Qed.

Lemma wpm_mono_val (A:Type) (EA:Enc A) E e (P:A -> vProp Σ) (Q:val -> vProp Σ) :
  wpm E e P -∗
  (∀ v, P v -∗ Q (enc v)) -∗
  wpm E e Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". monPred_unseal. iIntros (y) "#? Hm". simpl. iIntros.
  iApply wp_tempus_fugit.
  iApply (wp_mono_val with "[-Hm]").
  { iApply "Hwp". iModIntro. iApply (prec_trans _ y with "[$][$]"). }
  { iIntros (??). iIntros. iApply "Hm".
    { simpl. iModIntro. iApply (prec_trans _ j with "[$][$]").  }
    iFrame. }
Qed.

Lemma wpm_frame_step (A:Type) (EA:Enc A) P E e (Q:A -> vProp Σ) :
  ¬ is_val e ->
  ▷ P -∗
  wpm E e (fun v => P -∗ Q v) -∗
  wpm E e Q.
Proof.
  intros. rewrite bi.wand_curry. apply bi.entails_wand.
  constructor. iIntros (?). monPred_unseal. iIntros "(?&Hwp)".
  iIntros. iSpecialize ("Hwp" with "[$]").
  iApply wp_tempus_fugit.
  iApply (wp_mono with "[-]").
  by iApply (wp_frame_step with "[$][$]").
  iIntros (??) "(HP&?) #?".
  iApply "HP". iApply prec_refl. iApply monPred_mono. iFrame.
  iModIntro. by iApply (prec_trans i j with "[$][$]").
Qed.

Lemma wpm_val (A:Type) (EA:Enc A) E v v' (Q:A -> vProp Σ) :
  v = enc v' ->
  Q v' -∗
  wpm E (Val v) Q.
Proof.
  intros. apply bi.entails_wand.
  constructor. simpl.  iIntros.
  iApply wp_val. eauto. iApply monPred_mono. by iFrame.
Qed.

Lemma wpm_fupd (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  wpm E e (fun v => |={E}=> Q v) ⊢
  wpm E e Q.
Proof.
  iIntros. iApply wpm_end. iApply (wpm_mono with "[$]").
  iIntros. iApply fupd_wpm. by iApply wpm_val.
Qed.

Lemma wpm_alloc E (i:Z) (v:val) :
  (0 <= i)%Z ->
  ⊢ wpm E (Alloc i v) (fun (l:loc) => l ↦ (replicate (Z.to_nat i) v) ∗ embed (meta_token l (⊤ ∖ ↑dislog_nm))).
Proof.
  intros. constructor. monPred_unseal. iIntros (?) "_". iIntros (?) "#?".
  iApply wp_tempus_atomic. constructor.
  iApply (wp_vmementopre v). set_solver. iIntros.
  iApply wp_mono. iApply wp_alloc. eauto.
  iIntros (??) "(?&?)". iFrame.
  iApply mapsto_at. iFrame.
  by iApply all_abef_replicate.
Qed.

Lemma wpm_load (A:Type) (EA:Enc A) E bs q (i:Z) (v:A) (l:loc) :
  (0 <= i < length bs)%Z ->
  bs !! (Z.to_nat i) = Some (enc v) ->
  l ↦{q} bs  -∗
  wpm E (Load l i) (fun (w:A) => ⌜w=v⌝ ∗ l ↦{q} bs).
Proof.
  intros. apply bi.entails_wand. constructor. monPred_unseal.
  iIntros (?). rewrite mapsto_at.
  iIntros "(?&#?)". iIntros.
  iApply wp_tempus_atomic. constructor.
  iApply (wp_mono with "[-]").
  { iApply (wp_load with "[$]"); eauto. iFrame.
    iApply (vclock_mon with "[-][$]"). iApply (all_abef_extract with "[$]"). eauto. }
  { iIntros (??) "(->&?)".
    iSplitR; first easy. rewrite mapsto_at. iFrame. iApply (all_abef_mon with "[$][$]"). }
Qed.

Lemma wpm_store E bs (l:loc) (i:Z) (v:val) :
  (0 <= i < (length bs))%Z ->
  l ↦ bs -∗
  wpm E (Store l i v) (fun (_:unit) => l ↦ (<[Z.to_nat i:=v]> bs)).
Proof.
  intros. apply bi.entails_wand. constructor. iIntros (?). rewrite mapsto_at.
  iIntros "(?&#?)". simpl. iIntros.
  iApply wp_tempus_atomic. constructor.
  iApply (wp_vmementopre v). set_solver. iIntros.
  iApply (wp_mono with "[-]"). iApply wp_store; eauto.
  iIntros (??) "?". rewrite mapsto_at. iFrame.
  iApply all_abef_insert. lia.
  { iApply (all_abef_mon with "[$][$]"). }
  { iFrame "#". }
Qed.

Lemma wpm_bind (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E (e:expr) K (Q:A -> vProp Σ) :
  wpm E e (fun (v:B) => wpm E (fill_item K (enc v)) Q) -∗
  wpm E (fill_item K e) Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". simpl. iIntros.
  iApply wp_bind.
  iSpecialize ("Hwp" with "[$]").
  iApply (wp_mono with "Hwp"). iIntros (??) "Hwp".
  iApply "Hwp". iApply prec_refl.
Qed.

Lemma wpm_binds (A:Type) (EA:Enc A) E e ks (Q:A -> vProp Σ) :
  wpm E e (fun (v:val) => wpm E (fill_items ks v) Q) -∗
  wpm E (fill_items ks e) Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". simpl. iIntros.
  iApply wp_binds. iSpecialize ("Hwp" with "[$]").
  iApply (wp_mono with "[$]"). iIntros (??) "Hwp".
  iApply "Hwp". iApply prec_refl.
Qed.

Lemma wpm_let_val (A:Type) (EA:Enc A) E x (v:val) e (Q:A -> vProp Σ) :
  wpm E (subst' x v e) Q -∗
  wpm E (Let x v e) Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". iIntros (?). iIntros.
  iApply wp_let_val. by iApply "Hwp".
Qed.

Lemma wpm_if (A:Type) (EA:Enc A) E (b:bool) (e1 e2:expr) (Q:A -> vProp Σ) :
  wpm E (if b then e1 else e2) Q -∗
  wpm E (If b e1 e2) Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?). iIntros "Hwp". simpl. iIntros. iApply wp_if.
  destruct b; by iApply "Hwp".
Qed.

Lemma wpm_par (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E e1 e2 (Q1:A -> vProp Σ) (Q2:B -> vProp Σ) :
  (wpm E e1 Q1) -∗
  (wpm E e2 Q2) -∗
  wpm E (Par e1 e2)
  (fun (l:loc) => ∃ (v1:A) (v2:B), l ↦ [enc v1;enc v2] ∗ Q1 v1 ∗ Q2 v2).
Proof.
  apply bi.entails_wand.
  constructor. intros. monPred_unseal.
  iIntros "H1". iIntros (j0) "#I H2". iIntros.
  iAssert (i ≼ j)%I as "?". { iApply (prec_trans _ j0 with "[$][$]"). }
  iClear "I". iApply wp_tempus_fugit.

  iApply wp_par. iIntros (??) "(#?&#?)". iModIntro.
  iExists (fun t v => vclock v t ∗ (post Q1 v) t)%I.
  iExists (fun t v => vclock v t ∗ (post Q2 v) t)%I.
  iSplitL "H1".
  { iApply wp_mementopost. iApply (wp_mono_val with "[H1]"). iApply "H1".
    { iModIntro. iApply (prec_trans i j t1 with "[$][$]"). }
    { iIntros. iFrame "#". rewrite post_at. iExists _. by iFrame. } }
  iSplitL "H2".
  { iApply wp_mementopost. iApply (wp_mono_val with "[H2]"). iApply "H2".
    { iModIntro. iApply (prec_trans j0 j t2 with "[$][$]"). }
    { iIntros. iFrame "#". rewrite post_at. iExists _. by iFrame. } }
  { simpl. iIntros (??????). iIntros "(?&?&#?&#?&(#?&H1)&#?&H2)".
    rewrite !post_at. iDestruct "H1" as "[%w1 (->&H1)]". iDestruct "H2" as "[%w2 (->&H2)]".
    iModIntro. iIntros.
    iExists w1, w2. rewrite mapsto_at. iFrame.
    iSplitR.
    { iApply big_sepL_cons; simpl. rewrite right_id. iSplit.
      { iApply (vclock_mon (enc w1) with "[$][$]"). }
      { iApply (vclock_mon (enc w2) with "[$][$]"). } }
    iSplitL "H1"; iApply monPred_mono; by iFrame. }
Qed.

Lemma wpm_call (A:Type) (EA:Enc A) E self args body ts vs v (Q:A -> vProp Σ) :
  length args = length vs →
  ts = Val <$> vs ->
  Func v (Lam self args body) ∗
  ▷ (wpm E (substs' (zip (self::args) (v::vs)) body) Q) -∗
  wpm E (Call v ts) Q.
Proof.
  intros. apply bi.entails_wand. constructor. intros. monPred_unseal.
  rewrite /Func monPred_at_embed. iIntros "(?&Hwp)". iIntros.
  iApply (wp_mono with "[-]").
  { iApply (wp_call with "[$]"); eauto.
    iModIntro. iApply "Hwp". eauto. }
  { iIntros. eauto. }
Qed.

Lemma wpm_call_prim (A:Type) (EA:Enc A) E (p:prim) v1 v2 (v:A) :
  eval_call_prim p v1 v2 = Some (enc v) ->
  ⊢ wpm E (CallPrim p v1 v2) (fun (v':A) => ⌜v'=v⌝)%I.
Proof.
  constructor. intros. iIntros "_". monPred_unseal. iIntros.
  iApply wp_tempus_atomic. constructor.
  iApply wp_mono. iApply wp_call_prim; eauto.
  iIntros (??) "->"; subst. easy.
Qed.

Lemma wpm_closure E clo :
  ⊢ wpm E (Clo clo) (fun (l:loc) => Func l clo ∗ embed (meta_token l (⊤ ∖ ↑dislog_nm))).
Proof.
  constructor. intros. iIntros "_". iIntros (?). iIntros. simpl.
  iApply wp_tempus_atomic. constructor.
  iApply (wp_mono with "[-]"). iApply wp_closure.
  iIntros (??) "(?&?)".
  rewrite /Func monPred_at_sep !monPred_at_embed. by iFrame.
Qed.

Lemma wpm_length E bs (l:loc) p :
  l ↦{p} bs -∗
  wpm E (Length l) (fun (i:Z) => ⌜i = length bs⌝ ∗ l ↦{p} bs).
Proof.
  apply bi.entails_wand. constructor. intros. monPred_unseal.
  rewrite mapsto_at. iIntros "(?&#?)". iIntros.
  iApply wp_tempus_atomic. constructor.
  iApply (wp_mono with "[-]"). iApply wp_length. iFrame.
  iIntros (??) "(->&?)". iSplitR; first easy. rewrite mapsto_at.
  iFrame. iApply (all_abef_mon with "[$][$]").
Qed.

Lemma wpm_cas E (l:loc) (i:Z) (v1 v1' v2:val) bs q :
  (v1=v1' -> q=DfracOwn 1%Qp) ->
  (0 ≤ i < length bs)%Z ->
  bs !! Z.to_nat i = Some v1' ->
  l ↦{q} bs -∗
  wpm E (CAS l i v1 v2)
    (fun (b:bool) => ⌜b=bool_decide (v1=v1')⌝ ∗
       l ↦{q} (if decide (v1=v1') then <[Z.to_nat i:=v2]> bs else bs)).
Proof.
  intros. apply bi.entails_wand.
  constructor. intros. monPred_unseal. rewrite mapsto_at. iIntros "(?&#I)". iIntros.
  iApply wp_tempus_atomic. constructor.
  iDestruct (all_abef_mon with "[$][$]") as "#?". iClear "I".
  iApply (wp_vmementopre v2). set_solver. iIntros.
  iApply (wp_mono with "[-]").
  iApply wp_cas; eauto.
  { iFrame. iApply (all_abef_extract with "[$]"). eauto. }
  iIntros (? ?) "(->&?)". rewrite mapsto_at. iSplitR; first easy. iFrame. simpl.
  case_decide ; eauto.
  iApply (all_abef_insert with "[$][$]"). lia.
Qed.

Lemma wpm_mementopre l (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  l ∈ locs e ->
  (l ◷now -∗ wpm E e Q)-∗
  wpm E e Q.
Proof.
  intros. apply bi.entails_wand. constructor.
  intros. monPred_unseal. iIntros "Hwp". iIntros (?) "#?".
  iApply wp_mementopre. done.
  iIntros. iApply "Hwp". 2:by iFrame. by iFrame. by iApply prec_refl.
Qed.

Lemma wpm_vmementopre v (A:Type) (EA:Enc A) E e (Q:A -> vProp Σ) :
  locs v ⊆ locs e ->
  (v ◷?now -∗ wpm E e Q)-∗
  wpm E e Q.
Proof.
  destruct v; simpl; try (iIntros (?) "Hwp"; by iApply "Hwp").
  intros. apply wpm_mementopre. unfold locs, location_val, locs_val in H0. set_solver.
Qed.

Lemma wpm_atomic (A:Type) (EA:Enc A) E1 E2 e (Q:A -> vProp Σ) :
  Atomic e ->
  (|={E1,E2}=> wpm E2 e (fun v => |={E2,E1}=> Q v )) ⊢ wpm E1 e Q.
Proof.
  intros. constructor. iIntros (?) "Hwp". iIntros (?). iIntros. simpl.
  iApply (wp_atomic _ _ E1 E2); eauto. rewrite monPred_at_fupd.
  iMod "Hwp". iModIntro. iSpecialize ("Hwp" $! j with "[$]").
  iApply (wp_mono with "[$]").
  iIntros (??). rewrite monPred_at_fupd. iIntros ">?". by iFrame.
Qed.

End wpm.

Global Notation "l ↦{ dq } v" := (mapsto l dq v)
  (at level 20, format "l  ↦{ dq }  v") : bi_scope.
Global Notation "l ↦{# dq } v" := (mapsto l (DfracOwn dq) v)
  (at level 20, format "l  ↦{# dq }  v") : bi_scope.
Global Notation "l ↦ v" := (mapsto l (DfracOwn 1) v)
  (at level 20, format "l  ↦  v") : bi_scope.

Global Notation "l ◷now" := (clockm l)
  (at level 20, format "l  ◷now") : bi_scope.
Global Notation "v ◷?now" := (vclockm v)
  (at level 20, format "v  ◷?now") : bi_scope.

Global Instance is_except_0_wpm `{!interpGS Σ} A (EA:Enc A) E e (Q:A -> vProp Σ) :
  IsExcept0 (wpm E e Q).
Proof. by rewrite /IsExcept0 -{2}fupd_wpm -except_0_fupd -fupd_intro. Qed.

Global Instance elim_modal_bupd_wpm `{!interpGS Σ} (A:Type) (EA:Enc A) E e p P (Q:A -> vProp Σ) :
  ElimModal True p false (|==> P) P (wpm E e Q) (wpm E e Q).
Proof.
  by rewrite /ElimModal bi.intuitionistically_if_elim
       (bupd_fupd E) fupd_frame_r bi.wand_elim_r fupd_wpm.
Qed.

Global Instance elim_modal_fupd_wpm `{!interpGS Σ} (A:Type) (EA:Enc A) E e p P (Q:A -> vProp Σ) :
  ElimModal True p false
    (|={E}=> P) P
    (wpm E e Q) (wpm E e Q)%I.
Proof.
  rewrite /ElimModal bi.intuitionistically_if_elim.
  rewrite fupd_frame_r bi.wand_elim_r fupd_wpm //.
Qed.

Global Instance elim_modal_fupd_wpm_atomic `{!interpGS Σ} (A:Type) (EA:Enc A) E1 E2 e p P (Q:A -> vProp Σ) :
  ElimModal (Atomic e) p false
    (|={E1,E2}=> P) P
    (wpm E1 e Q) (wpm E2 e (fun v => |={E2,E1}=> Q v))%I | 100.
Proof.
  intros ?.
  rewrite bi.intuitionistically_if_elim
    fupd_frame_r bi.wand_elim_r.
  iIntros. iApply (wpm_atomic with "[$]"). eauto.
Qed.

Global Instance add_modal_fupd_wpm `{!interpGS Σ} (A:Type) (EA:Enc A) E e P (Q:A -> vProp Σ) :
  AddModal (|={E}=> P) P (wpm E e Q).
Proof. by rewrite /AddModal fupd_frame_r bi.wand_elim_r fupd_wpm. Qed.

Global Instance elim_acc_wpm_atomic `{!interpGS Σ} {X} (A:Type) (EA:Enc A) E1 E2 α clock γ e (Q:A -> vProp Σ) :
  ElimAcc (X:=X) (Atomic e)
    (fupd E1 E2) (fupd E2 E1)
    α clock γ (wpm E1 e Q)
    (λ x, wpm E2 e (fun v => |={E2}=> clock x ∗ (γ x -∗? Q v)))%I | 100.
Proof.
  iIntros (?) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
  iSpecialize ("Hinner" with "[$]").
  iApply (wpm_mono with "[$]").
  iIntros (v) ">[Hclock HΦ]". iApply "HΦ". by iApply "Hclose".
Qed.

Global Instance elim_acc_wpm_nonatomic `{!interpGS Σ} {X} (A:Type) (EA:Enc A) E α clock γ e (Q:A -> vProp Σ) :
  ElimAcc (X:=X) True (fupd E E) (fupd E E)
    α clock γ (wpm E e Q)
    (λ x, wpm E e (fun v => |={E}=> clock x ∗ (γ x -∗? Q v)))%I .
Proof.
  iIntros (_) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
  iApply wpm_fupd.
  iSpecialize ("Hinner" with "[$]").
  iApply (wpm_mono with "[$]").
  iIntros (v) ">[Hclock HΦ]". iApply "HΦ". by iApply "Hclose".
Qed.

From iris.base_logic.lib Require Import invariants cancelable_invariants.
From iris.proofmode Require Import classes.


Global Instance into_acc_inv `{interpGS Σ} E N P :
  IntoAcc (PROP := vProp Σ) (X := unit) (embed (inv N P))
    (↑N ⊆ E) True (fupd E (E ∖ ↑N)) (fupd (E ∖ ↑N) E)
    (λ _ : (), (▷ embed P)%I) (λ _ : (), (▷ embed P)%I) (λ _ : (), None).
Proof.
  rewrite /IntoAcc /accessor. iIntros (?) "#Hinv Hown".
  rewrite bi.exist_unit.
  iMod (inv_acc with "[$]") as "E". eauto.
  rewrite !embed_sep !embed_later. iDestruct "E" as "(?&HP)". iFrame.
  iModIntro. iIntros. rewrite -embed_later. by iMod ("HP" with "[$]").
Qed.


Global Instance into_acc_cinv `{interpGS Σ,  cinvG Σ} E N γ P p :
  IntoAcc (PROP := vProp Σ) (X:=unit) (embed (cinv N γ P))
    (↑N ⊆ E) (embed (cinv_own γ p)) (fupd E (E∖↑N)) (fupd (E∖↑N) E)
    (λ _, ▷ embed P ∗ embed (cinv_own γ p))%I (λ _, ▷ embed P)%I (λ _, None)%I.
Proof.
  rewrite /IntoAcc /accessor. iIntros (?) "#Hinv Hown".
  rewrite bi.exist_unit -assoc.
  iMod (cinv_acc with "[$][$]") as "E". eauto.
  rewrite !embed_sep !embed_later. iDestruct "E" as "(?&?&HP)". iFrame.
  iModIntro. iIntros. rewrite -embed_later. by iMod ("HP" with "[$]").
Qed.

Global Opaque wpm.
Global Opaque Func.
