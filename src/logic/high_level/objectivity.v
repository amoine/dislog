From iris.proofmode Require Import proofmode.
From iris.algebra Require Import gmap.

From dislog.lang Require Import semantics.
From dislog.lang Require Import notation atomic.
From dislog.logic Require Import interp enc wpg_more monpred2 all_abef wpm.

Section objectivity.
Context `{interpGS Σ}.

(******************************************************************************)
(* The whole machinery below is not needed if the locations does not
   points-to any location. *)

Lemma mapsto_at_unboxed l q vs :
  Forall (fun x => ¬ is_loc x) vs ->
  (l ↦{q} vs) ⊣⊢ embed (gen_heap.mapsto l q (SBlock vs)).
Proof.
  intros Hf. constructor. iIntros. rewrite mapsto_at monPred_at_embed.
  iSplit.
  { iIntros "(?&?)". iFrame. }
  { iIntros. iFrame. iInduction vs as [|v vs] "IH". by iApply all_abef_nil.
    iApply big_sepL_cons.
    apply Forall_cons in Hf. destruct Hf as (?&?).
    iSpecialize ("IH" with "[%//]"). iFrame "#". destruct v; done. }
Qed.

(******************************************************************************)

Definition safe X : vProp Σ :=
  [∗ set] v ∈ X, v ◷?now.

Lemma big_sepS_list_to_set_pers
  {PROP : bi} `{BiAffine PROP} {A : Type} `{EqDecision A} `{Countable A} (Φ : A → PROP) (l : list A) :
  (forall x, Persistent (Φ x)) ->
  ([∗ set] x ∈ list_to_set l, Φ x) ⊣⊢ ([∗ list] x ∈ l, Φ x).
Proof.
  intros. rewrite !big_sepS_forall big_sepL_forall.
  iSplit; iIntros "E".
  { iIntros (???). iApply "E". iPureIntro.
    apply elem_of_list_to_set. eapply elem_of_list_lookup; eauto. }
  { iIntros (? Hx). apply elem_of_list_to_set,elem_of_list_lookup in Hx. destruct Hx. by iApply "E". }
Qed.

Lemma get_clock l q vs :
  l ↦{q} vs -∗ safe (list_to_set vs).
Proof.
  iIntros "(_&E)". unshelve iApply big_sepS_list_to_set_pers. apply _. done.
Qed.

Lemma objectivize l q vs t :
  embed (safe (list_to_set vs) t) -∗
  l ↦{q} vs -∗
  embed ((l ↦{q} vs) t).
Proof.
  iIntros.
  iDestruct (monPred_in_intro (l ↦{q} vs)%I with "[$]") as "[% (_&X)]".
  iModIntro. rewrite !mapsto_at. iDestruct "X" as "(?&_)". iFrame.
  rewrite monPred_at_big_sepS big_sepS_list_to_set_pers.
  iApply (big_sepL_impl with "[$]"). iModIntro. iIntros.
  by destruct x.
Qed.

Lemma safe_weak X1 X2 t :
  X1 ⊆ X2 ->
  safe X2 t -∗ safe X1 t.
Proof.
  intros. rewrite /safe !monPred_at_big_sepS.
  by iApply big_sepS_subseteq.
Qed.

Lemma split_subj_obj (P:vProp Σ) :
  P ⊣⊢ (∃ t, monPred_in t ∗ embed (P t)).
Proof.
  iSplit; iIntros "H".
  { by iApply monPred_in_intro. }
  { iDestruct "H" as "[%t (?&?)]".
    iApply (monPred_in_elim with "[$][$]"). }
Qed.

Lemma punch_in l q vs :
  l ↦{q} vs -∗ ∃t, monPred_in t ∗ embed ((l ↦{q} vs) t ∗ safe (list_to_set vs) t).
Proof.
  iIntros "Hp".
  iAssert (∃t, monPred_in t ∗ embed (((l ↦{q} vs) ∗ safe (list_to_set vs)) t))%I with "[-]" as "[% (?&?)]".
  { rewrite -split_subj_obj. iDestruct (get_clock with "[$]") as "#?". by iFrame. }
  rewrite monPred_at_sep. iExists _. iFrame.
Qed.

End objectivity.
