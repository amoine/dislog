From iris.proofmode Require Import base proofmode.
From dislog.lang Require Import syntax semantics reducible.
From dislog.logic Require Import wpm wpg wp wpg_adequacy wp_adequacy.

Definition adequate σ α G T e Q :=
  match to_val e with
  | Some v => exists t, T = Leaf t /\ Q v
  | None => reducible σ α G T e end.

Lemma wpm_adequacy t e σ' α' G' T' e' Q :
  locs e = ∅ ->
  rtc step (∅,∅,∅,Leaf t,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ⊢ wpm ⊤ e (fun v => ⌜Q v⌝)) ->
  adequate σ' α' G' T' e' Q.
Proof.
  intros ? ? Hwp.
  eapply wp_adequacy; eauto.
  intros ??. destruct (Hwp _ _) as [Hwp0].
  iApply wp_mono.
  { iApply (Hwp0 t).
    { rewrite monPred_at_emp //. }
    { iApply prec_refl. } }
  { iIntros. rewrite monPred_at_pure //. }
Qed.

Lemma wpm_paper_adequacy t e σ' α' G' T' e' Q :
  locs e = ∅ ->
  rtc step (∅,∅,∅,Leaf t,e) (σ',α',G',T',e') ->
  (∀ `{!interpGS Σ},
      ⊢ wpm ⊤ e (fun v => ⌜Q v⌝)) ->
  (reducible σ' α' G' T' e' ∨ (is_val e' /\ is_leaf T')).
Proof.
  intros ?? Hwp. eapply wpm_adequacy in Hwp; eauto.
  rewrite /adequate in Hwp. destruct (to_val e') eqn:He'; eauto.
  apply to_val_Some_inv in He'. naive_solver.
Qed.
