From stdpp Require Import base sets fin_sets.
From dislog Require Import syntax semantics.

(******************************************************************************)
(* Inversion lemmas for the det_step relation. *)

Ltac not_ctx K H := apply step_no_val in H; elim_ctx_sure.

Lemma invert_step_if σ α G t (b:bool) e1 e2 σ' α' G' T' e' :
  step σ α G (Leaf t) (If b e1 e2) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ G'=G /\ T'=(Leaf t) /\ e'=(if b then e1 else e2).
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_let_val σ α G t x (v:val) e σ' α' G' T' e' :
  step σ α G (Leaf t) (Let x v e) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ G'=G /\ T'=(Leaf t) /\ e'=(subst' x v e).
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_fork σ α G t e1 e2 σ' α' G' T' e' :
  step σ α G (Leaf t) (Par e1 e2) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ e'=Par e1 e2 /\ exists v w, v ∉ vertices G /\ w ∉ vertices G /\ G'=graph_fork G t v w /\ T' = Node (Leaf v) (Leaf w).
Proof.
  intros E. inversion E; subst; elim_ctx.
  inversion H.
  { exfalso. subst. inversion H4. }
  naive_solver.
Qed.

Lemma invert_step_join σ α G T1 T2 (v1 v2:val) σ' α' G' T' e' :
  step σ α G (Node T1 T2) (Par v1 v2) σ' α' G' T' e' ->
  exists t1 t2 w (l:loc),
    l ∉ dom σ /\ w ∉ vertices G /\ T1 = Leaf t1 /\ T2 = Leaf t2 /\  G' = (graph_join G t1 t2 w) /\
    T' = Leaf w /\ e' = l /\ σ' = <[l:=SBlock [v1;v2]]> σ /\ α' = <[l:=w]> α.
Proof.
  inversion 1; subst; last first; elim_ctx.
  { exfalso. eapply step_no_val; eauto. }
  { exfalso. eapply step_no_val; eauto. }
  { inversion H0. naive_solver. }
Qed.

Lemma invert_step_par σ α G T1 e1 T2 e2 σ' α' G' T' e'  :
  ¬ (is_val e1 ∧ is_val e2) ->
  step σ α G (Node T1 T2) (Par e1 e2) σ' α' G' T' e' ->
  (exists T1' e1', T'=(Node T1' T2) /\ e'=Par e1' e2 /\ step σ α G T1 e1 σ' α' G' T1' e1') \/
  (exists T2' e2', T'=(Node T1 T2') /\ e'=Par e1 e2' /\ step σ α G T2 e2 σ' α' G' T2' e2').
Proof.
  inversion 2; subst; elim_ctx; try naive_solver.
  inversion H1; subst. exfalso. naive_solver.
Qed.

Lemma invert_step_fill_item σ α G T K e σ' α' G' T' e'  :
  ¬ is_val e ->
  step σ α G T (fill_item K e) σ' α' G' T' e' ->
  exists e1, e' = fill_item K e1 /\
          step σ α G T e σ' α' G' T' e1.
Proof.
  inversion 2; subst; elim_ctx.
  { exfalso. eapply sched_step_no_ctx; eauto. }
  { apply fill_item_inj in H1; eauto using step_no_val. naive_solver. }
Qed.

Definition init_block (i:Z) (v:val) :=
  SBlock ((replicate (Z.to_nat i) v)).

Lemma invert_step_alloc σ α G t (i:Z) (v:val) σ' α' G' T' e' :
  step σ α G (Leaf t) (Alloc i v) σ' α' G' T' e' ->
  exists l, l ∉ dom σ /\ σ'= <[l:=init_block i v]> σ /\ α' = <[l:=t]> α /\ G'=G /\ T' = Leaf t /\ e' = VLoc l.
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_closure σ α G t c σ' α' G' T' e' :
  step σ α G (Leaf t) (Clo c) σ' α' G' T' e' ->
  exists l, l ∉ dom σ /\ σ'= <[l:=SClo c]> σ /\ α' = <[l:=t]> α /\ G'=G /\ T' = Leaf t /\ e' = VLoc l.
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_load σ α G t (l:loc) (i:Z) σ' α' G' T' e' :
  step σ α G (Leaf t) (Load l i) σ' α' G' T' e' ->
  exists bs (v:val), σ !! l = Some (SBlock bs) /\ bs !! (Z.to_nat i) = Some v /\ σ'=σ /\ α'=α /\ G' = G /\ T'=Leaf t /\ e'=v.
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_call σ α G t ts σ' α' G' T' e' self args body vs:
  ts = Val <$> vs ->
  step σ α G (Leaf t) (Call (VCode (Lam self args body)) ts) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ G'=G /\ T'=(Leaf t) /\ e'=(substs' (zip (self::args) (VCode (Lam self args body)::vs)) body).
Proof.
  intros Hts. inversion 1; subst.
  2:{ exfalso. destruct K; try naive_solver; simpl in *.
      all:inversion H0; subst; eapply step_no_val; eauto using must_be_val. }
  inversion H0; subst. inversion H5. subst.
  apply fmap_inj in H13.
  2:{ intros ? ? E. injection E. easy. }
  naive_solver.
Qed.

Lemma invert_step_call_clo σ α G t (l:loc) ts σ' α' G' T' e' self args body vs:
  σ !! l = Some (SClo (Lam self args body)) ->
  ts = Val <$> vs ->
  step σ α G (Leaf t) (Call l ts) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ G'=G /\ T'=(Leaf t) /\ (forall l', l' ∈ locs body -> abef G α t l') /\ e'=(substs' (zip (self::args) (VLoc l::vs)) body).
Proof.
  intros Hl Hts. inversion 1; subst.
  2:{ exfalso. destruct K; try naive_solver; simpl in *.
      all:inversion H0; subst; eapply step_no_val; eauto using must_be_val. }
  inversion H0; subst. inversion H5. subst.
  apply fmap_inj in H10.
  2:{ intros ? ? E. injection E. easy. }
  naive_solver.
Qed.

Lemma invert_step_call_prim σ α G t σ' α' G' T' e' p (v1 v2:val):
  step σ α G (Leaf t) (CallPrim p v1 v2) σ' α' G' T' e' ->
  σ'=σ /\ α'=α /\ G'=G /\ T'=(Leaf t) /\ exists v, e'=Val v /\ eval_call_prim p v1 v2 = Some v.
Proof.
  inversion 1; subst.
  2:{ exfalso. destruct K; try naive_solver; simpl in *.
      all:inversion H0; subst; eapply step_no_val; eauto using must_be_val. }
  inversion H0; subst. inversion H5. subst. naive_solver.
Qed.

Lemma invert_step_store σ α G t (l:loc) (i:Z) σ' α' G' T' e' (v:val) :
  step σ α G (Leaf t) (Store l i v) σ' α' G' T' e' ->
  exists bs, σ !! l = Some (SBlock bs) /\ σ'=<[l := SBlock (<[Z.to_nat i := v]> bs)]> σ /\ α'=α /\ G' = G /\ T'=Leaf t /\ e'=VUnit.
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_length σ α G t (l:loc)  σ' α' G' T' e' :
  step σ α G (Leaf t) (Length l) σ' α' G' T' e' ->
  exists bs, σ !! l = Some (SBlock bs) /\ σ'=σ /\ α'=α /\ G' = G /\ T'=Leaf t /\ e'=(Z.of_nat (length bs)).
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.

Lemma invert_step_cas  σ α G t (l:loc) (i:Z) σ' α' G' T' e' (v1 v2:val) :
  step σ α G (Leaf t) (CAS l i v1 v2) σ' α' G' T' e' ->
  α'=α /\ G' = G /\ T'=Leaf t /\ exists bs v1',
      (0 <= i < Z.of_nat (length bs))%Z /\ σ !! l = Some (SBlock bs) /\ bs !! (Z.to_nat i) = Some v1' /\ vabef G α t v1' /\ e' = Val (VBool (bool_decide (v1=v1'))) /\ σ' = (if bool_decide (v1=v1') then (insert l (SBlock (insert (Z.to_nat i) v2 bs)) σ) else σ).
Proof.
  inversion 1; subst; try now not_ctx K H5.
  inversion H0; subst. inversion H5; naive_solver.
Qed.
