From stdpp Require Import base numbers.
From dislog.lang Require Import syntax semantics invert_step.

(* ------------------------------------------------------------------------ *)
(* Atomic expression reduces to a value in one step. *)

Inductive Atomic : expr -> Prop :=
| ALoad : forall (l:loc) (i:Z),
    Atomic (Load l i)
| AStore : forall (l:loc) (i:Z) (v:val),
    Atomic (Store l i v)
| ACAS : forall (l:loc) (i:Z) (v1 v2:val),
    Atomic (CAS l i v1 v2)
| ALength : forall (l:loc),
    Atomic (Length l)
| ACallPrim : forall (p:prim) (v1 v2:val),
    Atomic (CallPrim p v1 v2)
| AClo : forall (c:function),
    Atomic (Clo c)
| AAlloc : forall (i:Z) (v:val),
    Atomic (Alloc i v)
.

Lemma Atomic_correct σ α G t e σ' α' G' T' e' :
  Atomic e ->
  step σ α G (Leaf t) e σ' α' G' T' e' ->
  T' = Leaf t /\ exists v, e' = Val v.
Proof.
  inversion_clear 1; intros Hs.
  { apply invert_step_load in Hs. naive_solver. }
  { apply invert_step_store in Hs. naive_solver. }
  { apply invert_step_cas in Hs. naive_solver. }
  { apply invert_step_length in Hs. naive_solver. }
  { apply invert_step_call_prim in Hs. naive_solver. }
  { apply invert_step_closure in Hs. naive_solver. }
  { apply invert_step_alloc in Hs. naive_solver. }
Qed.

Lemma Atomic_no_val e :
  Atomic e ->
  to_val e = None.
Proof. by inversion 1. Qed.
