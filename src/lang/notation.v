From stdpp Require Import base.
From stdpp Require Export binders.
From dislog.lang Require Export syntax syntax_instances.

(* From https://plv.mpi-sws.org/coqdoc/iris/iris.bi.weakestpre.hexprl *)
Declare Scope expr_scope.
Delimit Scope expr_scope with T.
Bind Scope expr_scope with expr.

Declare Scope val_scope.
Delimit Scope val_scope with V.
Bind Scope val_scope with val.

(* ------------------------------------------------------------------------ *)

(* This allows opening the value scope after [BBlock]. *)

Notation SBlock vs :=
  (SBlock (vs%V)).

(* ------------------------------------------------------------------------ *)

(* Notations for values. *)

(* These notations can produce ASTs of type [val] and [expr], and are active
   in the scopes [%V] and [%T]. *)

(* [()] denotes the unit value. *)

Notation "()" :=
  (VUnit)
: val_scope.

(* [#l] denotes the memory location [l], viewed as a value. *)

Notation "# l" :=
  (VLoc l%V%stdpp)
  (at level 8, format "# l")
: val_scope.

(* [^ n] denotes the integer [n], as a value, and an expr. *)

Notation "^ n" :=
  (VInt n)%Z
  (at level 8, format "^ n")
  : val_scope.

(* [λ: xs, t] denotes the non-recursive function whose formal parameters are [xs]
   and whose body is [t]. The formal parameters must be expressed as a
   tuple, delimited with double square brackets. *)

Notation "λ: xs , e" :=
  (VCode (Lam BAnon%binder xs%binder e%T))
  (at level 200, xs at level 1, e at level 200,
   format "'[' 'λ:'  xs ,  '/  ' e ']'")
    : val_scope.

Notation "μ: x , xs , e" :=
  (VCode (Lam x%binder xs%binder e%T))
  (at level 200, xs at level 1, e at level 200,
    format "'[' 'μ:'  x ,  xs ,  '/  ' e ']'")
    : val_scope.

Notation "λ: xs , e" :=
  (Clo (Lam BAnon%binder xs%binder e%T))
  (at level 200, xs at level 1, e at level 200,
   format "'[' 'λ:'  xs ,  '/  ' e ']'")
    : expr_scope.

Notation "μ: x , xs , e" :=
  (Clo (Lam x%binder xs%binder e%T))
  (at level 200, xs at level 1, e at level 200,
  format "'[' 'μ:'  x ,  xs ,  '/  ' e ']'") : expr_scope.


(* This allows using tuple notation to construct a list of the formal
   parameters. *)

Notation "[[]]" :=
  (nil)
: binder_scope.

Notation "[[ x ]]" :=
  (cons (BNamed x) nil)
: binder_scope.

Notation "[[ x1 , x2 , .. , xn ]]" :=
  (cons (BNamed x1) (cons (BNamed x2) (.. (cons (BNamed xn) nil) .. )))
: binder_scope.

(* ------------------------------------------------------------------------ *)

(* Sequencing. *)

Notation "'let:' x := e1 'in' e2" := (Let x%binder e1%T e2%T)
  (at level 200, x at level 1, e1, e2 at level 200,
   format "'[' 'let:'  x  :=  '[' e1 ']'  'in'  '/' e2 ']'") : expr_scope.

Notation "e1 ;; e2" :=
  (Let BAnon e1%T e2%T)
  (at level 100, e2 at level 200,
   format "'[' '[hv' '['  e1  ']' ;; ']'  '/' e2 ']'")
: expr_scope.

(* ------------------------------------------------------------------------ *)

(* If. *)

Notation "'if:'  e1  'then'  e2  'else'  e3" :=  (If e1%T e2%T e3%T)
  (at level 200, e1, e2, e3 at level 200) : expr_scope.

(* ------------------------------------------------------------------------ *)

(* Function calls. *)

Coercion Call : expr >-> Funclass.

(* This allows using tuple notation to construct a list of the actual
   parameters. *)

Notation "[[]]" :=
  (nil)
    : expr_scope.

Notation "[[ e ]]" :=
  (cons (e : expr)%V nil)
    : expr_scope.

Notation "[[ e1 , e2 , .. , en ]]" :=
  (cons (e1 : expr)%V (cons (e2 : expr)%V (.. (cons (en : expr)%V nil) .. )))
    : expr_scope.

(* ------------------------------------------------------------------------ *)

Notation "'alloc' n" :=
  (Alloc (n%Z%V))
    (at level 10) : expr_scope.

Notation "src .[ ofs ]" :=
  (Load src%V ofs%Z%V)
    (at level 10, format "src .[ ofs ]") : expr_scope.

Notation "dst .[ ofs ] <- src" :=
  (Store dst%V ofs%Z%V src%T)
    (at level 10, format "dst .[ ofs ]  <-  src") : expr_scope.

(* ------------------------------------------------------------------------ *)

Notation par := Par.
Notation CAS := CAS.

(* ------------------------------------------------------------------------ *)
(* Op *)

Notation "x '+ y" :=
  (CallPrim (PrimIntOp IntAdd) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '- y" :=
  (CallPrim (PrimIntOp IntSub) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '* y" :=
  (CallPrim (PrimIntOp IntMul) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '/ y" :=
  (CallPrim (PrimIntOp IntDiv) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x 'mod y" :=
  (CallPrim (PrimIntOp IntMod) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x 'min y" :=
  (CallPrim (PrimIntOp IntMin) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x 'max y" :=
  (CallPrim (PrimIntOp IntMax) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '< y" :=
  (CallPrim (PrimIntCmp IntLt) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '≤ y" :=
  (CallPrim (PrimIntCmp IntLe) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '> y" :=
  (CallPrim (PrimIntCmp IntGt) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '≥ y" :=
  (CallPrim (PrimIntCmp IntGe) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '== y" :=
  (CallPrim (PrimEq) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '&& y" :=
  (CallPrim (PrimBoolOp BoolAnd) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.

Notation "x '|| y" :=
  (CallPrim (PrimBoolOp BoolOr) (x:expr)%T (y:expr)%T)
    (at level 10 ) : expr_scope.
