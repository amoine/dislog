From stdpp Require Import base sets fin_sets.
From dislog Require Import syntax semantics.

(******************************************************************************)
(* Reducible at path relation. *)

Inductive reducible : store -> amap -> graph -> task_tree -> expr -> Prop :=
| RedHead : forall σ α G T e σ' α' G' T' e',
  sched_step σ α G T e σ' α' G' T' e' ->
  reducible σ α G T e
| RedCtx : forall σ α G T e K,
    reducible σ α G T e ->
    reducible σ α G T (fill_item K e)
| RedPar : forall σ α G T1 T2 e1 e2,
    (¬ (is_val e1) ∨ ¬ (is_val e2)) ->
    (¬ is_val e1 -> reducible σ α G T1 e1) ->
    (¬ is_val e2 -> reducible σ α G T2 e2) ->
    reducible σ α G (Node T1 T2) (Par e1 e2).
#[export] Hint Constructors head_step : reducible.

Ltac solve_step := eauto using reducible,step, head_step, sched_step.
Ltac solve_reducible := try eapply RedHead; solve_step.

Lemma reducible_if σ α G t (b:bool) e1 e2 :
  reducible σ α G (Leaf t) (If b e1 e2).
Proof. solve_reducible. Qed.

Lemma reducible_let_val σ α G t x (v:val) e :
  reducible σ α G (Leaf t) (Let x v e).
Proof. solve_reducible. Qed.

Lemma reducible_fork σ α G t e1 e2:
  reducible σ α G (Leaf t) (Par e1 e2).
Proof.
  eapply RedHead. apply SchedFork.
  1,2:apply is_fresh. reflexivity.
Qed.

Lemma reducible_alloc σ α G t (i:Z) (v:val) :
  (0 ≤ i)%Z ->
  dom σ = dom α ->
  reducible σ α G (Leaf t) (Alloc i v).
Proof.
  intros ? Hdom. eapply RedHead.
  apply SchedHead, HeadAlloc; eauto.
  { apply is_fresh. }
  { rewrite Hdom. apply is_fresh. }
Qed.

Lemma reducible_closure σ α G t c :
  dom σ = dom α ->
  reducible σ α G (Leaf t) (Clo c).
Proof.
  intros Hdom.
  eapply RedHead.
  apply SchedHead, HeadFunc; eauto.
  { apply is_fresh. }
  { rewrite Hdom. apply is_fresh. }
Qed.

Lemma reducible_call_clo σ α G t (l:loc) ts vs self args body:
  σ !! l = Some (SClo (Lam self args body)) ->
  (forall l', l' ∈ locs body -> abef G α t l') ->
  ts = Val <$> vs ->
  length ts = length args ->
  reducible σ α G (Leaf t) (Call l ts).
Proof. intros. solve_reducible. Qed.

Lemma reducible_call σ α G t ts vs c:
  ts = Val <$> vs ->
  reducible σ α G (Leaf t) (Call (VCode c) ts).
Proof. intros. destruct c. solve_reducible. Qed.

Ltac conclude_reducible := intros; solve_reducible.

Lemma reducible_load σ α G t (bs:list val) (i:Z) (v:val) (l:loc) :
  σ !! l = Some (SBlock bs) ->
  (0 <= i < (Z.of_nat (length bs)))%Z ->
  bs !! (Z.to_nat i) = Some v ->
  vabef G α t v ->
  reducible σ α G (Leaf t) (Load l i).
Proof. conclude_reducible. Qed.

Lemma reducible_call_prim σ α G t p v1 v2 v :
  eval_call_prim p v1 v2 = Some v ->
  reducible σ α G (Leaf t) (CallPrim p v1 v2).
Proof. conclude_reducible. Qed.

Lemma reducible_store σ α G t (bs:list val) (i:Z) (v:val) (l:loc) :
  σ !! l = Some (SBlock bs) ->
  (0 <= i < (Z.of_nat (length bs)))%Z ->
  reducible σ α G (Leaf t) (Store l i v).
Proof. conclude_reducible. Qed.

Lemma reducible_length σ α G t (bs:list val) (l:loc) :
  σ !! l = Some (SBlock bs) ->
  reducible σ α G (Leaf t) (Length l).
Proof. conclude_reducible. Qed.

Lemma reducible_cas σ α G t (bs:list val) (i:Z) (l:loc) (v1 v1' v2:val) :
  (0 <= i < Z.of_nat (length bs))%Z ->
  σ !! l = Some (SBlock bs) ->
  bs !! (Z.to_nat i) = Some v1' ->
  vabef G α t v1' ->
  reducible σ α G (Leaf t) (CAS l i v1 v2).
Proof. conclude_reducible. Qed.

Lemma reducible_no_val σ α G T e :
  reducible σ α G T e ->
  ¬ is_val e.
Proof.
  inversion 1; subst.
  { eauto using sched_step_no_val. }
  { intros ?; elim_ctx.  }
  { compute_done. }
Qed.
