From stdpp Require Import base countable strings list binders.
From dislog Require Import syntax.

(******************************************************************************)
(* Various typeclass instances for the language. *)

Global Instance val_inhabited : Inhabited val := populate VUnit.
Global Instance expr_inhabited : Inhabited expr := populate (Val inhabitant).

Global Instance bool_op_eq_dec : EqDecision bool_op.
Proof. solve_decision. Qed.
Global Instance bool_op_countable : Countable bool_op.
Proof.
  refine (inj_countable'
    (fun op => match op with BoolAnd => 0 | BoolOr => 1 end)
    (fun n => match n with 0 => BoolAnd | _ => BoolOr end) _).
  intros []; reflexivity.
Qed.

Global Instance int_op_eq_dec : EqDecision int_op.
Proof. solve_decision. Qed.
Global Instance int_op_countable : Countable int_op.
Proof.
  refine (inj_countable'
    (fun op => match op with IntAdd => 0 | IntSub => 1 | IntMul => 2 | IntDiv => 3 | IntMod => 4 | IntMin => 5 | IntMax => 6 end)
    (fun n => match n with 0 => IntAdd | 1 => IntSub | 2 => IntMul | 3 => IntDiv | 4 => IntMod | 5 => IntMin | _ => IntMax end) _).
  intros []; reflexivity.
Qed.

Global Instance int_cmp_eq_dec : EqDecision int_cmp.
Proof. solve_decision. Qed.
Global Instance int_cmp_countable : Countable int_cmp.
Proof.
  refine (inj_countable'
    (fun op => match op with IntLt => 0 | IntLe => 1 | IntGt => 2 | IntGe => 3 end)
    (fun n => match n with 0 => IntLt | 1 => IntLe | 2 => IntGt | _ => IntGe end) _).
  intros []; reflexivity.
Qed.

Global Instance PrimEq_dec : EqDecision prim.
Proof. solve_decision. Qed.
Global Instance prim_countable : Countable prim.
Proof.
  refine
    (inj_countable'
       (λ op,
         match op with
         | PrimIntCmp x => inl (inl x)
         | PrimBoolOp x => inl (inr x)
         | PrimIntOp x => inr (inl x)
         | PrimEq => inr (inr tt) end)
       (λ x,
         match x with
         | inl (inl x) => PrimIntCmp x
         | inl (inr x) => PrimBoolOp x
         | inr (inl x) => PrimIntOp x
         | _ => PrimEq end) _).
  intros [ | ? | ? | ? ]; reflexivity.
Qed.

Lemma eq_val : forall (x y : val), {x = y} + {x ≠ y}
with eq_expr : forall (x y : expr), {x = y} + {x ≠ y}
with eq_func : forall (x y : function), {x = y} + {x ≠ y} .
Proof.
  { decide equality.
    { apply bool_eq_dec. }
    { apply Z.eq_dec. }
    { apply loc_eq_dec. } }
  { decide equality.
    { apply string_eq_dec. }
    { eapply (list_eq_dec l l0). Unshelve.
      intros ? ?. unfold Decision. apply eq_expr. }
    { apply PrimEq_dec. }
    { apply binder_dec_eq. } }
  { decide equality.
    { apply (list_eq_dec l l0). }
    { apply binder_dec_eq. } }
Defined.

Global Instance val_eq_dec : EqDecision val := eq_val.
Global Instance function_eq_dec : EqDecision function := eq_func.
Global Instance expr_eq_dec : EqDecision expr := eq_expr.

(* Intermediate to help for countable of expr *)
Inductive lit :=
| Lloc : loc -> lit
| Lbool : bool -> lit
| Lint : Z -> lit
| Lunit : lit
| Lprim : prim -> lit.

Global Instance lit_eq_dec : EqDecision lit.
Proof.
  intros ??. unfold Decision. decide equality.
  apply loc_eq_dec. apply bool_eq_dec. apply Z.eq_dec.
  apply PrimEq_dec.
Qed.

Global Instance lit_countable : Countable lit.
Proof.
  refine (inj_countable' (fun op => match op with
  | Lloc l => inl (inl (inl l))
  | Lbool b => inl (inl (inr b))
  | Lint n => inl (inr n)
  | Lunit => inr (inl tt)
  | Lprim p => inr (inr p)
  end) (λ n, match n with
             | inl (inl (inl l)) => Lloc l
             | inl (inl (inr b)) => Lbool b
             | inl (inr n) => Lint n
             | inr (inl tt) => Lunit
             | inr (inr p) => Lprim p end) _).
  intros []; eauto.
Qed.

Local Fixpoint enct (e:expr) :=
  match e with
  | Val v => GenNode 0 [encv v]
  | Call e es => GenNode 1 [enct e; GenNode 2 (enct <$> es)]
  | CallPrim p e1 e2 => GenNode 3 [GenLeaf (inr (Lprim p)); enct e1; enct e2]
  | Var x => GenNode 4 [GenLeaf (inl [BNamed x])]
  | Let x e1 e2 => GenNode 5 [GenLeaf (inl [x]); enct e1; enct e2]
  | If e1 e2 e3 => GenNode 6 [enct e1; enct e2; enct e3]
  | Alloc e1 e2 => GenNode 7 [enct e1; enct e2]
  | Load e1 e2 => GenNode 8 [enct e1; enct e2]
  | Store e1 e2 e3 => GenNode 9 [enct e1; enct e2; enct e3]
  | Length e => GenNode 10 [enct e]
  | Par e1 e2 => GenNode 11 [enct e1; enct e2]
  | CAS e1 e2 e3 e4 => GenNode 12 [enct e1; enct e2; enct e3; enct e4]
  | Clo c => GenNode 13 [encc c] end
with encv (v:val) :=
  match v with
  | VLoc l => GenLeaf (inr (Lloc l))
  | VBool b => GenLeaf (inr (Lbool b))
  | VInt n => GenLeaf (inr (Lint n))
  | VUnit => GenLeaf (inr Lunit)
  | VCode c => GenNode 14 [encc c] end
with encc (c:function) :=
  match c with
  | Lam x xs e => GenNode 15 [enct e; GenLeaf (inl [x]); GenLeaf (inl xs)] end.

Local Fixpoint dect (gt:gen_tree (list binder + lit)) : expr :=
  match gt with
  | GenNode 0 [v] => Val (decv v)
  | GenNode 1 [e;GenNode 2 ts] => Call (dect e) (dect <$> ts)
  | GenNode 3 [GenLeaf (inr (Lprim p)); e1; e2] => CallPrim p (dect e1) (dect e2)
  | GenNode 4 [GenLeaf (inl [BNamed x])] => Var x
  | GenNode 5 [GenLeaf (inl [x]); e1; e2] => Let x (dect e1) (dect e2)
  | GenNode 6 [e1; e2; e3] => If (dect e1) (dect e2) (dect e3)
  | GenNode 7 [e1; e2] => Alloc (dect e1) (dect e2)
  | GenNode 8 [e1; e2] => Load (dect e1) (dect e2)
  | GenNode 9 [e1; e2; e3] => Store (dect e1) (dect e2) (dect e3)
  | GenNode 10 [e] => Length (dect e)
  | GenNode 11 [e1; e2] => Par (dect e1) (dect e2)
  | GenNode 12 [e1; e2; e3; e4] => CAS (dect e1) (dect e2) (dect e3) (dect e4)
  | GenNode 13 [e1] => Clo (decc e1)
  | _ => Val VUnit end
with decv (v:gen_tree (list binder + lit)) :=
  match v with
  | GenLeaf (inr (Lloc l)) => VLoc l
  | GenLeaf (inr (Lbool b)) => VBool b
  | GenLeaf (inr (Lint n)) => VInt n
  | GenLeaf (inr Lunit) => VUnit
  | GenNode 14 [e] => VCode (decc e)
  | _ => VUnit end
with decc (c:gen_tree (list binder + lit)) :=
  match c with
  | GenNode 15 [e; GenLeaf (inl [x]); GenLeaf (inl xs)] =>
      Lam x xs (dect e)
  | _ => Lam BAnon nil VUnit end.

Global Instance expr_countable : Countable expr.
Proof.
  refine (inj_countable' enct dect _).
  refine (fix go (e:expr) {struct e} := _ with gov (v:val) {struct v} := _ with goc (c:function) {struct c} := _ for go).
  - destruct e as [ v | | | | | | | | | | | | ]; simpl; f_equal; try done.
    exact (gov v).
    exact (goc f).
    { clear e. induction l. done. simpl. f_equal. apply go. apply IHl. }
  - destruct v; simpl.
    1-4: easy. f_equal. easy.
  - destruct c. simpl. f_equal. easy.
Qed.
Global Instance val_countable : Countable val.
Proof.
  refine (inj_countable Val to_val  _).
  intros []; eauto.
Qed.
