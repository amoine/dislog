From Coq.ssr Require Import ssreflect.
From stdpp Require Import strings binders gmap gmultiset.

From dislog.utils Require Export graph.
From dislog.lang Require Export syntax syntax_instances.

(******************************************************************************)
(* The timestamp type is just a wrapper around nat. Any infinite countable type
   would work. *)

Inductive timestamp := to_timestamp : nat -> timestamp.
Definition of_timestamp : timestamp -> nat := fun '(to_timestamp x) => x.

(* We inherit various instances form nat. *)
#[export] Instance eqdec_timestamp : EqDecision timestamp.
Proof. solve_decision. Qed.
#[export] Instance countable_timestamp : Countable timestamp.
Proof. apply inj_countable' with of_timestamp to_timestamp. now intros []. Qed.
#[export] Instance infinite_timestamp : Infinite timestamp.
Proof. apply inj_infinite with to_timestamp (fun x => Some (of_timestamp x)). easy. Qed.
#[export] Instance inhabited_timestamp : Inhabited timestamp := populate (to_timestamp inhabitant).

(******************************************************************************)
(* Notations *)

Notation amap  := (gmap loc timestamp).
Notation store := (gmap loc storable).
Notation graph := (gset (timestamp*timestamp)).

Implicit Type t : timestamp.
Implicit Type α : amap.
Implicit Type σ : store.
Implicit Type G : graph.

(******************************************************************************)
(* [abef] & [vabef]: allocated before *)

(* [abef G α t l] asserts that l was allocated (as in α) before t in the
   graph G *)
Definition abef G α t l :=
  match α !! l with
  | Some v' => reachable G v' t
  | None => False end.

(* vabef extends abef to arbitrary values *)
Definition vabef G α t v :=
  match v with
  | VLoc l => abef G α t l
  | _ => True end.

Lemma vabef_no_loc G α t v :
  ¬ is_loc v ->
  vabef G α t v.
Proof. destruct v; naive_solver. Qed.

Lemma abef_mon_graph G G' α t l :
  G ⊆ G' ->
  abef G α t l ->
  abef G' α t l.
Proof.
  rewrite /abef.
  intros HG Hl.
  destruct (α !! l); eauto using reachable_mon.
Qed.

Lemma abef_mon_amap G α α' t l :
  α ⊆ α' ->
  abef G α t l ->
  abef G α' t l.
Proof.
  rewrite map_subseteq_spec /abef.
  intros Hi.
  destruct (α !! l) eqn:E; last easy.
  apply Hi in E. rewrite E //.
Qed.

Lemma abef_pre_reachable G α t t' x :
  reachable G t t' ->
  abef G α t x → abef G α t' x.
Proof.
  rewrite /abef.
  destruct (α !! x); last easy.
  intros. now transitivity t.
Qed.

Lemma abef_insert G l t α :
  abef G (<[l:=t]> α) t l.
Proof.
  rewrite /abef lookup_insert //.
Qed.

(******************************************************************************)
(* semantics of primitives *)

Definition exec_int_op op :=
  match op with
  | IntAdd => Z.add
  | IntMul => Z.mul
  | IntSub => Z.sub
  | IntDiv => Z.div
  | IntMod => Z.modulo
  | IntMin => Z.min
  | IntMax => Z.max end.

Definition exec_int_cmp op :=
  match op with
  | IntLt => Z.ltb
  | IntLe => Z.leb
  | IntGt => Z.gtb
  | IntGe => Z.geb end.

Definition exec_bool_bin_op op :=
  match op with
  | BoolAnd => andb
  | BoolOr => orb end.

Definition eval_call_prim p v1 v2 :=
  match p,v1,v2 with
  | PrimBoolOp op, VBool b1, VBool b2 => Some (VBool (exec_bool_bin_op op b1 b2))
  | PrimIntOp op, VInt m, VInt n => Some (VInt (exec_int_op op m n))
  | PrimIntCmp op, VInt m, VInt n => Some (VBool (exec_int_cmp op m n))
  | PrimEq, v1, v2 => Some (VBool (bool_decide (v1=v2)))
  | _,_,_ => None end.

(******************************************************************************)
(* head semantics *)

Inductive head_step : graph -> timestamp -> store -> amap -> expr -> store -> amap -> expr -> Prop :=
| HeadIf : forall G t σ α (b:bool) e1 e2,
    head_step G t
      σ α (If b e1 e2)
      σ α (if b then e1 else e2)
| HeadLet : forall G t σ α (v:val) x e,
    head_step G t
      σ α (Let x v e)
      σ α (subst' x v e)
| HeadCall : forall G t σ α ts vs self args code,
    ts = Val <$> vs ->
    head_step G t
      σ α (Call (VCode (Lam self args code)) ts)
      σ α (substs' (zip (self::args) (VCode (Lam self args code)::vs)) code)
| HeadCallClo : forall G t σ α ts vs self args body l,
    σ !! l = Some (SClo (Lam self args body)) ->
    (forall l', l' ∈ locs body -> abef G α t l') ->
    ts = Val <$> vs ->
    length args = length ts ->
    head_step G t
      σ α (Call (VLoc l) ts)
      σ α (substs' (zip (self::args) (VLoc l::vs)) body)
| HeadCallPrim : forall G t σ α v1 v2 v p,
    eval_call_prim p v1 v2 = Some v ->
    head_step G t
      σ α (CallPrim p v1 v2)
      σ α v
| HeadFunc : forall G t σ σ' α α' clo (l:loc),
    l ∉ dom σ ->
    l ∉ dom α ->
    σ' = <[l:=SClo clo]> σ ->
    α' = <[l:=t]> α ->
    head_step G t
      σ  α  (Clo clo)
      σ' α' (VLoc l)
| HeadAlloc : forall G t σ σ' α α' (n:Z) (v:val) (l:loc),
    (0 <= n)%Z ->
    l ∉ dom σ ->
    l ∉ dom α ->
    σ' = <[l:=SBlock (replicate (Z.to_nat n) v)]> σ ->
    α' = <[l:=t]> α ->
    head_step G t
      σ  α  (Alloc n v)
      σ' α' (Val l)
| HeadLoad : forall G t σ α (l:loc) (bs:list val) (i:Z) (v:val),
    σ !! l = Some (SBlock bs) ->
    (0 <= i < Z.of_nat (length bs))%Z ->
    bs !! (Z.to_nat i) = Some v ->
    vabef G α t v ->
    head_step G t
      σ α (Load l i)
      σ α v
| HeadStore : forall G t σ σ' α (l:loc) bs i (v:val),
    σ !! l = Some (SBlock bs) ->
    (0 <= i < Z.of_nat (length bs))%Z ->
    σ' = <[l := SBlock (<[Z.to_nat i := v]> bs)]> σ ->
    head_step G t
      σ  α (Store l i v)
      σ' α VUnit
| HeadLength : forall G t σ α (l:loc) bs,
    σ !! l = Some (SBlock bs) ->
    head_step G t
      σ  α (Length l)
      σ α (VInt (Z.of_nat (length bs)))
| HeadCAS : forall G t σ σ' α (l:loc) (i:Z) (v v0 v':val) bs,
    (0 <= i < Z.of_nat (length bs))%Z ->
    σ !! l = Some (SBlock bs) ->
    bs !! (Z.to_nat i) = Some v0 ->
    vabef G α t v0 ->
    σ' = (if bool_decide (v=v0)
          then (insert l (SBlock (<[Z.to_nat i := v']> bs)) σ) else σ) ->
    head_step G t
      σ  α (CAS l i v v')
      σ' α (Val (bool_decide (v=v0)))
.

#[export] Hint Constructors head_step : head_step.

Local Lemma middle_list {A:Type} (l:list A) x l1 l2 :
  l = l1 ++ x::l2 ->
  l !! length l1 = Some x.
Proof.
  intros ->.
  rewrite list_lookup_middle; easy.
Qed.

Lemma must_be_val vs xs e ys :
  Val <$> vs = (Val <$> xs) ++ e :: ys ->
  is_val e.
Proof.
  intros E.
  apply middle_list in E.
  rewrite list_lookup_fmap in E.
  destruct (vs !! length (Val <$> xs)); naive_solver.
Qed.

Lemma head_step_no_ctx G t σ α K e σ' α' e' :
  ¬ is_val e ->
  ¬ head_step G t σ α (fill_item K e) σ' α' e'.
Proof.
  intros ? Hstep. inversion Hstep; subst.
  all:destruct K; try naive_solver.
  all:inversion H0; eauto using must_be_val.
Qed.

Lemma head_step_no_val G t σ α e σ' α' e' :
  head_step G t σ α e σ' α' e' ->
  ¬ is_val e.
Proof. inversion 1; eauto. Qed.

Lemma head_step_inv_amap G t σ α e σ' α' e' :
  dom σ = dom α ->
  head_step G t σ α e σ' α' e' ->
  α ⊆ α'.
Proof.
  intros Hdom.
  inversion 1; eauto; subst.
  all:apply insert_subseteq; apply not_elem_of_dom; rewrite -?Hdom; eauto.
Qed.
