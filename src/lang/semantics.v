From Coq.ssr Require Import ssreflect.
From stdpp Require Import strings binders gmap gmultiset.

From dislog.utils Require Import graph.
From dislog.lang Require Export head_semantics.

(******************************************************************************)
(* Definition of a task_tree and operations on it. *)

Inductive task_tree : Set :=
| Leaf : timestamp -> task_tree
| Node : task_tree -> task_tree -> task_tree.

Fixpoint leaves (T:task_tree) : gset timestamp :=
  match T with
  | Leaf t => {[t]}
  | Node T1 T2 => leaves T1 ∪ leaves T2 end.

Lemma leaves_node T1 T2 :
  leaves (Node T1 T2) = leaves T1 ∪ leaves T2.
Proof. done. Qed.

Definition is_leaf T := match T with | Leaf _ => true | _ => false end.

(******************************************************************************)
(* Computation graphs *)

Definition graph_fork (G:graph) (t v w:timestamp) :=
  graph_upd G t {[v;w]}.
Definition graph_join (G:graph) (t v w:timestamp) :=
  graph_upd (graph_upd G t {[w]}) v {[w]}.

Lemma graph_fork_incl G t v w :
  G ⊆ (graph_fork G t v w).
Proof. apply graph_upd_incl. Qed.

Lemma graph_join_incl G t v w :
  G ⊆ (graph_join G t v w).
Proof.
  rewrite /graph_join.
  transitivity (graph_upd G t {[w]}); apply graph_upd_incl.
Qed.

(******************************************************************************)
(* [sched_step] either a head step, fork or join *)

Inductive sched_step :
  store -> amap -> graph -> task_tree -> expr ->
  store -> amap -> graph -> task_tree -> expr -> Prop :=
| SchedHead : forall σ α G t e σ' α' e',
    head_step G t σ α e σ' α' e' ->
    sched_step σ α G (Leaf t) e σ' α' G (Leaf t) e'
| SchedFork : forall σ α G G' t0 t1 t2 e1 e2,
    t1 ∉ vertices G ->
    t2 ∉ vertices G ->
    G' = graph_fork G t0 t1 t2 ->
    sched_step σ α G (Leaf t0) (Par e1 e2)
         σ α G' (Node (Leaf t1) (Leaf t2)) (Par e1 e2)
| SchedJoin : forall σ α G G' t1 t2 t3 (v1 v2:val) σ' α' (l:loc),
    t3 ∉ vertices G ->
    G' = graph_join G t1 t2 t3 ->
    l ∉ dom σ ->
    l ∉ dom α ->
    σ' = <[l:=SBlock [v1;v2]]> σ ->
    α' = <[l:=t3]> α ->
    sched_step σ  α  G (Node (Leaf t1) (Leaf t2)) (Par v1 v2)
      σ' α' G' (Leaf t3) l.
#[export] Hint Constructors sched_step : sched_step.

Lemma sched_step_no_val σ α G T e σ' α' G' T' e' :
  sched_step σ α G T e σ' α' G' T' e' -> ¬ is_val e.
Proof. destruct 1; eauto using head_step_no_val. Qed.

Lemma sched_step_no_ctx σ α G T e σ' α' G' T' e' K :
  ¬ is_val e → ¬ sched_step σ α G T (fill_item K e) σ' α' G' T' e'.
Proof.
  intros ? Hr. inversion Hr; subst.
  { eapply head_step_no_ctx; eauto. }
  all:destruct K; naive_solver.
Qed.

Lemma sched_step_inv_graph σ α G T2 e2 σ' α' G' T2' e2' :
  sched_step σ α G T2 e2 σ' α' G' T2' e2' ->
  G ⊆ G'.
Proof.
  destruct 1; subst; eauto; [apply graph_fork_incl | apply graph_join_incl].
Qed.

Lemma sched_step_inv_amap σ α G T e σ' α' G' T' e':
  dom σ = dom α ->
  sched_step σ α G T e σ' α' G' T' e' ->
  α ⊆ α'.
Proof.
  intros ?.
  destruct 1; subst; eauto using head_step_inv_amap.
  apply insert_subseteq. apply not_elem_of_dom. eauto.
Qed.

(******************************************************************************)
(* Deterministic distant semantics. *)

Inductive step :
  store -> amap -> graph -> task_tree -> expr ->
  store -> amap -> graph -> task_tree -> expr -> Prop :=
| StepHead : forall σ α G T2 e2 σ' α' G' T2' e2',
    sched_step σ α G T2 e2 σ' α' G' T2' e2' ->
    step σ α G T2 e2 σ' α' G' T2' e2'
| StepBind : forall σ α G T e σ' α' G' T' e' K,
    step σ α G T e σ' α' G' T' e' ->
    step σ α G T (fill_item K e) σ' α' G' T' (fill_item K e')
| StepParL : forall σ α G G' e1 e2 T1 T2 σ' α' T1' e1',
    step σ α G T1 e1 σ' α' G' T1' e1' ->
    step σ α G (Node T1 T2) (Par e1 e2) σ' α' G' (Node T1' T2) (Par e1' e2)
| StepParR : forall σ α G G' e1 e2 T1 T2 σ' α' T2' e2',
    step σ α G T2 e2 σ' α' G' T2' e2' ->
    step σ α G (Node T1 T2) (Par e1 e2) σ' α' G' (Node T1 T2') (Par e1 e2')
.

#[export] Hint Constructors step : step.

Lemma step_no_val σ α G T e σ' α' G' T' e' :
  step σ α G T e σ' α' G' T' e' ->
  ¬ is_val e.
Proof.
  inversion 1; subst; eauto using sched_step_no_val.
  destruct K; eauto.
Qed.

Lemma step_inv_graph σ α G T e σ' α' G' T' e'  :
  step σ α G T e σ' α' G' T' e' ->
  G ⊆ G'.
Proof.
  induction 1; subst; eauto using sched_step_inv_graph.
Qed.

Lemma step_inv_amap σ α G T e σ' α' G' T' e' :
  dom σ = dom α ->
  step σ α G T e σ' α' G' T' e' ->
  α ⊆ α'.
Proof.
  induction 2; subst; eauto using sched_step_inv_amap.
Qed.
