From Coq.ssr Require Import ssreflect.
From stdpp Require Import base sets gmap fin_sets.

Section Graph.

Context `{Countable K}.

(* We encode a graph as the set of its edges. *)
Definition graph := (gset (K*K)).

Implicit Type G : graph.
Implicit Type x y : K.

Definition vertices G : gset K := set_fold (fun '(x,y) acc => {[x;y]} ∪ acc) ∅ G.

Definition edge G x y := (x,y) ∈ G.

Lemma edge_mon G G' x y :
  G ⊆ G' ->
  edge G x y ->
  edge G' x y.
Proof. set_solver. Qed.

Definition reachable G x y := rtc (edge G) x y.

Lemma edge_reachable G x y :
  edge G x y -> reachable G x y.
Proof. apply rtc_once. Qed.

Global Instance reachable_pre_order G : PreOrder (reachable G).
Proof. apply rtc_po. Qed.

Lemma reachable_mon G G' x y :
  G ⊆ G' ->
  reachable G x y ->
  reachable G' x y.
Proof.
  intros ?. rewrite /reachable. revert x.
  apply rtc_ind_l.
  { apply rtc_refl. }
  { intros ? z ? ? ?.
    apply rtc_l with z; eauto using edge_mon. }
Qed.

(* ------------------------------------------------------------------------ *)
(* [graph_upd] *)

Definition edges (x:K) (X:gset K) : gset (K*K) := set_map (fun y => (x,y)) X.

Definition graph_upd G x (X:gset K) := (edges x X) ∪ G.

Lemma elem_of_edges x y X :
  y ∈ X <-> (x, y) ∈ edges x X.
Proof. set_solver. Qed.

Lemma edge_graph_upd G X x y :
  y ∈ X ∨ edge G x y ->
  edge (graph_upd G x X) x y.
Proof.
  rewrite /edge /graph_upd.
  rewrite elem_of_union. intros [|]; eauto. left. by apply elem_of_edges.
Qed.

Lemma graph_upd_incl G t S :
  G ⊆ (graph_upd G t S).
Proof. set_solver. Qed.

End Graph.
