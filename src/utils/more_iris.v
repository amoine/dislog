From iris.prelude Require Import prelude.
From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import own.
From iris.algebra Require Import gmap agree frac_auth gset auth.

Section big_sepS.

Lemma big_sepS_union_persistent `{Countable A} {PROP:bi} `{BiAffine PROP} (P:A -> PROP) (S1 S2:gset A) :
  (forall x, Persistent (P x)) ->
  ([∗ set] x ∈ (S1 ∪ S2), P x)%I ≡ (([∗ set] x ∈ S1, P x) ∗ ([∗ set] x ∈ S2, P x))%I .
Proof.
  intros.
  iSplit.
  { iIntros "#H".
    iSplitL; iIntros; rewrite !big_sepS_forall; iIntros; iApply "H";
      iPureIntro; set_solver. }
  { rewrite !big_sepS_forall.
    iIntros "(H1&H2)". iIntros (? Ht). apply elem_of_union in Ht.
    destruct Ht; [ iApply "H1" | iApply "H2"]; eauto. }
Qed.

End big_sepS.

Section utils.
Context `{Countable A}.
Context `{inG Σ (frac_authUR (gsetUR A))}.

Lemma update_model γ x S1 S2 q :
  own γ (●F S1) ∗ own γ (◯F{q} S2) ==∗ own γ (●F ({[x]} ∪ S1)) ∗ own γ (◯F{q} ({[x]} ∪ S2)).
Proof.
  iIntros "(?&?)".
  iMod (own_update_2 γ with "[$][$]") as "(?&?)".
  { apply frac_auth_update with (a':={[x]}∪S1) (b':={[x]}∪S2).
    apply local_update_unital_discrete. intros z Hv Hz. rewrite Hz.
    split; first easy. set_solver. }
  by iFrame.
Qed.

End utils.

Section gset.
Context `{Countable K}.
Context `{inG Σ (authUR (gsetUR K))}.

Implicit Type (x:K) (X:gset K).

Lemma gset_singleton_included x X : {[x]} ≼ X -> x ∈ X.
Proof. rewrite gset_included. set_solver. Qed.

Lemma auth_gset_elem_of γ X x :
  own γ (●X) -∗ own γ (◯ {[x]}) -∗ ⌜x ∈ X⌝.
Proof.
  iIntros.
  iDestruct (own_valid_2 γ with "[$][$]") as "%Hv".
  apply auth_both_valid_discrete in Hv.
  iPureIntro. apply gset_singleton_included. naive_solver.
Qed.

Lemma auth_gset_extract_witness γ X :
  own γ (●X) ==∗ own γ (●X) ∗ own γ (◯X).
Proof.
  iIntros.
  iApply own_op. iApply (own_update with "[$]").
  apply auth_update_alloc . apply gset_local_update. set_solver.
Qed.

Lemma auth_gset_conclude_frag γ S1 S2 :
  S1 ⊆ S2 ->
  own γ (◯ S2) -∗ own γ (◯ S1).
Proof.
  intros ?.
  replace S2 with (S1 ∪ S2) by set_solver.
  rewrite -gset_op. iIntros "(?&?)". by iFrame.
Qed.

Lemma auth_gset_insert x γ X:
  own γ (●X) ==∗ own γ (● ({[x]} ∪ X)) ∗ own γ (◯ {[x]}).
Proof.
  iIntros.
  iMod (own_update with "[$]") as "(?&?)".
  { apply auth_update_alloc. apply gset_local_update with (X':={[x]}∪X). set_solver. }
  iFrame. iApply (auth_gset_conclude_frag with "[$]"). set_solver.
Qed.

Lemma auth_gset_extract_witness_elem x γ X :
  x ∈ X ->
  own γ (●X) ==∗ own γ (●X) ∗ own γ (◯ {[x]}).
Proof.
  iIntros. iMod (auth_gset_extract_witness with "[$]") as "(?&?)".
  iFrame. iModIntro. iApply (auth_gset_conclude_frag with "[$]"). set_solver.
Qed.

End gset.
