From iris.prelude Require Import prelude.
From iris.algebra Require Import cmra local_updates proofmode_classes.

Section bool_and.
  Local Instance bool_and_unit_instance : Unit bool := true.
  Local Instance bool_and_valid_instance : Valid bool := λ x, True.
  Local Instance bool_and_validN_instance : ValidN bool := λ n x, True.
  Local Instance bool_and_pcore_instance : PCore bool := Some.
  Local Instance bool_and_op_instance : Op bool := andb.

  Definition bool_and_op x y : x ⋅ y = (andb x y) := eq_refl.

  Lemma bool_and_included x y : x ≼ y ↔ (is_true y -> is_true x).
  Proof.
    split.
    - intros [z ->]. rewrite bool_and_op. destruct x; easy.
    - exists y. rewrite /op /bool_and_op_instance.
      destruct y; destruct x; naive_solver.
  Qed.
  Lemma bool_and_ra_mixin : RAMixin bool.
  Proof.
    apply ra_total_mixin; apply _ || eauto.
    - intros ???. apply andb_assoc.
    - intros ??. apply andb_comm.
    - intros []; naive_solver.
  Qed.
  Canonical Structure bool_andR : cmra := discreteR bool bool_and_ra_mixin.

  Global Instance bool_and_cmra_discrete : CmraDiscrete bool_andR.
  Proof. apply discrete_cmra_discrete. Qed.

  Lemma bool_and_ucmra_mixin : UcmraMixin bool.
  Proof. split; try apply _ || done. Qed.
  Canonical Structure bool_andUR : ucmra := Ucmra bool bool_and_ucmra_mixin.

  Global Instance bool_and_core_id (x : bool) : CoreId x.
  Proof. by constructor. Qed.

  Lemma bool_and_local_update (x y : bool) :
    (x,y) ~l~> (false,false).
  Proof.
    rewrite local_update_unital_discrete => ? _ ?.
    done.
  Qed.

  Global Instance : IdemP (=@{bool}) (⋅).
  Proof. intros []; done. Qed.

  Global Instance bool_and_is_op (x y : bool) :
    IsOp (x && y) x y.
  Proof. done. Qed.
End bool_and.
