From iris.prelude Require Import prelude.
From iris.algebra Require Import dfrac.
From iris.bi.lib Require Import fractional.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmodem stdlib.

(******************************************************************************)
(* The code *)

Definition count_occ : val :=
  λ: [["l", "e"]],
    let: "r" := alloc ^1 0 in
    let: "f" := (λ: [["i"]], let: "x" := "l".["i"] in if: "x" '== "e" then incr [["r"]] else VUnit) in
    let: "length" := Length "l" in
    seqfor [[0, "length", "f"]];;
    "r".[0].

Definition filter : val :=
  λ: [["l", "length", "r", "e"]],
    let: "i" := alloc 1 0 in
    let: "f" := λ: [["j"]],
        let: "x" := "l".["j"] in
        if: "x" '== "e" then VUnit else
          let: "j" := "i".[0] in
          "r".["j"] <- "x";; incr [["i"]] in
    seqfor [[0, "length", "f"]].

Definition filter_compact : val :=
  λ: [["l", "e"]],
    let: "length" := Length "l" in
    let: "num_elem" := count_occ [["l","e"]] in
    let: "new_length" := "length" '- "num_elem" in
    let: "r" := alloc "new_length" VUnit in
    filter [["l", "length", "r", "e"]];; "r".

(******************************************************************************)
(* Spec *)

Section spec.

Lemma val_eq_dec' : forall (x y:val), {x=y} + {x ≠ y}.
Proof. intros. destruct_decide (decide (x=y)). now left. now right. Defined.

Definition count_occ_pure (vs:list val) (e:val) := List.count_occ val_eq_dec' vs e.

Lemma count_occ_spec `{interpGS Σ} E (l:loc) (q:dfrac) (vs:list val) (e:val) :
  l ↦{q} vs -∗
  wpm E (count_occ [[l, e]])%T (fun (v:Z) => ⌜v=count_occ_pure vs e⌝ ∗ l ↦{q} vs).
Proof.
  iIntros.
  wpm_call. iSteps. rename x into r.

  iAssert (∀ (x:nat) i, ⌜i <= length vs /\ x = count_occ_pure (take i vs) e⌝ -∗ l ↦{q} vs ∗ r ↦ [VInt x] -∗ wpm E (seqfor [[i,length vs, _]])%T (fun (_:unit) => l ↦{q} vs ∗ r ↦ [VInt (count_occ_pure vs e)]))%I with "[]" as "Hwp".
  { iIntros (v i) "(%Hi&%Hx) (?&?)".
    remember (length vs - i) as j.
    iInduction j as [|] "IH" forall (i v Hx Hi Heqj).
    { iApply seqfor_spec.
      assert (i = length vs) by lia. rewrite decide_True //.
      rewrite take_ge in Hx; last lia. subst v. by iFrame. }
    { iApply seqfor_spec.
      rewrite decide_False; last lia.

      wpm_call. wpm_bind.


      destruct (vs !! i) as [?|] eqn:Hv.
      2:{ exfalso. apply lookup_ge_None in Hv. lia. }

      iApply @wpm_load_tac; last iFrame. lia.
      { rewrite Nat2Z.id //. }

      iSteps. rew_enc.
      case_bool_decide as He; subst.
      { wpm_apply incr_spec. iIntros ([]) "?".
        rewrite -Nat2Z.inj_add.
        iApply ("IH" $! (i+1)  with "[%][%][%][$][$]").
        { rewrite /count_occ_pure.
          replace (i+1) with (S i) by lia.
          erewrite take_S_r; eauto. rewrite count_occ_app. simpl.
          rewrite /val_eq_dec'. rewrite decide_True //. }
        all:lia. }
      { iStep 2. iApply ("IH" $! (i+1) with "[%][%][%][$][$]").
        { rewrite /count_occ_pure. replace (i+1) with (S i) by lia.
          erewrite take_S_r; eauto. rewrite count_occ_app. simpl.
          rewrite /val_eq_dec' decide_False //. }
        all:lia. } } }
  iSteps.
Qed.

Definition filter_pure e (vs:list val) := base.filter (fun x => (x ≠ e)) vs.

Lemma length_filter_pure (vs:list val) e :
  length (filter_pure e vs) = length vs - count_occ_pure vs e.
Proof.
  rewrite /filter_pure.
  induction vs; simpl; first done.
  rewrite /val_eq_dec' filter_cons. case_decide; simpl; subst.
  { rewrite decide_False //.
    assert (count_occ_pure vs e <= length vs) by eauto using count_occ_bound.
    lia. }
  { rewrite decide_True //. }
Qed.

Lemma length_take_filter_pure v e i vs :
  vs !! i = Some v -> v ≠ e ->
  length (filter_pure e (take i vs)) < length (filter_pure e vs).
Proof.
  rewrite /filter_pure.
  revert i v. induction vs; simpl; intros i v Hiv Hv.
  { inversion Hiv. }
  { rewrite filter_cons. destruct i; simpl in *.
    { inversion Hiv. subst. rewrite decide_True //. simpl. lia. }
    { rewrite filter_cons. case_decide; apply IHvs in Hiv; eauto. simpl. lia. } }
Qed.

Record prefix_ok (xs vs: list val) (e:val) (i j:nat) :=
  { pof1 : i <= length vs;
    pof2 : length xs = length (filter_pure e vs);
    pof3 : j = length (filter_pure e (take i vs));
    pof4 : take j xs = take j (filter_pure e vs)
  }.

Lemma prefix_ok_incr1 xs vs e i j :
  vs !! i = Some e ->
  prefix_ok xs vs e i j ->
  prefix_ok xs vs e (i + 1) j.
Proof.
  intros Hi []. constructor; eauto; subst.
  { apply lookup_lt_Some in Hi. lia. }
  { rewrite !length_filter_pure.
    rewrite !Nat.add_1_r. erewrite take_S_r; eauto.
    rewrite /count_occ_pure count_occ_app. simpl.
    rewrite /val_eq_dec' decide_True // app_length take_length.
    simpl. lia. }
Qed.

Lemma prefix_incr i (xs ys:list val) v:
  take i xs = take i ys ->
  xs !! i = Some v ->
  ys !! i = Some v ->
  take (S i) xs = take (S i) ys.
Proof.
  intros.
  do 2 (erewrite take_S_r; eauto).
  by f_equal.
Qed.

Lemma prefix_ok_length_lt xs vs e i j v :
  vs !! i = Some v -> v ≠ e ->
  prefix_ok xs vs e i j -> j < length xs.
Proof.
  intros ? ? [].
  rewrite pof6 pof7.
  eauto using length_take_filter_pure.
Qed.

Lemma lookup_filter_pure vs i j e v :
  v ≠ e ->
  vs !! i = Some v -> j = length (filter_pure e (take i vs)) -> filter_pure e vs !! j = Some v.
Proof.
  intros He. rewrite /filter_pure.
  revert i j. induction vs; intros i j Hi Hj. naive_solver.
  rewrite filter_cons. destruct i.
  { inversion Hi. subst v. simpl.
    rewrite decide_True //.
    destruct j; first done.
    { exfalso. simpl in *. lia. } }
  { rewrite filter_cons in Hj. simpl in *.
    case_decide; eauto.
    destruct j; simpl in *; eauto.
    exfalso. lia. }
Qed.

Lemma prefix_ok_incr2 xs vs e i j v :
  vs !! i = Some v ->
  v ≠ e ->
  prefix_ok xs vs e i j ->
  prefix_ok (<[Z.to_nat j:=v]> xs) vs e (i+1) (j+1).
Proof.
  intros Hi Hv []. rewrite Nat2Z.id !Nat.add_1_r. constructor.
  { apply lookup_lt_Some in Hi. lia. }
  { rewrite insert_length. eauto. }
  { subst. rewrite !length_filter_pure.
     erewrite take_S_r; eauto. rewrite /count_occ_pure count_occ_app. simpl.
    replace (if val_eq_dec' v e then 1 else 0) with 0.
    2:{ rewrite /val_eq_dec' decide_False //. }
    rewrite app_length. simpl. rewrite Nat.add_1_r Nat.add_0_r.
    pose proof (count_occ_bound val_eq_dec' e (take i vs)). lia. }
  { apply prefix_incr with v.
    { rewrite take_insert //. }
    { rewrite list_lookup_insert // pof6 pof7.
      eauto using length_take_filter_pure. }
    { eauto using lookup_filter_pure.  } }
Qed.

Lemma filter_spec `{interpGS Σ} E (l r:loc) (q:dfrac) (vs vs':list val) (e:val) :
  length vs' = length vs - count_occ_pure vs e ->
  l ↦{q} vs ∗ r ↦ vs' -∗
  wpm E (filter [[l, length vs, r, e]])%T (fun (_:unit) => l ↦{q} vs ∗ r ↦ (filter_pure e vs)).
Proof.
  iIntros (?) "(?&?)".
  wpm_call.
  iSteps. rename x into z. rename x0 into f.

  iAssert (∀ (i j:nat) xs, ⌜prefix_ok xs vs e i j⌝ -∗ z ↦ [VInt j] ∗ l ↦{q} vs ∗ r ↦ xs -∗ wpm E (seqfor [[i,length vs, f]])%T (fun (_:unit) => l ↦{q} vs ∗ r ↦ (filter_pure e vs)))%I with "[]" as "Hwp".
  { iIntros (???) "%Hpof (?&?&?)".
    remember (length vs - i) as k.
    iInduction k as [|] "IH" forall (i j xs Hpof Heqk).
    { iApply seqfor_spec. destruct Hpof.
      assert (i=length vs) as -> by lia.
      rewrite decide_True //.
      subst. rewrite !firstn_all //.
      rewrite length_filter_pure firstn_all // take_ge -length_filter_pure in pof8.
      2:{ rewrite pof6. rewrite length_filter_pure. lia. }
      rewrite firstn_all in pof8.
      iSteps. }
    { iApply seqfor_spec. rewrite decide_False; last lia.
      wpm_call. wpm_bind.


      destruct (vs !! i) as [v|] eqn:Hv.
      2:{ exfalso. apply lookup_ge_None in Hv. lia. }

      iApply @wpm_load_tac; last iFrame. lia.
      { rewrite Nat2Z.id //. }

      iSteps. rew_enc.
      case_bool_decide as He; subst.
      { wpm_val. iApply ("IH" with "[%][%][$][$][$]"); eauto using prefix_ok_incr1.
        lia. }
      { iSteps.
        { iPureIntro. apply Nat2Z.inj_lt. eauto using prefix_ok_length_lt. }
        wpm_apply incr_spec.
        iIntros ([]) "Hz". rewrite -Nat2Z.inj_add.
        iApply ("IH" with "[%][%][$][$][$]"); eauto using prefix_ok_incr2.
        lia. } } }
  iApply ("Hwp" with "[%][$]").
  constructor; eauto. lia. rewrite length_filter_pure //.
Qed.

Lemma filter_compact_spec `{interpGS Σ} E (l:loc) (q:dfrac) (vs:list val) (e:val) :
  l ↦{q} vs -∗
  wpm E (filter_compact [[l, e]])%T
    (fun (r:loc) => l ↦{q} vs ∗ r ↦ (filter_pure e vs)).
Proof.
  iIntros.
  wpm_call. iSteps.
  wpm_apply count_occ_spec. iIntros (?) "(%Hv&?)".
  assert (count_occ_pure vs e <= length vs) by apply count_occ_bound.
  iSteps.
  wpm_apply filter_spec.
  { rewrite replicate_length. lia. }
  iSteps.
Qed.

End spec.
