From iris.prelude Require Import prelude.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.
From iris.algebra Require Import dfrac.
From iris.bi.lib Require Import fractional.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp objectivity.
From dislog.examples Require Import proofmodem big_opN stdlib parfor.
From dislog.examples.deduplication Require Import hashset.

(******************************************************************************)
(* The code *)

Definition dedup : val :=
  λ: [["h", "d", "l"]],
    let: "length" := Length "l" in
    let: "table" := hs_init [["length", "d"]] in
    let: "f" := λ: [["i"]],
        let: "x" := "l".["i"] in
        hs_add [["h","table", "d", "x"]] in
    parfor [[0, "length", "f"]];;
    hs_elems [["table", "d"]].

(******************************************************************************)
(* The spec *)

Section dedup_spec.
Context `{interpGS Σ, hashsetG Σ}.
Context (N:namespace).

Local Existing Instance hashset_invG.

Lemma cinvown_splitn γ qp (n:nat) :
  n ≠ 0 ->
  ( [∗ nat] i ∈ [ 0; n],  (cinv_own γ (qp/nat_to_Qp n)))%I ≡ (cinv_own γ qp)%I.
Proof.
  intros Hn. symmetry.
  apply (fractional_split_n_times (cinv_own γ)%I n). apply _. lia.
Qed.

Lemma hashset_splitn l p S0 n :
  n ≠ 0 ->
  hashset N l p S0 ∅ 1 -∗ ([∗ nat] i ∈ [ 0; n], ( hashset N l p S0 ∅ (1/nat_to_Qp n)))%I.
Proof.
  iIntros (?) "?". iApply (fractional_split_n_times (hashset N l p S0 ∅) n); eauto.
  rewrite /Fractional. intros. rewrite -hashset_split left_id_L //.
Qed.

Lemma list_to_set_singleton_L (x:val) :
  list_to_set [x] = ({[x]} : gset val).
Proof. rewrite list_to_set_cons list_to_set_nil. set_solver. Qed.

Lemma hashset_union l p S0 vs q :
  length vs ≠ 0 ->
  ([∗ nat] i ∈ [ 0; length vs], ( hashset N l p S0 {[vs!!!i]} (1/q)))%I -∗
  hashset N l p S0 (list_to_set vs) (nat_to_Qp (length vs) / q).
Proof.
  iIntros (?) "Hvs".
  iInduction vs as [|] "IH" using List.rev_ind.
  { easy. }
  rewrite app_length. simpl. rewrite Nat.add_1_r.
  rewrite big_sepN_snoc; last lia. iDestruct "Hvs" as "(?&?)".
  destruct vs; simpl.
  { rewrite right_id_L. replace (nat_to_Qp 1) with 1%Qp by compute_done.
    iFrame. }
  rewrite (nat_to_Qp_succ (S (length vs))) //.
  rewrite list_to_set_app_L list_to_set_singleton_L.
  replace ({[v]} ∪ (list_to_set vs ∪ {[x]})) with (({[x]} ∪ ({[v]} ∪ list_to_set vs)) : gset val) by set_solver.
  rewrite Qp.div_add_distr.
  rewrite hashset_split.

  rewrite list_lookup_total_alt lookup_app_r //.
  replace (length vs - length vs) with 0 by lia. simpl. iFrame.
  iApply ("IH" with "[%//]").
  iApply (big_sepN_impl with "[$]").
  iModIntro. iIntros. rewrite !list_lookup_total_alt.
  destruct i; iFrame. simpl.
  rewrite lookup_app_l. iFrame. lia.
Qed.

Lemma dedup_spec (l:loc) (p:dfrac) (d:val) (vs:list val) (h:loc) (hash:val -> nat) :
  d ∉ vs ->
  ([∗ nat] i ∈ [0;length vs],
    wpm (↑N) (h [Val (vs !!! i)])%T (fun v => ⌜v = VInt (hash (vs !!! i))⌝)) -∗
  l ↦{p} vs -∗
  wpm (↑N) (dedup [[h,d,l]])%T (fun l' => ∃ ws,  l' ↦ ws ∗ l ↦{p} vs ∗ ⌜deduped ws (list_to_set vs)⌝)%I.
Proof.
  iIntros (He) "Hh Hl".
  wpm_call. iStep 3.

  iDestruct (get_clock with "[$]") as "#Hc".

  iDestruct (hs_init_spec N (list_to_set vs) (length vs) d hash with "[Hc]") as "?".
  { intros X. apply elem_of_list_to_set in X. done. }
  { done.  }
  rew_enc.
  wpm_bind. iApply (wpm_mono_val with "[$]").
  iSteps as (l' f) "#Hf H1 H8". iClear "H8". Unshelve. 2:apply _.

  (* We can handle an empty vs, with some work. *)
  destruct_decide (decide (length vs = 0)) as Hlvs.
  { wpm_apply (parfor_specm _ _ _ _ (fun _ => embed True)%I). lia.
    rewrite !big_sepN_0; try lia. rewrite left_id. iIntros.
    simpl. wpm_apply hs_elems_spec. done. iIntros (?) "[%xs (?&%Hx)]".
    assert (xs=[]) as ->.
    { inversion Hx. destruct xs; try done. assert (v0 ∈ (∅ :gset val)); last set_solver.
      apply H2. set_solver. }
    iExists _. iFrame. iPureIntro.
    destruct vs; done. }

  iDestruct (punch_in with "[$]") as "[%t (#?&(?&#?))]".
  iMod (cinv_alloc _ N ((l ↦{p} vs) t) with "[$]")%I as "[%γ (#HI&Htok)]".
  iDestruct (cinvown_splitn _ _ (length vs) with "[$]") as "?". lia.
  iDestruct (hashset_splitn _ _ _ (length vs) with "[$]") as "?". lia.
  rewrite embed_big_sepN.

  do 2 iDestruct (big_sepN_sep with "[$]") as "Hpre".

  remember ({| size := length vs; dumm := d; hash := hash |}) as params.
  remember (nat_to_Qp (length vs)) as r.

  pose (Q := (fun i => hashset N l' params (list_to_set vs) {[vs !!! i]} (1/r) ∗ embed (cinv_own γ (1 / r)))%I).

  wpm_apply (parfor_specm _ _ _ _ Q). lia.
  iSplitL "Hpre".

  iApply (big_sepN_impl with "[$]").
  { iModIntro. iIntros (i Hi) "((?&?)&Hh)".
    wpm_call. wpm_bind.


    destruct (vs !! i) as [w|] eqn:Hv.
    2:{ exfalso. apply lookup_ge_None in Hv. lia. }

    iInv "HI" as "(>?&Hl)". constructor.
    iDestruct (monPred_in_elim with "[$][$]") as "?".
    iApply @wpm_load_tac; last iFrame. lia.
    { rewrite Nat2Z.id //. }
    iIntros "?". simpl.
    iDestruct (objectivize with "[$][$]") as "?"; last iFrame.
    rew_enc. iFrame. iModIntro.

    iApply (wpm_mono with "[-Hl]").
    { iApply (hs_add_spec with "[Hh]"); last iFrame.
      1,2:naive_solver.
      { subst. apply elem_of_list_lookup_2 in Hv. set_solver. }
      { rewrite list_lookup_total_alt Hv. by iApply "Hh". } }
    iIntros ([]) "?". iFrame. rewrite list_lookup_total_alt Hv. rewrite !right_id_L. iSteps. }

  iIntros ([]) "Hpost". simpl.

  iAssert ([∗ nat] i ∈ [0; length vs], (hashset N l' params _ {[vs !!! i]} (1%Qp / r) ∗ embed (cinv_own γ (1 / r))))%I with "[Hpost]" as "Hpost".
  { iApply (big_sepN_impl with "[$]"). iModIntro. iSteps. }
  rewrite big_sepN_sep.
  iDestruct "Hpost" as "(?&?)". subst r.
  rewrite -embed_big_sepN cinvown_splitn //.
  iDestruct (hashset_union with "[$]") as "?". easy.
  rewrite Qp.div_diag.

  iMod (cinv_cancel with "[$][$]") as ">?". set_solver.
  iDestruct (monPred_in_elim with"[$][$]") as "?".

  wpm_apply hs_elems_spec. naive_solver.
  iIntros (?) "[%(?&%E)]". iExists _. iFrame. eauto.
Qed.

End dedup_spec.
