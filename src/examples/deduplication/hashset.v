From iris.prelude Require Import prelude.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.
From iris.algebra Require Import gmap agree frac_auth gset.

From dislog.utils Require Import more_iris.
From dislog.lang Require Import notation.
From dislog.logic Require Import wp objectivity.
From dislog.examples Require Import map_agree proofmodem big_opN elems filter_compact.

(******************************************************************************)
(* From https://www.cs.cmu.edu/~swestric/22/thesis.pdf
   Section 2.3.2: Parallel Deduplication by Concurrent Hashing *)

Definition hs_init : val :=
  λ: [["size", "dummy"]], Alloc "size" "dummy".

(* This will be used for a closure body. *)
Definition hs_put_body table size dummy x i f : expr :=
    if: (CAS table i dummy x) '|| (table.[i] '== x)
    then
      VUnit
    else
      f [[(i '+ 1) 'mod size]].

Definition hs_add : val :=
  λ: [["hash", "table", "dummy", "x"]],
    let: "size" := Length "table" in
    let: "i" := "hash" [["x"]] 'mod "size" in
    let: "put" :=  μ: "f", [["i"]], hs_put_body "table" "size" "dummy" "x" "i" "f" in
    "put" [["i"]].

Definition hs_elems : val :=
  λ: [["table", "dummy"]],
    filter_compact [["table", "dummy"]].

(******************************************************************************)
(* The cmras we need. *)

Local Canonical Structure valO := leibnizO val.
Class hashsetG Σ :=
  HashSetG {
      hashset_modelG : inG Σ (frac_authUR (gsetUR val));
      hashset_elemsG  : map_agreeG Σ nat val;
      hashset_invG : cinvG Σ
    }.

Definition hashsetΣ : gFunctors :=
  #[ GFunctor (frac_authUR (gsetUR val));
     map_agreeΣ nat val;
     cinvΣ
    ].

Global Instance subG_queuΣ {Σ} : subG hashsetΣ Σ → hashsetG Σ.
Proof. solve_inG. Qed.

Local Existing Instance hashset_modelG.
Local Existing Instance hashset_elemsG.
Local Existing Instance hashset_invG.

(******************************************************************************)
(* The ghost state name. *)

Record ghashset :=
  mk_ghs { gm : gname; ge : gname; vert:timestamp}.

Global Instance ghashset_eq_dec : EqDecision ghashset.
Proof. solve_decision. Qed.
Global Instance ghashset_countable : Countable ghashset.
Proof.
  apply inj_countable with
    (f:= fun x => (x.(gm),x.(ge), x.(vert)))
    (g:= fun '(x1,x2,x3) => Some (mk_ghs x1 x2 x3)).
  intros []; eauto.
Qed.

Section Hashset.
Context `{interpGS Σ, hashsetG Σ}.
Context (N:namespace).

(* Some parameters. *)
Record params := mkp { size:nat; dumm:val; hash: val -> nat }.

(******************************************************************************)
(* Invariant. *)
(* The model is a map (M:gmap val nat) associating to each value in the set to
   its "idealized" index (the "real" index is taken modulo the size)
 *)

(* [before_full p vs i v] asserts that for all index [j]
   between [hash v] and [i], vs !! j contains other elements than v. *)
Definition before_full (p:params) (vs:list val) (i:nat) (v:val) :=
  ∀ j, p.(hash) v ≤ j < i -> ∃ v', vs !! (j `mod` p.(size)) = Some v' /\ v' ≠ p.(dumm) /\ v' ≠ v.

Definition hashset_inv_elem (p:params) (M:gmap val nat) (vs:list val) (v:val) (i:nat) :=
  v ≠ p.(dumm) /\ p.(hash) v ≤ i /\ vs !! (i mod p.(size)) = Some v /\ before_full p vs i v.

Definition hashset_inv_model (p:params) (M:gmap val nat) (vs:list val) :=
  map_Forall (hashset_inv_elem p M vs) M.

(* [tie p M vs] asserts that each element in [vs] is either dummy or not dummy and in the map. *)
Definition tie p (M:gmap val nat) (vs:list val) :=
  ∀ i v, vs !! i = Some v -> (v = p.(dumm) \/ (v ≠ p.(dumm) /\ ∃ i', M !! v = Some i' /\ i= i' mod p.(size))).

(* The actual invariant *)
Definition hashset_inv (γ:ghashset) (p:params) (A:gset val) (t:timestamp) (l:loc) : iProp Σ :=
  ∃ (M:gmap val nat) (vs:list val),
    own γ.(gm) (●F (dom M)) ∗ safe (list_to_set vs ∪ A) t ∗ (l ↦ vs) t ∗ elems γ.(ge) p.(dumm) vs ∗
    ⌜length vs = p.(size) /\ tie p M vs /\ hashset_inv_model p M vs /\ p.(dumm) ∉ A /\ dom M ⊆ A⌝.

Definition nm_inv := nroot.@"inv".
Definition nm_gh := nroot.@"gh".

(* [hashset] is defined as a cancelable invariant, to allow regaining the pointsto at the end.  *)
Definition hashset (l:loc) (p:params) (A:gset val) (S:gset val) (q:Qp) : vProp Σ :=
  ∃ γ η, monPred_in γ.(vert) ∗ embed (meta l nm_gh γ ∗ meta l nm_inv η ∗ cinv_own η q ∗ cinv N η (hashset_inv γ p A γ.(vert) l) ∗ own γ.(gm) (◯F{q} S)).

Definition hashset_inv_open : string :=
  "(>[%M [%vs (?&#Hsafe&?&Helems&(%Hlength&%Htie&%Hmodel&%Hdum&%Hsub))]]&Htok)".

(* The predicate "safe" comes from the objectivity module. *)
Lemma hs_init_spec (A:gset val) (C:nat) (d:val) h :
  d ∉ A ->
  safe A ⊢ wpm (↑N) (hs_init [[C, d]])%T (fun l => hashset l (mkp C d h) A ∅ 1).
Proof.
  iIntros (?) "HM".
  iApply wpm_fupd.
  iApply (wpm_vmementopre d). set_solver. iIntros "He".
  wpm_call.

  iStep 4 as (l) "Hl Hmeta".
  iMod (own_alloc (●F ∅ ⋅ ◯F ∅)) as "[%gm (?&?)]".
  { now apply frac_auth_valid. }

  iAssert (safe ({[d]} ∪ A)) with "[HM He]" as "?".
  { iApply big_sepS_union_persistent. rewrite big_sepS_singleton. iFrame. }

  iDestruct (monPred_in_intro (l ↦ _ ∗ safe _) with "[$]")%I as "[%t (#?&X)]".
  rewrite monPred_at_sep.
  iDestruct "X" as "(Hg&#Hs)".
  iMod elems_alloc as "[%ge Hge]".
  pose (γ:=mk_ghs gm ge t).
  pose (p:=mkp C d h).

  iMod (cinv_alloc_open (↑N) _ (hashset_inv γ p A t l)) as "[%η (?&?&HP)]".
  eauto.

  iFrame. iExists γ,η. iFrame "#∗".

  rewrite (meta_token_difference l ((⊤ ∖ ↑dislog_nm)∖↑nm_inv)).
  2:{ solve_ndisj. }
  iDestruct "Hmeta" as "(Hm1&Hm2)".
  iMod (meta_set _ l γ nm_gh with "Hm1"). solve_ndisj.
  iMod (meta_set _ l η nm_inv with "Hm2"). solve_ndisj.
  iFrame.

  iApply embed_pure.
  iApply embed_fupd. iModIntro.
  iApply "HP".

  iModIntro.
  iExists ∅,_. rewrite dom_empty_L. iFrame.
  iSplitR.
  { rewrite /safe. rewrite !monPred_at_big_sepS. iApply (big_sepS_subseteq with "[$]").
    intros x. rewrite elem_of_union elem_of_list_to_set elem_of_replicate. set_solver. }
  iPureIntro.
  rewrite replicate_length // Nat2Z.id .
  split_and !; eauto.
  { intros ? ? Hv. left. apply lookup_replicate in Hv. naive_solver. }
  { intros ?. set_solver. }
  { set_solver. }
Qed.

Lemma hashset_split l t A q1 q2 X1 X2 :
  hashset l t A (X1 ∪ X2) (q1 + q2) ≡ (hashset l t A X1 q1 ∗ hashset l t A X2 q2)%I.
Proof.
  rewrite -gset_op.
  iSplit.
  { iIntros "[%[%(#?&#?&#?&(Hq1&?)&#?&E)]]". rewrite frac_auth_frag_op.
    iDestruct "E" as "(E1&?)".
    iSplitL "Hq1 E1". all:iExists _,_; iFrame "#∗". }
  { iIntros "([%[%(#?&#X1&#X1'&?&?&?)]]&[%[% (_&#X2&#X2'&?&_&?)]])".
    iDestruct (meta_agree with "X2 X1") as "->".
    iDestruct (meta_agree with "X2' X1'") as "->".
    iExists _,_. iFrame "#∗". rewrite frac_auth_frag_op own_op.
    iFrame. rewrite fractional.fractional. iFrame. }
Qed.

Lemma update_model γ x S1 S2 q :
  own γ (●F S1) ∗ own γ (◯F{q} S2) ==∗ own γ (●F ({[x]} ∪ S1)) ∗ own γ (◯F{q} ({[x]} ∪ S2)).
Proof.
  iIntros "(?&?)".
  iMod (own_update_2 γ with "[$][$]") as "(?&?)".
  { apply frac_auth_update with (a':={[x]}∪S1) (b':={[x]}∪S2).
    apply local_update_unital_discrete. intros z Hv Hz. rewrite Hz.
    split; first easy. set_solver. }
  by iFrame.
Qed.

Lemma tie_add p vs i i' M x :
  vs !! i = Some p.(dumm) ->
  i = i' `mod` p.(size) ->
  x ≠ p.(dumm) ->
  x ∉ dom M ->
  tie p M vs ->
  tie p (<[x:=i']> M) (<[i:=x]> vs) .
Proof.
  intros  Hvsi ? Hx Hdx Htie.
  intros j v Hj. destruct_decide (decide (i=j)); try subst j.
  { right. rewrite list_lookup_insert in Hj.
    2:{ apply lookup_lt_is_Some; eauto. }
    injection Hj. intros ->. split; eauto.
    exists i'. rewrite lookup_insert. naive_solver. }
  rewrite list_lookup_insert_ne // in Hj.
  apply Htie in Hj. destruct Hj as  [?|(?&[j' (?&Hj')])].
  left; eauto. right. split; eauto. exists j'. rewrite lookup_insert_ne //.
  intros ->. apply Hdx. now apply elem_of_dom.
Qed.

Lemma model_add p vs i i' M x :
  p.(size) ≠ 0 ->
  p.(size) = length vs ->
  vs !! i = Some p.(dumm) ->
  i = i' `mod` p.(size) ->
  p.(hash) x ≤ i' ->
  x ≠ p.(dumm) ->
  before_full p vs i' x ->
  hashset_inv_model p M vs ->
  hashset_inv_model p (<[x:=i']> M) (<[i:=x]> vs).
Proof.
  intros ? Hsize Hvs Hi Hx Hh Hfull Hmodel.
  intros v j Hv. rewrite /hashset_inv_elem.
  destruct_decide (decide (x=v)); try subst v.
  { rewrite lookup_insert in Hv. injection Hv. intros <-.
    rewrite -Hi list_lookup_insert.
    2:{ apply lookup_lt_is_Some; eauto. }
    split_and ! ; eauto.
    rewrite /before_full.
    intros z Hz.
    apply Hfull in Hz. destruct Hz as (y&?&?&?).
    exists y. split_and !; eauto. rewrite list_lookup_insert_ne //.
    intros Hz. rewrite Hz in Hvs. naive_solver. }
  { rewrite lookup_insert_ne // in Hv.
    destruct (Hmodel _ _ Hv) as (Hv1&Hv2&Hv3&Hv4).
    assert (i ≠ (j `mod` p.(size))).
    { subst. intros E. rewrite -E in Hv3. rewrite Hv3 in Hvs.
      naive_solver. }
    split_and !; eauto.
    { rewrite list_lookup_insert_ne //. }
    { intros z Hz.
      destruct_decide (decide ( i = z `mod` p.(size))) as Eq; rewrite ?Eq.
      { exists x. rewrite list_lookup_insert //. rewrite -Hsize. now apply Nat.mod_upper_bound. }
      destruct (Hv4 _ Hz) as (v',(Hv1'&Hv2'&Hv3')).
      exists v'. rewrite list_lookup_insert_ne //. } }
Qed.

Definition elem l i v : iProp Σ :=
  ∃ γ, meta l nm_gh γ ∗ elem_int γ.(ge) i v.
Definition ifull_before p l x j : iProp Σ :=
  [∗ nat] i ∈ [p.(hash) x ; j], ∃ v, elem l (i `mod` p.(size)) v ∗ ⌜v ≠ x⌝.
Local Instance ifull_before_persistent p l x j : Persistent (ifull_before p l x j).
Proof. rewrite /ifull_before. rewrite -big_sepL_sepN. apply _. Qed.

Definition ifull_before_int γ p x j : iProp Σ :=
  [∗ nat] i ∈ [p.(hash) x ; j], ∃ v, elem_int γ (i `mod` p.(size)) v ∗ ⌜v ≠ x⌝.
Local Instance ifull_before_int_persistent γ p x j : Persistent (ifull_before_int γ p x j).
Proof. rewrite /ifull_before_int. rewrite -big_sepL_sepN. apply _. Qed.

Lemma ifull_before_int_use γ p (x:val) i (vs:list val) :
  p.(hash) x ≤ i ->
  vs !! (i `mod` p.(size)) = Some p.(dumm) ->
  ifull_before_int γ.(ge) p x i -∗
  elems γ.(ge) p.(dumm) vs -∗
  ⌜before_full p vs i x⌝.
Proof.
  iIntros. iIntros (z Hz).
  iDestruct (big_sepN_elem_of_acc _ _ z with "[$]") as "([% (?&%)]&_)".
  { lia. }
  iDestruct (use_elem_int with "[$][$]") as "(%&%Hvi)". eauto.
Qed.

Lemma before_full_confront (x:val) p i M (vs:list val) :
  hashset_inv_model p M vs ->
  p.(hash) x ≤ i ->
  vs !! (i `mod` p.(size)) = Some p.(dumm) ->
  before_full p vs i x ->
  x ∉ dom M.
Proof.
  intros Hmap Hx Hvs Hfull Hdx.

  (* if x ∈ dom M, x is already in vs as index (j `mod` size). *)
  apply elem_of_dom in Hdx. destruct Hdx as (j,Hj).
  apply Hmap in Hj. destruct Hj as (?&?&?&?).
  destruct_decide (decide (i=j)).
  { exfalso. naive_solver. }
  destruct (Nat.le_gt_cases i j).
  { exfalso. destruct (H4 i) as (?&?). lia. naive_solver. }
  destruct (Hfull j) as (?&?&?&?). lia.
  exfalso. naive_solver.
Qed.

Lemma elem_open l γ i v : meta l nm_gh γ -∗ elem l i v -∗ elem_int γ.(ge) i v.
Proof.
  iIntros "? [% (?&?)]".
  iDestruct (meta_agree with "[$][$]") as "->".
  iFrame.
Qed.

Lemma ifull_before_open l p γ x j :
  meta l nm_gh γ -∗ ifull_before p l x j -∗ ifull_before_int γ.(ge) p x j.
Proof.
  iIntros. iApply (big_sepN_impl with "[$]"). iModIntro.
  iIntros (? ?) "[% (?&?)]".
  iDestruct (elem_open with "[$][$]") as "?". iSteps.
Qed.

Lemma elem_of_list_insert {A:Type} (l:list A) x i v :
  v ∈ <[i:=x]> l ->
  v = x ∨ v ∈ l.
Proof.
  revert i. induction l; intros i.
  { rewrite list_insert_ge; last (simpl; lia). set_solver. }
  { rewrite elem_of_cons. destruct i; set_solver. }
Qed.

Lemma tryput_spec p (l:loc) q A X (i i':nat) (x:val) :
  p.(size) ≠ 0 ->
  i = i' mod p.(size) ->
  p.(hash) x ≤ i' ->
  x ∈ A ->
  hashset l p A X q ∗ embed (ifull_before p l x i')
  ⊢ wpm (↑N) (CAS l i p.(dumm) x)%T
    (fun (b:bool) => hashset l p A (if b then {[x]} ∪ X else X) q ∗ embed (if b then elem l i x else ∃v, elem l i v)).
Proof.
  iIntros (? Hi ??) "([%[%(#?&#?&#?&?&#I&Hfrag)]]&Hif0)".
  iInv "I" as hashset_inv_open. constructor.
  iDestruct (ifull_before_open with "[$][$]") as "#Hif".
  iClear "Hif0".
  rewrite -Hlength in Hi.
  pose proof (Nat.mod_upper_bound i' p.(size)).
  destruct (vs !! i) as [v|] eqn:Hvi.
  2:{ exfalso. apply lookup_ge_None in Hvi. subst i. rewrite Hlength in Hvi.
      lia. }

  iDestruct (monPred_in_elim with "[$][$]") as "Hl". simpl.

  unshelve iApply @wpm_cas_tac; last iFrame "#∗". apply _. eauto.
  done.
  { subst i. rewrite Hlength. lia. }
  { rewrite Nat2Z.id //. }

  case_decide as He; try subst v; last first.
  { rewrite bool_decide_eq_false_2//.
    iMod (elems_extract with "[$]") as "(?&Hv)". eauto. eauto.
    iIntros "Hl".
    iDestruct (objectivize with "[][$]") as "?".
    { iApply (safe_weak with "[$]"). intros ?. set_solver. }

    iModIntro. simpl.
    iSplitR "Hfrag Hv Htok".
    { iModIntro. iExists _,_. iFrame "∗#". eauto. }
    iSplitR "Hv". iExists _,_. iFrame "#∗".
    iExists v,_. iFrame "#∗". }
  rewrite bool_decide_eq_true_2 //. simpl. rewrite Nat2Z.id.
  iDestruct (ifull_before_int_use with "[$][$]") as "%"; only 1,2: eauto.
  { subst. rewrite -Hlength. naive_solver. }
  iMod (elems_insert x with "[$]") as "(?&Hx)"; only 1,2:eauto.
  { set_solver. }
  iMod (update_model _ x with "[$]") as "(?&Hfrag)".

  iIntros "Hl".
  iDestruct (objectivize with "[][$]") as "?".
  { iApply (safe_weak with "[$]").
    intros ? Hl.
    apply elem_of_list_to_set in Hl.
    apply elem_of_list_lookup_1 in Hl. destruct Hl as (j&Hj).
    destruct_decide (decide (i=j)); try subst j.
    { apply lookup_lt_Some in Hvi. rewrite list_lookup_insert // in Hj. inversion Hj. subst. set_solver. }
    { rewrite list_lookup_insert_ne // in Hj. apply elem_of_list_lookup_2 in Hj. set_solver. } }

  iSplitR "Hfrag Hx Htok".
  { iModIntro. iExists (<[x:=i']> M),_. rewrite dom_insert_L. iFrame "∗#".
    iModIntro. iSplitR.
    { iModIntro. iApply (safe_weak with "[$]").
      { intros ?. rewrite elem_of_union elem_of_list_to_set.
        intros [Hv|?]. apply elem_of_list_insert in Hv. set_solver.
        set_solver. } }
    iPureIntro. split_and !; eauto.
    { rewrite insert_length //. }
    { apply tie_add; eauto.
      { rewrite -Hlength //. }
      { set_solver. }
      { eapply before_full_confront; eauto.
        rewrite -Hlength -Hi //. } }
    { apply model_add; eauto.  rewrite -Hlength //. set_solver. }
    { set_solver. } }
  iModIntro.
  iSplitR "Hx". iExists _,_. by iFrame "#∗".
  iExists _. by iFrame "#∗".
Qed.

Lemma load_elem p l i v A q S :
  embed (elem l i v) -∗
  hashset l p A q S -∗
  wpm (↑N) (#l.[^i])%T (fun v' => ⌜v'=v⌝ ∗ hashset l p A q S).
Proof.
  iIntros "? [%[% (#?&#?&#?&?&#I&Hfrag)]]".
  iDestruct (elem_open with "[$][$]") as "#?".
  iInv "I" as hashset_inv_open. constructor.
  iDestruct (use_elem_int with "[$][$]") as "(%_&%Hv)".

  iDestruct (monPred_in_elim with "[$][$]") as "?".
  rewrite -(Nat2Z.id i) in Hv.

  unshelve iStep 7.
  iPureIntro.
  { apply lookup_lt_Some in Hv. lia. }

  iDestruct (objectivize with "[][$]") as "?".
  { iApply (safe_weak with "[$]"). set_solver. }

  iSplitR "Hfrag Htok".
  { do 2 iModIntro. iExists _,_. iFrame "∗#". eauto. }
  iSplitR; first easy. iExists γ,η. by iFrame "#∗".
Qed.

Lemma elem_add_model l p i x A q S :
  embed (elem l i x) -∗
  hashset l p A S q ={↑N}=∗ hashset l p A ({[x]}∪S) q.
Proof.
  iIntros "? [%[%(#?&#?&#?&?&#I&Hfrag)]]".
  iDestruct (elem_open with "[$][$]") as "#?".
  iInv "I" as hashset_inv_open.
  iDestruct (use_elem_int with "[$][$]") as "(%&%Hv)".
  assert (x ∈ dom M).
  { apply Htie in Hv. destruct Hv as [?|(?&(?,(?&?)))]. naive_solver.
    now apply elem_of_dom. }
  iMod (update_model _ x with "[$]") as "(?&Hfrag)".
  replace ({[x]} ∪ dom M) with (dom M) by set_solver.
  iModIntro. iSplitR "Hfrag Htok".
  { iModIntro. iExists _,_. iFrame "∗#". eauto. }
  iModIntro. iExists _,_. iFrame "#∗".
Qed.

Lemma hs_put_spec put p (l:loc) A q X (i i':nat) (x:val) :
  p.(size) ≠ 0 ->
  i = i' mod p.(size) ->
  p.(hash) x ≤ i' ->
  x ∈ A ->
  Func put (Lam "f" [BNamed "i"] (hs_put_body l p.(size) p.(dumm) x "i" "f")) -∗
  hashset l p A X q ∗ embed (ifull_before p l x i') -∗
  wpm (↑N) (put [[i]])%T
    (fun (_:unit) => hashset l p A ({[x]}∪X) q).
Proof.
  iIntros (? Hx Hi ?) "#HClo (?&#Hif)". iLöb as "IH" forall (i i' Hx Hi) "Hif".
  iApply (@wpm_call Σ _ unit _ _ _ _ _ [Val i] [VInt i] put); last iFrame "#".
  1,2:reflexivity. iModIntro. simpl.
  wpm_apply tryput_spec; eauto.
  iIntros (b) "(?&Hv)".
  destruct b.
  { do 3 wpm_bind. iApply (wpm_apply nil with "[Hv]"). iApply (load_elem with "Hv").
    iFrame. iIntros (?) "(->&?)". simpl. Unshelve. 2:apply _. iSteps. }
  iDestruct "Hv" as "[%v #Hv]".
  do 3 wpm_bind. iApply (wpm_apply nil with "[Hv]"). iApply (load_elem with "Hv").
  iFrame. iIntros (?) "(->&?)". simpl. Unshelve. 2:apply _. subst.
  iSteps. rew_enc.
  case_bool_decide as Heq; subst.
  { iMod (elem_add_model with "[$][$]") as "?". iSteps. }

  iSteps.

  rewrite -Nat2Z.inj_add -Nat2Z.inj_mod. rew_enc.
  remember ((((i' `mod` p.(size)) + 1) `mod` p.(size))) as j.
  iSpecialize ("IH" $! j (i'+1) with "[%][%][$][]").
  { subst. destruct (size p) as [|r]; first lia.
    destruct r as [|r].
    { rewrite Nat.mod_1_r //. }
    rewrite (Nat.Div0.add_mod i').
    do 2 f_equal. rewrite Nat.mod_small //. lia. }
  { lia. }
  { iModIntro. rewrite /ifull_before Nat.add_1_r.
    iModIntro. iApply big_opN_snoc. lia.
    iFrame "#". iExists _. iFrame "#%". }
  { iSteps. }
Qed.

Lemma hashset_length l p A q X  :
  hashset l p A q X  -∗
  wpm (↑N) (Length l) (fun (n:Z) => ⌜n=p.(size)⌝ ∗ hashset l p A q X).
Proof.
  iIntros "[% [% (#H1&H2&H3&H4&#I&H5)]]".
  iInv "I" as hashset_inv_open. constructor.

  iDestruct (monPred_in_elim with "[$][$]") as "Hl".
  wpm_length.
  iDestruct (objectivize with "[][$]") as "Hl".
  { iApply (safe_weak with "[$]"). set_solver. }
  iModIntro. rewrite Hlength.
  iSplitR "H2 H3 H5 Htok".
  { iSteps. iModIntro. iExists _,_. iFrame "#∗". eauto. }
  iSplitR; first done. iExists _,_. iFrame "#∗".
Qed.

Lemma hs_add_spec (p:params) d (l:loc) A (q:Qp) (X:gset val) (h:val)  (x:val) :
  p.(size) ≠ 0 ->
  d = p.(dumm) ->
  x ∈ A ->
  (wpm (↑N) (h [[x]])%T (fun v => ⌜v = VInt (p.(hash) x)⌝)) -∗
  hashset l p A X q  -∗
  wpm (↑N) (hs_add [[h,l,d,x]])%T
    (fun (_:unit) =>  hashset l p A ({[x]} ∪ X) q).
Proof.
  iIntros (???) "? ?". subst.
  wpm_call.
  wpm_apply (hashset_length l p A X q). iIntros (?) "(->&?)".
  iStep 8 as (put) "#Hclo H8". iClear "H8". rewrite -Nat2Z.inj_mod.
  wpm_apply hs_put_spec; eauto.
  iSplit.
  { iModIntro. by iApply big_sepN_0. }
  iSteps.
Qed.

Definition NoDup_except `{EqDecision A} (x:A) (xs:list A) :=
  NoDup (base.filter (fun y => y ≠ x) xs).

(* A correctness lemma *)
Lemma nodup_ok p M vs :
  tie p M vs -> NoDup_except p.(dumm) vs.
Proof.
  intros Htie. apply NoDup_alt.
  intros i j x Hi Hj.
  destruct_decide (decide (i=j)) as Hij; eauto.
  exfalso.
  assert (exists i' j', i' ≠ j' /\ x ≠ p.(dumm) /\ vs !! i' = Some x /\ vs !! j'= Some x) as (i',(j',(Hij'&Hx&Hi'&Hj'))).
  { clear Htie. revert i Hi j Hj Hij. induction vs; intros.
    { rewrite filter_nil in Hi.  rewrite lookup_nil in Hi. congruence. }
    { rewrite filter_cons in Hi,Hj. case_decide.
      { destruct i,j; simpl in *. congruence.
        { apply elem_of_list_lookup_2 in Hj. apply elem_of_list_filter in Hj. destruct Hj as (?&Hj).
          apply elem_of_list_lookup_1 in Hj. destruct Hj as (j'&?). exists 0,(S j'). naive_solver. }
        { apply elem_of_list_lookup_2 in Hi. apply elem_of_list_filter in Hi. destruct Hi as (?&Hi).
          apply elem_of_list_lookup_1 in Hi. destruct Hi as (i'&?). exists (S i'),0. naive_solver. }
        destruct (IHvs i Hi j Hj) as (i',(j',(?&?&?&?))). naive_solver.
        exists (S i'), (S j'). naive_solver. }
      { destruct (IHvs i Hi j Hj) as (i',(j',(?&?&?&?))). naive_solver.
        exists (S i'), (S j'). naive_solver. } } }
  destruct (Htie _ _ Hi') as [?|(_&Hmi)]; first naive_solver.
  destruct (Htie _ _ Hj') as [?|(_&Hmj)]; first naive_solver.
  naive_solver.
Qed.

Lemma hashset_model_ok p M vs :
  tie p M vs ->
  hashset_inv_model p M vs ->
  (dom M ⊆ list_to_set vs ⊆ dom M ∪ {[p.(dumm)]}).
Proof.
  intros Htie HM. split; intros v; rewrite elem_of_list_to_set elem_of_list_lookup; intros Hv.
  { apply elem_of_dom in Hv. destruct Hv as (?,Hv).
    destruct (HM _ _ Hv). naive_solver. }
  { destruct Hv as (i,Hi).
    apply Htie in Hi. destruct Hi as [Hi|(_&(?,(Hi&?)))]. set_solver.
    rewrite elem_of_union elem_of_dom. eauto. }
Qed.

Record correct_result (dumm:val) (size:nat) (vs:list val) (X:gset val) :=
  mkcr
    { cr_nodup  : NoDup_except dumm vs;
      cr_length : length vs = size;
      cr_elem   : X ⊆ list_to_set vs ⊆ X ∪ {[dumm]};
      cr_notin  : dumm ∉ X
    }.

Lemma hashset_end l p A X :
  hashset l p A X 1 ={↑N}=∗ ∃ vs, ⌜correct_result p.(dumm) p.(size) vs X⌝ ∗ l ↦ vs.
Proof.
  iIntros "[%[%(#?&#?&#?&?&#I&Hfrag)]]".
  iMod (cinv_cancel with "[$][$]") as "I'". set_solver.
  iDestruct "I'" as ">[% [%vs (Ha&?&?&?&(%R2&%R3&%R4&%R5&%R6))]]".
  iDestruct (own_valid_2 with "Ha Hfrag") as "%Hv".
  apply frac_auth_agree_L in Hv. rewrite -Hv.
  iModIntro. iExists vs.
  iDestruct (monPred_in_elim with "[$][$]") as "?". iFrame.
  iPureIntro. constructor; eauto using nodup_ok,hashset_model_ok.
Qed.

Definition deduped (xs:list val) (X:gset val) :=
  NoDup xs /\ (∀ x, x ∈ xs <-> x ∈ X).

Lemma list_to_set_subseteq (xs ys: list val) :
  list_to_set xs ⊆ (list_to_set ys : gset val) ->
  xs ⊆ ys.
Proof.
  intros Hincl x Hx. specialize (Hincl x).
  rewrite !elem_of_list_to_set in Hincl. eauto.
Qed.

Lemma correct_result_dedup e s xs X :
  correct_result e s xs X ->
  deduped (filter_pure e xs) X.
Proof.
  intros []. constructor; eauto. intros x.
  destruct cr_elem0 as (E1&E2).
  rewrite /filter_pure elem_of_list_filter.
  set_solver.
Qed.

Lemma hs_elems_spec l p A X d:
  d = p.(dumm) ->
  hashset l p A X 1 -∗
  wpm (↑N) (hs_elems [[l,d]])%T (fun (l:loc) => ∃vs, l ↦ vs ∗ ⌜deduped vs X⌝).
Proof.
  iIntros (->) "?".
  iMod (hashset_end with "[$]") as "[% (%&?)]".
  wpm_call. wpm_apply filter_compact_spec. iIntros (?) "(_&?)". iExists _. iFrame.
  iPureIntro. eauto using correct_result_dedup.
Qed.

End Hashset.

Global Opaque hashset.
