From iris.algebra Require Import excl.
From iris.base_logic.lib Require Import invariants.

From dislog.lang Require Import notation.
From dislog.logic Require Import wpm objectivity.
From dislog.examples Require Import proofmodem stdlib.

(* We encode a lock as a pair of closures. *)
Definition new_lock : val :=
  λ: [[]],
    let: "r" := alloc 1 false in
    let: "try_lock" := λ: [], CAS "r" 0 false true in
    let: "unlock"  := λ: [], "r".[0] <- false in
    pair [["try_lock", "unlock"]].

(* pasted from
   https://plv.mpi-sws.org/coqdoc/iris/iris.heap_lang.lib.spin_lock.html *)
Class lockG Σ := LockG { lock_tokG : inG Σ (exclR unitO) }.
Local Existing Instance lock_tokG.

Definition lockΣ : gFunctors := #[GFunctor (exclR unitO)].

Global Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.
Proof. solve_inG. Qed.

Section lock.

Context `{interpGS Σ, lockG Σ}.
Let N := nroot .@ "spin_lock".

Definition lock_inv (γ:gname) (l:loc) (R:iProp Σ) : iProp Σ :=
  (∃ (b:bool), (mapsto l (DfracOwn 1) (SBlock [VBool b])) ∗
              if b then True else own γ (Excl ()) ∗ R)%I.

Definition is_lock (lck ulck:loc) (R:iProp Σ) : iProp Σ :=
  ∃ (γ:gname) (l:loc),
    meta lck N γ ∗ (* [γ] is a ghost variable everyone agree on. *)
    interp.Func lck (Lam BAnon nil (CAS l 0 false true)) ∗ (* [lck] is a closure doing a CAS *)
    interp.Func ulck (Lam BAnon nil (Store l 0 false)) ∗ (* [ulck] is a closure doing a Store *)
    inv N (lock_inv γ l R). (* the [lock_inv] holds. *)

Definition locked (l:loc) : iProp Σ :=
  ∃ γ, meta l N γ ∗ own γ (Excl ()).

(* The lock will protect the (iProp) resource [R] *)
Lemma new_lock_spec (R:iProp Σ) :
  embed R -∗
  wpm ⊤ (new_lock [])
    (fun (l:loc) => ∃ (lck ulck:loc),
       l ↦ [VLoc lck; VLoc ulck] ∗
       embed (is_lock lck ulck R)
    ).
Proof.
  iIntros "HR". wpm_call. iSteps as (r lck ulck) "E1 E2 E3 E4 E5 E6". rew_enc.
  iApply wpm_fupd. iClear "E4 E6".
  wpm_apply pair_spec. rewrite left_id. iIntros (l) "Hl".

  iMod (own_alloc (Excl ())) as "[%γ Hγ]". done.
  iDestruct (mapsto_at_unboxed with "E3") as "E3". compute_done.
  iMod (meta_set _ lck γ N with "E5") as "#?". solve_ndisj.

  iMod (inv_alloc N _ (lock_inv γ _ R) with "[HR Hγ E3]") as "#HI".
  { iModIntro. iSteps. }
  iModIntro. iExists lck,ulck. iFrame. rewrite !Func_eq.
  iModIntro. iExists _,_. iFrame "#".
Qed.

Lemma lck_spec lck ulck R :
  embed (is_lock lck ulck R) -∗
  wpm ⊤ (lck []) (fun (b:bool) => if b then embed (locked lck) ∗ embed R else True).
Proof.
  iIntros "[%[% (#?&Hlck&_&I)]]". rewrite -Func_eq.
  wpm_call.
  iInv "I" as "[% (>?&HR)]". constructor.
  rewrite -mapsto_at_unboxed; last compute_done.
  iApply (wpm_frame_step with "HR"). compute_done.
  wpm_apply wpm_cas. 3:done. 1,2:done.
  iIntros (?) "(->&?)". case_decide; destruct b; try done; simpl;
    [iIntros "(X&?)"; iFrame | iIntros "_"; rewrite right_id]; iFrame.
  all: rewrite mapsto_at_unboxed; last compute_done.
  2:do 3 iModIntro; iSteps.
  iModIntro. iSplitR "X"; repeat iModIntro; iSteps.
Qed.

Lemma ulck_spec lck ulck R :
  embed (is_lock lck ulck R) -∗
  embed (locked lck) ∗ embed R -∗
  wpm ⊤ (ulck []) (fun (_:unit) => True).
Proof.
  iIntros "[%[% (Hmeta&_&Hulck&I)]] ([% (Hmeta1&?)]&?)".
  rewrite /locked -Func_eq.
  wpm_call.
  iInv "I" as "[% (>?&_)]". constructor.
  rewrite -mapsto_at_unboxed; last compute_done.
  iStep 4. rewrite mapsto_at_unboxed; last compute_done.
  iDestruct (meta_agree with "Hmeta Hmeta1") as "->".
  rewrite right_id. do 3 iModIntro. iSteps.
Qed.

End lock.
