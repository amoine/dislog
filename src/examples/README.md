This directory stores the proof of various examples.

* `tactics/` stores files for tactics, and the instance of Diaframe.
* `lib/` stores various useful cmras, and `big_opN`, the iterated
  conjunction of a predicate between two nat.
* `det_race/` stores a proof in the base logic and a proof in the
  high-level logic of a simple determinacy race.
* `deduplication` stores the proofs of a deduplication function on
  pre-allocated data.
* `deduplication_lazy` stores the proofs of a more elaborated
  deduplication algorithm.
