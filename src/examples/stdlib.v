From iris.prelude Require Import prelude.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmode.

(******************************************************************************)
(* The code *)

Definition ref (e:expr) : expr := alloc ^1 e.
Definition get (e:expr) : expr := e.[0].
Definition set (e:expr) (e':expr) :expr := e.[0] <- e'.

Definition incr : val :=
  λ: [["r"]], let: "x" := "r".[0] in "r".[0] <- ("x" '+ 1).

Definition pair : val :=
  λ: [["x", "y"]],
    let: "p" := alloc 2 VUnit in
    "p".[0] <- "x";;
    "p".[1] <- "y";;
    "p".

Definition seqfor : val :=
  μ: "self", [["i", "j", "f"]],
    if: "i" '== "j" then VUnit else
    "f" [["i"]];;
    let: "ni" := "i" '+ 1 in
    "self" [["ni", "j", "f"]].

(******************************************************************************)

From dislog.logic Require Import wpm.
From dislog.examples Require Import proofmodem.

Lemma seqfor_spec `{interpGS Σ} E (i j:nat) (f:loc) (Q:unit -> vProp Σ) :
  (if (decide (i=j)) then Q tt else wpm E (f [[i]])%T (fun (_:unit) => wpm E (seqfor [[i+1,j,f]])%T Q))
  -∗  wpm E (seqfor [[i,j,f]])%T Q.
Proof.
  iIntros.
  wpm_call. iSteps.
  case_bool_decide as Hij.
  { rewrite decide_True; last naive_solver. iSteps. }
  { rewrite decide_False; last naive_solver. iSteps.
    rew_nat2z_inj. iSteps. }
Qed.

Lemma incr_spec `{interpGS Σ} E (r:loc) (x:Z) :
  r ↦ [VInt x] -∗
  wpm E (incr [[r]])%T (fun (_:unit) => r ↦ [VInt (x+1%nat)]).
Proof. iIntros. wpm_call. iSteps. Qed.

Lemma pair_spec `{interpGS Σ} E (x:val) (y:val) :
  True -∗ wpm E (pair [[x,y]])%T (fun (l:loc) => l ↦ [x;y]).
Proof. iIntros. wpm_call. iSteps. Qed.
