From dislog.examples Require Export proofmode_base.
From iris.proofmode Require Import coq_tactics tactics reduction spec_patterns intro_patterns.

From dislog.examples Require Import proofmode_base diaframe.
From dislog.logic Require Import conversions.
From dislog.logic Require Export wp.

Global Opaque clock prec.

(******************************************************************************)
(* [Convertible] *)

Lemma wp_convertible `{interpGS Σ} (B:Type) `{Convertible B A} E t e (Q:Post A Σ) :
  wp E t e (fun t (b:B) => Q t (conv b)) -∗
  wp E t e Q.
Proof.
  rewrite !wp_eq.
  iIntros. iApply (wpg.wpg_mono with "[$]").
  iIntros.
  iApply (@post_convertible _ _ B).
  iFrame.
Qed.

(******************************************************************************)
(* [bind] *)

Lemma wp_let `{interpGS Σ} (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E (t:timestamp) x (e1 e2:expr) (Q: Post A Σ) :
  wp E t e1 (fun t' (v:B) => wp E t' (subst' x (enc v) e2) Q) -∗
  wp E t (Let x e1 e2) Q.
Proof.
  iIntros "Hwp".
  iApply (@wp_bind _ _ _ _ B _ _ _ _ (CtxLet x e2) Q).
  iApply (wp_mono with "Hwp").
  iIntros. iApply wp_let_val. iFrame.
Qed.

Ltac wp_bind typ :=
  lazymatch goal with
  | |- envs_entails _ (wp _ _ ?e ?Q) =>
      lazymatch type of Q with
      | _ -> ?typer -> _=>
      let ctx := infer_ctx e in
      lazymatch ctx with
      | CtxLet _ _ => iApply (@wp_let _ _ typer _ typ _)
      | ?K =>
          iApply (@wp_bind _ _ typer _ typ _ _ _ _ K) end end end.

Tactic Notation "wp_bind" constr(t) := wp_bind t.
Tactic Notation "wp_bind" := wp_bind val.

(******************************************************************************)
(* [alloc] *)

Lemma wp_alloc_tac' `{interpGS Σ} `(CO:@Convertible loc Enc_loc A EA) E t (i:Z) (v:val) (Q:Post A Σ) :
  (0 <= i)%Z ->
  (∀ (l:loc), l ↦ (replicate (Z.to_nat i) v) ∗ l ◷ t ∗ meta_token l (⊤ ∖ ↑dislog_nm) -∗
     Q t (@conv _ _ _ _ CO l)) -∗
  wp E t (Alloc i v) Q.
Proof.
  iIntros (?) "Hpost". iApply wp_mementopost.
  iApply (@wp_convertible _ _ loc Enc_loc _ _ CO).
  iApply wp_tempus_atomic. constructor.
  iApply wp_mono. iApply wp_alloc. eauto.
  iSteps. rewrite conv_enc. iSteps.
Qed.

Lemma wp_alloc_tac `{interpGS Σ} `(CO:@Convertible loc Enc_loc A EA) E t (i:Z) (v:val) (Q:Post A Σ) :
  (0 <= i)%Z ->
  (∀ (l:loc), l ↦ (replicate (Z.to_nat i) v) ∗ l ◷ t -∗
     Q t (@conv _ _ _ _ CO l)) -∗
  wp E t (Alloc i v) Q.
Proof.
  iIntros (?) "Hpost". iApply wp_alloc_tac'. lia. iSteps.
Qed.

Ltac wp_alloc' :=
  unshelve iApply @wp_alloc_tac';
  [ apply _ | | sanitize].

Ltac wp_alloc l Hpost :=
  unshelve iApply @wp_alloc_tac;
  [ apply _ | | iIntros (l) Hpost; sanitize].

Tactic Notation "wp_alloc" "as" ident(l) constr(Hpost) :=
  wp_alloc l Hpost.
Tactic Notation "wp_alloc" "as" ident(l) :=
  wp_alloc as l "(?&?)".
Tactic Notation "wp_alloc" :=
  let l := fresh "l" in
  wp_alloc as l.

(******************************************************************************)
(* [clo] *)

Lemma wp_closure_tac `{interpGS Σ} `(CO:@Convertible loc Enc_loc A EA) E t clo  (Q:Post A Σ) :
  (∀ (l:loc), Func l clo ∗ l ◷ t -∗
     Q t (@conv _ _ _ _ CO l)) -∗
  wp E t (Clo clo) Q.
Proof.
  iIntros "Hpost".
  iApply wp_mementopost.
  iApply (@wp_convertible _ _ loc Enc_loc _ _ CO).
  iApply wp_tempus_atomic. constructor.
  iApply wp_mono.
  iApply wp_closure. iSteps. rewrite conv_enc. iSteps.
Qed.

Ltac wp_clo l Hpost :=
  unshelve iApply @wp_closure_tac;
  [ apply _ | iIntros (l) Hpost; sanitize].

Tactic Notation "wp_clo" "as" ident(l) constr(Hpost) :=
  wp_clo l Hpost.
Tactic Notation "wp_clo" "as" ident(l) :=
  wp_clo as l "(?&?)".
Tactic Notation "wp_clo" :=
  let l := fresh "l" in
  wp_clo as l.

(******************************************************************************)
(* [store] *)

Lemma wp_store_tac `{interpGS Σ} `(CO:@Convertible unit Enc_unit A EA) E t (bs:list val) (l:loc) (i:Z) (v:val) (Q:Post A Σ) :
  (0 <= i < (Z.of_nat (length bs)))%Z ->
  l ↦ bs ∗
  (l ↦ (<[Z.to_nat i:=v]> bs) -∗ Q t (conv tt)) -∗
  wp E t (Store l i v) Q.
Proof.
  iIntros (?) "(Hpt&Hpost)".
  iApply wp_tempus_atomic. constructor.
  iApply (@wp_convertible _ _ _ _ _ _ CO).
  iApply (wp_mono with "[Hpt]").
  iApply (wp_store with "Hpt"). eauto.
  iIntros (?[]) "?".
  iApply "Hpost".
  iFrame.
Qed.

Ltac wp_store_base :=
  unshelve (iApply @wp_store_tac); first apply _.
Ltac solve_arith := try first [now vm_compute| now lia].

Ltac wp_store_tac tac :=
  wp_store_base; last (tac tt);
  [ solve_arith | sanitize ].

Tactic Notation "wp_store" constr(Hclo) :=
  wp_store_tac ltac:(fun _ => iFrame Hclo).
Tactic Notation "wp_store" :=
  wp_store_tac ltac:(fun _ => iFrame).

(******************************************************************************)
(* [length] *)

Lemma wp_length_tac `{interpGS Σ} `(CO:@Convertible Z Enc_int A EA) E t bs (l:loc) q (Q:Post A Σ) :
  l ↦{q} bs ∗
  (l ↦{q} bs -∗ Q t (conv (Z.of_nat (length bs)))) -∗
  wp E t (Length l) Q.
Proof.
  iIntros "(Hpt&Hpost)".
  iApply wp_tempus_atomic. constructor.
  iApply (@wp_convertible _ _ _ _ _ _ CO).
  iApply (wp_mono with "[Hpt]").
  iApply (wp_length with "Hpt").
  iIntros (??) "(->&?)".
  iApply "Hpost".
  iFrame.
Qed.

Ltac wp_length_tac tac :=
  unshelve (iApply @wp_length_tac); first apply _; last ((tac tt); sanitize).

Tactic Notation "wp_length" constr(H) :=
  wp_length_tac ltac:(fun _ => iFrame H).
Tactic Notation "wp_length" :=
  wp_length_tac ltac:(fun _ => iFrame).

(******************************************************************************)
(* [load] *)

Lemma wp_load_tac `{interpGS Σ} (A:Type) (EA:Enc A) E t bs q (i:Z) (a:A) (l:loc) (Q:Post A Σ) :
  (0 <= i < Z.of_nat (length bs))%Z ->
  bs !! (Z.to_nat i) = Some (enc a) ->
  vclock (enc a) t -∗
  l ↦{q} bs ∗ (l ↦{q} bs -∗ Q t a) -∗
  wp E t (Load l i) Q.
Proof.
  iIntros (??) "HB (Hpt&Hpost)".
  iApply wp_tempus_atomic. constructor.
  iApply (wp_mono with "[HB Hpt]").
  iApply (wp_load with "Hpt HB"); eauto.
  iIntros (??) "(->&?)".
  iApply "Hpost". iFrame.
Qed.

Ltac wp_load_base tac :=
  unshelve (iApply @wp_load_tac); last (tac tt); only 3:reflexivity; only 1: solve_arith; rew_enc.

Tactic Notation "wp_load" constr(Hyp) :=
  wp_load_base ltac:(fun _ => iFrame Hyp).
Tactic Notation "wp_load" :=
  wp_load_base ltac:(fun _ => iFrame).

(******************************************************************************)
(* [call] *)


Lemma wp_call_clo_tac `{interpGS Σ} (A:Type) (EA:Enc A) E t self args body ts v (Q:Post A Σ) :
  forallb is_val ts →
  length args = length ts →
  Func v (Lam self args body) ∗
  ▷(wp E t (substs' (zip (self::args) (v::(to_val_force <$> ts))) body) Q) -∗
  wp E t (Call v ts) Q.
Proof.
  iIntros (??) "(?&?)".
  iApply (wp_call with "[$]"); eauto.
  { rewrite fmap_length //. }
  { rewrite fmap_to_val_force //. }
Qed.

Lemma wp_call_code_tac `{interpGS Σ} (A:Type) (EA:Enc A) E t self args body ts (Q:Post A Σ) :
  forallb is_val ts →
  length args = length ts →
  locs body = ∅ ->
  ▷(wp E t (substs' (zip (self::args) (VCode (Lam self args body)::(to_val_force <$> ts))) body) Q) -∗
  wp E t (Call (VCode (Lam self args body)) ts) Q.
Proof.
  iIntros.
  iApply wp_call_clo_tac. eauto.
  2:{ iSplitR. 2:iFrame. rewrite Func_eq. iPureIntro. split; eauto. }
  eauto.
Qed.

Definition is_VCode e :=
  match e with
  | Val (VCode _) => true
  | _ => false end.

Ltac wp_call_clo_tac tac :=
  iApply @wp_call_clo_tac;
  [ now vm_compute | | tac tt ];
  [ try (now vm_compute) | sanitize; iModIntro ].

Ltac wp_call_code tac :=
  iApply @wp_call_code_tac;
  [ try (now vm_compute) | try (now vm_compute) | tac tt | sanitize; iModIntro ].

Ltac wp_call_tac :=
  lazymatch goal with
  | |- envs_entails _ (wp _ _ (Call ?e _) _) =>
      let b := eval vm_compute in (is_VCode e) in
        lazymatch b with
        | true => wp_call_code ltac:(fun _ => set_solver)
        | false => wp_call_clo_tac ltac:(fun _ => iFrame "#∗") end end.

Tactic Notation "wp_call_clo" constr(Hyp) :=
  wp_call_clo_tac ltac:(fun _ => iFrame Hyp).
Tactic Notation "wp_call" :=
  wp_call_tac.

(******************************************************************************)
(* [wp_call_prim] *)

Lemma wp_call_prim_tac `{!interpGS Σ} (A:Type) (EA:Enc A) E t p v1 v2 (w:A) (Q:Post A Σ) :
  head_semantics.eval_call_prim p v1 v2 = Some (enc w) ->
  Q t w -∗
  wp E t (CallPrim p v1 v2) Q.
Proof.
  iIntros (Hw) "?".
  iApply wp_tempus_atomic. constructor.
  iApply wp_mono.
  iApply wp_call_prim. eauto. iSteps.
Qed.

Ltac wp_call_prim :=
  iApply @wp_call_prim_tac;
  [try reflexivity | sanitize].

(******************************************************************************)
(* [wp_if] *)

Ltac wp_if := iApply @wp_if.

(******************************************************************************)
(* [cas] *)

Lemma wp_cas_tac `{interpGS Σ} `(CO:@Convertible bool Enc_bool A EA) E t bs (l:loc) (i:Z) (v1 v1' v2:val) q (Q:Post A Σ) :
  (v1=v1' -> q=DfracOwn 1%Qp) ->
  (0 ≤ i < length bs)%Z ->
  bs !! Z.to_nat i = Some v1' ->
  l ↦{q} bs ∗ vclock v1' t ∗ (l ↦{q} (if decide (v1=v1') then <[Z.to_nat i:=v2]> bs else bs) -∗ Q t (conv (bool_decide (v1=v1')))) -∗
  wp E t (CAS l i v1 v2) Q.
Proof.
  iIntros (? ? ?) "(?&?&HQ)".
  iApply wp_tempus_atomic. constructor.
  iApply (@wp_convertible _ _ bool Enc_bool _ _ CO).
  iApply (wp_mono with "[-HQ]").
  iApply wp_cas; eauto. iSteps.
Qed.

(******************************************************************************)
(* [wp_apply] *)

Lemma wp_apply `{interpGS Σ} `(CO:@Convertible A EA B EB) E t e P (Q':Post A Σ) (Q:timestamp -> B -> iProp Σ):
  (P -∗ wp E t e Q') -∗ P ∗ (∀ t' v, Q' t' v -∗ Q t' (conv v))%I -∗
  wp E t e Q.
Proof.
  iIntros "HP (?&?)".
  iSpecialize ("HP" with "[$]").
  iApply wp_convertible.
  iApply (wp_mono with "[$]"). eauto.
Qed.

Ltac wp_apply H :=
  unshelve (iApply @wp_apply); only 6:iApply H; first apply _; last iFrame.

Tactic Notation "wp_apply" uconstr(H) := wp_apply H.
