From stdpp Require Export namespaces.
From iris.base_logic.lib Require Export gen_heap.
From iris.proofmode Require Export proofmode.
From iris.proofmode Require Import coq_tactics tactics reduction spec_patterns intro_patterns.

From dislog.lang Require Export syntax notation.
From dislog.logic Require Import enc.

(******************************************************************************)
(* [Convertible A EA B EB] asserts that the encodable type A can be converted in
   the encodable type B
   We keep track of the encoders directly inside the typeclass. *)

Class Convertible (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) := mkco {conv:A -> B; conv_enc : forall x, enc (conv x) = enc x }.

Global Instance Conv_refl (A:Type) (EA:Enc A) : Convertible A EA A EA :=
  {conv:=id; conv_enc:=fun _ => eq_refl}.
Global Instance Conv_val (A:Type) (EA:Enc A) : Convertible A EA val Enc_val :=
  {conv := enc; conv_enc:=fun _ => eq_refl}.

Lemma post_convertible {PROP:bi} `{BiAffine PROP} `{@Convertible B EB A EA} (Q:A -> PROP) v :
  post (fun b:B => Q (conv b)) v -∗
  post Q v.
Proof.
  rewrite !post_eq.
  iIntros "[%a (-> & ?)]".
  iExists (conv a).
  rewrite !conv_enc //. iFrame. eauto.
Qed.

(******************************************************************************)
(* [bind] *)

Ltac infer_ctx_list_aux acc xs :=
  lazymatch xs with
  | ?y::?ys =>
      lazymatch y with
      | Val ?v => infer_ctx_list_aux constr:(v::acc) ys
      | _ => constr:((List.rev acc,y,ys)) end end.

(* [e]   is an expression
   [ks]  is a list of eval. context
   [tac] is a continuation, taking as input a list of eval. context and the
   expression.
 *)
Ltac reshape_expr_aux e ks tac :=
  match e with
  | _ => tac ks e
  | If ?e ?e2 ?e3 => reshape_expr_aux e (CtxIf e2 e3::ks) tac
  | Let ?x ?e ?e2 => reshape_expr_aux e (CtxLet x e2::ks) tac
  | Load (Val ?v1) ?e => reshape_expr_aux e (CtxLoad2 v1::ks) tac
  | Load e ?e2 => reshape_expr_aux e (CtxLoad1 e2::ks) tac
  | Store (Val ?v1) (Val ?v2) ?e => reshape_expr_aux e (CtxStore3 v1 v2::ks) tac
  | Store (Val ?v1) ?e ?e1 => reshape_expr_aux e (CtxStore2 v1 e1::ks) tac
  | Store ?e ?e1 ?e2 => reshape_expr_aux e (CtxStore1 e1 e2::ks) tac
  | Call (Val ?v) ?xs =>
      lazymatch infer_ctx_list_aux constr:(@nil val) xs with
      | (?vs,?e,?ts) => reshape_expr_aux e (CtxCall1 v vs ts::ks) tac end
  | CallPrim ?p (Val ?v) ?e =>
      reshape_expr_aux e (CtxCallPrim2 p v::ks) tac
  | CallPrim ?p ?e ?t =>
      reshape_expr_aux e (CtxCallPrim1 p t::ks) tac
  | fill_item ?K e => reshape_expr_aux e (K::ks) tac
  end.

(* Try to apply [tac] to every possible decomposition of the expression e *)
Ltac reshape_expr e tac := reshape_expr_aux e (@nil ctx) tac.

Ltac infer_ctx_inner ks e :=
  lazymatch ks with
  | [?x] => constr:(x)
  | _ => fail "infer_ctx" end.

Ltac infer_ctx e := reshape_expr e infer_ctx_inner.

(******************************************************************************)
(* [sanitize] *)

Ltac sanitize := rew_enc; simpl substs'; simpl subst'.

Ltac rew_nat2z_inj_step tt :=
  first [ rewrite -Nat2Z.inj_add |
          rewrite -Nat2Z.inj_mod |
          rewrite -Nat2Z.inj_div |
          rewrite -Nat2Z.inj_sub |
          rewrite -Nat2Z.inj_mul |
          rewrite -Nat2Z.inj_min |
          rewrite -Nat2Z.inj_max
    ].
Ltac rew_nat2z_inj := repeat (rew_nat2z_inj_step tt).

(******************************************************************************)
(* [call] *)

Definition to_val_force e : val :=
  match e with
  | Val v => v
  | _ => VUnit end.

Lemma fmap_to_val_force ts :
  forallb is_val ts →
  Val <$> (to_val_force <$> ts) = ts.
Proof.
  intros.
  induction ts as [|t ts]; try easy.
  do 2 rewrite fmap_cons.
  destruct t; simpl in *; try easy.
  f_equal. eauto.
Qed.

From dislog.logic.base Require Import wpg_more wp.

(* ------------------------------------------------------------------------ *)
(* [fill_items_val ks v] fills [v] inside the contexts ks, and if it appears
   in a let binding, tries to substitute it.  *)

Definition fill_val K (v:val) :=
  match K with
  | CtxLet x e => subst' x v e
  | _ => fill_item K v end.

Definition fill_items_val ks (v:val) :=
  match ks with
  | [] => Val v
  | k::ks => fill_items ks (fill_val k v) end.

(* ------------------------------------------------------------------------ *)

Lemma wp_bind_inv `{interpGS Σ} A (EA:Enc A) E t e K (Q:Post A Σ) :
  ([∗ set] l ∈ locs K, l ◷ t) ∗ wp E t (fill_item K e) Q -∗
  wp E t e (fun t' (v:val) => wp E t' (fill_item K v) Q ).
Proof.
  iIntros. rewrite !wp_eq. iDestruct (wpg_bind_inv with "[$]") as "?".
  iApply (wpg.wpg_mono with "[$]"). iIntros. rewrite post_val //.
Qed.

Lemma wp_post `{interpGS Σ} (A:Type) (EA:Enc A) E t e (Q:Post A Σ ):
  wp E t e (fun t (v:val) => post (Q t) v) ⊣⊢
  wp E t e Q.
Proof.
  rewrite !wp_eq.
  iSplit. all:iIntros; iApply (wpg.wpg_mono with "[$]"); iIntros; rewrite post_val //.
Qed.

Lemma wp_ctx_wp_pre `{interpGS Σ} E t ks (v:val) (Q:Post val Σ) :
  wp E t (fill_items_val ks v) Q -∗
  wp E t (fill_items ks v) Q.
Proof.
  iIntros "Hwp". iInduction ks as [|] "IH" using List.rev_ind forall (Q).
  { done. }
  destruct ks.
  { destruct x; try done. by iApply wp_let_val. }
  { rewrite !wp_eq. iApply mementopre_iterated. iIntros "#HX". rewrite -!wp_eq.
    rewrite fill_items_app.
    iApply (@wp_bind _ _ val). Unshelve. 3:apply _.
    simpl. rewrite fill_items_app.
    iDestruct (wp_bind_inv with "[Hwp]") as "?".
    { iFrame. rewrite /all_abef_set. rewrite !big_sepS_forall. iIntros.
      iApply "HX". iPureIntro. rewrite locs_fill_item. set_solver. }
    by iApply "IH". }
Qed.

Lemma wp_ctx_wp `{interpGS Σ} (A:Type) (EA:Enc A) E t ks v (Q:Post A Σ) :
  wp E t (fill_items_val ks v) Q -∗
  wp E t (fill_items ks v) Q.
Proof.
  iIntros. iApply wp_post. iApply wp_ctx_wp_pre.
  destruct ks; simpl; try rewrite wp_post //.
Qed.
