From dislog.examples Require Export proofmode_base.
From iris.proofmode Require Import coq_tactics tactics reduction spec_patterns intro_patterns.

From dislog.lang Require Import notation.
From dislog.logic Require Import wpm.

From diaframe Require Import pure_solver.
From diaframe Require Export proofmode_base.

(* Make some things opaque so diaframe does not expand them. *)
Global Opaque mapsto.
Global Opaque coPset_difference.

(******************************************************************************)
(* [ReshapeExpr] asserts that [e = fill_items ks e'] *)
Class ReshapeExpr (e:expr) (ks:list ctx) (e':expr) :=
  ConstructReshape {
      ReshapeExpr_equality : e = fill_items ks e';
    }.
Global Hint Mode ReshapeExpr + - - : typeclass_instances.

Ltac find_reshape e ks e' :=
  reshape_expr e
    ltac:(fun ks' e'' =>
            unify ks ks'; unify e' e'';
            notypeclasses refine (ConstructReshape e ks' e'' (eq_refl)); tc_solve).

Global Hint Extern 4 (ReshapeExpr ?e ?K ?e') =>
         find_reshape e K e' : typeclass_instances.

(******************************************************************************)
(* [Convertible_if ks A EA B EB] requires (Convertible A EA B EB) iff ks not
   nil. This is very useful to apply lemmas under an evaluation context. *)

Class Convertible_if (ks:list ctx) (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) :=
  { coif : match ks with nil => (@Convertible A EA B EB)| _ => True end }.

Lemma Convertible_if_nil ks (A:Type) (EA:Enc A) (B:Type) (EB:Enc B)  :
  TCEq ks nil ->
  Convertible_if ks A EA B EB ->
  Convertible A EA B EB.
Proof. by intros -> []. Defined.

Global Instance Convertible_if_Build ks A EA B EB :
  TCEq ks nil ->
  Convertible A EA B EB ->
  Convertible_if ks A EA B EB.
Proof. intros -> ?. by constructor. Defined.
Global Instance Convertible_if_Buildnotnil k ks ks' A EA B EB :
  TCEq ks (k::ks') ->
  Convertible_if ks A EA B EB.
Proof. intros ->. by constructor. Defined.

Section proofmode.
  Context `{interpGS Σ}.

(* ------------------------------------------------------------------------ *)
(* Instances for values. *)

Global Instance val_instance (A:Type) (EA:Enc A) E (v:val) (Φ:A -> vProp Σ):
  HINT1 empty_hyp_first ✱ [post Φ v] ⊫ [id]; wpm E v Φ.
Proof.
  iStepS. rewrite post_eq. iDestruct "H1" as "[% (->&?)]".
  iApply wpm_val. reflexivity. iFrame.
Qed.

Global Instance post_val_instance (v:val) (Φ:val -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ v] ⊫ [id]; post Φ v.
Proof. rewrite post_val. iStepsS. Qed.
Global Instance post_nat_instance  (v:Z) (Φ:Z -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ v] ⊫ [id]; post Φ v.
Proof. rewrite post_int. iStepsS. Qed.
Global Instance post_loc_instance (v:loc) (Φ:loc -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ v] ⊫ [id]; post Φ v.
Proof. rewrite post_loc. iStepsS. Qed.
Global Instance post_unit_instance (Φ:unit -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ tt] ⊫ [id]; post Φ VUnit.
Proof. rewrite post_unit. iStepsS. Qed.
Global Instance post_unit_instance' (Φ:unit -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ tt] ⊫ [id]; post Φ ().
Proof. rewrite post_unit. iStepsS. Qed.
Global Instance post_enc_instance (A:Type) (EA:Enc A) a (Φ:A -> vProp Σ):
  HINT1 empty_hyp_first ✱ [Φ a] ⊫ [id]; post Φ (enc a).
Proof. rewrite post_eq. iStepsS. Qed.

(* We can always rather prove a fill_items_val (that do a substitution on
   leaves if possible) than a fill_items *)
Lemma wpm_ctx_impl (A:Type) (EA:Enc A) E ks v (Q:A -> vProp Σ) :
  wpm E (fill_items_val ks v) Q ⊢ wpm E (fill_items ks v) Q.
Proof. constructor. iIntros. iIntros (?) "?". iApply wp_ctx_wp. iSteps. Qed.

(* [wpm_conv E ks CO v Q] implies [wpm E (fill_items ks (enc v)) Q],
   if [ks = nil], then rather implies [Q v], avoiding some steps.
   Features a dependent pattern-matching! *)
Definition wpm_conv `{EA:Enc A} `{EB:Enc B} E (ks:list ctx) (CO:Convertible_if ks A EA B EB) (v:A) (Q:B -> vProp Σ) :=
  match ks,CO with
  | nil,CO => Q (@conv A EA B EB (Convertible_if_nil _ _ _ _ _ _ CO) v)
  | _,_ => wpm E (fill_items_val ks (enc v)) Q end.

Lemma wpm_conv_impl (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E (ks:list ctx) (CO:Convertible_if ks A EA B EB) (v:A) (Q:B -> vProp Σ) :
  wpm_conv E ks CO v Q ⊢ wpm E (fill_items ks (enc v)) Q.
Proof.
  iIntros "Hwp". iApply wpm_ctx_impl.
  destruct ks; try done. iApply wpm_val; last iFrame. rewrite conv_enc //.
Qed.

Global Instance mono_instance (A:Type) (EA:Enc A) (B:Type) (EB:Enc B)
  E e ks e' (Q1:B -> vProp Σ) (Q2:A -> vProp Σ) (CO:Convertible_if ks A EA B EB) :
  ReshapeExpr e ks e' ->
  HINT1 (wpm E e' Q2) ✱
    [∀ v, Q2 v -∗ wpm_conv E ks CO v Q1]
    ⊫ [id]; wpm E e Q1.
Proof.
  intros [->]. iStep.
  iApply wpm_binds. iApply (wpm_mono_val with "[$]").
  iIntros. iApply wpm_conv_impl. iSteps.
Qed.


Global Instance load_instance (A:Type) (EA:Enc A) E e (ks:list ctx) (l:loc) (i:Z) (Q:A -> vProp Σ) :
  ReshapeExpr e ks (Load l i) ->
  HINT1 empty_hyp_first ✱ [∃ q (vs:list val) (v:val), l ↦{q} vs ∗ ⌜0 <= i < length vs⌝%Z ∗ ⌜(vs !! Z.to_nat i) = Some v⌝ ∗ (l ↦{q} vs  -∗ wpm E (fill_items_val ks (enc v)) Q)] ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds.
  unshelve iPoseProof (wpm_load val with "[$]") as "?"; eauto. apply _. eauto.
  iApply (wpm_mono_val with "[$]"). iStep 2. iApply wpm_ctx_impl. iSteps.
Qed.


Global Instance let_val_instance (A:Type) (EA:Enc A) E (x:binder) v e' (Q:A -> vProp Σ):
  HINT1 empty_hyp_first ✱ [wpm E (subst' x v e') Q] ⊫ [id]; wpm E (Let x (Val v) e') Q.
Proof. iSteps. by iApply wpm_let_val. Qed.

Global Instance closure_instance (A:Type) (EA:Enc A) E e (ks:list ctx) clo (Q:A -> vProp Σ) (CO:Convertible_if ks loc Enc_loc A EA) :
  ReshapeExpr e ks (Clo clo) ->
  HINT1 empty_hyp_first ✱ [(∀ (l:loc), Func l clo ∗ embed (meta_token l (⊤ ∖ ↑dislog_nm)) -∗ wpm_conv E ks CO l Q)]
  ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds.
  iApply wpm_mono_val. iApply wpm_closure. iStep 2.
  iApply wpm_conv_impl. iSteps.
Qed.

Global Instance length_instance (A:Type) (EA:Enc A) E e ks (l:loc) (Q:A -> vProp Σ) (CO:Convertible_if ks Z Enc_int A EA) :
  ReshapeExpr e ks (Length l) ->
  HINT1 empty_hyp_first ✱ [(∃ q (vs:list val), l ↦{q} vs ∗ (l ↦{q} vs  -∗ wpm_conv E ks CO (length vs) Q))]  ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds.
  iPoseProof (wpm_length with "H1") as "?".
  iSteps. iApply wpm_conv_impl. iSteps.
Qed.

Global Instance call_prim_instance (A:Type) (EA:Enc A) E e ks p (v1 v2:val) (Q:A -> vProp Σ) :
  ReshapeExpr e ks (CallPrim p v1 v2) ->
  HINT1 empty_hyp_first ✱ [∃w, ⌜head_semantics.eval_call_prim p v1 v2 = Some w⌝ ∗wpm E (fill_items_val ks w) Q]  ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds.
  iApply wpm_mono. iApply wpm_call_prim. eauto.
  iSteps. iApply wpm_ctx_impl. iSteps.
Qed.

Lemma wpm_call_clo_tac (A:Type) (EA:Enc A) E self args body ts v (Q:A -> vProp Σ) :
  forallb is_val ts →
  length args = length ts →
  Func v (Lam self args body) ∗
  ▷(wpm E (substs' (zip (self::args) (v::(to_val_force <$> ts))) body) Q) -∗
  wpm E (Call v ts) Q.
Proof.
  iIntros (??) "(?&?)".
  iApply (wpm_call with "[$]"); eauto.
  { rewrite fmap_length //. }
  { rewrite fmap_to_val_force //. }
Qed.


Global Instance call_clo_instance (A:Type) (EA:Enc A) E self args body ts (l:loc) (Q:A -> vProp Σ) :
  HINT1  empty_hyp_first ✱ [Func l (Lam self args body) ∗ ⌜forallb is_val ts /\ length args = length ts⌝ ∗ ▷(wpm E (substs' (zip (self::args) (VLoc l::(to_val_force <$> ts))) body) Q)]
  ⊫ [id]; wpm E (Call l ts) Q.
Proof. iSteps. iApply wpm_call_clo_tac; eauto. Qed.

Global Instance store_instance (A:Type) (EA:Enc A) E e ks (l:loc) (i:Z) (v:val) (Q:A -> vProp Σ) (CO:Convertible_if ks unit Enc_unit A EA) :
  ReshapeExpr e ks (Store l i v) ->
  HINT1 empty_hyp_first ✱ [∃ (vs:list val), l ↦ vs ∗ ⌜0 <= i < length vs⌝%Z ∗ (l ↦ (<[Z.to_nat i:=v]> vs) -∗ wpm_conv E ks CO tt Q)]
    ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds. iApply (wpm_mono_val with "[H1]").
  iDestruct (wpm_store with "[$]") as "?"; eauto. iIntros ([]). iSteps.
  iApply wpm_conv_impl. iSteps.
Qed.

Global Instance alloc_instance (A:Type) (EA:Enc A) E e ks (i:Z) (v:val) (Q:A -> vProp Σ) (CO:Convertible_if ks loc Enc_loc A EA):
  ReshapeExpr e ks (Alloc i v) ->
  HINT1 empty_hyp_first ✱ [⌜(0 <= i)%Z⌝ ∗ (∀ (l:loc), l ↦ (replicate (Z.to_nat i) v) ∗ embed (meta_token l (⊤ ∖ ↑dislog_nm)) -∗ wpm_conv E ks CO l Q)]
  ⊫ [id]; wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds. iApply wpm_mono_val. iApply wpm_alloc. eauto.
  iSteps. iApply wpm_conv_impl. iSteps.
Qed.

Global Instance if_instance (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E e ks (b:bool) e1 e2 (Q:A -> vProp Σ) (CO:Convertible_if ks B EB A EA):
  ReshapeExpr e ks (if: b then e1 else e2) ->
  HINT1 empty_hyp_first ✱ [wpm E (if b then e1 else e2) (λ v:B, wpm_conv E ks CO v Q)]
  ⊫ [id];  wpm E e Q.
Proof.
  intros [->]. iStep. iApply wpm_binds. iApply wpm_if. iSteps.
  iApply wpm_conv_impl. iSteps.
Qed.

End proofmode.
