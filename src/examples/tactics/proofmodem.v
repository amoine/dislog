From dislog.examples Require Export proofmode_base.
From iris.proofmode Require Import coq_tactics tactics reduction spec_patterns intro_patterns.

From dislog.logic Require Export wpm conversions.

From dislog.examples Require proofmode_base proofmode.
From dislog.examples Require Export diaframe.

(******************************************************************************)
(* [Convertible] *)

Lemma wpm_convertible `{interpGS Σ} (B:Type) `{Convertible B A} E e (Q:A -> vProp Σ) :
  wpm E e (fun (b:B) => Q (conv b)) -∗
  wpm E e Q.
Proof.
  apply bi.entails_wand.
  constructor. iIntros (?) "Hwp". iIntros (?). iIntros.
  iSpecialize ("Hwp" with "[$]").
  rewrite !wp.wp_eq.
  iIntros. iApply (wpg.wpg_mono with "[$]").
  iIntros.
  iApply (@post_convertible _ _ B).
  iFrame.
Qed.

(******************************************************************************)
(* Toward [wpm_bind]: goes bellow exactly one eval. context. *)

Lemma wpm_let `{interpGS Σ} (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E x (e1 e2:expr) (Q:A -> vProp Σ) :
  wpm E e1 (fun (v:B) => wpm E (subst' x (enc v) e2) Q) -∗
  wpm E (Let x e1 e2) Q.
Proof.
  iIntros "Hwp".
  iApply (@wpm_bind _ _ _ _ B _ _ _ (CtxLet x e2) Q).
  iApply (wpm_mono with "Hwp").
  iIntros. iApply wpm_let_val. iFrame.
Qed.

Ltac wpm_bind typ :=
  lazymatch goal with
  | |- envs_entails _ (wpm _ ?e ?Q) =>
      lazymatch type of Q with
      | ?typer -> _=>
      let ctx := infer_ctx e in
      lazymatch ctx with
      | CtxLet _ _ => iApply (@wpm_let _ _ typer _ typ _)
      | ?K =>
          iApply (@wpm_bind _ _ typer _ typ _ _ _ K) end end end.

Tactic Notation "wpm_bind" constr(t) := wpm_bind t.
Tactic Notation "wpm_bind" := wpm_bind val.

(******************************************************************************)
(* [alloc] *)

Tactic Notation "wpm_alloc" "as" ident(l) constr(Hpost) := unshelve iStep 2; iIntros (l) Hpost.
Tactic Notation "wpm_alloc" "as" ident(l) := unshelve iStep 2; iIntros (l) "(?&_)".
Tactic Notation "wpm_alloc" := unshelve iStep 2; iIntros (?) "(?&_)".

(******************************************************************************)
(* [clo] *)

Tactic Notation "wpm_clo" "as" ident(l) constr(Hpost) := iStep; iIntros (l) Hpost.
Tactic Notation "wpm_clo" "as" ident(l) := iStep; iIntros (l) "#?".
Tactic Notation "wpm_clo" := iStep 3.

(******************************************************************************)
(* [load] *)

Ltac solve_arith := try first [now vm_compute| now lia].

Lemma wpm_load_tac `{interpGS Σ} (A:Type) (EA:Enc A) E bs q (i:Z) (a:A) (l:loc) (Q:A -> vProp Σ) :
  (0 <= i < Z.of_nat (length bs))%Z ->
  bs !! (Z.to_nat i) = Some (enc a) ->
  l ↦{q} bs ∗ (l ↦{q} bs -∗ Q a) -∗
  wpm E (Load l i) Q.
Proof. iSteps. Qed.

Ltac wpm_load_base tac :=
  unshelve (iApply @wpm_load_tac); last (tac tt); only 3:reflexivity; only 1: solve_arith; rew_enc.

Tactic Notation "wpm_load" constr(Hyp) :=
  wpm_load_base ltac:(fun _ => iFrame Hyp).
Tactic Notation "wpm_load" :=
  wpm_load_base ltac:(fun _ => iFrame).

(******************************************************************************)
(* [call] *)

Lemma wpm_call_code_tac `{interpGS Σ} (A:Type) (EA:Enc A) E self args body ts (Q:A -> vProp Σ) :
  forallb is_val ts →
  length args = length ts →
  locs body = ∅ ->
  ▷(wpm E (substs' (zip (self::args) (VCode (Lam self args body)::(to_val_force <$> ts))) body) Q) -∗
  wpm E (Call (VCode (Lam self args body)) ts) Q.
Proof.
  iIntros.
  iApply wpm_call_clo_tac; eauto.
  iSplitR. 2:iFrame. rewrite Func_eq. rewrite interp.Func_eq. iPureIntro. split; eauto.
Qed.

Definition is_VCode e :=
  match e with
  | Val (VCode _) => true
  | _ => false end.

Ltac wpm_call_code tac :=
  iApply @wpm_call_code_tac;
  [ try (now vm_compute) | try (now vm_compute) | tac tt | sanitize; iModIntro ].

Ltac wpm_call_tac :=
  lazymatch goal with
  | |- envs_entails _ (wpm _ (Call ?e _) _) =>
      let b := eval vm_compute in (is_VCode e) in
        lazymatch b with
        | true => wpm_call_code ltac:(fun _ => set_solver)
        | false => iStep 4 end end.

Tactic Notation "wpm_call" :=
  wpm_call_tac.

(******************************************************************************)
(* [par] *)

Lemma wpm_par_tac `{interpGS Σ} (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) (C:Type) (EC:Enc C) `(CO:@Convertible loc Enc_loc C EC) E  (e1 e2:expr) (Q1:A -> vProp Σ) (Q2:B -> vProp Σ) (Q:C -> vProp Σ):
  wpm E e1 Q1 ∗ wpm E e2 Q2 ∗
  (∀ l (v1:A) (v2:B),
       l ↦ [enc v1;enc v2] ∗ Q1 v1 ∗ Q2 v2 -∗ Q(conv l)) -∗
  wpm E (par e1 e2) Q.
Proof.
  iIntros "(H1&H2&HP)".
  iApply (@wpm_convertible _ _ loc Enc_loc _ _ CO).
  iApply (wpm_mono with "[H1 H2]").
  { iApply (wpm_par with "H1 H2"). }
  iSmash.
Qed.

Ltac wpm_par e1 e2 :=
  unshelve iApply (@wpm_par_tac _ _ e1 _ e2 _); [apply _ | shelve | shelve | sanitize ].

(******************************************************************************)
(* [wpm_call_prim] *)

Ltac wpm_call_prim := iStep 3.
Ltac wpm_if := iStep.
Ltac wpm_val := iStep 2.
Ltac wpm_store := iStep 4.
Ltac wpm_length := iStep 3.

(******************************************************************************)
(* [cas] *)

Lemma wpm_cas_tac `{interpGS Σ} `(CO:@Convertible bool Enc_bool A EA) E bs (l:loc) (i:Z) (v1 v1' v2:val) q (Q:A -> vProp Σ) :
  (v1=v1' -> q=DfracOwn 1%Qp) ->
  (0 ≤ i < length bs)%Z ->
  bs !! Z.to_nat i = Some v1' ->
  l ↦{q} bs ∗  (l ↦{q} (if decide (v1=v1') then <[Z.to_nat i:=v2]> bs else bs) -∗ Q (conv (bool_decide (v1=v1')))) -∗
  wpm E (CAS l i v1 v2) Q.
Proof.
  iSteps. iDestruct (wpm_cas with "[$]") as "?"; eauto.
  iSteps.
Qed.

(******************************************************************************)
(* [wpm_apply] *)

Lemma wpm_apply `{interpGS Σ} ks e (A:Type) (EA:Enc A) (B:Type) (EB:Enc B) E P (Q':A -> vProp Σ) (Q:B -> vProp Σ) (CO:Convertible_if ks A EA B EB):
  (P -∗ wpm E e Q') -∗ P ∗ (∀ v, Q' v -∗ wpm_conv E ks CO v Q)%I -∗
  wpm E (fill_items ks e) Q.
Proof.
  iIntros "H1 H2". iApply wpm_binds. iSteps.
  iApply wpm_conv_impl. iSteps.
Qed.

Ltac wpm_apply H :=
  lazymatch goal with
  | |- envs_entails _ (wpm _ ?e _) =>
      reshape_expr e ltac:(fun ks e' =>  unshelve iApply (wpm_apply ks e');
  only 6:first [iApply H | iIntros; by iApply H];
  first apply _;
  last (unfold wpm_conv; simpl; iFrame "∗#"; rew_enc))
  end.

Tactic Notation "wpm_apply" uconstr(H) := wpm_apply H.
