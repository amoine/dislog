From iris.prelude Require Import prelude.
From dislog.lang Require Import notation.
From dislog.logic Require Import wonly.
From dislog.examples Require Import proofmodem parfor stdlib big_opN.

Definition lookup : val :=
  λ: [["k", "n"]],
    let: "r" := alloc 1 VUnit in
    let: "h" :=
      λ: [["i"]],
        let: "x" := "k" [["i"]] in
        if: "x" '== VUnit then VUnit else "r".[0] <- "x" in
    parfor [[0, "n", "h"]];;
    "r".[0].

Section lookup.
Context `{interpGS Σ, !wonlyG val Σ}.

Definition is_unit v :=
  match v with
  | VUnit => true
  | _ => false end.

Lemma mapsto_wom_cancel q γ l n (PS : nat -> gset val) :
  n ≠ 0 ->
  ([∗ nat] i ∈ [0; n], l ⟾{γ,(1/q)} (PS i) ) -∗
  ∃ S : gset val,
   ⌜∀ x, x ∈ S <-> ∃i, (i < n) /\ x ∈ PS i⌝ ∗ l ⟾{γ,(nat_to_Qp n/q)} S.
Proof.
  iIntros (Hn) "Ho".
  iInduction n as [|] "IH". done.
  destruct n.
  { iExists (PS 0).
    iSplitR.
    { iPureIntro. intros. split. naive_solver. intros (i,(?&?)). assert (i=0) by lia. naive_solver. }
    iClear "IH". rewrite big_sepN_snoc //. iDestruct "Ho" as "(_&?)".
    replace (nat_to_Qp 1) with 1%Qp by compute_done. iFrame. }
  { rewrite (big_sepN_snoc _ (S n)); last lia. iDestruct "Ho" as "(Ho&?)".
    iDestruct ("IH" with "[%][$]") as "[%X (%HX&?)]". done.
    iExists (PS (S n) ∪ X). iSplitR.
    { iPureIntro. intros. rewrite elem_of_union. split.
      { intros []. naive_solver. naive_solver. }
      { intros (i,(?&?)). destruct_decide (decide (i= S n)). naive_solver. right. apply HX.
        exists i. split; first lia. eauto. } }
    rewrite (nat_to_Qp_succ (S n)) //  Qp.div_add_distr.
    iApply mapsto_wo_frac. iFrame. }
Qed.

Definition found n vs v :=
  length vs = n /\ if is_unit v then Forall is_unit vs else v ∈ vs.

Lemma lookup_spec (k:val) (Q:val -> vProp Σ) n :
  ([∗ nat] i ∈ [0;n], wpm ⊤ (k [[i]])%T Q) -∗
  wpm ⊤ (lookup [[k, n]])%T
    (fun v => ∃ vs, ⌜found n vs v⌝ ∗ [∗ list] v ∈ vs, Q v).
Proof.
  iIntros "Hwps".
  wpm_call. iSteps as (l find) "X1 X2 #Hfind H6". iClear "H6".
  iMod (mapsto_wo_start nroot with "[$]") as "[%γ (#?&?)]".

  destruct n.
  { wpm_apply (parfor_specm _ _ _ _ (fun _ => True)%I). lia.
    rewrite !big_sepN_0; try lia. rewrite left_id.
    iIntros ([]) "_". simpl.
    iMod (mapsto_wo_cancel with "[$]"). set_solver. wpm_load. iIntros.
    iExists nil. rewrite big_sepL_nil right_id.
    iPureIntro. split; naive_solver. }

  rewrite (fractional_split_n_times (fun q => l ⟾{γ,q} ∅)%I (S n)); last lia.

  iDestruct (big_sepN_sep with "[$]") as "?".
  pose (P:= fun (i:nat) => (∃ (v:val), l ⟾{γ,(1 / nat_to_Qp (S n))} (if is_unit v then ∅ else {[v]}) ∗ Q v)%I).
  wpm_apply (parfor_specm _ _ _ _ P). lia.
  iSplitL.
  { iApply (big_sepN_impl with "[$]"). iModIntro. iIntros (??) "(?&?)".
    iSteps. rew_enc. case_bool_decide.
    { wpm_val. iExists _. subst. iFrame. }
    { wpm_apply wpm_store_wo. set_solver. iIntros. iExists _. iFrame. destruct x; done. } }

  iIntros ([]) "Hpost". subst P. simpl.
  rewrite big_sepN_exists_from_zero. iDestruct "Hpost" as "[%xs (%Hlxs&Hpost)]".
  rewrite big_sepN_sep. iDestruct "Hpost" as "(H1&H2)".
  iDestruct (big_sepN_to_list with "[$]") as "?". done.

  iDestruct (mapsto_wom_cancel with "[$]") as "[%X (%HX&?)]". done.
  rewrite Qp.div_diag.
  destruct_decide (decide (X=∅)).
  { subst.
    iMod (mapsto_wo_cancel with "[$]") as "?". set_solver.
    iSteps. iPureIntro.
    apply Forall_forall. intros x Hx. apply elem_of_list_lookup_1 in Hx. destruct Hx as (i,Hi).
    specialize (HX x). destruct (is_unit x) eqn:Hu. done.
    { exfalso. eapply elem_of_empty with x. apply HX. exists i. rewrite list_lookup_total_alt Hi Hu. simpl.
      apply lookup_lt_Some in Hi. split; first lia. set_solver. } }
  { iMod (mapsto_wo_end with "[$]") as "[% (%&?)]". set_solver. done.
    iSteps. iPureIntro.
    apply HX in H1. destruct H1 as (i,(Hi&Hxs)). rewrite list_lookup_total_alt in Hxs.
    rewrite -Hlxs in Hi. apply lookup_lt_is_Some_2 in Hi. destruct Hi as (x&Hi). rewrite Hi in Hxs.
    simpl in *. destruct (is_unit x) eqn:Hux. set_solver.
    apply elem_of_singleton in Hxs. subst. rewrite Hux. by eapply elem_of_list_lookup_2. }
  Unshelve. all:apply _.
Qed.

End lookup.
