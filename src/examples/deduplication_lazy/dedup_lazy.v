From iris.prelude Require Import prelude.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.
From iris.algebra Require Import dfrac.
From iris.bi.lib Require Import fractional.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmodem big_opN stdlib parfor filter_compact.
From dislog.examples.deduplication Require Import dedup hashset.
From dislog.examples.deduplication_lazy Require Import lhashset.

(* [dedup_pre] launches [p+1] tasks, the [p] first handling [n/p]
   elements, while the last one handles [n mod p] elements.

   [dedup_pre] is proven to return an array containing the elements
   produced by [f] between indices [0] and [n], plus the dummy elements [d].
   Their multiplicity is not specified.

   [d] is an element that will _never_ be produced by [f].
   [h] is the hashing function
*)

Definition dedup_pre : val :=
  λ: [["h", "p", "d", "k", "n"]],
    let: "t" := hs_init [["n", "d"]] in
    let: "interval" := "n" '/ "p" in
    let: "task" :=
      λ: [["tid"]],
        let: "subtask" := λ: [["i"]],
            let: "x" := "k" [["i"]] in
            hs_add [["t", "n", "d", "h", "tid", "x"]] in
        let: "i" := "interval" '* "tid" in
        let: "j" := if: "tid" '== "p" then "n" else "interval" '* ("tid" '+ 1) in
        seqfor [["i", "j", "subtask"]] in
    parfor [[0, "p" '+ 1, "task"]];;
    hs_support [["t"]].

Definition dedup_lazy : val :=
  λ: [["h", "p", "d", "k", "n"]],
    let: "a" := dedup_pre [["h", "p", "d", "k", "n"]] in
    let: "a'" := filter_compact [["a", "d"]] in
    dedup [["h", "d", "a'"]].

Section dedup_spec.

Context `{interpGS Σ, hashset.hashsetG Σ, hashsee2G Σ}.
Context (N:namespace).

Lemma list_to_set_concat `{Countable A} (xs:list (list A)) :
  (list_to_set (concat xs) : gset A) = ⋃ (list_to_set <$> xs).
Proof.
  induction xs.
  { rewrite concat_nil fmap_nil list_to_set_nil //. }
  { rewrite concat_cons list_to_set_app_L fmap_cons union_list_cons IHxs //. }
Qed.

(* don't ask me why it holds *)
Lemma conclusion_aux k xs r p (Q:nat -> val -> vProp Σ) :
  (∀ i, 0 <= i < p → length (xs !!! i) = r) ->
  length xs = S p ->
  ([∗ nat] i ∈ [0; S p], [∗ list] j↦v ∈ (xs !!! i), Q (k + r * i + j) v) -∗
  [∗ list] i↦v ∈ concat xs, Q (k+i) v.
Proof.
  iIntros (Hforall Hxs). iIntros "Hl".
  iInduction p as [|] "IH" forall (k xs Hxs Hforall).
  { destruct xs; simpl in *; try lia. destruct xs; last easy. simpl.
    rewrite !right_id. rewrite big_sepN_S; last lia.
    iDestruct "Hl" as "(?&_)". rewrite Nat.mul_0_r Nat.add_0_r. iFrame. }
  destruct xs; first easy. simpl.
  iApply big_sepL_app.
  rewrite big_sepN_S; last lia.
  iDestruct "Hl" as "(H1&?)".
  iSplitL "H1".
  { simpl. iApply (big_sepL_mono with "[$]"). intros. f_equiv. lia. }
  rewrite (big_opN_reindex_sub _ _ _ 1); last lia. simpl.
  iSpecialize ("IH" $! (k+r) xs  with "[%][%][-]").
  { simpl in *. lia. }
  { intros. specialize (Hforall (S i)).
    rewrite Hforall; lia. }
  { iApply (big_sepN_mono with "[$]"). iIntros.
    rewrite Nat.add_1_r. simpl.
    iApply (big_sepL_mono with "[$]"). intros.
    f_equiv. lia. }
  iApply (big_sepL_mono with "[$]"). intros. f_equiv.
  specialize (Hforall 0). simpl in *. rewrite Hforall; lia.
Qed.

Lemma conclusion xs p r (Q:nat -> val -> vProp Σ) :
  (∀ i, 0 <= i < p → length (xs !!! i) = r) ->
  length xs = S p ->
  ([∗ nat] i ∈ [0; S p], [∗ list] j↦v ∈ (xs !!! i), Q (r * i + j) v) -∗
  [∗ list] i↦v ∈ concat xs, Q i v.
Proof. apply (conclusion_aux 0). Qed.

Lemma length_concat {A:Type} (xs:list (list A)) p n :
  length xs = p ->
  (forall i, 0 <= i < p → length (xs !!! i) = n) ->
  length (concat xs) = p * n.
Proof.
  revert xs; induction p; intros xs Hxs Hf.
  all:destruct xs as [|x xs]; try done; simpl.
  rewrite app_length (Hf 0); last lia.
  rewrite IHp //.
  { simpl in *. lia. }
  { intros. specialize (Hf (S i)). simpl in *. lia. }
Qed.

Lemma length_concat_result {A:Type} (xs:list (list A)) p n :
  p ≠ 0 ->
  length xs = S p ->
  (forall i, 0 <= i < S p → length (xs !!! i) = (if decide (i = p) then n `mod` p else n `div` p)) ->
  length (concat xs) = n.
Proof.
  intros ? Hl Hf.
  assert (xs ≠ nil) as Hxs by (now destruct xs).
  apply last_is_Some in Hxs. destruct Hxs as (x,Hxs).
  apply last_Some in Hxs. destruct Hxs as (xs',Hxs). subst.
  rewrite concat_app app_length. simpl. rewrite right_id.
  rewrite app_length in Hl. simpl in *.
  rewrite (length_concat _ p (n `div` p)); try lia.
  { specialize (Hf p). rewrite lookup_total_app_r in Hf; last lia.
    replace (p - length xs') with 0 in Hf by lia. rewrite decide_True // in Hf.
    rewrite Hf; last lia. symmetry. apply Nat.div_mod_eq. }
  { intros. specialize (Hf i). rewrite lookup_total_app_l in Hf; last lia.
    rewrite Hf; try lia. rewrite decide_False; lia. }
Qed.

Lemma dedup_pre_spec (k h:loc) (d:val) (p n:nat) (hash:val -> nat) (Q:nat -> val -> vProp Σ) :
  (* We must run on more than one thread *)
  p ≠ 0 ->
  (* [h] is a hashing function *)
  □(∀ (x:val), wpm (↑N) (h [[x]])%T (fun v => ⌜v = VInt (hash x)⌝)) -∗
  (* [f] is a producer *)
  ([∗ nat] i ∈ [0;n], wpm (↑N) (k [[i]])%T (fun v => ⌜v≠d⌝ ∗ Q i v)) -∗
  wpm (↑N) (dedup_pre [[h,p,d,k,n]])%T
  (fun (l:loc) => ∃ xs vs, ([∗ list] i ↦ v ∈ vs, Q i v) ∗ l ↦ xs ∗ ⌜length vs = n /\ tie d (list_to_set vs) xs⌝).
Proof.
  iIntros (Hp) "#Hh Hwp".
  wpm_call.

  wpm_bind.
  iApply (wpm_mono_val with "[]").
  iApply (@hs_init_spec _ _ _ N n d (S p) hash). lia.
  iSteps. rew_nat2z_inj.

  rewrite (big_opN_factorize'' _ n p).
  iDestruct (big_sepN_sep with "[$]") as "Hyp".

  rewrite Nat.add_1_r.
  wpm_apply (parfor_specm _ _ _ _ (fun i => ∃ vs, ⌜length vs = if decide (i=p) then n `mod` p else n/p⌝ ∗ lhashset N _ _ i (∅ ∪ list_to_set vs) ∗ [∗ list] j ↦ v ∈ vs, Q ((n/p)*i+j) v))%I. lia.
  iSplitL "Hyp".
  { iApply (big_sepN_impl with "[$]").
    iModIntro. iIntros (i Ht) "(?&Hwp)".
    iSteps. rew_nat2z_inj.

    iAssert (wpm (↑N)
    (if bool_decide ((^i)%V = (^p)%V) then (^n)%V else
     CallPrim (PrimIntOp IntMul) (^(n `div` p)%nat)%V
       (CallPrim (PrimIntOp IntAdd) (^i)%V (^1%nat)%V)) (fun (x:Z) => ⌜x=if decide (i=p) then n else ((n/p) * (i+1))%Z⌝)%I) as "Hwpm".
    {  case_bool_decide as Hx; iSteps; iPureIntro.
      { rewrite decide_True //. }
      { rewrite decide_False; last naive_solver. rew_nat2z_inj. lia. } }

    iApply (wpm_mono_val with "[$]").
    iIntros (?) "->". simpl. rew_enc.
    iClear "Hwpm".

    replace (Zpos xH : Z) with (Z.of_nat 1) by lia. rew_nat2z_inj.

    destruct_decide (decide (n=0)).
    { subst. rewrite Nat.Div0.mod_0_l // Nat.Div0.div_0_l // !Nat.mul_0_l.
      replace (if decide _ then _ else _) with 0 by (case_decide; lia).
      replace (if @decide (@eq nat i p) (@decide_rel nat nat (@eq nat) Nat.eq_dec i p) then  Z.of_nat O else Z.of_nat O) with (Z.of_nat 0%nat) by (case_decide; lia).
      iApply @seqfor_spec. iExists nil. rewrite big_sepL_nil. rewrite right_id.
      rewrite list_to_set_nil. rewrite left_id_L. by iFrame. }

    rename x into v.
    iAssert (∀ x g ri np, ⌜x = {| capa := n; empt := d; hash := hash; nb_threads := S p |} ⌝ -∗ lhashset N v x i g -∗
      ([∗ nat] x ∈ [0; np],
          wpm (↑N) ((#k)%V [[(^(x + ri)%nat)%V]])%T
            (λ v0 : val, ⌜v0 ≠ d⌝ ∗ Q (x + ri) v0)) -∗
     wpm (↑N) (seqfor [[(^ri)%V, (^(ri + np)%nat)%V, (#_)%V]])%T
    (λ _ : (),
       ∃ vs : list val, ⌜length vs = np⌝ ∗ lhashset N _ x i (g ∪ list_to_set vs) ∗
         ([∗ list] j↦v0 ∈ vs, Q (ri + j) v0)))%I as "Hseqfor".
    { iIntros (????) "% Hhs Hwp".
      iInduction np as [|] "IH" forall (g ri).
      { iApply seqfor_spec. rewrite decide_True; last lia.
        iExists nil.
        rewrite list_to_set_nil big_sepL_nil //. rewrite right_id_L.
        iFrame.
        iSteps. }
      iApply seqfor_spec. rewrite decide_False; last lia.
      rewrite (big_sepN_S 0 (S np)); last lia.
      iDestruct "Hwp" as "(?&Hwp)". simpl.
      wpm_call. iSteps.
      wpm_apply (hs_add_spec _ _).
      { lia. }
      1-3:naive_solver.
      { iApply "Hh". }
      iIntros ([]) "?".
      rewrite (big_opN_reindex_sub _ 1 _ 1); last lia. simpl.
      rewrite Nat.sub_0_r.
      iSpecialize ("IH" $! _ (S ri) with "[$][Hwp]").
      { iApply (big_sepN_mono with "[$]").
        intros. replace (i0 + 1 + ri) with (i0+S ri) by lia. easy. }
      rewrite Nat.add_1_r. replace (S ri + np) with (ri + S np) by lia.
      iStep. iIntros ([]) "[%xs (%&?&?)]".
      iExists (_::xs). rewrite big_sepL_cons.
      rewrite Nat.add_0_r list_to_set_cons assoc_L (comm_L _ g). iFrame.
      iSteps. iApply (big_sepL_mono with "[$]"). intros. f_equiv. lia. }


    case_decide.
    { subst i.
      rewrite (big_opN_reindex_sub _ _ _ (p * (n / p))).
      2:{ split; try lia. apply Nat.Div0.mul_div_le. }
      replace (p * n `div` p - p * n `div` p) with 0 by lia.
      replace (n - p * n `div` p) with (n `mod` p).
      2:{ rewrite {2}(Nat.div_mod_eq n p). lia. }
      rewrite Nat.mul_comm.
      assert (n=n `div` p * p + n `mod` p ) as Hn.
      { rewrite {1}(Nat.div_mod_eq n p). lia. }
      rewrite {12}Hn. clear Hn.
      iApply ("Hseqfor" with "[%//][$][$]"). }
    { rewrite (big_opN_reindex_sub _ _ _ ((n / p)*i)); last lia.
      replace ((n/p) * i - (n/p) * i) with 0 by lia.
      replace ((n/p) * (i + 1) - (n/p) * i) with (n/p) by lia.
      generalize (∅ : gset val). intros. replace (Zpos xH : Z) with (Z.of_nat 1) by lia. rew_nat2z_inj.
      rewrite Nat.mul_add_distr_l Nat.mul_1_r.
      iApply ("Hseqfor" with "[%//][$][$]"). } }

  iIntros ([]) "Hpost". rewrite big_sepN_exists.
  iDestruct "Hpost" as "[%xs (%Hxs&Hpost)]".
  rewrite !big_sepN_sep. rewrite big_sepN_pure.
  iDestruct "Hpost" as "(%Hforall&Hp1&Hp2)".

  iAssert ([∗ nat] i ∈ [0; S p], lhashset N _ _ i ((list_to_set <$> xs) !!! i))%I with "[Hp1]" as "Hp1".
  { iApply (big_sepN_mono with "[$]"). intros. rewrite left_id_L.
    rewrite Nat.sub_0_r. rewrite -list_lookup_total_fmap //. lia. }

  wpm_apply hs_support_spec.
  1-2:naive_solver.
  { rewrite fmap_length. lia. }

  iIntros (l) "[%vs (?&%Htie)]". simpl.
  iExists vs, (List.concat xs). iFrame.
  iSplitL; last first.
  { iPureIntro. split.
    { eapply length_concat_result. done. lia. intros. apply Hforall in H2. rewrite Nat.sub_0_r // in H2. }
    { rewrite list_to_set_concat //. } }
  { iApply (conclusion _ _ (n/p) with "[Hp2]"); eauto.
    { intros z Hx. specialize (Hforall z).
      rewrite Nat.sub_0_r decide_False in Hforall; last lia.
      rewrite -Hforall; lia. }
    { iApply (big_sepN_mono with "[$]"). iIntros.
      rewrite !Nat.sub_0_r //. } }
Qed.

Lemma elem_of_filter_pure e x xs :
  x ∈ filter_pure e xs <-> x ≠ e /\ x ∈ xs .
Proof. rewrite /filter_pure elem_of_list_filter. naive_solver. Qed.

Lemma pure_dedup e vs xs xs' :
  tie e (list_to_set vs) xs ->
  deduped xs' (list_to_set (filter_pure e xs))  ->
  deduped xs' (list_to_set vs).
Proof.
  intros [He1 He2] [HD1 HD2].
  constructor. eauto.
  intros x. rewrite HD2 elem_of_list_to_set elem_of_filter_pure.
  naive_solver.
Qed.

Lemma dedup_lazy_spec (h k:loc) (d:val) (p n:nat) (hash:val -> nat) (Q:nat -> val -> vProp Σ) :
  p ≠ 0 ->
  □(∀ (x:val), wpm (↑N) (h [[x]])%T (fun v => ⌜v = VInt (hash x)⌝)) -∗
  ([∗ nat] i ∈ [0;n], wpm (↑N) (k [[i]])%T (fun v => ⌜v≠d⌝ ∗ Q i v)) -∗
  wpm (↑N) (dedup_lazy [[h,p,d,k,n]])%T
  (fun l => ∃ vs ws, ⌜length vs = n /\ deduped ws (list_to_set vs)⌝ ∗ l ↦ ws ∗ ([∗ list] i ↦ v ∈ vs, Q i v)).
Proof.
  iIntros (?) "#Hh ?".
  wpm_call.

  wpm_apply dedup_pre_spec; eauto.
  iIntros (l) "[%xs[%vs (?&?&%&%Htie)]]". simpl.
  wpm_apply filter_compact_spec.
  iIntros (l') "(_&?)". simpl.
  wpm_apply (dedup_spec _ _ _ _ _ _ hash).
  { rewrite elem_of_filter_pure. naive_solver. }
  { iApply big_sepN_intro. iModIntro. iIntros.
    iApply "Hh". }
  iIntros (?) "[%xs' (?&?&%)]".
  iExists vs,xs'. iFrame "∗". eauto using pure_dedup.
Qed.

End dedup_spec.
