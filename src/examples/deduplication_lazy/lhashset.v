From iris.prelude Require Import prelude.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.

From dislog.utils Require Import more_iris.
From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmode big_opN map_agree elems split_array.

From iris.algebra Require Import gmap agree frac_auth gset.

(******************************************************************************)
(* The code *)

(* The lhashset is a record of two fields:
   + The data
   + An equally capad array of integers, governing the access to the data
     We use (-1) to indicate an unclaimed slot. *)

Definition hs_init : val :=
  λ: [["capa", "empty"]],
    let: "h" := alloc 2 VUnit in
    ("h".[0] <- alloc "capa" "empty");;
    ("h".[1] <- alloc "capa" (-1)%Z);;
    "h".

Definition hs_tryclaim : val :=
  λ: [["gov", "tid", "i"]],
    if: "gov".["i"] '== "tid"
    then true
    else (CAS "gov" "i" (-1)%Z "tid").

Definition hs_tryput : val :=
  λ: [["table", "empty", "tid", "i", "x"]],
    let: "htbl" := "table".[0] in
    let: "gov"  := "table".[1] in
    if: hs_tryclaim [["gov", "tid", "i"]] then
      let: "elem" := "htbl".["i"] in
      if: "elem" '== "empty" then
        "htbl".["i"] <- "x";; true
      else "elem" '== "x"
    else false.

Definition hs_put : val :=
  μ: "self", [["table", "capa", "empty", "tid", "i", "x"]],
    if: hs_tryput [["table", "empty", "tid", "i", "x"]]
    then VUnit
    else
      let: "si" := ("i" '+ 1) in
      let: "j" :=  "si" 'mod "capa" in
      "self" [["table", "capa", "empty", "tid", "j", "x"]].

Definition hs_add : val :=
  λ: [["table", "capa", "empty", "hash", "tid", "x"]],
    let: "hx" := "hash" [["x"]] in
    let: "i" := "hx" 'mod "capa" in
    hs_put [["table", "capa", "empty", "tid", "i", "x"]].

Definition hs_support : val :=
  λ: [["table"]], "table".[0].

(******************************************************************************)
(* The cmras we need. *)

Local Canonical Structure valO := leibnizO val.
Class hashsee2G Σ :=
  LhashsetG {
      lhashset_modelG : inG Σ (frac_authUR (gsetUR val));
      lhashset_elemsG  : ghost_mapG Σ nat val;
      lhashset_govG  : map_agreeG Σ nat Z;
      lhashset_invG : cinvG Σ;
      lhashset_nsG : inG Σ (authUR (gsetUR natUR));
      lhashset_arrayG : split_arrayG Σ val
    }.

Definition hashsee2Σ : gFunctors :=
  #[ GFunctor (frac_authUR (gsetUR val));
     ghost_mapΣ nat val;
     map_agreeΣ nat Z;
     cinvΣ;
     GFunctor (authUR (gsetUR natUR));
     split_arrayΣ val
    ].

Global Instance subG_queuΣ {Σ} : subG hashsee2Σ Σ → hashsee2G Σ.
Proof. solve_inG. Qed.

Local Existing Instance lhashset_modelG.
Local Existing Instance lhashset_elemsG.
Local Existing Instance lhashset_govG.
Local Existing Instance lhashset_invG.
Local Existing Instance lhashset_nsG.
Local Existing Instance lhashset_arrayG.

(******************************************************************************)
(* The ghost state name. *)

Record ghashset :=
  mk_ghs { gm : gname; ge : gname; ga : gname }.

Global Instance ghashset_eq_dec : EqDecision ghashset.
Proof. solve_decision. Qed.
Global Instance ghashset_countable : Countable ghashset.
Proof.
  apply inj_countable with
    (f:= fun x => (x.(gm), x.(ge), x.(ga)))
    (g:= fun '(x1,x2,x3) => Some (mk_ghs x1 x2 x3)).
  intros []; eauto.
Qed.
Global Instance ghashset_inhabited : Inhabited ghashset := populate (mk_ghs inhabitant inhabitant inhabitant).

Section spec.
Context `{interpGS Σ, hashsee2G Σ}.
Context (N:namespace).

(* Some parameters. *)
Record params :=
  mkp { capa:nat;
        empt:val;
        hash: val -> nat;
        nb_threads:nat;
    }.

Record iparams :=
  mkip { arr:loc;
         gov:loc;
         ns:list gname }.

Global Instance iparams_eq_dec : EqDecision iparams.
Proof. solve_decision. Qed.
Global Instance iparams_countable : Countable iparams.
Proof.
  apply inj_countable with
    (f:= fun x => (x.(arr), x.(gov), x.(ns)))
    (g:= fun '(x1,x2,x3) => Some (mkip x1 x2 x3)).
  intros []; eauto.
Qed.
Global Instance iparams_inhabited : Inhabited iparams := populate (mkip inhabitant inhabitant inhabitant).
Definition tie e (S:gset val) (vs:list val) :=
  (∀ x, x ∈ vs -> x = e ∨ x ∈ S) /\ (forall x, x ∈ S -> x ≠ e /\ x ∈ vs).

Definition claimedby (ns:list gname) (i:nat) (tid:nat) : iProp Σ :=
  match ns !! tid with
  | Some η => own η (◯({[i]} : gset nat))
  | None => False end.

Global Instance claimbesby_persistent ns i tid : Persistent (claimedby ns i tid).
Proof. rewrite /claimedby. destruct (ns!!tid); apply _. Qed.
Global Instance claimbesby_timeless ns i tid : Timeless (claimedby ns i tid).
Proof. rewrite /claimedby. destruct (ns!!tid); apply _. Qed.

Definition elem_inv γ (ns:list gname) e (i:nat) (z:Z) : iProp Σ :=
  ((⌜z=-1⌝%Z ∗ mapstoa γ.(ga) i e) ∨ (⌜(z≠-1)%Z⌝ ∗ claimedby ns i (Z.to_nat z)))%I.

(* The actual invariant *)
Definition lhashset_inv (γ:ghashset) (p:params) (i:iparams) (l:loc) : iProp Σ :=
  ∃ (S:gset val) (vs:list val) (zs:list Z),
    l ↦ [VLoc i.(arr); VLoc i.(gov)] ∗ i.(arr) ↦ vs ∗ i.(gov) ↦ (VInt <$> zs) ∗
    own γ.(gm) (●F S) ∗ split_array γ.(ga) vs ∗ elems γ.(ge) (-1)%Z zs ∗
    ([∗ list] j ↦ z ∈ zs, elem_inv γ i.(ns) p.(empt) j z) ∗
    ⌜length vs = p.(capa) /\ length zs = p.(capa)/\ length i.(ns) = p.(nb_threads) /\ tie p.(empt) S vs⌝.

Lemma hashet_inv_timeless γ p i l: Timeless (lhashset_inv γ p i l).
Proof. apply _. Qed.

Definition tid_inv (γ:gname) (ns:list gname) (tid:nat) (t:timestamp) : iProp Σ :=
  ∃ (ρ:gname) S, ⌜ns !! tid = Some ρ⌝ ∗ own ρ (● S) ∗ [∗set] j ∈ S, ∃ (v:val), mapstoa γ j v ∗ v ◷? t.

Definition go_back γ ns i t tid : iProp Σ :=
  (∀ v, mapstoa γ i v -∗ v ◷? t -∗ tid_inv γ ns tid t).

Lemma tid_inv_insert (γ:gname) (ns:list gname) (tid:nat) i v t :
  tid_inv γ ns tid t -∗
  mapstoa γ i v ==∗
  claimedby ns i tid ∗ mapstoa γ i v ∗ go_back γ ns i t tid.
Proof.
  iIntros "[% [% (%Heq&?&?)]] ?".
  destruct_decide (decide (i ∈ S)).
  { iExFalso. iDestruct (big_sepS_elem_of with "[$]") as "[% (?&?)]". eauto.
    iDestruct (mapstoa_ne with "[$][$]") as "%". congruence. }
  iMod (auth_gset_insert i with "[$]") as "(?&#?)".
  iModIntro. iFrame. iSplitR.
  { rewrite /claimedby Heq //. }
  iIntros (?) "??". iExists _. iFrame "%". iExists _. iFrame.
  iApply big_sepS_insert. eauto. iFrame. iExists _. iFrame.
Qed.

Lemma tid_inv_lookup (γ:gname) (ns:list gname) (tid:nat) i t :
  tid_inv γ ns tid t -∗
  claimedby ns i tid -∗
  ∃ v, mapstoa γ i v ∗ v ◷? t ∗ go_back γ ns i t tid.
Proof.
  iIntros "[% [% (%Heq&?&?)]] ?". rewrite /claimedby Heq.
  iDestruct (auth_gset_elem_of with "[$][$]") as "%".
  iDestruct (big_sepS_elem_of_acc with "[$]") as "([%(?&?)]&Hb)". eauto.
  iExists _. iFrame. iIntros (?) "??". iExists _. iFrame "%". iExists _. iFrame.
  iApply "Hb". iExists _. by iFrame.
Qed.

Definition lhashset_nm : namespace := nroot.@"hs".

Definition lhashset_at (l:loc) (p:params) (tid:nat) (S:gset val) (t:timestamp) : iProp Σ :=
  let q := (1/nat_to_Qp p.(nb_threads))%Qp in ⌜p.(nb_threads) ≠ 0⌝ ∗
  ∃ γ ι η, meta l lhashset_nm (γ,ι,η) ∗ cinv N η (lhashset_inv γ p ι l) ∗
     all_abef t [p.(empt); VLoc ι.(arr); VLoc ι.(gov)] ∗
     cinv_own η q ∗ own γ.(gm) (◯F{q} S) ∗
     (* The "all_abef" par is contained in both next lines,
        but the two are useful at various places *)
     ([∗ set] v ∈ S, v ◷? t ) ∗
     tid_inv γ.(ga) ι.(ns) tid t.

Definition lhashset_inv_open : string :=
  "(>[%S[%vs[%zs (Hl&Ha&Hg&Hmodel&Harray&Helems&Hga&(%Hlvs&%Hlzs&%Hns&%Htie))]]]&Htok)".

Lemma alloc_empty_tid_invs γ n t :
  ⊢ |==> ∃ ns, ⌜length ns=n⌝ ∗ ([∗ nat] i ∈ [0;n], tid_inv γ ns i t).
Proof.
  iInduction n as [|] "IH".
  { iExists nil. iSplitR; first easy. iApply big_sepN_0. lia. easy. }
  { iMod "IH" as "[%ns (%&Hns)]".
    iMod (own_alloc (● ∅)) as "[%η ?]".
    { by apply auth_auth_valid. }
    iExists (ns++[η]). iModIntro.
    iSplitR.
    { iPureIntro. rewrite app_length. simpl. lia. }
    iApply big_sepN_snoc. lia.
    iSplitL "Hns".
    { iApply (big_sepN_mono with "[$]").
      iIntros (??) "[%[% (%&?&?)]]".
      iExists _,_. iFrame. iPureIntro.
      rewrite lookup_app_l //. eauto using lookup_lt_Some. }
    iExists _,_. iFrame. rewrite lookup_app_r; last lia.
    replace (n - length ns) with 0 by lia. iSplitR; first easy. by iApply big_sepS_empty. }
Qed.

Lemma init_impl γ γ' ns s e j :
  γ.(ga) = γ' ->
  ([∗ list] i↦v ∈ replicate s e, mapstoa γ' (j+i) v )-∗
  [∗ list] i↦z ∈ replicate s (-1)%Z, elem_inv γ ns e (j+i) z.
Proof.
  intros.
  iInduction s as [|] "IH" forall (j).
  { iIntros. easy. }
  { iIntros "(H1&?)". iSplitL "H1".
    { iLeft. iSplitR; first easy. subst. by iFrame. }
    iSpecialize ("IH" $! (S j)).
    iDestruct (big_sepL_mono with "[$]") as "?".
    2:iDestruct ("IH" with "[$]") as "E".
    { iIntros. replace (j + S k) with (S j + k) by lia. iFrame. }
    iApply (big_sepL_mono with "[$]").
    iIntros. replace (j + S k) with (S j + k) by lia. iFrame. }
Qed.

Lemma init_impl' γ γ' ns s e :
  γ.(ga) = γ' ->
  ([∗ list] i↦v ∈ replicate s e, mapstoa γ' i v )-∗
  [∗ list] i↦z ∈ replicate s (-1)%Z, elem_inv γ ns e i z.
Proof.
  iIntros.
  iDestruct (init_impl _ _ _ _ _ 0 ) as "OK". eauto.
  simpl. by iApply "OK".
Qed.

Lemma hs_init_spec_at t (s:nat) (e:val) n h :
  n ≠ 0 ->
  ⊢ wp (↑N) t (hs_init [[s, e]])%T (fun t' l => [∗ nat] i ∈ [0;n], lhashset_at l (mkp s e h n) i ∅ t').
Proof.
  iIntros.
  iApply wp_fupd.
  wp_call.

  iApply (wp_vmementopre e). set_solver. iIntros.

  wp_bind.
  wp_alloc'. lia. iIntros (?) "(?&?&Hmeta)".
  wp_bind. wp_bind. wp_alloc as a "(?&#?)". lia. wp_store. iIntros.
  wp_bind. wp_bind. wp_alloc as g "(?&#?)". lia. wp_store. iIntros. simpl.
  iApply wp_val. reflexivity.
  iMod (own_alloc (●F ∅ ⋅ ◯F ∅)) as "[%gm (?&Hm)]".
  { now apply frac_auth_valid. }
  iMod elems_alloc as "[%ge Ha]".
  iMod (split_array_alloc ) as "[%ga (Hga&Hga')]".
  iMod (alloc_empty_tid_invs ga n) as "[%ns (%&Hns)]".
  pose (γ:=mk_ghs gm ge ga).
  pose (p:=mkp s e h n).
  pose (i:= mkip a g ns).

  iMod (cinv_alloc_open (↑N) _ (lhashset_inv γ p i l)) as "[%η (#?&H2&HP)]".
  eauto.

  iAssert (▷ lhashset_inv γ p i l)%I with "[-Hns Hmeta HP H2 Hm]" as "?".
  { iModIntro. iExists ∅,_,_.
    rewrite -fmap_replicate Nat2Z.id. iFrame "∗#".
    iSplitL. { by iApply (init_impl' with "[$]"). }
    { iPureIntro.
      rewrite !replicate_length //.
      subst γ p i. simpl.
      split_and !; eauto.
      split.
      { intros ? He. left. apply elem_of_replicate in He. naive_solver. }
      { set_solver. } }
  }

  iMod (meta_set _ l (γ,i,η) lhashset_nm with "Hmeta") as "#?". solve_ndisj.

  iMod ("HP" with "[$]") as "_". iModIntro.

  iAssert ([∗ nat] i ∈ [0; n], (cinv_own η (1/nat_to_Qp n)))%I with "[H2]" as "?".
  { iApply fractional_split_n_times. lia. iFrame. }

  iAssert ([∗ nat] i ∈ [0; n], (own gm (◯F{1/nat_to_Qp n} ∅)))%I with "[Hm]" as "?".
  { iApply (fractional_split_n_times (fun n => own gm (◯F{n} ∅))).
    { intros ? ?. rewrite -own_op -frac_auth_frag_op left_id_L //. }
    lia. iFrame. }

  iDestruct (big_sepN_sep with "[$]") as "?".
  iDestruct (big_sepN_sep with "[$]") as "?".
  iApply (big_sepN_impl with "[$][]"). iModIntro.
  iIntros (??) "((?&?)&?)". iSplitR. eauto. iExists γ,i,η. iFrame "∗#".
  rewrite big_sepL_nil big_sepS_empty //.
Qed.

Lemma hs_tryclaim_spec t l p q γ ι η (tid:nat) (i:nat):
  (0 <= i < p.(capa))%Z ->
  cinv N η (lhashset_inv γ p ι l) -∗
  (empt p) ◷? t -∗
  cinv_own η q ∗ tid_inv γ.(ga) ι.(ns) tid t -∗
  wp (↑N) t (hs_tryclaim [[ι.(gov), tid, i]])%T
    (fun t' (b:bool) => ⌜t'=t⌝ ∗ cinv_own η q ∗ if b then elem_int γ.(ge) i (Z.of_nat tid) ∗  go_back γ.(ga) ι.(ns) i t tid ∗ ∃ v, mapstoa γ.(ga) i v ∗ v ◷? t else tid_inv γ.(ga) ι.(ns) tid t).
Proof.
  iIntros (?) "#I #? (Htok&Hinv)".

  wp_call. wp_bind. wp_bind.
  iInv "I" as lhashset_inv_open. constructor.
  destruct (zs !! i) as [z|] eqn:Hvi.
  2:{ exfalso. apply lookup_ge_None in Hvi. lia. }

  iApply wp_convertible.
  iApply (@wp_load_tac _ _ Z); last iFrame.
  { apply lookup_lt_Some in Hvi. rewrite fmap_length. split;  lia. }
  { rewrite list_lookup_fmap Nat2Z.id Hvi //. }
  { easy. }

  iIntros "Hg".
  iAssert (|==> elems (ge γ) (-1)%Z zs ∗( [∗ list] j↦z ∈ zs, elem_inv γ (ns ι) (empt p) j z) ∗ ((⌜z=-1⌝%Z ∗  tid_inv (ga γ) (ns ι) tid t) ∨ (elem_int γ.(ge) i z ∗ ((⌜z≠tid⌝ ∗ tid_inv (ga γ) (ns ι) tid t) ∨ (⌜z=tid⌝ ∗ go_back γ.(ga) ι.(ns) i t tid ∗ (∃ v, mapstoa γ.(ga) i v ∗ v ◷? t))))))%I with "[Helems Hinv Hga]" as ">(Helems&Hga&Hcase)".
  { destruct_decide (decide (z=-1)%Z).
    { subst. iFrame. eauto. }
    { iDestruct (big_sepL_lookup_acc with "[$]") as "(H1&Hback)". eauto.
      iDestruct "H1" as "[(%&?)| (?&#?)]"; first by lia.
      iMod (elems_extract with "[$]") as "(He&#?)". all:eauto.
      iModIntro. iFrame "He". iSplitR "Hinv".
      { iFrame. iApply "Hback". iRight. iFrame "#∗". }
      iRight. iFrame "#".
      destruct_decide (decide (z=tid)); subst.
      { iRight. iSplitR; first easy. rewrite Nat2Z.id //.
        iDestruct (tid_inv_lookup with "[$][$]") as "[%v (?&?&?)]". iFrame.
        iExists _. iFrame. }
      { iLeft. by iFrame. } } }

  iModIntro.
  iSplitL "Ha Hg Harray Hmodel Hga Helems".
  { iModIntro. iExists S,vs,zs. by iFrame "#∗". }

  clear dependent S vs zs Hns.
  Unshelve. 2:{ apply Conv_val. }
  simpl. wp_call_prim. simpl. wp_if.
  case_bool_decide as Heq.
  { inversion Heq. subst. iApply wp_val. reflexivity.
    iDestruct "Hcase" as "[(%&?)|(?&HE)]"; first by lia.
    iDestruct "HE" as "[ (%&_) | (_&?&[% ?])]". lia. iFrame.  iSplitR; first easy.
    iExists _. iFrame. }

  iInv "I" as lhashset_inv_open. constructor.
  destruct (zs !! i) as [z'|] eqn:Hvi.
  2:{ exfalso. apply lookup_ge_None in Hvi. lia. }

  unshelve iApply @wp_cas_tac; last iFrame "∗". apply _.
  4:{ rewrite list_lookup_fmap Nat2Z.id Hvi //. }
  { intros. reflexivity. }
  { rewrite fmap_length. split; lia. }
  iSplitR; first easy.

  iIntros "Hg". case_decide as Hz'.
  { rewrite Nat2Z.id. inversion Hz'. subst z'.
    iDestruct "Hcase" as "[ (%&?) | (?&?)]".
    2:{ iDestruct (use_elem_int _ _ _ z with "[$][$]") as "%". exfalso. naive_solver. }

    iMod (elems_insert (Z.of_nat tid) _ _ _ i with "[$]") as "(Helems&?)". eauto. lia.
    iDestruct (big_sepL_insert_acc with "[$]") as "(H1&Hback)". eauto.
    iDestruct "H1" as "[(%&?)| (%&#?)]"; last by lia.

    iMod (tid_inv_insert with "[$][$]") as "(#?&?&?)".
    iSpecialize ("Hback" $!tid with "[]").
    { iRight. rewrite Nat2Z.id. iFrame "#". iPureIntro. lia. }
    iSplitL "Ha Hg Hback Harray Hmodel Helems".
    { do 2 iModIntro. iExists S,vs,_. iFrame "∗#".
      rewrite -list_fmap_insert. iFrame.
      { iPureIntro. rewrite insert_length //. } }
    iModIntro. iSplitR; first easy.
    iFrame. iExists _. iFrame "#∗". }
  { iAssert (tid_inv (ga γ) (ns ι) tid t ∗ elems (ge γ) (-1)%Z zs)%I with "[Hcase Helems]" as "(?&Helems)".

    { iDestruct "Hcase" as "[ (%&?) | (?&Hcase)]"; first by iFrame.
      iDestruct (use_elem_int _ _ _ z with "[$][$]") as "%".
      assert (z'=z) by naive_solver. subst.
      iDestruct "Hcase" as "[ (_&?) | (%&_) ]"; first by iFrame.
      exfalso. naive_solver. }

    iSplitL "Ha Hg Hga Hmodel Harray Helems".
    { iModIntro. iExists S,vs,zs. by iFrame "#∗". }
    iModIntro. rewrite bool_decide_eq_false_2 //. simpl.
    by iFrame. }
Qed.

Lemma tie_insert i v e S vs :
  i < length vs ->
  vs !! i = Some e ->
  v ≠ e ->
  tie e S vs ->
  tie e ({[v]} ∪ S) (<[i:=v]> vs).
Proof.
  intros ? Hi ? (E1&E2). split; intros x Hx.
  { apply elem_of_list_lookup_1 in Hx. destruct Hx as (j,Hj).
    destruct_decide (decide (j=i)); subst.
    { rewrite list_lookup_insert // in Hj. set_solver. }
    { rewrite list_lookup_insert_ne // in Hj. apply elem_of_list_lookup_2 in Hj. set_solver. } }
  { apply elem_of_union in Hx. destruct Hx as [Hx|Hx].
    { apply elem_of_singleton in Hx. subst. split; eauto. by apply list_elem_of_insert. }
    { apply E2 in Hx. destruct Hx as (?&Hx). split; first easy.
      destruct_decide (decide (x=v)); subst.
      { apply elem_of_list_lookup_2 with i. rewrite list_lookup_insert //. }
      { apply elem_of_list_lookup_1 in Hx. destruct Hx as (j,Hj).
        apply elem_of_list_lookup_2 with (i:=j).
      destruct_decide (decide (i=j)).
      { exfalso. naive_solver. }
      { rewrite list_lookup_insert_ne //. } } } }
Qed.

Lemma hs_tryput_spec t l p E (tid:nat) (i:nat) (x:val):
  (0 <= i < p.(capa))%Z ->
  x ≠ p.(empt) ->
  lhashset_at l p tid E t -∗
  wp (↑N) t (hs_tryput [[l, p.(empt), tid, i, x]])%T
    (fun t' (b:bool) => if b then lhashset_at l p tid ({[x]} ∪ E) t' else lhashset_at l p tid E t').
Proof.
  iIntros (??) "(%&[%[%[% (#?&#I&#Habef&?&?&#?&?)]]])".

  wp_call. wp_bind.
  iInv "I" as lhashset_inv_open. constructor.
  wp_load.
  { by iApply (all_abef_extract _ _ 1 with "[$]"). }
  iIntros "Hl". iModIntro.
  iSplitL "Hl Ha Hg Hga Harray Hmodel Helems".
  { iModIntro. do 3 iExists _. by iFrame "#∗". }
  simpl. clear dependent S vs zs Hns.

  wp_bind.
  iInv "I" as lhashset_inv_open. constructor.
  wp_load.
  { by iApply (all_abef_extract _ _ 2 with "[$]"). }
  iIntros "Hl". iModIntro.
  iSplitL "Hl Ha Hg Hga Harray Hmodel Helems".
  { iModIntro. do 3 iExists _. by iFrame "#∗". }
  simpl. clear dependent S vs zs Hns.

  wp_bind. wp_apply hs_tryclaim_spec; eauto.
  { by iApply (all_abef_extract _ _ 0 with "[$]"). }
  iIntros (? b) "(->&?&Hb)". simpl. wp_if.
  destruct b.
  2:{ iApply wp_val. reflexivity. iSplitR; first eauto.
      do 3 iExists _. iFrame "#∗". }

  wp_bind.
  iInv "I" as lhashset_inv_open. constructor.


  destruct (vs !! i) as [v|] eqn:Hvi.
  2:{ exfalso. apply lookup_ge_None in Hvi. lia. }

  iDestruct "Hb" as "(?&Hb&[%v' (Hv'&#?)])".
  iDestruct (use_elem_int with "[$][$]") as "%Hclaimed".
  iDestruct (mapstoa_use with "[$][$]") as "%".
  assert (v'=v) by naive_solver. subst v'.

  iApply (@wp_load_tac _ _ val); last iFrame.
  { apply lookup_lt_Some in Hvi. lia. }
  { rewrite Nat2Z.id Hvi //. }
  { iFrame "#". }

  iIntros "Ha". iModIntro.
  iSplitL "Ha Hg Harray Hmodel Hga Helems".
  { iModIntro. do 3 iExists _. by iFrame "#∗". }
  simpl.  clear dependent S vs zs Hns.

  wp_bind. wp_call_prim. simpl. wp_if.
  case_bool_decide as Hv; subst.
  { wp_bind. iApply (wp_vmementopre x). set_solver. iIntros.

    iInv "I" as lhashset_inv_open. constructor.

    destruct (vs !! i) as [v|] eqn:Hvi.
    2:{ exfalso. apply lookup_ge_None in Hvi. lia. }

    wp_store. iIntros "Ha".
    iMod (update_model _ x with "[$]") as "(Hmodel&?)".

    destruct (zs !! i) as [z|] eqn:Hzi.
    2:{ exfalso. apply lookup_ge_None in Hzi. lia. }
    iDestruct (use_elem_int with "[$][$]") as "%Hclaimed".

    iDestruct (mapstoa_use with "[$][$]") as "%".
    iMod (mapstoa_update x with "[$][$]") as "(Harray&Hx)".
    iSpecialize ("Hb" with "[$][$]").

    iModIntro.
    iSplitL "Ha Hg Harray Hmodel Hga Helems".
    { iModIntro. iExists ({[x]} ∪ S),(<[i:=x]>vs),zs. iFrame "∗#".
      rewrite Nat2Z.id. iFrame.
      iPureIntro. rewrite insert_length; split_and!; eauto.
      apply tie_insert; eauto using lookup_lt_Some. }
    iApply wp_val. reflexivity. iSplitR; first eauto.
    do 3 iExists _. iFrame "Habef".
    rewrite big_sepS_union_persistent big_sepS_singleton.
    iFrame "#∗". }
  { iApply wp_fupd. wp_call_prim.
    case_decide; subst.
    { iInv "I" as lhashset_inv_open.
      iDestruct (mapstoa_use with "[$][$]") as "%Hx".
      assert (x ∈ S).
      { destruct Htie as (E1&E2). apply elem_of_list_lookup_2 in Hx. set_solver. }
      iMod (update_model _ x with "[$]") as "(Hmodel&?)".
      replace (({[x]} ∪ S)) with S by set_solver.
      iModIntro.
      iSplitL "Ha Hl Hg Harray Hmodel Hga Helems".
      { iModIntro. do 3 iExists _. by iFrame "#∗". }
      iModIntro. iSplitR; first eauto. do 3 iExists _.
      rewrite big_sepS_union_persistent big_sepS_singleton.
      iFrame "#∗". iApply ("Hb" with "[$][$]"). }
    { iModIntro. iSplitR; first eauto. do 3 iExists _. iFrame "#∗". iApply ("Hb" with "[$][$]"). } }
Qed.

Lemma hs_put_spec p t (l:loc) tid S (i:nat) (x:val) :
  (0 ≤ i < capa p)%Z ->
  x ≠ p.(empt) ->
  lhashset_at l p tid S t -∗
  wp (↑N) t (hs_put [[l,p.(capa),p.(empt),tid,i,x]])%T
  (fun t' (_:unit) => lhashset_at l p tid ({[x]}∪S) t').
Proof.
  iIntros (Hcapa ?) "?".

  iLöb as "IH" forall (i Hcapa t).
  wp_call.
  wp_bind.
  wp_apply hs_tryput_spec.
  1-2:eauto.
  iIntros (? b) "?". simpl. rew_enc.
  wp_if. destruct b.
  { iApply wp_val. reflexivity. by iFrame. }
  wp_bind. wp_call_prim.
  wp_bind. wp_call_prim.
  rewrite -Nat2Z.inj_add -Nat2Z.inj_mod.
  iApply "IH"; last by iFrame. iPureIntro.
  split; try lia.
  rewrite Nat2Z.inj_mod.
  destruct (Z.mod_bound_pos (i+1) (capa p)); lia.
  Unshelve. exact tt.
Qed.

Lemma eliminate_list_meta X l (γs:list ghashset) (ιs:list iparams) (ηs:list gname) (n:nat) :
  n ≠ 0 ->
  length γs = n -> length ιs = n -> length ηs = n ->
  ([∗ nat] i ∈ [0; n], meta l X (γs !!! i, ιs !!! i, ηs !!! i)) -∗
  ∃ (γ:ghashset) (ι:iparams) (η:gname), meta l X (γ, ι, η) ∗ ⌜γs = replicate n γ /\ ιs = replicate n ι /\ ηs = replicate n η⌝.
Proof.
  revert γs ιs ηs. induction n; intros γs ιs ηs. congruence.
  iIntros (? Hlγ Hlι Hlη) "HE". destruct n.
  { rewrite big_sepN_S; last lia. iDestruct "HE" as "(?&_)".
    destruct γs, ιs, ηs; simpl in *; try lia.
    destruct γs, ιs, ηs; simpl in *; try lia.
    iExists _,_,_. iFrame. iPureIntro. eauto. }
  rewrite big_sepN_snoc; last lia.
  iDestruct "HE" as "(HE&#?)".
  destruct (list_snoc_exists γs) as (γ,(γs',?)); first lia.
  destruct (list_snoc_exists ιs) as (ι,(ιs',?)); first lia.
  destruct (list_snoc_exists ηs) as (η,(ηs',?)); first lia.
  subst.
  rewrite app_length cons_length nil_length in Hlγ.
  rewrite app_length cons_length nil_length in Hlι.
  rewrite app_length cons_length nil_length in Hlη.
  do 3 (rewrite lookup_total_app_r; last lia).
  replace (S n - length γs') with 0 by lia.
  replace (S n - length ιs') with 0 by lia.
  replace (S n - length ηs') with 0 by lia. simpl.
  iExists γ, ι, η. iFrame "#".
  iAssert ([∗ nat] i ∈ [0; S n], meta l X (γs' !!! i, ιs' !!! i, ηs' !!! i))%I with "[-]" as "HE".
  { iApply (big_sepN_mono with "[$]"). iIntros. do 3 (rewrite lookup_total_app_l; last lia). easy. }
  iDestruct (IHn with "[$]") as "[%[%[% (?&(%&%&%))]]]"; try lia. subst.
  iDestruct (meta_agree with "[$][$]") as "%Eq". inversion Eq. subst. iPureIntro.
  rewrite -!replicate_cons_app. easy.
Qed.

Lemma own_model_to_full_aux γ n m xs :
  n < m -> length xs = m - n ->
  ([∗ nat] i ∈ [n; m], own γ (◯F{1 / nat_to_Qp m} (xs !!! (i-n))) )-∗
  own γ (◯F{(nat_to_Qp (m-n)) / nat_to_Qp m} (⋃ xs)) .
Proof.
  remember (m-n) as k. revert xs m n Heqk. induction k; intros xs m n Heqk ? Hl. lia.
  destruct m; first lia.
  rewrite big_sepN_S; last lia. iIntros "(E&?)".
  destruct xs as [|y ys]; simpl in *; first lia.
  replace (n-n) with 0 by lia. simpl.
  destruct (decide (m=n)). subst.
  { assert (k=0) by lia. subst.
    destruct ys; simpl in *; last lia. rewrite right_id_L.
    replace (nat_to_Qp 1) with 1%Qp by compute_done. iFrame. }
  rewrite (nat_to_Qp_succ k); last lia. rewrite Qp.div_add_distr.
  rewrite frac_auth_frag_op own_op. iFrame.
  iApply (IHk _ (S m) (S n)); try lia.
  iApply (big_sepN_mono with "[$]"). intros.
  iIntros. rewrite lookup_total_cons_ne_0; last lia.
  replace  (Init.Nat.pred (i - n)) with (i - S n) by lia. iFrame.
Qed.

Lemma own_model_to_full γ n xs :
  length xs = n -> n ≠ 0 ->
  ([∗ nat] i ∈ [0; n], own γ (◯F{1 / nat_to_Qp n} (xs !!! i)) )-∗
  own γ (◯F (⋃ xs)) .
Proof.
  iIntros.
  iDestruct (own_model_to_full_aux _ 0 n xs) as "Hgo". lia. lia.
  replace (n - 0) with n by lia. rewrite Qp.div_diag. iApply "Hgo".
  iApply (big_sepN_mono with "[$]"). iIntros. replace ((i - 0)) with i by lia. iFrame.
Qed.

Lemma hahset_decompose t l p xs n :
  p.(nb_threads) = n ->
  n ≠ 0 ->
  length xs = n ->
  ([∗ nat] i ∈ [0;n], lhashset_at l p i (xs !!! i) t)-∗
  ∃ γ ι η, all_abef t [empt p; (#(arr ι))%V; (#(gov ι))%V] ∗ cinv N η (lhashset_inv γ p ι l) ∗ cinv_own η 1 ∗ own γ.(gm) (◯F (⋃ xs)) ∗ [∗ set] v ∈ (⋃ xs), v ◷? t.
Proof.
  iIntros (Hn ? Hl) "HE". rewrite /lhashset_at.
  rewrite big_sepN_sep. iDestruct "HE" as "(_&HE)".
  rewrite big_sepN_exists_from_zero. iDestruct "HE" as "[%γs (%Hγ&HE)]".
  rewrite big_sepN_exists_from_zero. iDestruct "HE" as "[%ιs (%Hι&HE)]".
  rewrite big_sepN_exists_from_zero. iDestruct "HE" as "[%ηs (%Hη&HE)]".
  rewrite big_sepN_sep. iDestruct "HE" as "(Hmeta&HE)".
  iDestruct (eliminate_list_meta with "[$]") as "[%γ [%ι [%η (_&(%&%&%))]]]"; eauto.
  subst γs ιs ηs. clear Hγ Hι Hη.
  iAssert ([∗ nat] i ∈ [0; n], cinv N η (lhashset_inv γ p ι l) ∗
        all_abef t [empt p; (#(arr ι))%V; (#(gov ι))%V] ∗
         cinv_own η (1 / nat_to_Qp (nb_threads p)) ∗
         own (gm γ) (◯F{1 / nat_to_Qp (nb_threads p)} (xs !!! i)) ∗
         ([∗ set] v ∈ (xs !!! i), v ◷? t) ∗
         tid_inv (ga γ) (ns ι) i t)%I with "[-]" as "HE".
  { iApply (big_sepN_mono with "[$]"). iIntros.
    rewrite !list_lookup_total_alt !lookup_replicate_2; try lia. eauto. }
  iAssert (all_abef t [empt p; (#(arr ι))%V; (#(gov ι))%V]) with "[-]" as "#?".
  { rewrite big_sepN_S; last by lia. iDestruct "HE" as "((_&?&_)&_)". iFrame. }
  rewrite !big_sepN_sep. iDestruct "HE" as "(H1&H2&H3&H4&#H5&H6)".
  rewrite (big_sepN_S _ _ (fun _ => cinv _ _ _)); last lia. iDestruct "H1" as "(#?&_)".
  rewrite Hn.
  rewrite -fractional_split_n_times //.
  iExists γ,ι,η. iFrame "#∗". iDestruct (own_model_to_full with "[$]") as "?"; eauto. iFrame.

  iClear "H2 H6".
  iApply big_sepS_forall. iIntros (x Hx). apply elem_of_union_list in Hx.
  destruct Hx as (X,(Hxs&Hx)).
  apply elem_of_list_lookup_1 in Hxs. destruct Hxs as (i,Hi).
  iDestruct (big_sepN_elem_of_acc _ _ i with "[$]") as "(Hend&_)".
  { apply lookup_lt_Some in Hi. rewrite -Hl. split; eauto. lia. }

  rewrite list_lookup_total_alt Hi. rewrite big_sepS_forall.
  iApply "Hend". eauto.
Qed.

End spec.

From dislog.examples Require Import proofmodem.

Section monspec.
Context `{interpGS Σ, hashsee2G Σ}.
Context (N:namespace).

Lemma tid_inv_mon γ ns tid t t' :
  t ≼ t' -∗
  tid_inv γ ns tid t -∗ tid_inv γ ns tid t'.
Proof.
  iIntros "#? [%[% (?&?&?)]]".
  iExists _,_. iFrame.
  iApply (big_sepS_impl with "[$]"). iModIntro.
  iIntros (? ?) "[% (?&?)]". iExists _. iFrame. iApply (vclock_mon with "[$][$]").
Qed.

Lemma lhashset_at_mon l p tid t t' S :
  t ≼ t' -∗
  lhashset_at N l p tid S t -∗ lhashset_at N l p tid S t'.
Proof.
  iIntros "#? (?&[%[%[% (#?&#I&#?&?&?&#?&?)]]])". iFrame.
  iExists _,_,_. iFrame "#". iFrame.
  iSplitR.
  { iApply (all_abef_mon with "[$][$]"). }
  iSplitR.
  { iApply (big_sepS_impl with "[$]"). iModIntro. iIntros.
    iApply (vclock_mon with "[$][$]"). }
  iApply (tid_inv_mon with "[$][$]").
Qed.

Program Definition lhashset (l:loc) (p:params) (tid:nat) (S:gset val) : vProp Σ :=
  MonPred' (lhashset_at N l p tid S) _.
Next Obligation. iIntros. iApply (lhashset_at_mon with "[$][$]"). Qed.

Lemma hs_init_spec (s:nat) (e:val) n h :
  n ≠ 0 ->
  ⊢ wpm (↑N) (hs_init [[s, e]])%T (fun l => [∗ nat] i ∈ [0;n], lhashset l (mkp s e h n) i ∅).
Proof.
  intros. apply bi.wand_entails. apply wp_wpm_whole_world.
  iIntros.
  iApply wp_mono. iApply hs_init_spec_at; eauto.
  iIntros. rewrite monPred_at_big_sepN. iFrame.
  Unshelve. exact h.
Qed.

Lemma hs_add_spec (p:params) s e (l:loc) (tid:nat) (S:gset val) (h:val)  (x:val) :
  s ≠ 0 ->
  s = p.(capa) ->
  e = p.(empt) ->
  x ≠ p.(empt) ->
  (wpm (↑N) (h [[x]])%T (fun v => ⌜v = VInt (p.(hash) x)⌝)) -∗
  lhashset l p tid S -∗
  wpm (↑N) (hs_add [[l,s,e,h,tid,x]])%T
  (fun (_:unit) => lhashset l p tid ({[x]} ∪ S)).
Proof.
  intros. apply bi.entails_wand. apply bi.wand_intro_r. apply bi.wand_entails.
  apply wp_wpm_whole_world. iIntros (?) "E". rewrite monPred_at_sep.
  iDestruct "E" as "(Hwp&?)".
  iSpecialize ("Hwp" $! t). simpl. iAssert (prec t t) as "?". iApply prec_refl.
  iSpecialize ("Hwp" with "[$]"). subst.

  wp_call.
  wp_bind.

  iApply wp_tempus_fugit.
  iApply (wp_mono with "[$]").
  iIntros (??). rewrite monPred_at_pure. iIntros "-> #?". simpl.
  wp_bind. wp_call_prim.
  rewrite -Nat2Z.inj_mod.
  iDestruct (lhashset_at_mon with "[$][$]") as "?".
  wp_apply hs_put_spec.
  { split; try lia. rewrite Nat2Z.inj_mod.
    destruct (Z.mod_bound_pos (hash p x)%nat (capa p)); lia. }
  { eauto. }
  iIntros. iFrame.
Qed.

Lemma hs_support_spec l p xs n :
  p.(nb_threads) = n ->
  n ≠ 0 ->
  length xs = n ->
  ([∗ nat] i ∈ [0;n], lhashset l p i (xs !!! i)) -∗
    wpm (↑N) (hs_support [[l]])%T (fun (l':loc) => ∃ vs, l' ↦ vs ∗ ⌜tie p.(empt) (⋃ xs) vs⌝).
Proof.
  intros. apply wp_wpm_whole_world. iIntros.
  rewrite monPred_at_big_sepN.
  iDestruct (hahset_decompose with "[$]") as "[% [% [% (#?&?&?&Hfrag&Hend)]]]"; eauto.
  iApply fupd_wp.
  iMod (cinv_cancel with "[$][$]") as ">[%S[%vs[%zs (Hl&Ha&Hg&Hmodel&Harray&Helems&Hga&(%Hlvs&%Hlzs&%Hns&%Htie))]]]". set_solver. iModIntro.

  wp_call.

  wp_load.
  { by iApply (all_abef_extract _ _ 1 with "[$]"). }
  iIntros.

  rewrite monPred_at_exist. iExists vs.
  rewrite monPred_at_sep monPred_at_pure.
  iFrame.
  iDestruct (own_valid_2 with "Hmodel Hfrag") as "%Hv".
  apply frac_auth_agree_L in Hv. rewrite -Hv. iFrame "%".
  rewrite mapsto_at. iFrame.
  rewrite /all_abef. iApply big_sepL_forall.
  iIntros (i v Hi).
  destruct Htie as [He1 He2].
  apply elem_of_list_lookup_2 in Hi. apply He1 in Hi.
  destruct Hi as [-> | Hi].
  { by iApply (all_abef_extract _ _ 0 with "[$]"). }
  { rewrite big_sepS_forall. iApply "Hend". eauto. }
Qed.

End monspec.
