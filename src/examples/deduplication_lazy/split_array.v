From iris.prelude Require Import prelude.
From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.

From iris.algebra Require Import gmap agree frac_auth gset.

Class split_arrayG (Σ:gFunctors) A := { sa_gm : ghost_mapG Σ nat A }.
Definition split_arrayΣ A := ghost_mapΣ nat A.

Global Instance subG_split_arrayΣ {Σ} A : subG (split_arrayΣ A) Σ → split_arrayG Σ A.
Proof. solve_inG. Qed.

Local Existing Instance sa_gm.

Section split_array.
Context `{Countable A}.
Context `{split_arrayG Σ A}.

Definition tie (M:gmap nat A) (vs:list A) :=
  map_Forall (fun i x => vs !! i = Some x) M.

Lemma tie_alloc (vs:list A) :
  tie (list_to_map (imap (fun i x => (i, x)) vs)) vs.
Proof.
  intros i x Hx.
  apply elem_of_list_to_map_2 in Hx.
  apply elem_of_lookup_imap_1 in Hx. destruct Hx as (j,(y,(Eq&?))).
  inversion Eq. naive_solver.
Qed.

Definition split_array γ (vs:list A) : iProp Σ :=
  ∃ (M:gmap nat A), ghost_map_auth γ 1%Qp M ∗ ⌜tie M vs⌝.

Definition mapstoa γ i v : iProp Σ := i ↪[γ] v.

Lemma big_sepM_big_sepL_aux γ vs M j :
  (forall i x, vs !! i = Some x -> M !! (i+j) = Some x) ->
  ([∗ map] k↦v ∈ M, k ↪[γ] v ) -∗
  [∗ list] i↦v ∈ vs, mapstoa γ (i+j) v.
Proof.
  iIntros (Htie) "Hm".
  iInduction vs as [|v] "IH" forall (M j Htie).
  { by iApply big_sepL_nil. }
  iApply big_sepL_cons.
  generalize Htie. intros Htie'.
  specialize (Htie 0 v eq_refl). simpl in *.
  rewrite big_sepM_delete. 2:eauto. iDestruct "Hm" as "(?&?)". iFrame.
  iDestruct ("IH" $! _ (j+1) with "[%][$]") as "?".
  { intros ? ? Hx. specialize (Htie' (S i) _ Hx).
    rewrite lookup_delete_ne.
    { rewrite -Htie'. f_equal. lia. }
    lia. }
  iApply (big_sepL_mono with "[$]"). intros.
  replace (k + (j + 1)) with (S (k + j)) by lia.
  reflexivity.
Qed.

Lemma big_sepM_big_sepL γ vs M :
  (forall i x, vs !! i = Some x -> M !! i = Some x) ->
  ([∗ map] k↦v ∈ M, k ↪[γ] v ) -∗
  [∗ list] i↦v ∈ vs, mapstoa γ i v.
Proof.
  iIntros.
  iDestruct (big_sepM_big_sepL_aux _ _ _ 0 with "[$]") as "?".
  { intros. rewrite right_id. eauto. }
  iApply (big_sepL_mono with "[$]"). intros. rewrite right_id. reflexivity.
Qed.

Lemma NoDup_ok {X} (vs:list X) :
  NoDup (imap (fun i x => (i, x)) vs).*1.
Proof.
  apply NoDup_alt.
  intros i j x.
  do 2 rewrite list_lookup_fmap.
  do 2 rewrite list_lookup_imap.
  intros Hi Hj.
  destruct (vs !! i); inversion Hi. subst.
  destruct (vs !! j); inversion Hj. subst.
  eauto.
Qed.

Definition split_array_alloc vs :
  ⊢ |==> ∃ γ, split_array γ vs ∗ [∗ list] i ↦ v ∈ vs, mapstoa γ i v .
Proof.
  iIntros.
  iMod (ghost_map_alloc (list_to_map (imap (fun i x => (i,x)) vs))) as "[%γ (Ha&?)]".
  iModIntro. iExists γ.
  iSplitL "Ha".
  { iExists _. iFrame. eauto using tie_alloc. }
  iApply (big_sepM_big_sepL with "[$]").
  intros.
  apply elem_of_list_to_map_1.
  { apply NoDup_ok. }
  apply elem_of_lookup_imap.
  eauto.
Qed.

Lemma mapstoa_use γ i v vs :
  split_array γ vs -∗ mapstoa γ i v -∗ ⌜vs !! i = Some v⌝.
Proof.
  iIntros "[%M (?&%Htie)] ?".
  iDestruct (ghost_map_lookup with "[$][$]") as "%Hi".
  eauto.
Qed.

Lemma tie_insert M vs i v v' :
  tie M vs ->
  M !! i = Some v ->
  tie (<[i:=v']> M) (<[i:=v']> vs).
Proof.
  intros Htie Hi.
  intros j x Hx.
  assert (i < length vs) by eauto using lookup_lt_Some.
  destruct_decide (decide (i=j)); subst.
  { rewrite lookup_insert in Hx. inversion Hx. subst.
    rewrite list_lookup_insert //. }
  { rewrite list_lookup_insert_ne //.
    rewrite lookup_insert_ne // in Hx. eauto. }
Qed.

Lemma mapstoa_update v' γ i v vs :
  split_array γ vs -∗ mapstoa γ i v ==∗ split_array γ (<[i:=v']>vs) ∗ mapstoa γ i v'.
Proof.
  iIntros "[%M (?&%Htie)] ?".
  iDestruct (ghost_map_lookup with "[$][$]") as "%Hi".
  iMod (ghost_map_update  with "[$][$]") as "(?&?)". iFrame.
  iModIntro. iExists _. iFrame. eauto using tie_insert.
Qed.

Lemma mapstoa_ne γ i1 i2 v1 v2 :
  mapstoa γ i1 v1 -∗ mapstoa γ i2 v2 -∗ ⌜i1≠i2⌝.
Proof. apply ghost_map_elem_ne. Qed.
End split_array.
