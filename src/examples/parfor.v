From iris.base_logic.lib Require Import invariants.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmode big_opN.

(* The parallel for loop, as in MPL
   https://github.com/MPLLang/mpl/blob/f10dab421626e4da2c289c4fa1ce5520808b45de/basis-library/schedulers/shh/Scheduler.sml
 *)

Definition ignore (e:expr) : expr := (e ;; VUnit)%T.

Definition parfor : val :=
  μ: "self", [["i", "j", "f"]],
    let: "diff" := "j" '- "i" in
    if: "diff" '== 0 then VUnit else
      if: "diff" '== 1 then "f" [["i"]] else
      let: "mid" := "i" '+ ("diff" '/ 2) in
    ignore (par ("self" [["i", "mid", "f"]]) ("self" [["mid", "j", "f"]])).

Lemma parfor_spec `{interpGS Σ} E n m (f:loc) (Q:timestamp -> nat -> timestamp -> iProp Σ) t :
  n <= m ->
  ([∗ nat] i ∈ [n;m], ∀ t1, t ≼ t1 -∗
     wp E t1 (f [[i]])%T (fun t1' (_:unit) => Q t1 i t1')) -∗
  wp E t (parfor [[n, m, f]])%T
    (fun t' (_:unit) => [∗ nat] i ∈ [n;m], ∃ t1 t2, Q t1 i t2 ∗ t2 ≼ t').
Proof.
  iIntros (Hnm) "Hwps".

  iLöb as "IH" forall (t n m Hnm).
  wp_call. fold parfor.

  wp_bind.
  wp_call_prim.
  wp_bind.
  wp_call_prim. simpl.
  wp_if. rewrite bool_decide_decide. case_decide as eq.
  { unshelve iApply wp_val. easy. easy.
    inversion eq.
    assert (n=m) by lia. subst.
    iApply big_sepN_0. lia. easy. }
  wp_bind. wp_call_prim. simpl.
  wp_if. rewrite bool_decide_decide. case_decide as eq'.
  { inversion eq'. rewrite big_opN_S. 2:lia.
    iDestruct "Hwps" as "(Hwp&_)".
    iSpecialize ("Hwp" $! t with "[]"). iApply prec_refl.
    iApply (wp_mono with "[$]").
    iIntros (t1' []) "?". assert (m=S n) by lia. subst. rewrite big_opN_S. 2:lia.
    rewrite big_opN_0. 2:lia. rewrite right_id. iExists t,_. iFrame. iApply prec_refl. }

  wp_bind. wp_bind.
  wp_call_prim. wp_call_prim. simpl.
  pose (mid := (n + (m-n) /2) : nat).
  replace (VInt (n + (m - n) `div` 2%nat)) with (VInt mid).
  2:{ f_equal. subst mid. rewrite Nat2Z.inj_add Nat2Z.inj_div Nat2Z.inj_sub//. }
  assert (n <= mid <= m).
  { subst mid; split; try lia.
    rewrite {2}(Nat.le_add_sub n m) //.
    apply Nat.add_le_mono_l. apply Nat.Div0.div_le_upper_bound; lia. }

  rewrite (big_sepN_add mid); last eauto.

  iDestruct "Hwps" as "(Hwl&Hwr)".
  wp_bind loc.

  iApply wp_par. iIntros (??) "(#?&#?)". iModIntro. iExists _,_.
  iSplitL "Hwl".
  { iApply ("IH" $! t1 with "[%]"). lia.
    iApply (big_sepN_impl with "[$]").
    iModIntro. iIntros (? ?) "HP". iIntros. iApply "HP".
    iApply (prec_trans _ t1 with "[$][$]"). }
  iSplitL "Hwr".
  { iApply ("IH" $! t2 with "[%]"). lia.
    iApply (big_sepN_impl with "[$]").
    iModIntro. iIntros (? ?) "HP". iIntros. iApply "HP".
    iApply (prec_trans _ t2 with "[$][$]"). }

  iIntros (???? [][]) "(_&_&#?&#?&H1&H2)". iModIntro.
  iApply wp_val. easy.
  iApply (big_sepN_add mid). eauto.
  iSplitL "H1".
  { iApply (big_sepN_impl with "[$]").
    iModIntro. iIntros (? ?) "[% [% (?&?)]]".
    iExists _,_. iFrame. iApply (prec_trans with "[$][$]"). }
  { iApply (big_sepN_impl with "[$]").
    iModIntro. iIntros (? ?) "[% [% (?&?)]]".
    iExists _,_. iFrame. iApply (prec_trans with "[$][$]"). }
  Unshelve. exact tt.
Qed.

From dislog Require Import proofmodem.

(* A derived spec from the parfor_spec *)
Lemma parfor_specm `{interpGS Σ} E n m (f:loc) (Q:nat -> vProp Σ) :
  n <= m ->
  ([∗ nat] i ∈ [n;m], wpm E (f [[i]])%T (fun (_:unit) => Q i)) -∗
  wpm E (parfor [[n, m, f]])%T
    (fun (_:unit) => [∗ nat] i ∈ [n;m], Q i).
Proof.
  intros. apply wp_wpm_whole_world.
  iIntros.
  iApply (wp_mono with "[-]").
  { iApply (parfor_spec _ _ _ _ (fun _ i t => Q i t)). lia.
    rewrite monPred_at_big_sepN. iApply (big_sepN_mono with "[$]").
    iIntros (??) "Hwp". iIntros. by iApply "Hwp". }
  { iIntros. iApply monPred_at_big_sepN. iApply (big_sepN_mono with "[$]").
    iIntros (??) "[%[% (?&#?)]]". iApply monPred_mono. by iFrame. }
Qed.
