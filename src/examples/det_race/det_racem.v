From dislog.lang Require Import notation.
From dislog.logic Require Import wpm wonly.
From dislog.examples Require Import proofmodem stdlib.

Definition simple_det_race : val :=
  λ: [[]],
    let: "i" := ref 0 in
    let: "r" := ref "i" in
    par (set "r" (ref 1)) (set "r" (ref 2));;
    let: "j" := get "r" in
    get "j".

Section simple_det_race.
Context `{interpGS Σ, !wonlyG val Σ}.

Lemma simpl_det_race_spec :
  ⊢ wpm ⊤ (simple_det_race [])%T (fun i:Z => ⌜i=1 ∨ i=2⌝).
Proof.
  iStartProof. wpm_call.

  iSteps as (x l) "H1 H2 H3 H4".

  iMod (mapsto_wo_start nroot _ l with "[$]") as "[% (_&Hl)]".
  rewrite -{2}Qp.half_half.
  rewrite -(left_id_L _ _ (∅:gset val)).
  rewrite mapsto_wo_frac.
  iDestruct "Hl" as "(X1&X2)".

  wpm_bind.

  pose (Q (v:Z) := (∃ l', l ⟾{γ,1 / 2} {[(#l')%V]} ∗ l' ↦ [VInt v])%I : vProp Σ).

  iApply wpm_convertible.
  iApply (wpm_mono with "[-]").
  { iApply (wpm_par _ _ _ _ _ _ _ (fun (_:unit) => Q 1) (fun (_:unit) => Q 2) with "[X1]").
    { wpm_alloc. wpm_apply wpm_store_wo. done. iSmash. }
    { wpm_alloc. wpm_apply wpm_store_wo. done. iSmash. } }
  Unshelve. 2:apply _.
  iIntros (?) "[%[% (_&[%l1 (?&?)]&[%l2 (?&?)])]]".
  iDestruct (mapsto_wo_frac with "[$]") as "?".
  rewrite Qp.half_half.
  iMod (mapsto_wo_end with "[$]") as "[%v' (%Hv&Hl)]".
  1,2:set_solver.
  iSteps.
  rewrite elem_of_union !elem_of_singleton in Hv.
  destruct Hv as [-> | ->]; iSteps.
Qed.

End simple_det_race.
