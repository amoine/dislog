From iris.base_logic.lib Require Import invariants.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import proofmode stdlib.

Definition simple_det_race : val :=
  λ: [[]],
    let: "r" := ref ()%V in
    let: "f" := (λ: [], set "r" (ref ()%V)) in
    par ("f" []) ("f" []);;
    get "r".

Section simple_det_race.
Context `{interpGS Σ}.
Context {N:namespace}.

Definition inv_loc_def (t1 t2:timestamp) (r:loc) : iProp Σ :=
  ∃ t v, ⌜t=t1 ∨ t=t2⌝ ∗ r ↦ [v] ∗ v ◷? t.

Definition inv_loc (t1 t2:timestamp) (l:loc) :=
  own_inv N (inv_loc_def t1 t2 l).

Lemma det_race_spec (t:timestamp) :
  ⊢ wp (↑N) t (simple_det_race [])%T (fun _ _ => True)%I.
Proof.
  iIntros. wp_call.

  wp_bind.
  wp_alloc as l "(Hl&_)". lia. simpl.

  wp_bind.
  wp_clo as f "(#Hf&_)".

  iAssert (□(∀ t1 t2 t, inv N (inv_loc_def t1 t2 l) -∗ ⌜t=t1 ∨ t=t2⌝ -∗ wp (↑N) t ((#f)%V [])%T (fun t' (_:unit) => ⌜t'=t⌝)))%I as "#Hwp".
  { iModIntro. iIntros (???) "#Hinv %".
    wp_call_clo "Hf".
    wp_bind.
    wp_alloc. lia.

    iInv "Hinv" as ">[% [% (%Hu&Hl&_)]]". constructor.
    wp_store. simpl. iIntros. iModIntro.
    iSplitL; last easy.
    iModIntro. iExists _,_. iFrame. eauto. }

  wp_bind loc.
  iApply wp_par.
  iIntros (? ?) "(#?&#?)".

  iMod (inv_alloc N _ (inv_loc_def t1 t2 l) with "[Hl]") as "#Hinv".
  { iModIntro. iExists t1,VUnit. iFrame. eauto. }

  iModIntro. iExists _,_.

  iSplitR. iApply "Hwp"; eauto.
  iSplitR. iApply "Hwp"; eauto.
  iIntros (l' t1' t2' t' ? ?) "(?&?&#?&#?&(%&%))". subst.
  iModIntro.
  iInv "Hinv" as ">[% [%v (%Hu&Hl&#Hv)]]". constructor.

  wp_load.
  { destruct Hu; subst; iApply (vclock_mon with "[$][$]"). }

  iIntros. iModIntro.
  rewrite right_id. iModIntro. iExists _,_. iFrame "#∗%".
Qed.

End simple_det_race.

Definition cas_det_race : val :=
  λ: [[]],
    let: "r" := ref ()%V in
    let: "p" := ref ()%V in
    let: "f" := (λ: [], CAS "r" 0 ()%V "p") in
    par ("f" []) ("f" []);;
    get "r".

Section cas_det_race.
Context `{interpGS Σ}.
Context {N:namespace}.

Definition inv_loc_def' (t:timestamp) (r:loc) : iProp Σ :=
  ∃ v, r ↦ [v] ∗ v ◷? t.

Definition inv_loc' (t:timestamp) (l:loc) :=
  own_inv N (inv_loc_def' t l).

Lemma cas_det_race_spec (t:timestamp) :
  ⊢ wp (↑N) t (cas_det_race [])%T (fun _ _ => True)%I.
Proof.
  iIntros. wp_call.

  wp_bind.
  wp_alloc as l "(Hl&_)". lia. simpl.

  wp_bind.
  wp_alloc as r "(Hr&#Hclockr)". lia. simpl.

  wp_bind.
  wp_clo as f "(#Hf&_)".

  iAssert (□(∀ t', inv N (inv_loc_def' t l) -∗ t ≼ t' -∗ wp (↑N) t' ((#f)%V [])%T (fun t'' (_:val) => ⌜t''=t'⌝)))%I as "#Hwp".
  { iModIntro. iIntros (?) "#Hinv #?".
    wp_call_clo "Hf".

    iInv "Hinv" as ">[% (Hl&#?)]". constructor.
    iApply @wp_cas_tac. 4:iFrame. 3:reflexivity. 2:compute_done. eauto. Unshelve. 2:apply _.
    iSplitL. { iApply (vclock_mon with "[$][$]"). }

    iIntros. iSplitL; last easy. do 2 iModIntro.
    destruct_decide (decide (v=VUnit)).
    { subst. simpl. iExists _. iFrame. simpl. eauto. }
    { rewrite decide_False //.  iExists _. iFrame. simpl. eauto. } }

  wp_bind loc.

  iApply wp_par.
  iIntros (? ?) "(#?&#?)".

  iMod (inv_alloc N _ (inv_loc_def' t l) with "[Hl]") as "#Hinv".
  { iModIntro. iExists VUnit. iFrame. }
  iModIntro. iExists _,_.
  iSplitR. iApply "Hwp"; eauto.
  iSplitR. iApply "Hwp"; eauto.
  iIntros (l' t1' t2' t' ? ?) "(?&?&#?&#?&(%&%))". subst.
  iModIntro.
  iInv "Hinv" as ">[% (?&#?)]". constructor.

  wp_load.
  { iApply (vclock_mon with "[$]"). iApply (prec_trans t t1 with "[$][$]"). }

  iIntros. iModIntro.
  rewrite right_id. iModIntro. iExists _. iFrame "#∗%".
Qed.

End cas_det_race.
