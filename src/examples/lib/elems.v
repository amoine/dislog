From iris.prelude Require Import prelude.
From iris.base_logic.lib Require Import cancelable_invariants ghost_map gen_heap.
From iris.algebra Require Import gmap agree frac_auth gset.

From dislog.lang Require Import notation.
From dislog.logic Require Import wp.
From dislog.examples Require Import map_agree proofmode big_opN.

(******************************************************************************)
(* elems *)

Section elems.
Context `(Countable A).
Local Canonical Structure valO := leibnizO A.
Context `{interpGS Σ, map_agreeG Σ nat A}.

Definition elems γ e (vs:list A) : iProp Σ :=
  ∃ (σ:gmap nat A), map_agree_auth γ σ ∗ [∗ map] i ↦ v ∈ σ, ⌜v ≠ e /\ vs !! i = Some v⌝.

Definition elem_int γ i v := map_agree_elem γ i v.

Lemma elems_alloc e vs :
  ⊢ |==> ∃ γ, elems γ e vs.
Proof.
  iMod (map_agree_alloc ∅) as "[%ge Hge]".
  iModIntro. iExists ge,∅. iFrame. by iApply big_sepM_empty.
Qed.

Lemma elems_extract γ e vs i v :
  vs !! i = Some v ->
  v ≠ e ->
  elems γ e vs ==∗ elems γ e vs ∗ elem_int γ i v.
Proof.
  iIntros (Hvi ?) "[% (?&#?)]".
  destruct (σ !! i) as [w|] eqn:Hi.
  { iDestruct (big_sepM_lookup with "[$]") as "(%&%Hvi')". eauto.
    rewrite Hvi in Hvi'. inversion Hvi'. subst w.
    iMod (map_agree_extract with "[$]") as "(?&?)". eauto. iFrame.
    iModIntro. iExists _. iFrame "#∗". }
  { iMod (map_agree_insert with "[$]") as "(?&?)". eauto. iFrame.
    iModIntro. iExists _. iFrame. iApply big_sepM_insert. eauto.
    iFrame "#". eauto. }
Qed.

Lemma elems_insert v γ e vs i :
  vs !! i = Some e ->
  v ≠ e ->
  elems γ e vs ==∗ elems γ e (<[i:=v]> vs) ∗ elem_int γ i v.
Proof.
  iIntros (Hvi ?) "[% (?&%Hσ)]".
  destruct (σ !! i) as [w|] eqn:Hi.
  { exfalso. apply Hσ in Hi. naive_solver. }
  { iMod (map_agree_insert with "[$]") as "(?&?)". eauto. iFrame.
    iModIntro. iExists _. iFrame. iPureIntro.
    apply map_Forall_insert. eauto. split.
    { rewrite list_lookup_insert //. apply lookup_lt_is_Some_1. eauto. }
    intros j x Hj. apply Hσ in Hj. destruct Hj as (?&?). split; eauto.
    destruct_decide (decide (i=j)); subst.
    { exfalso. naive_solver. }
    { rewrite list_lookup_insert_ne //. } }
Qed.

Lemma use_elem_int γ e i (v:A) (vs:list A) :
  elem_int γ i v -∗
  elems γ e vs -∗
  ⌜v ≠ e /\ vs !! i = Some v⌝.
Proof.
  iIntros "? [% (?&%Hm)]".
  iDestruct (map_agree_lookup with "[$][$]") as "%Hv".
  eauto.
Qed.

End elems.
