From iris.algebra Require Import excl.
From iris.base_logic.lib Require Import invariants.

From dislog.lang Require Import notation.
From dislog.logic Require Import wpm objectivity.
From dislog.examples Require Import proofmodem stdlib parfor lock big_opN.

(* ------------------------------------------------------------------------ *)
(* We present two variants of a parallel scratchpad.
   + [scratch] that forks only two threads
   + [scratchmany] that forks arbitrary many threads.
 *)

Section scratchpad.
Context (N:nat) (numTasks:nat).

Definition clearScratchPad : val :=
  λ: [["d", "scratchpad"]],
    let: "task" := λ: [["i"]], "scratchpad".["i"] <- "d" in
    seqfor [[0, N, "task"]].

Definition scratch : val :=
  λ: [["d", "doWork"]],
    let: "shared" := alloc N "d" in
    let: "l" := new_lock [[]] in
    let: "tryLock" := "l".[0] in
    let: "unlock" := "l".[1] in
    let: "task" := λ: [["i"]],
        if: "tryLock" []
        then
          "doWork" [["i", "shared"]];;
          clearScratchPad [["d", "shared"]];;
          "unlock" []
        else
          "doWork" [["i", alloc N "d"]] in
    ignore (par ("task" [[0]]) ("task" [[1]])).

Section proof.
Context `{interpGS Σ, lockG Σ}.

Lemma clearScratchPad_spec E (d:val) (s:loc) (vs:list val) :
  length vs = N ->
  s ↦ vs -∗
  wpm E (clearScratchPad [[d,s]])%T (fun (_:unit) => s ↦ (replicate N d)).
Proof.
  iIntros (Hl) "Hs". wpm_call. iSteps. rew_enc.

  iAssert (∀ (N j:nat) (vs:list val),
    ⌜N = length vs /\ j <= N /\ forall i, i < j -> vs !! i = Some d⌝ -∗
    s ↦ vs -∗
    (wpm E (seqfor [[j,N, x]])%T (λ (_:unit), s ↦ replicate N d)))%I as "Haux".
  { clear vs. iIntros (N j).
    remember (N - j) as n. iRevert (N j Heqn).
    iInduction n as [|] "IH"; iIntros (N j Heqn vs (HN&Hl&Hvs)) "Hs".
    { iApply seqfor_spec. rewrite decide_True; last lia.
      replace vs with (replicate N d); first done.
      apply replicate_as_Forall_2. lia.
      apply Forall_forall. intros x' Hi. apply elem_of_list_lookup_1 in Hi.
      destruct Hi as (i&Hi). pose proof (lookup_lt_Some _ _ _ Hi).
      rewrite Hvs in Hi. naive_solver. lia. }
    { iApply seqfor_spec. rewrite decide_False; last lia.
      iStep 8.
      iApply ("IH" with "[%][%][$]"); first lia.
      rewrite insert_length //.
      split_and !; try lia.
      intros. rewrite Nat2Z.id. destruct_decide (decide (i=j)); subst.
      { rewrite list_lookup_insert //. lia. }
      { rewrite list_lookup_insert_ne //. apply Hvs. lia. } } }
  iApply ("Haux" with "[%][$]").
  split_and !; lia.
Qed.

Lemma scratch_spec (d doWork:val) Q :
  ([∗ nat] i ∈ [0;2],
    ∀ l, l ↦ (replicate N d) -∗
    wpm ⊤ (doWork [[i,l]])%T (fun (_:unit) => Q i ∗ ∃ vs, ⌜length vs=N⌝ ∗ l ↦ vs)) -∗
  wpm ⊤ (scratch [[d, doWork]])%T
    (fun (_:unit) => [∗ nat] i ∈ [0;2], Q i).
Proof.
  iIntros "Hdw".
  wpm_call. iSteps as (s) "H4 H5". iClear "H5". rewrite Nat2Z.id.

  iDestruct (punch_in with "[$]") as "[%t (#?&(?&#?))]".
  wpm_apply (new_lock_spec ((s ↦ replicate N d) t)).
  iIntros (l) "[%lock [%unlock (?&#?)]]". rew_enc.
  iSteps. rew_enc.

  iAssert ([∗ nat] i ∈ [0; 2],
    wpm ⊤ ((#x)%V [[i]])%T (fun (_:unit) => Q i))%I with "[Hdw]" as "Hdw".
  { iApply (big_sepN_impl with "[$]"). iModIntro. iIntros. iSteps.
    wpm_apply lck_spec. iIntros ([]); last first.
    (* We didn't win the lock *)
    { iSteps. rewrite Nat2Z.id. iSteps. }
    (* We won the lock *)
    { iIntros "(?&?)".
      iDestruct (monPred_in_elim with "[$][$]") as "?".
      iSteps.
      wpm_apply clearScratchPad_spec. lia. iIntros.

      iDestruct (objectivize with "[$][$]") as "?".
      wpm_apply ulck_spec. iModIntro. iFrame "#". iSteps. } }

  do 2 (rewrite big_sepN_S; last lia). rewrite big_sepN_0; last lia.
  iDestruct "Hdw" as "(?&?&_)".
  wpm_bind. wpm_par unit unit. iFrame. iSteps.
  do 2 (rewrite big_sepN_S; last lia). rewrite big_sepN_0; last lia. by iFrame.
Qed.

End proof.
End scratchpad.

Section scratchpadmany.
Context (N:nat) (numTasks:nat).

Definition scratchmany : val :=
  λ: [["d", "doWork"]],
    let: "shared" := alloc N "d" in
    let: "l" := new_lock [[]] in
    let: "tryLock" := "l".[0] in
    let: "unlock" := "l".[1] in
    let: "task" := λ: [["i"]],
        if: "tryLock" []
        then
          "doWork" [["i","shared"]];;
          clearScratchPad N [["d", "shared"]];;
          "unlock" []
        else
          "doWork" [["i",alloc N "d"]] in
    parfor [[0, numTasks, "task"]].

Section proof.
Context `{interpGS Σ, lockG Σ}.

Lemma scratchmany_spec (d doWork:val) Q :
  ([∗ nat] i ∈ [0;numTasks],
    ∀ l, l ↦ (replicate N d) -∗
    wpm ⊤ (doWork [[i, l]])%T (fun (_:unit) => Q i ∗ ∃ vs, ⌜length vs=N⌝ ∗ l ↦ vs) ) -∗
  wpm ⊤ (scratchmany [[d, doWork]])%T
    (fun (_:unit) => [∗ nat] i ∈ [0;numTasks], Q i).
Proof.
  iIntros.
  wpm_call. iSteps as (s) "H3 H4". iClear "H4". rewrite Nat2Z.id.

  iDestruct (punch_in with "[$]") as "[%t (#?&(?&#?))]".
  wpm_apply (new_lock_spec ((s ↦ replicate N d) t)).
  iIntros (l) "[%lock [%unlock (?&#?)]]". rew_enc.
  iSteps.

  wpm_apply (parfor_specm _ _ _ _ Q). lia.
  iSplitL.
  { iApply (big_sepN_impl with "[$]").
    iModIntro. iSteps.
    wpm_apply lck_spec. iIntros ([]); last first.
    (* We didn't win the lock *)
    { iSteps. rewrite Nat2Z.id. iSteps. }
    (* We wine the lock *)
    { iIntros "(?&?)".
      iDestruct (monPred_in_elim with "[$][$]") as "?".
      iSteps. wpm_apply clearScratchPad_spec. lia. iIntros.

      iDestruct (objectivize with "[$][$]") as "?".
      wpm_apply ulck_spec. iModIntro. iFrame "#". iSteps. } }
  iSteps.
Qed.

End proof.
End scratchpadmany.
