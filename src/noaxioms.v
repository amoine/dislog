From dislog Require Import wp wp_adequacy wpm wpm_adequacy dedup dedup_lazy.

(* This file shows that the project does not depend on any axiom.
   It is _not_ built by default. *)

Print Assumptions wp_alloc.
Print Assumptions wp_load.
Print Assumptions wp_store.
Print Assumptions wp_par.
Print Assumptions wp_adequacy.

Print Assumptions wpm_alloc.
Print Assumptions wpm_load.
Print Assumptions wpm_store.
Print Assumptions wpm_par.
Print Assumptions wpm_adequacy.

Print Assumptions dedup_spec.
Print Assumptions dedup_lazy_spec.
