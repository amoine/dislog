#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export OPAMYES=true

if ! command -v opam >/dev/null ; then
  echo "You are missing opam, the OCaml package manager."
  echo "See"
  echo "  https://opam.ocaml.org/doc/Install.html"
  exit 1
fi

echo "Updating our local copy of the opam package database."
echo "This can take a few minutes..."
opam update

if [ -d _opam ] ; then
  echo "There is already a local opam switch in the current directory."
  echo "If it is OK to remove it, please type:"
  echo "  opam switch remove ."
  echo "then run ./setup.sh again."
  exit 1
fi

echo "Creating a local opam switch in the current directory."
echo "This will take a while (perhaps over 10 minutes)..."

# We test if pre-downloaded custom packages exist.
if [ -d "./deps/iris" ]
then
    # If yes we use them.
    echo "./deps is populated. I am using the local dependencies."
    opam switch --switch . import deps/packages.switch
    eval "$(opam env --set-switch)"

    echo "Installing custom iris packages."
    opam install deps/stdpp/coq-stdpp.opam
    opam install deps/iris/coq-iris.opam
    opam install deps/diaframe/coq-diaframe.opam
else
    # If no, we install everything via opam.
    opam switch create \
	 --repositories=default,coq-released=https://coq.inria.fr/opam/released,iris-dev=git+https://gitlab.mpi-sws.org/iris/opam.git \
	 --deps-only \
	 .
fi

eval $(opam env)

make -j 2
