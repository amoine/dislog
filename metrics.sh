#!/bin/bash

count_lines () {
    num=0
    for line in $(find $1 -type f -name "*.v" -printf '%p\n' )
    do
	spec=$(coqwc -s $line | awk '{print $1;}')
	proof=$(coqwc -r $line | awk '{print $1;}')
	num=$(($num + $spec + $proof))
    done
    echo $num
}

echo "Number of lines as counted by coqwc, excluding comments. "
echo -n "Language: "
count_lines "src/lang/"

echo -n "Logics: "
count_lines "src/logic/ src/utils"

echo -n "Examples: "
count_lines "src/examples"

echo -n "Tactics: "
count_lines "src/examples/tactics"
