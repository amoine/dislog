# DisLog: A Separation Logic for Disentanglement

## Setup & Build

See `INSTALL.md`.

## Architecture

Coq files are located in the `src/` directory.

The architecture is as follows:

* `lang/` contains the syntax and semantics of the language.
* `logic/base` contains the DisLog logic.
  * The `wpg.v` file defines the general wpg.
  * The `wp.v` file specializes wpg to leaves, and gather
    lemmas.
  * The `wpg_adequacy.v` file contains the soundness theorem of wpg
	which is specialized in `wp_adequacy.v`
* `logic/high_level` contains the DisLog+ logic.
  * The `monpred2.v` file defines a language-agnostic vProp for an
	arbitrary pre-order in iProp.
  * The `wpm.v` file defines the monotonic WP.
* `examples/` contains various examples.
* `utils/` contains some utility files.

## ProofGeneral

NB: There is a hack to work with ProofGeneral.
We have a dumb `src/_CoqProject` which makes visible the files
produced by dune.

See issue: https://github.com/ProofGeneral/PG/issues/477
